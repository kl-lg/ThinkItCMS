package com.thinkit.cms.api.related;
import com.thinkit.cms.base.BaseService;
import com.thinkit.cms.dto.content.ContentDto;
import com.thinkit.cms.dto.related.RelatedDto;
import com.thinkit.cms.dto.related.Relateds;

import java.util.List;

/**
* @author LG
*/


public interface RelatedService extends BaseService<RelatedDto> {


    /**
     * 更新推荐
     * @param relateds
     */
    void upRelateds(Relateds relateds);


    /**
     * 查询推荐
     * @param contentId
     * @return
     */
    List<ContentDto> listRelateds(String contentId);


    List<RelatedDto> getDatas(String siteId);
}