package com.thinkit.cms.api.site;

import cn.hutool.extra.spring.SpringUtil;
import com.thinkit.cms.user.SiteConf;
import com.thinkit.cms.user.SysUserDetail;
import lombok.Data;

/**
 * 获取站点的默认配置
 * @author LONG
 */
@Data
public class SiteDetail extends SysUserDetail {

    private SiteService siteService = SpringUtil.getBean(SiteService.class);

    @Override
    public SiteConf getSite(boolean throwExp){
        return siteService.getDefConf(throwExp);
    }

    @Override
    public SiteConf getSite(boolean throwExp, String userId) {
        return siteService.getDefConf(throwExp,userId);
    }

    @Override
    public SiteConf getSite(String siteId) {
        return siteService.getDefConf(siteId);
    }


}
