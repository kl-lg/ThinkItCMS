package com.thinkit.cms.api.sysorganize;

import com.thinkit.cms.base.BaseService;
import com.thinkit.cms.utils.Tree;
import com.thinkit.cms.dto.sysorganize.SysOrganizeDto;

import java.util.List;

/**
* @author LG
*/


public interface SysOrganizeService extends BaseService<SysOrganizeDto> {


    /**
     * 查询组织列表
     * @return
     */
    List<Tree> orgTableTree();

    /**
     * 保存组织
     * @param sysOrganizeDto
     */
    void save(SysOrganizeDto sysOrganizeDto);


    /**
     * 更新组织
     * @param sysOrganizeDto
     */
    void update(SysOrganizeDto sysOrganizeDto);


    /**
     * 组织详情
     * @param id
     * @return
     */
    SysOrganizeDto detail(String id);

    /**
     * 删除组织
     * @param id
     * @return
     */
    boolean delete(String id);
}