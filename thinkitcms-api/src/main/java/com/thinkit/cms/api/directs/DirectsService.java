package com.thinkit.cms.api.directs;
import com.thinkit.cms.base.BaseService;
import com.thinkit.cms.dto.directs.DirectsDto;
import com.thinkit.cms.model.KeyValueModel;

import java.util.List;

/**
* @author lg
*/


public interface DirectsService extends BaseService<DirectsDto> {


    /**
     * 保存指令
     * @param directsDto
     */

    void save(DirectsDto directsDto);


    /**
     * 获取详情
     * @param id
     * @return
     */
    DirectsDto detail(String id);


    /**
     * 更新
     * @param directsDto
     */
    void update(DirectsDto directsDto);


    /**
     * 启用禁用
     * @param id
     * @param enable
     */
    void enable(String id, Integer enable);


    /**
     * 指令应用
     */
    void apply();

    /**
     * 清除指令缓存
     * @param directs
     * @return
     */
    boolean delCache(String directs);


    /**
     * 加载指令列表并格式化
     * @param siteId
     * @return
     */
    List<KeyValueModel> loadDirects(String siteId);


    List<KeyValueModel> loadCodes(String type);

    /**
     * 加载格式化处理器
     * @return
     */
    List<String> loadHands();


    /**
     * 加载自定义指令
     * @return
     */
    List<String> loadInstructs();
}