package com.thinkit.cms.api.sysdict;
import com.thinkit.cms.base.BaseService;
import com.thinkit.cms.dto.sysdict.SysDictDto;
import com.thinkit.cms.utils.Tree;

import java.util.List;

/**
* @author LG
*/


public interface SysDictService extends BaseService<SysDictDto> {


    List<Tree> dictTableTree();

    void save(SysDictDto sysDictDto);

    void update(SysDictDto sysDictDto);

    boolean delete(String id);
}