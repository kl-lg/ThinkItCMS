package com.thinkit.cms.api.userrole;

import com.thinkit.cms.base.BaseService;
import com.thinkit.cms.dto.userrole.UserRoleDto;

import java.util.List;

/**
* @author LG
*/


public interface UserRoleService extends BaseService<UserRoleDto> {


    /**
     * 根据用户ID 查询用户所有的角色ID
     * @param userId
     * @return
     */
    List<String> roleIdsByUserId(String userId);

    void save(UserRoleDto userRoleDto);
}