package com.thinkit.cms.api.syslicense;
import com.thinkit.cms.base.BaseService;
import com.thinkit.cms.dto.syslicense.SysLicenseDto;
/**
* @author LG
*/


public interface SysLicenseService extends BaseService<SysLicenseDto> {


    /**
     * 创建证书
     * @param sysLicenseDto
     */
    void save(SysLicenseDto sysLicenseDto);

    /**
     * 证书校验
     * // 0:不存在 1:不合法 2:证书未生效 3:证书已过期  4:证书未知问题(查看公钥私钥)  5：证书域名未授权 6：通过
     * @return
     */
    int checkLicense();


    /**
     * 下载证书
     * @param licenseUrl
     */
    void downLicense(String licenseUrl);


    /**
     * 部署到本服务器
     * @param fileUrl
     */
    void deploy(String fileUrl);
}