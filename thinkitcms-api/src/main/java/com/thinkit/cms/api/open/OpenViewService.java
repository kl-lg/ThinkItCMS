package com.thinkit.cms.api.open;

import com.thinkit.cms.dto.open.OpenViewDto;

public interface OpenViewService {

    /**
     * 记录文章点击的记录+1 点赞次数
     * @param openViewDto
     */
    Long click(OpenViewDto openViewDto);


    /**
     * 文章浏览量
     * @param openViewDto
     */
    Long view(OpenViewDto openViewDto);

    /**
     * 查寻点赞次数
     * @param openViewDto
     * @return
     */
    Long queryClick(OpenViewDto openViewDto);
}
