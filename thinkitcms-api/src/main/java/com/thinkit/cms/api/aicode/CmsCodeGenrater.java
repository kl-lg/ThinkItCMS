package com.thinkit.cms.api.aicode;

import com.thinkit.cms.dto.sysautocode.SysAutoCodeDto;

public interface CmsCodeGenrater {

    /**
     * 代码生成
     */
    void genrateCode(SysAutoCodeDto sysAutoCode);

    /**
     * 生成页面模板
     * @param sysAutoCode
     */
    void genMyPageCode(SysAutoCodeDto sysAutoCode);
}
