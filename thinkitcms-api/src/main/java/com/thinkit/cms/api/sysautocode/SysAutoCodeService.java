package com.thinkit.cms.api.sysautocode;

import com.thinkit.cms.base.BaseService;
import com.thinkit.cms.dto.sysautocode.SysAutoCodeDto;
import com.thinkit.cms.model.CmsResult;

/**
* @author LG
*/


public interface SysAutoCodeService extends BaseService<SysAutoCodeDto> {


    CmsResult genCode(String id);

    CmsResult genMyPageCode(String id);
}