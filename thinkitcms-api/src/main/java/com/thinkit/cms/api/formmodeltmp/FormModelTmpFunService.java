package com.thinkit.cms.api.formmodeltmp;

import com.thinkit.cms.model.ModelTemp;
import com.thinkit.cms.model.ModelTempParma;

import java.util.function.Function;

/**
* @author LG
*/


public interface FormModelTmpFunService extends Function<ModelTempParma, ModelTemp> {


}