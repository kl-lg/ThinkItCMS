package com.thinkit.cms.api.formmodeltmp;
import com.thinkit.cms.base.BaseService;
import com.thinkit.cms.dto.formmodel.FormModelDto;
import com.thinkit.cms.dto.formmodeltmp.FormModelTmpDto;
import com.thinkit.cms.model.ModelTempParma;

import java.util.function.Function;

/**
* @author LG
*/


public interface FormModelTmpService extends BaseService<FormModelTmpDto> {


    /**
     * 保存模型模板
     * @param formModelTmpDto
     */
    void save(FormModelTmpDto formModelTmpDto);

    /**
     * 保存模板
     * @param formModelDto
     */
    void saveTmp(FormModelDto formModelDto);

    /**
     * 获取详情
     * @param formModelDto
     */
    void getDetail(FormModelDto formModelDto);


    /**
     * 更新模板
     * @param formModelDto
     */
    void updateTmp(FormModelDto formModelDto);


}