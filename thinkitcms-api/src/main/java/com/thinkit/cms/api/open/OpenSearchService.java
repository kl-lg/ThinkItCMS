package com.thinkit.cms.api.open;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.thinkit.cms.dto.open.OpenSearchDto;
import com.thinkit.cms.model.PageModel;

import java.util.Map;

public interface OpenSearchService {
    Page<Map> searchDoc(PageModel<OpenSearchDto> openSearchDto);
}
