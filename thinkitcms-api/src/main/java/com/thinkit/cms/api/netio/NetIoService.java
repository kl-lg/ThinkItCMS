package com.thinkit.cms.api.netio;

import com.thinkit.cms.dto.netio.Chunk;
import com.thinkit.cms.model.CmsResult;

import java.io.File;
import java.io.InputStream;

/**
 * @author LONG
 */
public interface NetIoService {




    /**
     * 上传文件
     * @param chunk
     */
    CmsResult upload(Chunk chunk);



    /**
     * 上传文件
     * @param originalFilename
     * @param name
     * @param bytes
     */
    CmsResult upload(String originalFilename, String name, byte[] bytes);


    /**
     * 上传文件
     * @param originalFilename
     * @param name
     * @param inputStream
     */
    CmsResult upload(String originalFilename, String name, InputStream inputStream,long size);


    /**
     * 上传文件
     * @param originalFilename
     * @param name
     * @param file
     */
    CmsResult upload(String originalFilename, String name, File file);

    /**
     * 删除文件
     * @param key
     * @return
     */
    CmsResult delete(String key);


    /**
     * 下载远程文件到本地
     * @param fileUrl
     * @param file
     */
    Boolean down(String fileUrl, File file);


    /**
     * 文件上传到本地
     * @param originalFilename
     * @param name
     * @param inputStream
     * @param size
     * @param path
     * @return
     */
    CmsResult uploadLocal(String path ,String originalFilename, String name, InputStream inputStream, long size);


    /**
     * 判断文件是否存在上传过
     * @param chunk
     * @return
     */
    CmsResult checkFileIsExist(Chunk chunk);

    CmsResult merge(Chunk chunk);
}
