package com.thinkit.cms.api.content;
import com.thinkit.cms.base.BaseService;
import com.thinkit.cms.dto.content.CkRow;
import com.thinkit.cms.dto.content.ContentDto;
import com.thinkit.cms.dto.content.RowsDto;
import com.thinkit.cms.dto.content.SelectRows;
import com.thinkit.cms.model.PageModel;

import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
* @author LG
*/


public interface ContentService extends BaseService<ContentDto> {


    /**
     * 保存内容
     * @param contentDto
     */
    void saveContent(ContentDto contentDto);


    /**
     * 获取详情
     * @param contentId
     * @return
     */
    ContentDto getDetail(String contentId);



    /**
     * 获取详情
     * @param contentIds
     * @param  siteId
     * @return
     */
    List<ContentDto> getDetails(Collection<String> contentIds,String siteId);

    /**
     * 更新文章内容
     * @param contentDto
     */
    void update(ContentDto contentDto);

    /**
     * 获取常用的内容标签
     * @return
     */
    Set<String> tags();


    /**
     * 更新文章标签
     * @param tags
     * @param id
     */
    void updateTag(List<String> tags, String id);


    /**
     * 内容发布
     * @param rowsDto
     */
    void publish(RowsDto rowsDto);


    /**
     * 内容发布（异步）
     * @param rowsDto
     */
    void asyncPublish(RowsDto rowsDto);



    /**
     * 生成详情页面
     * @param rowsDto
     */
    void generate(RowsDto rowsDto);


    /**
     * 生成详情页面
     * @param rowsDto
     */
    void genContent(RowsDto rowsDto,String takeId);



    List<CkRow> getContents(String siteId,String categoryId);

    /**
     * 定时发布
     * @param rowsDto
     */
    void jobPublish(RowsDto rowsDto);


    /**
     * 同步content 到 es
     * @param taskId
     * @param siteId
     */
    void syncEsData(String taskId, String siteId,String categoryId);


    /**
     * 回收站列表
     * @param pageDto
     * @return
     */
    PageModel<ContentDto> listRecycle(PageModel<ContentDto> pageDto);


    /**
     * 还原
     * @param ckRow
     */
    void reduction(RowsDto ckRow);

    /**
     * 删除
     * @param ckRow
     */
    void readDel(RowsDto ckRow);

    void updateField(String id, String field, Object val);

    String queryFormModelCode(String id);


    /**
     * 查找浏览数
     * @param contentId
     * @return
     */
    Long queryViews(String contentId);


    /**
     * 查找点赞数
     * @param contentId
     * @return
     */
    Long queryClicks(String contentId);

    void topIt(SelectRows ckRow);

    void recomdIt(SelectRows ckRow);

    void headIt(SelectRows ckRow);

    void upViews(String contentId, Long views);

    void upLikes(String contentId, Long views);
}