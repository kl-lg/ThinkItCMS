package com.thinkit.cms.api.site;

import com.thinkit.cms.dto.site.WebFileDto;
import com.thinkit.cms.model.CmsResult;
import com.thinkit.cms.model.FileInfoModel;
import com.thinkit.cms.model.FileViewModel;

import java.util.List;

public interface WebStaticFileService {

    List<FileViewModel> listFile(String path);
    /**
     * 获取内容详情
     * @param path
     * @return
     */
    CmsResult fileContent(String path);


    /**
     * 修改内容接口
     * @param webFileDto
     * @return
     */
    CmsResult setContent(WebFileDto webFileDto);


    /**
     * 删除文件
     * @param filePath
     */
    void deleteFile(String filePath);


    /**
     * 打开文件所在目录
     * @param absolutePath
     */
    void openDir(String absolutePath);


    /**
     * 压缩下载
     * @param path
     */
    void zipDown(String path);
}
