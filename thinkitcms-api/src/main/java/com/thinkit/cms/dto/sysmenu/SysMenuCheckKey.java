package com.thinkit.cms.dto.sysmenu;

import com.thinkit.cms.utils.Tree;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.List;

/**
* @author LG
*/
@Getter
@Setter
@Accessors(chain = true)
public class SysMenuCheckKey  {

    private List<Tree> menus;

    private List<String> checkedKeys;

    private List<String> expandedKeys;


    public SysMenuCheckKey(List<Tree> menus, List<String> checkedKeys,List<String> expandedKeys) {
        this.menus = menus;
        this.checkedKeys = checkedKeys;
        this.expandedKeys = expandedKeys;
    }
}