package com.thinkit.cms.dto.sysautocode;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.thinkit.cms.model.BaseDto;
import com.thinkit.cms.model.BaseModel;
import com.thinkit.cms.utils.Checker;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
* @author LG
*/
@Getter
@Setter
@Accessors(chain = true)
public class SysAutoCodeDto extends BaseDto {
       /**
         表名称
       */
        private String tableName;

        private String tableComment;

       /**
         模块名称
       */
        private String moduleName;

       /**
         输出目录
       */
        private String outputDir;

       /**
         作者名称
       */
        private String author;

       /**
         是否删除 0：正常 1：删除
       */
        private Boolean deleted;

        private String[] gmtCreates;


         @Override
        public <T extends BaseModel> QueryWrapper<T> toWrapper() {
             QueryWrapper<T> queryWrapper=new QueryWrapper<>();
             queryWrapper.eq(Checker.BeNotBlank(tableName),"table_name",tableName);
             queryWrapper.like(Checker.BeNotBlank(tableComment),"table_comment",tableComment);
             queryWrapper.like(Checker.BeNotBlank(moduleName),"module_name",moduleName);
             queryWrapper.like(Checker.BeNotBlank(author),"author",author);
             if (Checker.BeNotEmpty(gmtCreates)){
                 queryWrapper.between("gmt_create",gmtCreates[0],gmtCreates[1]);
             }
             return queryWrapper;
        }
}