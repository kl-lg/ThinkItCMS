package com.thinkit.cms.dto.template;

import com.thinkit.cms.model.BaseDto;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;

/**
* @author LG
*/
@Getter
@Setter
@Accessors(chain = true)
public class UploadTemplate extends BaseDto {

       /**
         模板ID
       */
        @NotBlank(message = "模板路径不能为空")
        private String tmpDir;

        @NotBlank(message = "模板不能为空")
        private String templateId;

}