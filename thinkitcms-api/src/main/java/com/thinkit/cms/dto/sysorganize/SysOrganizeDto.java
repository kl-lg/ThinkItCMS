package com.thinkit.cms.dto.sysorganize;

import com.thinkit.cms.annotation.ValidGroup1;
import com.thinkit.cms.annotation.ValidGroup2;
import com.thinkit.cms.model.BaseDto;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;

/**
* @author LG
*/
@Getter
@Setter
@Accessors(chain = true)
public class SysOrganizeDto extends BaseDto {
       /**
         父组织编码
       */
       @NotBlank(message = "#{5032}",groups = {ValidGroup1.class})
        private String pid;

       /**
         组织编码
       */
        @NotBlank(message = "#{5032}",groups = {ValidGroup1.class})
        private String orgCode;

       /**
         组织名称
       */
        @NotBlank(message = "#{5033}",groups = {ValidGroup2.class, ValidGroup1.class})
        private String orgName;

       /**
         是否删除 0：正常 1：删除
       */
        private Boolean deleted;


        private Integer order;

        private String parentName;
}