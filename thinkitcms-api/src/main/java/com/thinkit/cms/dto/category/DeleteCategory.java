package com.thinkit.cms.dto.category;

import com.thinkit.cms.annotation.ValidGroup3;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
* @author LG
*/
@Getter
@Setter
@Accessors(chain = true)
public class DeleteCategory implements Serializable {


        @NotEmpty(message = "栏目ID不能为空",groups = {ValidGroup3.class})
        private List<String> categoryIds = new ArrayList<>();
}