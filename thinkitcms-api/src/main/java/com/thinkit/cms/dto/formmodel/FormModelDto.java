package com.thinkit.cms.dto.formmodel;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.thinkit.cms.annotation.IndexField;
import com.thinkit.cms.annotation.IndexId;
import com.thinkit.cms.annotation.IndexName;
import com.thinkit.cms.annotation.ValidGroup4;
import com.thinkit.cms.constant.SysConst;
import com.thinkit.cms.enums.FieldType;
import com.thinkit.cms.model.BaseDto;
import com.thinkit.cms.model.BaseModel;
import com.thinkit.cms.utils.Checker;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;

/**
* @author LG
*/
@Getter
@Setter
@Accessors(chain = true)
@IndexName("tk_form_model")
public class FormModelDto extends BaseDto {


       @IndexId(prefix = SysConst.formModePrefix)
       private String id;

    /**
         站点ID
       */
        @IndexField(fieldType = FieldType.TAG, ignoreCase = true)
        private String siteId;

       /**
         表单名称
       */
        @IndexField(fieldType = FieldType.TEXT, ignoreCase = true)
        private String formName;

       /**
         模板路径
       */
        @IndexField(fieldType = FieldType.TEXT, ignoreCase = true)
        private String tmpPath;

       /**
         手机端模板路径
       */
        @IndexField(fieldType = FieldType.TEXT, ignoreCase = true)
        private String mobileTmpPath;

       /**
         表单类型  1、内容模型 2、分类模型 3、页面片段
       */
        @IndexField(fieldType = FieldType.TAG, ignoreCase = true)
        private String formType;

       /**
         是否删除 0：正常 1：删除
       */
        @IndexField(fieldType = FieldType.NUMERIC, ignoreCase = true)
        private Boolean deleted;


        /**
         * 模板id
         */
        @IndexField(fieldType = FieldType.TAG, ignoreCase = true)
        private String templateId;


        @IndexField(exist = false)
        private Boolean showTmp = false;

        @IndexField(fieldType = FieldType.TEXT, ignoreCase = true)
        private String formDesc;



        @IndexField(fieldType = FieldType.TAG, ignoreCase = true)
        @NotBlank(message = "请输入部件编码",groups = {ValidGroup4.class})
        private String formCode;

        /**
         * 设置配置项目条数
         */
        @IndexField(exist = false)
        private Integer confRows=0;

        @Override
        public <T extends BaseModel> QueryWrapper<T> toWrapper() {
            QueryWrapper queryWrapper = new QueryWrapper<>();
            queryWrapper.like(Checker.BeNotBlank(formName),"form_name",formName);
            queryWrapper.eq(Checker.BeNotBlank(formType) && !"-1".equals(formType),"form_type",formType);
            return queryWrapper;
        }

}