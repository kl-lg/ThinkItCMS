package com.thinkit.cms.dto.related;

import com.thinkit.cms.annotation.IndexField;
import com.thinkit.cms.annotation.IndexId;
import com.thinkit.cms.annotation.IndexName;
import com.thinkit.cms.constant.SysConst;
import com.thinkit.cms.enums.FieldType;
import com.thinkit.cms.enums.IdType;
import com.thinkit.cms.model.BaseDto;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;


/**
* @author LG
*/
@Getter
@Setter
@Accessors(chain = true)
@IndexName("tk_content_related")
public class RelatedDto extends BaseDto {

        @IndexId(type = IdType.CUSTOMIZE,prefix = SysConst.relatePrefix)
        public String id;

       /**
         站点ID
       */
        @IndexField(fieldType = FieldType.TEXT, ignoreCase = true)
        private String siteId;

       /**
         当前内容id
       */
        @IndexField(fieldType = FieldType.TEXT, ignoreCase = true)
        private String contentId;

       /**
         关联的内容ID
       */
        @IndexField(fieldType = FieldType.TEXT, ignoreCase = true)
        private String relatedId;

       /**
         创建人名称
       */
        @IndexField(fieldType = FieldType.TEXT, ignoreCase = true)
        private String createName;

       /**
         修改人名称
       */
        @IndexField(fieldType = FieldType.TEXT, ignoreCase = true)
        private String modifiedName;


}