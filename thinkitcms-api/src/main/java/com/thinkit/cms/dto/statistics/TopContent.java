package com.thinkit.cms.dto.statistics;

import com.thinkit.cms.model.BaseDto;
import lombok.Data;

@Data
public class TopContent extends BaseDto {
    private String title;
    private Long clicks;
    private Long likes;
    private Long comments;
    private String mUrl;
    private String url;
    private String orderField="clicks";
    private Boolean order = true;
}
