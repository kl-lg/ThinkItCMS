package com.thinkit.cms.dto.sysrolemenu;

import com.thinkit.cms.annotation.ValidGroup1;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import java.util.List;

/**
* @author LG
*/
@Getter
@Setter
@Accessors(chain = true)
public class SysRoleAssignDto   {
       /**
         角色名称
       */
        @NotBlank(message = "角色名称不能为空",groups = {ValidGroup1.class})
        private String roleId;


        /**
         *资源ID
         */
        private List<String> menuIds;

}