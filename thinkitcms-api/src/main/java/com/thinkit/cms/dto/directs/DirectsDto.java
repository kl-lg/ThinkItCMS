package com.thinkit.cms.dto.directs;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.thinkit.cms.model.BaseDto;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.List;
/**
* @author lg
*/
@Getter
@Setter
@Accessors(chain = true)
public class DirectsDto extends BaseDto {
       /**
         指令名称
       */
        private String name;

       /**
         指令定义
       */
        private String direct;

       /**
         是否启用 1：未启用 2:启用
       */
        private Integer enable;

       /**
         模板sql
       */
        private String sql;

       /**
         返回值类型 Object Map List 等
       */
        private String result;

       /**
         返回结果转化器（需要继承 AbstractDataHandler）
       */
        private String resultConvert;

       /**
         入参参数
       */
        private List<InParams> inParams;

        private String inParamsStr;

       /**
         出参参数格式化
       */

        private List<OutParams> outParams;

       /**
         指令配置
       */
        private ConfigParam config;

       /**
         站点ID
       */
        private String siteId;

       /**
         是否共享全局
       */
        private Integer isShare;


        private String sqlType="mysql";

        /**
         指令类型 1:动态 2：固定
         */
        @JsonSerialize(using = ToStringSerializer.class)
        private Integer directType;

}