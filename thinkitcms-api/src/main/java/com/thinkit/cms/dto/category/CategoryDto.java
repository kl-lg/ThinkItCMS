package com.thinkit.cms.dto.category;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.thinkit.cms.annotation.IndexField;
import com.thinkit.cms.annotation.IndexId;
import com.thinkit.cms.annotation.IndexName;
import com.thinkit.cms.constant.SysConst;
import com.thinkit.cms.enums.FieldType;
import com.thinkit.cms.enums.IdType;
import com.thinkit.cms.model.ModelData;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.List;

/**
* @author LG
*/
@Getter
@Setter
@Accessors(chain = true)
@IndexName("tk_category")
public class CategoryDto extends ModelData {

        @IndexId(type = IdType.CUSTOMIZE,prefix = SysConst.categoryPrefix)
        public String id;

    /**
         名称
       */
        @IndexField(fieldType = FieldType.TEXT)
        private String name;

       /**
         父分类ID
       */
        @IndexField(fieldType = FieldType.TAG)
        private String pid;

       /**
         站点ID
       */
        @IndexField(fieldType = FieldType.TAG)
        private String siteId;

       /**
         编码
       */
        @IndexField(fieldType = FieldType.TAG)
        private String code;

       /**
         封面图片
       */
        @IndexField(fieldType = FieldType.TEXT,exist = false)
        private String cover;

       /**
         生成路径规则
       */
        @IndexField(fieldType = FieldType.TEXT,exist = false)
        private String pathRule;

       /**
         模板路径
       */
        @IndexField(fieldType = FieldType.TEXT,exist = false)
        private String tmpPath;

       /**
         手机端模板路径
       */
        @IndexField(fieldType = FieldType.TEXT,exist = false)
        private String mobileTmpPath;

       /**
         外链跳转地址
       */
        @IndexField(fieldType = FieldType.TEXT,exist = false)
        private String goUrl;

        /**
         地址
         */
        @IndexField(fieldType = FieldType.TEXT,exist = false)
        private String url;


        /**
         * 手机端地址
         */
        @IndexField(fieldType = FieldType.TEXT,exist = false)
        private String mUrl;

       /**
         每页数据条数
       */
       @IndexField(fieldType = FieldType.NUMERIC)
        private Integer pageSize;

       /**
         允许投稿
       */
       @IndexField(fieldType = FieldType.NUMERIC)
       @JsonSerialize(using = ToStringSerializer.class)
        private Integer allowCreate;

       /**
         顺序
       */
       @IndexField(fieldType = FieldType.NUMERIC)
        private Integer sort;

       /**
         是否在首页隐藏
       */
       @IndexField(fieldType = FieldType.NUMERIC)
       @JsonSerialize(using = ToStringSerializer.class)
        private Integer hidden;

       /**
         栏目扩展模型ID
       */
       @IndexField(fieldType = FieldType.TAG)
        private String formModelId;

       /**
         是否是单页
       */
        @IndexField(fieldType = FieldType.NUMERIC)
        @JsonSerialize(using = ToStringSerializer.class)
        private Integer singlePage;

       /**
         标题(分类标题用于 SEO 关键字优化)
       */
       @IndexField(fieldType = FieldType.TEXT)
        private String title;

       /**
         关键词用于 SEO 关键字优化
       */
        @IndexField(fieldType = FieldType.TEXT)
        private String keywords;

       /**
         描述用于 SEO 关键字优化
       */
        @IndexField(fieldType = FieldType.TEXT)
        private String description;

       /**
         是否包含字内容
       */
        @IndexField(fieldType = FieldType.NUMERIC)
        private Integer containChild;

       /**
         是否删除 0：正常 1：删除
       */
        @IndexField(fieldType = FieldType.NUMERIC,exist = false)
        private Boolean deleted;


        @IndexField(exist = false)
        private List<String> modelIds;

        /**
         * 栏目定时
         */
        @IndexField(exist = false)
        private CategoryJob categoryJob;

        /**
         * 是否强制更新栏目地址
         */
        @IndexField(exist = false)
        private Boolean forceGen = false;


        @IndexField(fieldType = FieldType.TAG)
        private String formModelCode;
}