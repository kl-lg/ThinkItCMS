package com.thinkit.cms.dto.content;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.thinkit.cms.annotation.ValidGroup1;
import com.thinkit.cms.annotation.ValidGroup3;
import com.thinkit.cms.annotation.ValidGroup4;
import com.thinkit.cms.model.BaseDto;
import com.thinkit.cms.user.User;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
* @author LG
*/
@Getter
@Setter
@Accessors(chain = true)
public class SelectRows extends BaseDto {
           /**
             发布内容
           */
        @NotEmpty(message = "请选择要操作的记录",groups = {ValidGroup4.class})
        private List<String> ckRowList;

        @NotNull(message = "请选择要操作的记录",groups = {ValidGroup4.class})
        private Integer status;
}