package com.thinkit.cms.dto.content;

import com.thinkit.cms.annotation.ValidGroup2;
import com.thinkit.cms.model.ModelData;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import java.util.List;

/**
* @author LG
*/
@Getter
@Setter
@Accessors(chain = true)
public class TagDto extends ModelData {
       /**
         主键
       */
        @NotBlank(message = "主键不合法",groups = {ValidGroup2.class})
        private String id;

       /**
         标签id
       */
        private List<String> tags;
}