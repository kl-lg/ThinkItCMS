package com.thinkit.cms.dto.sysrolemenu;

import com.thinkit.cms.model.BaseDto;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
/**
* @author LG
*/
@Getter
@Setter
@Accessors(chain = true)
public class SysRoleMenuDto extends BaseDto {
       /**
         角色ID
       */
        private String roleId;

       /**
         菜单ID
       */
        private String menuId;


        public SysRoleMenuDto(){

        }

        public SysRoleMenuDto(String roleId, String menuId) {
            this.roleId = roleId;
            this.menuId = menuId;
        }
}