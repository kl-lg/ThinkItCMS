package com.thinkit.cms.dto.modeldesign;

import lombok.Data;

import java.io.Serializable;

/**
 * @author LONG
 */
@Data
public class OptionsDto implements Serializable {

    private String label;
    private String value;

}
