package com.thinkit.cms.dto.category;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
* @author LG
*/
@Getter
@Setter
@Accessors(chain = true)
public class IndexCategory implements Serializable {


        @NotBlank(message = "站点不能为空")
        private String siteId;
}