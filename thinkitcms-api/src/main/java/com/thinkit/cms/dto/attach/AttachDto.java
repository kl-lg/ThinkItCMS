package com.thinkit.cms.dto.attach;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.thinkit.cms.model.BaseDto;
import com.thinkit.cms.model.BaseModel;
import com.thinkit.cms.utils.Checker;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
/**
* @author LG
*/
@Getter
@Setter
@Accessors(chain = true)
public class AttachDto extends BaseDto {
       /**
         附件地址 绝对路径
       */
        private String fullPath;

       /**
         附件大小
       */
        private Integer fileSize;

       /**
         相对路径
       */
        private String relativePath;

       /**
         桶
       */
        private String bucket;

       /**
         附件名称
       */
        private String objectName;

       /**
         下载数
       */
        private Integer downs;

       /**
         文件后缀
       */
        private String suffix;

       /**
         排序
       */
        private Integer sort;

       /**
         
       */
        private String siteId;

       /**
         是否删除 0：正常 1：删除
       */
        private Boolean deleted;


    @Override
    public <T extends BaseModel> QueryWrapper<T> toWrapper() {
        QueryWrapper<T> queryWrapper=new QueryWrapper<>();
        queryWrapper.eq(Checker.BeNotBlank(siteId),"site_id",siteId);
        queryWrapper.like(Checker.BeNotBlank(objectName),"object_name",objectName);
        queryWrapper.orderByDesc("`gmt_create`");
        return queryWrapper;
    }

}