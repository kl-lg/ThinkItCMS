package com.thinkit.cms.dto.site;

import com.thinkit.cms.annotation.ValidGroup1;
import com.thinkit.cms.annotation.ValidGroup2;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Data
public class WebFileDto implements Serializable {


    @NotBlank(message = "内容不能为空",groups = {ValidGroup1.class})
    private String content;

    @NotBlank(message = "路径不能为空",groups = {ValidGroup2.class})
    private String path;
}
