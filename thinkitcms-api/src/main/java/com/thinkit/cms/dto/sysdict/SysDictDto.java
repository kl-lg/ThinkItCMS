package com.thinkit.cms.dto.sysdict;

import com.thinkit.cms.annotation.ValidGroup1;
import com.thinkit.cms.model.BaseDto;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;

/**
* @author LG
*/
@Getter
@Setter
@Accessors(chain = true)
public class SysDictDto extends BaseDto {
       /**
         父级主键
       */
        private String pid;

       /**
         标签名
       */
        @NotBlank(message = "请输入名称",groups = {ValidGroup1.class})
        private String name;

       /**
         数据值
       */
        @NotBlank(message = "请输入值",groups = {ValidGroup1.class})
        private String value;

       /**
         类型
       */
        private String type;

       /**
         描述
       */
        private String description;

       /**
         排序（升序）
       */
        private Integer num;

       /**
         是否删除 0：正常 1：删除
       */
        private Boolean deleted;



}