package com.thinkit.cms.dto.sysmenu;

import com.thinkit.cms.annotation.ValidGroup1;
import com.thinkit.cms.annotation.ValidGroup2;
import com.thinkit.cms.model.BaseDto;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
* @author LG
*/
@Getter
@Setter
@Accessors(chain = true)
public class SysMenuDto extends BaseDto {
       /**
         父ID
       */
        @NotBlank(message = "请输入菜单父ID",groups = {ValidGroup1.class, ValidGroup2.class})
        private String pid;

        private String api;

        /**
         菜单名称
         */
         @NotBlank(message = "请输入菜单名称",groups = {ValidGroup1.class, ValidGroup2.class})
         private String  menuName;

         private String parentName;

       /**
         路由名称
       */
        private String name;

       /**
         路由路径
       */
        private String path;

       /**
         菜单组件地址
       */
        private String component;

       /**
         排序
       */
        private Integer order;

       /**
         路由 Meta 元信息
       */
        private Object meta;

       /**
         0:目录 1：菜单 2：按钮
       */
        @NotNull(message = "请输入菜单类型",groups = {ValidGroup1.class, ValidGroup2.class})
        private Integer type;

       /**
         是否删除 0：正常 1：删除
       */
        private Boolean deleted;


        private Boolean isLeaf;


        private List<String> authSign;
}