package com.thinkit.cms.dto.sysuser;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.thinkit.cms.model.BaseDto;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.List;
import java.util.Set;

/**
* @author LG
*/
@Getter
@Setter
@Accessors(chain = true)
public class SysUserInfoDto extends BaseDto {
       /**
         用户账户名
       */

        private String userName;

       /**
         用户名称(昵称)
       */
        private String nickName;


       /**
         邮箱
       */
        private String email;

       /**
         手机号
       */
        private String mobile;

       /**
         状态 0:正常，1:禁用
       */
        @JsonIgnore
        private String status;

       /**
         0:男 1：女
       */
        private Integer sex;

       /**
         组织id
       */
        private String orgId;

       /**
         组织代码
       */
        private String orgCode;

       /**
         组织名
       */
        private String orgName;

       /**
         默认站点ID
       */
        private String defSiteId;


        /**
         * 角色标识
         */
        private List<String> roleSigns;


        /**
         * 所有权限标识
         */
        private Set<String> authSign;


        private String avatar;

        private String remark;
}