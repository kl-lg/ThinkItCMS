package com.thinkit.cms.dto.open;

import com.thinkit.cms.annotation.ValidGroup1;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Data
public class OpenViewDto implements Serializable {

    @NotBlank(message = "文章ID不能为空",groups = {ValidGroup1.class})
    private String contentId;

    private String categoryId;

    @NotBlank(message = "站点不能为空",groups = {ValidGroup1.class})
    private String siteId;
}
