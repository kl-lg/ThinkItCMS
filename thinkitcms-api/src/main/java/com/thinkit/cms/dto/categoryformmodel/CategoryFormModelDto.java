package com.thinkit.cms.dto.categoryformmodel;
import lombok.Getter;
import lombok.Setter;
import java.math.BigDecimal;
import lombok.experimental.Accessors;
import java.util.Date;
import com.thinkit.cms.model.BaseDto;
/**
* @author LG
*/
@Getter
@Setter
@Accessors(chain = true)
public class CategoryFormModelDto extends BaseDto {
       /**
         分类ID
       */
        private String categoryId;

       /**
         表单模型ID
       */
        private String formModelId;

        /**
         表单模型 名称
         */
        private String formModelName;

       /**
         站点ID
       */
        private String siteId;

       /**
         是否删除 0：正常 1：删除
       */
        private Boolean deleted;


}