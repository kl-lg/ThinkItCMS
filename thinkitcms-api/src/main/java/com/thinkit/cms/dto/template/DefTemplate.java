package com.thinkit.cms.dto.template;

import lombok.Data;

import javax.validation.constraints.NotBlank;
@Data
public class DefTemplate {

    @NotBlank(message = "请选择您的模板")
    private String tempId;

    @NotBlank(message = "请选择当前站点")
    private String siteId;
}
