package com.thinkit.cms.dto.sysconf;
import com.thinkit.cms.model.BaseDto;
import lombok.Getter;
import lombok.Setter;
import java.math.BigDecimal;
import lombok.experimental.Accessors;
import java.util.Date;
/**
* @author System
*/
@Getter
@Setter
@Accessors(chain = true)
public class SysConfDto extends BaseDto {
       /**
         配置项名称
       */
        private String name;

       /**
         配置项编码
       */
        private String code;

       /**
         配置项值
       */
        private String value;

       /**
         描述
       */
        private String description;

       /**
         是否删除 0：正常 1：删除
       */
        private Boolean deleted;


}