package com.thinkit.cms.dto.directs;

import lombok.Data;

@Data
public class InParams {

    private String name;

    private String type;

    private Boolean required;

}
