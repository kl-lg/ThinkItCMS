package com.thinkit.cms.dto.formmodeltmp;
import lombok.Getter;
import lombok.Setter;
import java.math.BigDecimal;
import lombok.experimental.Accessors;
import java.util.Date;
import com.thinkit.cms.model.BaseDto;
/**
* @author LG
*/
@Getter
@Setter
@Accessors(chain = true)
public class FormModelTmpDto extends BaseDto {
       /**
         
       */
        private String templateId;

       /**
         
       */
        private String formModelId;

       /**
         模板路径
       */
        private String tmpPath;

       /**
         手机端模板路径
       */
        private String mobileTmpPath;

       /**
         是否删除 0：正常 1：删除
       */
        private Boolean deleted;


}