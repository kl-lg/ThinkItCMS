package com.thinkit.cms.dto.sysmenu;

import com.thinkit.cms.utils.Tree;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * @author LONG
 */
@Getter
@Setter
@Accessors(chain = true)
public class MenuSlide<T> extends Tree<T> {
    /**
     路由名称
     */
    private String name;
    /**
     路由路径
     */
    private String path;

    private String  menuName;
    /**
     菜单组件地址
     */
    private String component;

    private Integer order;

}
