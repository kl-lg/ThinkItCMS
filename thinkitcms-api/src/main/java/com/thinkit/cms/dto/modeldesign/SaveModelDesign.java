package com.thinkit.cms.dto.modeldesign;

import com.thinkit.cms.model.BaseDto;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
* @author LG
*/
@Getter
@Setter
@Accessors(chain = true)
public class SaveModelDesign extends BaseDto {
       /**
         表单模型主键
       */
        @NotBlank(message = "请选择模型")
        private String modelId;

        /**
         * 设计
         */
        @NotEmpty(message = "请添加设计项")
        private List<ModelDesignDto> rows;

}