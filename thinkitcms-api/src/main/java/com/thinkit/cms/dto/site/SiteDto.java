package com.thinkit.cms.dto.site;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.thinkit.cms.annotation.IndexField;
import com.thinkit.cms.annotation.IndexId;
import com.thinkit.cms.annotation.ValidGroup1;
import com.thinkit.cms.annotation.ValidGroup2;
import com.thinkit.cms.constant.SysConst;
import com.thinkit.cms.enums.FieldType;
import com.thinkit.cms.enums.IdType;
import com.thinkit.cms.model.BaseDto;
import com.thinkit.cms.model.BaseModel;
import com.thinkit.cms.utils.Checker;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;

/**
* @author LG
*/
@Getter
@Setter
@Accessors(chain = true)
public class SiteDto extends BaseDto {


        @IndexId(type = IdType.CUSTOMIZE, prefix = SysConst.sitePrefix)
        private  String id;

       /**
         站点名称
       */
        @IndexField(fieldType = FieldType.TEXT, ignoreCase = true)
        @NotBlank(message = "请输入站点名称",groups = {ValidGroup1.class, ValidGroup2.class})
        private String name;

       /**
         站点关键字
       */
      // @NotBlank(message = "请输入站点关键字",groups = {ValidGroup1.class, ValidGroup2.class})
        @IndexField(fieldType = FieldType.TEXT, ignoreCase = true)
        private String keyword;

       /**
         站点描述
       */

       /**
         站点LOGO 地址
       */
       @IndexField(fieldType = FieldType.TEXT, ignoreCase = true)
        private String logoImg;

       /**
         域名地址
       */
        @IndexField(fieldType = FieldType.TEXT, ignoreCase = true)
        @NotBlank(message = "请输入站点关键字",groups = {ValidGroup1.class, ValidGroup2.class})
        private String domain;

       /**
         站点跟目录地址
       */
        @IndexField(fieldType = FieldType.TEXT, ignoreCase = true)
        @NotBlank(message = "请输入站点目录",groups = {ValidGroup1.class, ValidGroup2.class})
        private String dir;

       /**
         手机端模板根目录
       */
        @IndexField(fieldType = FieldType.TEXT, ignoreCase = true)
        @NotBlank(message = "请输入站点手机目录",groups = {ValidGroup1.class, ValidGroup2.class})
        private String mobileDir;

       /**
         站点备案信息
       */

       /**
         站点版权
       */
        @IndexField(fieldType = FieldType.TEXT, ignoreCase = true)
        private String copyright;

       /**
         是否删除 0：正常 1：删除
       */
        private Boolean deleted;

       /**
         默认模板ID
       */
        @IndexField(fieldType = FieldType.TEXT, ignoreCase = true)
        private String defTemplateId;

        @IndexField(fieldType = FieldType.TEXT, ignoreCase = true)
        private String description;

        @IndexField(fieldType = FieldType.NUMERIC, ignoreCase = true)
        private Integer order;

        @IndexField(fieldType = FieldType.TEXT, ignoreCase = true)
        private String icp;

        private Boolean defsite=false;


        @IndexField(fieldType = FieldType.TEXT, ignoreCase = true)
        private String templateName;

        @IndexField(fieldType = FieldType.TEXT, ignoreCase = true)
        private String pcIndex;


        @IndexField(fieldType = FieldType.TEXT, ignoreCase = true)
        private String mobileIndex;

        @IndexField(fieldType = FieldType.TEXT, ignoreCase = true)
        @NotBlank(message = "请输入站点编码",groups = {ValidGroup1.class})
        private String code;

        @Override
        public <T extends BaseModel> QueryWrapper<T> toWrapper() {
            QueryWrapper<T> queryWrapper=new QueryWrapper<>();
            queryWrapper.like(Checker.BeNotBlank(name),"name",name);
            queryWrapper.like(Checker.BeNotBlank(domain),"domain",domain);
            queryWrapper.like(Checker.BeNotBlank(description),"description",description);
            queryWrapper.orderByDesc("`order`");
            return queryWrapper;
        }
}