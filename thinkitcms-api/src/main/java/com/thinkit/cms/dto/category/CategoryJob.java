package com.thinkit.cms.dto.category;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.thinkit.cms.user.User;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
* @author LG
*/
@Getter
@Setter
@Accessors(chain = true)
public class CategoryJob implements Serializable {
       /**
         定时任务启用状态
       */
        private Boolean enable = true;

       /**
         定时任务开始时间
       */
        private String jobStartTime;

       /**
         定时任务执行周期（小时）
       */
        private Integer cycle;

        /**
         * 上一次的执行时间
         */
        private String lastExecTime;


        /**
         * 是否正在执行
         */
        private Boolean isRuing = false;

        /**
         * 栏目ID
         */
        private String categoryId;


        /**
         * 栏目名称
         */
        private String categoryName;

        /**
         * 上下文用户
         */
        @JsonIgnoreProperties(ignoreUnknown = true)
        private User user;

        /**
         * 站点id
         */
        private String siteId;

        /**
         * 模板ID
         */
        private String templateId;
}