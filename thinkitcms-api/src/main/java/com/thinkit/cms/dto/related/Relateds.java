package com.thinkit.cms.dto.related;

import com.thinkit.cms.annotation.ValidGroup1;
import com.thinkit.cms.annotation.ValidGroup2;
import com.thinkit.cms.model.BaseDto;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
* @author LG
*/
@Getter
@Setter
@Accessors(chain = true)
public class Relateds extends BaseDto {


    @NotEmpty(message = "请选择要推荐的内容",groups = {ValidGroup1.class})
    private List<String> relatedIds;

    /**
     当前内容id
     */
    @NotBlank(message = "请输入内容ID",groups = {ValidGroup1.class, ValidGroup2.class})
    private String contentId;

}