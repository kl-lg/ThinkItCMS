package com.thinkit.cms.dto.directs;

import lombok.Data;

@Data
public class OutParams {

    private String name;

    private String type;

    private String alias;

}
