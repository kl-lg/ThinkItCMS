package com.thinkit.cms.dto.template;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.thinkit.cms.model.BaseDto;
import com.thinkit.cms.model.BaseModel;
import com.thinkit.cms.utils.Checker;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
/**
* @author LG
*/
@Getter
@Setter
@Accessors(chain = true)
public class TemplateDto extends BaseDto {
       /**
         模板名称
       */
        private String name;

       /**
         封面
       */
        private String cover;

       /**
         模板 code 
       */
        private String code;

       /**
         模板描述
       */
        private String description;

       /**
         资源目录
       */
        private String dir;


        /**
         * 模板路径
         */
        private String tmpDir;


        private String tmpFileUrl;

        /**
         *  是否共享
         */
        private Integer share;


    @Override
    public <T extends BaseModel> QueryWrapper<T> toWrapper() {
        QueryWrapper queryWrapper = new QueryWrapper<>();
        queryWrapper.like(Checker.BeNotBlank(name),"name",name);
        queryWrapper.like(Checker.BeNotBlank(code),"code",code);
        return queryWrapper;
    }

}