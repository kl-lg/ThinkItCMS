package com.thinkit.cms.dto.content;

import lombok.Data;

import java.io.Serializable;

/**
 * @author LONG
 */
@Data
public class CkRow implements Serializable {

    /**
     内容IDid
     */
    private String id;

    private String categoryId;

    private String formModelId;

    private String categoryName;

}
