package com.thinkit.cms.dto.sysorganize;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.thinkit.cms.utils.Tree;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
* @author LG
*/
@Getter
@Setter
@Accessors(chain = true)
public class SysOrganizeTree extends Tree {
       /**
         组织编码
       */
        private String orgCode;

        private Integer order;

        @JsonProperty("key")
        private String id;
}