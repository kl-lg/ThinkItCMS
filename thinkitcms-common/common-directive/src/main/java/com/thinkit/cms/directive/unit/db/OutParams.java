package com.thinkit.cms.directive.unit.db;

import lombok.Data;

@Data
public class OutParams {

    private String name;

    private String type;

    private String alias;

}
