package com.thinkit.cms.directive.unit.db;
import com.thinkit.cms.model.BaseDto;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import java.util.List;

/**
* @author lg
*/
@Getter
@Setter
@Accessors(chain = true)
public class DirectsModel extends BaseDto {
       /**
         指令名称
       */
        private String name;

       /**
         指令定义
       */
        private String direct;

       /**
         是否启用 1：未启用 2:启用
       */
        private Integer enable;

       /**
         模板sql
       */
        private String sql;

       /**
         返回值类型 Object Map List 等
       */
        private String result;

       /**
         返回结果转化器（需要继承 AbstractDataHandler）
       */
        private String resultConvert="default";

       /**
         入参参数
       */
        private String inParams;

       /**
         出参参数格式化
       */

        private String outParams;

       /**
         指令配置
       */
        private String config;

       /**
         站点ID
       */
        private String siteId;

       /**
         是否共享全局
       */
        private Integer isShare;


        private String sqlType;

        /**
         * 指令类型 1：动态 2：固定
         */
        private Integer directType;
}