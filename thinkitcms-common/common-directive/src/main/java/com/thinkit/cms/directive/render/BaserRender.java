package com.thinkit.cms.directive.render;

import com.thinkit.cms.directive.emums.LongEnum;
import com.thinkit.cms.directive.unit.DirectiveUnit;
import freemarker.template.TemplateModel;

import java.util.Map;

/**
 *模板渲染 基类 用于包装 模板指令参数封装等
 */
public interface BaserRender {

     /**
      * 模板渲染调用方法
      * @throws Exception
      */
     void render() throws Exception;

     /**
      * 获取string 参数
      * @param name
      * @return
      * @throws Exception
      */
     String getString(String name) throws Exception;

     /**
      * 获取string参数 并默认值
      * @param name
      * @param defaultVal
      * @return
      * @throws Exception
      */
     String getString(String name,String defaultVal) throws Exception;

     /**
      * 获取 integer
      * @param name
      * @return
      * @throws Exception
      */
     Integer getInteger(String name) throws Exception;

     /**
      * 获取 integer
      * @param name
      * @param defaultVal
      * @return
      * @throws Exception
      */
     Integer getInteger(String name,Integer defaultVal) throws Exception;

     /**
      * 获取Long
      * @param name
      * @return
      * @throws Exception
      */
     Long getLong(String name) throws Exception;


     /**
      * 获取Long
      * @param name
      * @param defaultVal
      * @return
      * @throws Exception
      */
     Long getLong(String name,Long defaultVal) throws Exception;


     /**
      * 获取数据
      * @param name
      * @return
      * @throws Exception
      */
     Short getShort(String name) throws Exception;

     /**
      * 获取数据
      * @param name
      * @param defaultVal
      * @return
      * @throws Exception
      */
     Short getShort(String name,Short defaultVal) throws Exception;

     /**
      * 获取数据
      * @param name
      * @return
      * @throws Exception
      */
     Double getDouble(String name) throws Exception;

     /**
      * 获取数据
      * @param name
      * @param defaultVal
      * @return
      * @throws Exception
      */
     Double getDouble(String name,Double defaultVal) throws Exception;

     /**
      * 获取字符串
      * @param name
      * @return
      * @throws Exception
      */
     String[] getStringArray(String name) throws Exception;

     /**
      * 获取数据
      * @param name
      * @return
      * @throws Exception
      */
     Long[] getLongArray(String name) throws Exception;

     /**
      * 获取数据
      * @param name
      * @return
      * @throws Exception
      */
     Integer[] getIntegerArray(String name) throws Exception;


     /**
      * 获取数据
      * @param name
      * @return
      * @throws Exception
      */
     Float getFloat(String name) throws Exception;


     /**
      * 获取数据
      * @param name
      * @param defaultVal
      * @return
      * @throws Exception
      */
     Float getFloat(String name,Float defaultVal) throws Exception;


     /**
      * 获取数据
      * @param name
      * @return
      * @throws Exception
      */
     Boolean getBoolean(String name) throws Exception;

     /**
      * 获取数据
      * @param name
      * @param defaultVal
      * @return
      * @throws Exception
      */
     Boolean getBoolean(String name,Boolean defaultVal) throws Exception;

     /**
      * 获取数据
      * @param name
      * @param defaultVal
      * @param longEnum
      * @return
      */
     Object getData(String name, Object defaultVal, LongEnum longEnum);

     /**
      * 获取数据
      * @param name
      * @param clz
      * @param <T>
      * @return
      * @throws Exception
      */
     <T>T getBean(String name,Class<T> clz) throws Exception;

     /**
      * 获取数据
      * @param name
      * @return
      * @throws Exception
      */
     TemplateModel getMap(String name) throws Exception;

     /**
      * 设置数据
      * @param maps
      * @return
      */
     BaserRender putAll(Map  maps);


     /**
      * 设置数据
      * @param key
      * @param value
      * @return
      */
     BaserRender put(String key  ,Object value);

     /**
      * 获取数据
      * @return
      */
     DirectiveUnit getUnit();
}