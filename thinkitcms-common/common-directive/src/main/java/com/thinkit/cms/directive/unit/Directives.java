package com.thinkit.cms.directive.unit;

import lombok.Data;

import java.util.List;

/**
 * @author LONG
 */
@Data
public class Directives {

    private List<DirectiveUnit> directives;

}
