package com.thinkit.cms.directive.annotation;

import java.lang.annotation.*;

@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface Instruct {
    String value() default "";
    String description() default "";
}
