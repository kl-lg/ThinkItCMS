package com.thinkit.cms.directive.render;

/**
 * 
 * Directive 指令接口
 *
 */
public interface Directive {


    /**
     * 渲染执行器
     * @param render
     * @throws Exception
     */
     void execute(BaserRender render) throws  Exception;

}
