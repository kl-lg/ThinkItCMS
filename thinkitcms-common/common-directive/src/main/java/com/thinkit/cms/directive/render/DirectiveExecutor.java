package com.thinkit.cms.directive.render;

import cn.hutool.core.codec.Base64;
import cn.hutool.extra.spring.SpringUtil;
import com.thinkit.cms.constant.SysConst;
import com.thinkit.cms.directive.cache.CacheManager;
import com.thinkit.cms.directive.emums.LongEnum;
import com.thinkit.cms.directive.exception.TemplateException;
import com.thinkit.cms.directive.funs.BaseFunInterface;
import com.thinkit.cms.directive.mapper.BaseMapper;
import com.thinkit.cms.directive.mapper.RsMapper;
import com.thinkit.cms.directive.mapper.SqlMapper;
import com.thinkit.cms.directive.source.SpeltExpression;
import com.thinkit.cms.directive.unit.CacheUnit;
import com.thinkit.cms.directive.unit.DirectiveUnit;
import com.thinkit.cms.directive.unit.ParamUnit;
import com.thinkit.cms.utils.Checker;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author LONG
 */
@Slf4j
public abstract class DirectiveExecutor extends AbstractExecutor {
    protected static SqlMapper sqlMapper= SpringUtil.getBean(SqlMapper.class);
    protected static RsMapper rsMapper = SpringUtil.getBean(RsMapper.class);
    private static final Map<String, BaseMapper> mapperMap= new HashMap<>();

    protected Map<String, BaseFunInterface> baseFunMap;

    public void setFuncInterface(Map<String, BaseFunInterface> baseFunInterfaceMap){
        this.baseFunMap = baseFunInterfaceMap;
    }

    protected BaseFunInterface getBaseFunInterface(String directiveName){
        if(baseFunMap.containsKey(directiveName)){
             return baseFunMap.get(directiveName);
        }
        log.error("自定义指令类型为:");
        return null;
    }

    static {
        mapperMap.put("mysql",sqlMapper);
        mapperMap.put("redis", rsMapper);
    }

    protected CacheManager cacheManager = SpringUtil.getBean(CacheManager.class);;
    protected String getSql(BaserRender render){
        return SpeltExpression.resolver(render);
        //return DataResolve.resolver(render);
    }
    protected Object executeSql(String sql,BaserRender render){
        DirectiveUnit directiveUnit = getDirectiveMeta();
        String result = directiveUnit.getConfig().getResult().getType();
        if(Checker.BeBlank(result)){
            throw new TemplateException("ERROR:请指定指令参数[result] 的返回值类型!");
        }
        String sqlType = directiveUnit.getSqlType();
        LongEnum longEnum =LongEnum.getEnum(result);
        if(Checker.BeNotNull(longEnum)){
            List<ParamUnit> outs = directiveUnit.getConfig().getParams().getOut();
            CacheUnit cacheUnit = directiveUnit.getConfig().getCacheUnit();
            String directive = getUnit().getDirective();
            boolean enableCache = Checker.BeNotNull(cacheUnit) && cacheUnit.getEnable();
            String key = Checker.BeNotBlank(cacheUnit.getCacheName())?(SysConst.defCache+cacheUnit.getCacheName()+SysConst.colon):
            SysConst.defCache+directive+SysConst.colon;
            key = key+Base64.encode(sql);
            if(enableCache){
                Object obj = cacheManager.getCacheData(key);
                if(Checker.BeNotNull(obj)){
                   return obj;
                }
            }
            Object object =  switch (longEnum){
                case INTEGER -> dataHandler.handData(mapperMap.get(sqlType).getInteger(sql),render);
                case SHORT -> dataHandler.handData(mapperMap.get(sqlType).getShort(sql),render);
                case LONG -> dataHandler.handData(mapperMap.get(sqlType).getLong(sql),render);
                case FLOAT -> dataHandler.handData(mapperMap.get(sqlType).getFloat(sql),render);
                case DOUBLE -> dataHandler.handData(mapperMap.get(sqlType).getDouble(sql),render);
                case BOOLEAN -> dataHandler.handData(mapperMap.get(sqlType).getBoolean(sql),render);
                case STRING -> dataHandler.handData(mapperMap.get(sqlType).getString(sql),outs,render);
                case DATE -> dataHandler.handData(mapperMap.get(sqlType).getDate(sql),render);
                case MAP, OBJECT, BEAN ->dataHandler.handData(mapperMap.get(sqlType).getOne(sql),outs,render);
                case LONG_ARRAY -> dataHandler.handData(mapperMap.get(sqlType).getLongList(sql),render);
                case STRING_ARRAY -> dataHandler.handData(mapperMap.get(sqlType).getStringList(sql),render);
                case INTEGER_ARRAY -> dataHandler.handData(mapperMap.get(sqlType).getIntegerList(sql),render);
                case OBJECT_ARRAY, LIST -> dataHandler.handData(mapperMap.get(sqlType).getList(sql),outs,render);
                default -> null;
            };
            if(enableCache){
                cacheManager.put(key,object,cacheUnit.getCacheTime());
            }
            return object;
        }
        return null;
    }



}
