package com.thinkit.cms.directive.wrapper.actuator;

import com.thinkit.cms.constant.SysConst;
import com.thinkit.cms.directive.annotation.Executer;
import com.thinkit.cms.directive.emums.ActuatorEnum;
import com.thinkit.cms.directive.event.ContentUpEsEvent;
import com.thinkit.cms.directive.kit.Kv;
import com.thinkit.cms.directive.wrapper.CategoryMData;
import com.thinkit.cms.directive.wrapper.ModelAndView;
import com.thinkit.cms.directive.wrapper.rule.PathRule;
import com.thinkit.cms.enums.EventEnum;
import com.thinkit.cms.model.ModelJson;
import com.thinkit.cms.user.SiteConf;
import com.thinkit.cms.utils.Checker;
import com.thinkit.cms.utils.CmsFolderUtil;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static cn.hutool.extra.spring.SpringUtil.publishEvent;

/**
 * @author LONG
 */
@Slf4j
@Executer(ActuatorEnum.ARTICLE)
public class ArticleExecute extends AbstractActuator{

    @Override
    public void execute(ModelAndView modelAndView) {
        super.execute(modelAndView);
    }

    @Override
    public String rulePath(ModelAndView modelAndView) {
        SiteConf siteConf = getSiteConf(modelAndView);
        CategoryMData categoryMData = modelAndView.getCategoryMData();
        if(Checker.BeNotNull(categoryMData)){
            String rulePath =  categoryMData.getRuleRule();
            String code =  categoryMData.getCategoryCode();
            String contentId=categoryMData.getContentId();
            Boolean forceGen = categoryMData.getForceGen();
            String destPath = PathRule.getPathRule(rulePath).format(code,contentId);
            // 编辑
            // 按照规则来说 pc 应该的 目标地址
            String pcDir = CmsFolderUtil.getSitePathHtml(null,siteConf.getDomain(),siteConf.getDir(), destPath);
            // 手机端应该的目标地址
            String mobileDir = CmsFolderUtil.getSitePathHtml(null,siteConf.getDomain(),siteConf.getMobileDir(), destPath);
            // 数据库存在栏目pc地址
            boolean hasPcUrl =  super.hasRelativePath(true);
            // 数据库存在栏目手机端地址
            boolean hasMUrl =  super.hasRelativePath(false);

            String pcRelativePath = CmsFolderUtil.prefix(siteConf.getDir()+destPath+ SysConst.html);
            String mRelativePath= CmsFolderUtil.prefix(siteConf.getMobileDir()+destPath+SysConst.html);
            if(hasPcUrl &&!forceGen){
                pcDir = CmsFolderUtil.getSitePathUrl(null,siteConf.getDomain(),super.getRelativePath(true));
            }else{
                super.getModelTemp().setRelativePath(pcRelativePath);
            }
            if(hasMUrl &&!forceGen){
                mobileDir = CmsFolderUtil.getSitePathUrl(null,siteConf.getDomain(),super.getRelativePath(false));
            }else{
                super.getModelTemp().setRelativeMPath(mRelativePath);
            }
            super.setPcTarget(pcDir).setMobileTarget(mobileDir);
        }
        return null;
    }

    @Override
    protected void success(Boolean pc,String tempPath,String targetPath,ModelAndView modelAndView) {
        String relativeMPath  = super.getModelTemp().getRelativeMPath();
        String relativePath = super.getModelTemp().getRelativePath();
        String contentId = modelAndView.getCategoryMData().getContentId();
        String fieldName = pc?SysConst.url:SysConst.m_url;
        String val = pc?relativePath:relativeMPath;
        if(Checker.BeNotBlank(relativeMPath) || Checker.BeNotBlank(relativePath)){
            if(Checker.BeNotBlank(val)){
                sqlMapper.updatePath("tk_content",contentId,fieldName,val);
                Map<String,Object> map = Kv.by(fieldName,val);
                ModelJson modelJson = new ModelJson();
                modelJson.setObjectMap(map);
                modelJson.setPk(contentId);
                modelJson.setPrefix(SysConst.contentPrefix);
                publishEvent(new ContentUpEsEvent(modelJson, EventEnum.UPDATE_CONTENT_FIELD));
                clearCache(modelAndView);
            }
        }
        if(pc){
            String taskId = modelAndView.getTaskId();
            upTimes(taskId,SysConst.okTimes);
        }
    }


    private void upTimes(String taskId,String mapKey){
        if(Checker.BeNotBlank(taskId)){
            taskService.upProgress(taskId,mapKey,1);
        }
    }

    private void clearCache(ModelAndView modelAndView){
        String contentId = modelAndView.getCategoryMData().getContentId();
        List<String> keys = new ArrayList<>();
        keys.add("cached::class com.thinkit.cms.boot.service.content.ContentServiceImpl."+contentId);
        redisTemplate.delete(keys);
    }
    @Override
    protected void fail(Boolean pc,String tempPath,String  targetPath,ModelAndView modelAndView) {
        if(pc){
            String taskId = modelAndView.getTaskId();
            upTimes(taskId,SysConst.failTimes);
        }
        log.error("文章内容生成失败:{}",targetPath);
    }


}
