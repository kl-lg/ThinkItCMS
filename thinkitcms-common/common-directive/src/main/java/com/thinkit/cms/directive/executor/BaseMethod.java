package com.thinkit.cms.directive.executor;

import com.thinkit.cms.directive.emums.LongEnum;
import com.thinkit.cms.directive.emums.MethodEnum;
import freemarker.template.TemplateHashModelEx;
import freemarker.template.TemplateMethodModelEx;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * 
 * BaseMethod FreeMarker自定义方法基类
 *
 * @author LONG
 */
@Data
public abstract class BaseMethod extends MethodWrapper implements TemplateMethodModelEx {


    public static String getString(int index, List<TemplateModel> arguments, LongEnum longEnum) throws TemplateModelException {
         return getParam(index,arguments,longEnum);
    }

    public static Short getShort(int index, List<TemplateModel> arguments, LongEnum longEnum) throws TemplateModelException {
        return getParam(index,arguments,longEnum);
    }

    public static Integer getInteger(int index, List<TemplateModel> arguments,LongEnum longEnum) throws TemplateModelException {
        return getParam(index,arguments,longEnum);
    }

    public static Boolean getBoolean(int index, List<TemplateModel> arguments,LongEnum longEnum) throws TemplateModelException {
        return getParam(index,arguments,longEnum);
    }

    public static Date getDate(int index, List<TemplateModel> arguments,LongEnum longEnum) throws TemplateModelException {
        return getParam(index,arguments,longEnum);
    }

    public static Long getLong(int index, List<TemplateModel> arguments,LongEnum longEnum) throws TemplateModelException {
        return getParam(index,arguments,longEnum);
    }

    public static Double getDouble(int index, List<TemplateModel> arguments,LongEnum longEnum) throws TemplateModelException {
        return getParam(index,arguments,longEnum);
    }

    public static String[] getStringArray(int index, List<TemplateModel> arguments,LongEnum longEnum) throws TemplateModelException {
        return getParam(index,arguments,longEnum);
    }

    public static String[] getIntegerArray(int index, List<TemplateModel> arguments,LongEnum longEnum) throws TemplateModelException {
        return getParam(index,arguments,longEnum);
    }


    public static String[] getLongArray(int index, List<TemplateModel> arguments,LongEnum longEnum) throws TemplateModelException {
        return getParam(index,arguments,longEnum);
    }

    public static TemplateHashModelEx getMap(int index, List<TemplateModel> arguments,LongEnum longEnum) throws TemplateModelException {
        return getParam(index,arguments,longEnum);
    }

    public abstract MethodEnum getName();
}
