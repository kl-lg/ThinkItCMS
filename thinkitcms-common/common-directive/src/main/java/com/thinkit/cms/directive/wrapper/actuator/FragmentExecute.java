package com.thinkit.cms.directive.wrapper.actuator;

import com.thinkit.cms.directive.annotation.Executer;
import com.thinkit.cms.directive.emums.ActuatorEnum;
import com.thinkit.cms.directive.wrapper.ModelAndView;
import lombok.extern.slf4j.Slf4j;

/**
 * @author LONG
 */
@Slf4j
@Executer(ActuatorEnum.FRAGMENT)
public class FragmentExecute extends AbstractActuator{

    @Override
    public void execute(ModelAndView modelAndView) {

        super.execute(modelAndView);
    }

    @Override
    public String rulePath(ModelAndView modelAndView) {
        super.setPcTarget(super.getRelativePath(true)).
        setMobileTarget(super.getRelativePath(false));
        return null;
    }

    @Override
    protected void success(Boolean pc,String tempPath,String  targetPath,ModelAndView modelAndView) {
        log.error("页面片段容生成成功:{}",targetPath);
    }

    @Override
    protected void fail(Boolean pc,String tempPath,String  targetPath,ModelAndView modelAndView) {
        log.error("页面片段生成失败:{}",targetPath);
    }
}
