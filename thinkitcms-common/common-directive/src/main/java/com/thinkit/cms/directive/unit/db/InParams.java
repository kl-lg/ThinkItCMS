package com.thinkit.cms.directive.unit.db;

import lombok.Data;

@Data
public class InParams {

    private String name;

    private String type;

    private Boolean required;

}
