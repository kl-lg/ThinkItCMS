package com.thinkit.cms.directive.exception;
import com.thinkit.cms.constant.SysConst;
import com.thinkit.cms.utils.CmsConf;
import freemarker.core.Environment;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;
import lombok.extern.slf4j.Slf4j;
import java.io.IOException;
import java.io.Writer;

/**
 * @author LONG
 */
@Slf4j
public class FreemarkerExceptionHandler implements TemplateExceptionHandler {

    @Override
    public void handleTemplateException(TemplateException e, Environment environment, Writer out) throws TemplateException {
        log.error("生成模板出现异常请您查看错误日志:{}",e.getMessage());
        try {
            out.write(CmsConf.get(SysConst.defaultTmpErrMsg));
        } catch (IOException ex) {
            log.error(ex.getMessage());
        }
        if(!Boolean.valueOf(CmsConf.get(SysConst.errKeep))){
            throw e;
        }
    }
}
