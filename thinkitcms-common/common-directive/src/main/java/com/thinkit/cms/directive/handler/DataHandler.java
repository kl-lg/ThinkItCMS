package com.thinkit.cms.directive.handler;

import com.thinkit.cms.directive.render.BaserRender;
import com.thinkit.cms.directive.unit.ParamUnit;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author LONG
 */
public interface DataHandler {

    /**
     * 获取数据
     * @param value
     *  @param render
     * @return
     */
    Object handData(Map value, BaserRender render);

    /**
     * 格式化数据
     * @param value
     *  @param render
     * @return
     */
    Object handData(Long value,BaserRender render);

    /**
     * 格式化数据
     * @param value
     *  @param render
     * @return
     */
    Object handData(Integer value, BaserRender render);

    /**
     * 格式化数据
     * @param value
     *  @param render
     * @return
     */
    Object handData(Short value, BaserRender render);

    /**
     * 格式化数据
     * @param value
     *  @param render
     * @return
     */
    Object handData(Float value, BaserRender render);

    /**
     * 格式化数据
     * @param value
     *  @param render
     * @return
     */
    Object handData(Double value, BaserRender render);

    /**
     * 格式化数据
     * @param value
     *  @param render
     * @return
     */
    Object handData(Boolean value, BaserRender render);

    /**
     * 格式化数据
     * @param value: 值
     * @param outs :输出
     *              @param render
     * @return
     */
    Object handData(String value,List<ParamUnit> outs, BaserRender render);

    /**
     * 格式化数据
     * @param value
     *  @param render
     * @return
     */
    Object handData(Date value, BaserRender render);

    /**
     * 格式化数据
     * @param value
     *  @param render
     * @return
     */
    Object handData(List  value, BaserRender render);

    /**
     * 格式化数据
     * @param value
     *  @param render
     * @param outs
     * @return
     */
    Object handData(Map value,List<ParamUnit> outs, BaserRender render);

    /**
     * 格式化数据
     * @param values
     *  @param render
     * @param outs
     * @return
     */
    Object handData(List<Map>  values,List<ParamUnit> outs, BaserRender render);

    /**
     * 获取数据
     * @param value
     *  @param render
     * @return
     */
    Object getData(Object value, BaserRender render);
}
