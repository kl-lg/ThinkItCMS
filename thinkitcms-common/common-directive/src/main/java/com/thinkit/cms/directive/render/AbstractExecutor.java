package com.thinkit.cms.directive.render;

import com.thinkit.cms.directive.handler.DataHandler;
import com.thinkit.cms.directive.unit.DirectiveUnit;
import lombok.Data;

/**
 * @author LONG
 */
@Data
public abstract class AbstractExecutor extends BaseDirective {

    protected DirectiveUnit directiveMeta;

    protected DataHandler dataHandler;

    @Override
    public DirectiveUnit getUnit(){
        return directiveMeta;
    }
}
