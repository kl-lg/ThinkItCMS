package com.thinkit.cms.directive.factory;

import cn.hutool.extra.spring.SpringUtil;
import cn.hutool.json.JSONUtil;
import com.google.common.collect.Lists;
import com.thinkit.cms.directive.executor.Executor;
import com.thinkit.cms.directive.handler.DataHandler;
import com.thinkit.cms.directive.mapper.SqlMapper;
import com.thinkit.cms.directive.render.DirectiveExecutor;
import com.thinkit.cms.directive.unit.*;
import com.thinkit.cms.directive.unit.db.DirectsModel;
import com.thinkit.cms.utils.Checker;
import freemarker.template.SimpleHash;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author LONG
 */
@Slf4j
public class DirectiveFactory {

    public static Directives directives = new Directives();

    public static SqlMapper sqlMapper = SpringUtil.getBean(SqlMapper.class);

//    static {
//        ClassPathResource classPathResource = new ClassPathResource("directive/directives.json");
//        try {
//            InputStream inputStream = classPathResource.getInputStream();
//            BufferedReader bf = new BufferedReader(new InputStreamReader(inputStream));
//            String content = IoUtil.read(bf);
//            directives = JSONUtil.toBean(content,Directives.class);
//        } catch (IOException e) {
//            log.error("读取 directives JSON 配置文件异常!{}",e.getMessage());
//            e.printStackTrace();
//        }
//    }


//    public static List<DirectiveUnit> directives(){
//        List<DirectiveUnit> directiveUnits = directives.getDirectives();
//        if(Checker.BeNotEmpty(directiveUnits)){
//            directiveUnits.removeIf(directiveUnit -> !directiveUnit.getEnable());
//        }
//        return Checker.BeNotEmpty(directiveUnits)?directiveUnits:Lists.newArrayList();
//    }

    public static List<DirectiveUnit> directives(){
        List<DirectiveUnit> directiveUnits = new ArrayList<>();
        List<DirectsModel> directsModels = sqlMapper.queryDirects();
        if(Checker.BeNotEmpty(directsModels)){
            for(DirectsModel directs:directsModels){
                DirectiveUnit directiveUnit = new DirectiveUnit();
                directiveUnit.setDirective(directs.getDirect());
                directiveUnit.setSqlTemplate(directs.getSql());
                directiveUnit.setName(directs.getName());
                directiveUnit.setEnable(directs.getEnable().equals(2));
                directiveUnit.setDirectType(directs.getDirectType());
                directiveUnit.setSqlType(Checker.BeNotBlank(directs.getSqlType())?directs.getSqlType():"mysql");
                ParamConfUnit paramConfUnit = new ParamConfUnit();
                ParamDefUnit paramDefUnit = new ParamDefUnit();
                if(Checker.BeNotBlank(directs.getInParams())){
                    List<ParamUnit> in = JSONUtil.toList(directs.getInParams(),ParamUnit.class);
                    paramDefUnit.setIn(in);
                }
                if(Checker.BeNotBlank(directs.getOutParams())){
                    List<ParamUnit> out = JSONUtil.toList(directs.getOutParams(),ParamUnit.class);
                    paramDefUnit.setOut(out);
                }

                ReturnUnit result = new ReturnUnit();
                result.setHandler(directs.getResultConvert());
                result.setType(directs.getResult());

                paramConfUnit.setParams(paramDefUnit);
                paramConfUnit.setResult(result);

                if(Checker.BeNotBlank(directs.getConfig())){
                    CacheUnit cacheUnit = JSONUtil.toBean(directs.getConfig(),CacheUnit.class);
                    paramConfUnit.setCacheUnit(cacheUnit);
                }
                directiveUnit.setConfig(paramConfUnit);
                directiveUnits.add(directiveUnit);
            }
        }
        return directiveUnits;
    }



}
