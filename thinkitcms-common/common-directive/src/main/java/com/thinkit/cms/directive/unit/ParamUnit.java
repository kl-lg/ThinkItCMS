package com.thinkit.cms.directive.unit;

import lombok.Data;

/**
 * @author LONG
 */
@Data
public class ParamUnit {
      private String name;
      private String type;
      private Boolean required;
      private String alias;
}
