
package com.thinkit.cms.directive.annotation;

import com.thinkit.cms.directive.emums.ActuatorEnum;

import java.lang.annotation.*;

/**
 * @author LONG
 */
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface Executer {

    ActuatorEnum value() default ActuatorEnum.INDEX;
}