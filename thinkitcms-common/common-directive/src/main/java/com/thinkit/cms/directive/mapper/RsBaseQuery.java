package com.thinkit.cms.directive.mapper;

import cn.hutool.json.JSONUtil;
import com.google.common.collect.Lists;
import com.thinkit.cms.directive.kit.RedisLuaUtils;
import com.thinkit.cms.utils.Checker;
import lombok.extern.slf4j.Slf4j;
import redis.clients.jedis.search.Document;
import redis.clients.jedis.search.SearchResult;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Slf4j
public class RsBaseQuery {
    /**
     *  该功能为计划优化功能 基于数据库压力过大的情况下 采用 redisearch
     */


    private static Map toMap(String data){
        if(Checker.BeNotBlank(data)){
            return JSONUtil.toBean(data,Map.class);
        }
        return null;
    }


    public  static  List<Map>  toMap(List<Document> documents){
        List<Map> datas = new ArrayList<>();
        if(Checker.BeNotEmpty(documents)){
             for(Document document:documents){
                   String data = document.getString("$");
                   datas.add(toMap(data));
             }
        }
        return datas;
    }

    private  <T> List<T>  toArray(List<Document> documents, Class clz){
        List<T> datas = new ArrayList<>();
        if(Checker.BeNotEmpty(documents)){
            for(Document document:documents){
                Iterable<Map.Entry<String, Object>> entrys = document.getProperties();
                entrys.forEach(entry->{
                    Boolean isBaseType =
                    clz.isAssignableFrom(String.class)||
                    clz.isAssignableFrom(Number.class);
                    if(isBaseType){
                        datas.add((T) entry.getValue());
                    }
                });
            }
        }
        return datas;
    }


    private  <T> T  toT(Document document, Class clz){
        if(Checker.BeNotNull(document)){
            Iterable<Map.Entry<String, Object>> entrys = document.getProperties();
            return (T) entrys.iterator().next().getValue();
        }
        return null;
    }




    protected <T> List<T> getList(String sql,Class clz){
       try {
           SearchResult res = RedisLuaUtils.ftSearch(sql);
           return toArray(res.getDocuments(),clz);
       }catch (Exception e){
           log.error("查询 getList ES失败：{}",e.getMessage());
           return  Lists.newArrayList();
       }
    }


    protected  List<Map> getList(String sql){
        try {
            log.info("RS SQL:{}",sql);
            SearchResult res = RedisLuaUtils.ftSearch(sql);
            if(Checker.BeNotEmpty(res.getDocuments())){
                return toMap(res.getDocuments());
            }
        }catch (Exception e){
            log.error("查询 getList RS失败：{}",e.getMessage());
            return  Lists.newArrayList();
        }
        return  Lists.newArrayList();
    }

    protected String getString(String sql){
         return get(sql,String.class);
    }

    protected Long getLong(String sql){
        return get(sql,Long.class);
    }

    protected Integer getInteger(String sql){
        return get(sql,Integer.class);
    }

    protected Double getDouble(String sql) {
        return get(sql,Double.class);
    }

    protected Short getShort(String sql) {
        return get(sql,Short.class);
    }
    protected Float getFloat(String sql) {
        return get(sql,Float.class);
    }

    protected Boolean getBoolean(String sql) {
        return get(sql,Boolean.class);
    }

    protected Date getDate(String sql) {
        return get(sql,Date.class);
    }


    protected List<Long> getLongList(String sql){
        return gets(sql,Long.class);
    }


    protected List<Integer> getIntegerList(String sql){
        return gets(sql,Integer.class);
    }

    protected List<String> getStringList(String sql){
        return gets(sql,String.class);
    }

    private <T> List<T> gets(String sql,Class clz){
        try {
            log.info("RS SQL:{}",sql);
            SearchResult res = RedisLuaUtils.ftSearch(sql);
            if(Checker.BeNotEmpty(res.getDocuments())){
                return toArray(res.getDocuments(),clz);
            }
        }catch (Exception e){
            log.error("查询 getString ES失败：{}",e.getMessage());
        }
        return Lists.newArrayList();
    }

    private <T> T get(String sql,Class clz){
        try {
            log.info("RS SQL:{}",sql);
            SearchResult res = RedisLuaUtils.ftSearch(sql);
            if(Checker.BeNotEmpty(res.getDocuments())){
                return toT(res.getDocuments().get(0),clz);
            }
        }catch (Exception e){
            log.error("查询 getString ES失败：{}",e.getMessage());
        }
        return null;
    }




}
