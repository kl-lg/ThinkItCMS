package com.thinkit.cms.directive.render;

import com.thinkit.cms.directive.emums.LongEnum;
import com.thinkit.cms.directive.unit.DirectiveUnit;
import com.thinkit.cms.utils.Checker;
import freemarker.core.Environment;
import freemarker.ext.beans.BeanModel;
import freemarker.template.*;

import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

public class Wrapper {

    private Environment environment;
    private Map<String, TemplateModel> map;
    private TemplateModel[] templateModels;
    private TemplateDirectiveBody templateDirectiveBody;
    private Boolean isRender = false;
    private DirectiveUnit directiveUnit;

    public Wrapper(Environment environment, Map map, TemplateModel[] templateModels,
                   TemplateDirectiveBody templateDirectiveBody,DirectiveUnit directiveUnit){
        this.environment = environment;
        this.map = map;
        this.templateModels = templateModels;
        this.templateDirectiveBody = templateDirectiveBody;
        this.environment = environment;
        this.directiveUnit = directiveUnit;
    }

    public <T> T getParam(String name, LongEnum langEnum) throws TemplateModelException {
         switch (langEnum){
             case STRING:
                 SimpleScalar simpleScalar = getStringModel(name,langEnum);
                 if(Checker.BeNotNull(simpleScalar)){
                     return (T) simpleScalar.getAsString();
                 }
                 break;
             case BOOLEAN:
                 TemplateBooleanModel booleanModel = getBooleanModel(name,langEnum);
                 if(Checker.BeNotNull(booleanModel)){
                     return (T) Boolean.valueOf(booleanModel.getAsBoolean());
                 }
                 break;
             case DATE:
                 TemplateDateModel dateModel = getDateModel(name,langEnum);
                 if(Checker.BeNotNull(dateModel)){
                     return (T) dateModel.getAsDate();
                 }
                 break;
             case MAP:
                 TemplateHashModel hashModel = getMapModel(name,langEnum);
                 if(Checker.BeNotNull(hashModel)){
                     return (T) hashModel;
                 }
                 break;
             case BEAN:
                 Object object = getBeanModel(name,langEnum);
                 if(Checker.BeNotNull(object)){
                     return (T) object;
                 }
                 break;
             case STRING_ARRAY:
                 TemplateSequenceModel seqStringModel = getArrayModel(name,langEnum);
                 if(Checker.BeNotNull(seqStringModel)){
                     return (T) formatArray(seqStringModel,langEnum);
                 }
                 break;
             case LONG_ARRAY:
                 TemplateSequenceModel seqLongModel = getArrayModel(name,langEnum);
                 if(Checker.BeNotNull(seqLongModel)){
                     return (T) formatArray(seqLongModel,langEnum);
                 }
                 break;
             case INTEGER_ARRAY:
                 TemplateSequenceModel seqIntegerModel = getArrayModel(name,langEnum);
                 if(Checker.BeNotNull(seqIntegerModel)){
                     return (T) formatArray(seqIntegerModel,langEnum);
                 }
                 break;
             case OBJECT:
                 Object obj = getObjectModel(name,langEnum);
                 if(Checker.BeNotNull(obj)){
                     return (T) obj;
                 }
                 break;
             case LIST:
                 Object listObj = getListModel(name,langEnum);
                 if(Checker.BeNotNull(listObj)){
                     return (T) listObj;
                 }
                 break;
             default:
                 if(LongEnum.isNumber(langEnum)){
                     Number number = getNumberModel(name,langEnum).getAsNumber();
                     if(Checker.BeNotNull(number)){
                         return (T) formatVal(number,langEnum);
                     }
                 }
         }
        return null;
    }


    private <T> T formatArray(TemplateSequenceModel sequenceModel, LongEnum langEnum) throws TemplateModelException {
        if(LongEnum.STRING_ARRAY.equals(langEnum)){
            String[] values = new String[sequenceModel.size()];
            for (int i = 0; i < sequenceModel.size(); i++) {
                values[i] = toStringArray(sequenceModel.get(i));
            }
            return (T) values;
        }else if(LongEnum.LONG_ARRAY.equals(langEnum)){
            Long[] values = new Long[sequenceModel.size()];
            for (int i = 0; i < sequenceModel.size(); i++) {
                values[i] = toLongArray(sequenceModel.get(i));
            }
            return (T) values;
        }else if(LongEnum.INTEGER_ARRAY.equals(langEnum)){
            Integer[] values = new Integer[sequenceModel.size()];
            for (int i = 0; i < sequenceModel.size(); i++) {
                values[i] = toIntegerArray(sequenceModel.get(i));
            }
            return (T) values;
        }else if(LongEnum.LIST.equals(langEnum)){
            if (sequenceModel instanceof TemplateModelListSequence) {
                TemplateModelListSequence templateModelListSequence =(TemplateModelListSequence) sequenceModel;
                return (T) templateModelListSequence.getWrappedObject();
            }
        }
        return null;
    }


    public static String toStringArray(TemplateModel templateModel) throws TemplateModelException {
        if (Checker.BeNotNull(templateModel)) {
            if (templateModel instanceof TemplateSequenceModel) {
                toStringArray(((TemplateSequenceModel) templateModel).get(0));
            }
            if (templateModel instanceof TemplateScalarModel) {
                return ((TemplateScalarModel) templateModel).getAsString();
            } else if ((templateModel instanceof TemplateNumberModel)) {
                Number number = ((TemplateNumberModel) templateModel).getAsNumber();
                if(Checker.BeNotNull(number)){
                    return number.toString();
                }
            }
        }
        return null;
    }

    public static Long toLongArray(TemplateModel templateModel) throws TemplateModelException {
        if (Checker.BeNotNull(templateModel)) {
            if (templateModel instanceof TemplateSequenceModel) {
                toLongArray(((TemplateSequenceModel) templateModel).get(0));
            }
            if (templateModel instanceof TemplateScalarModel) {
                return Long.valueOf(((TemplateScalarModel) templateModel).getAsString());
            } else if ((templateModel instanceof TemplateNumberModel)) {
                Number number = ((TemplateNumberModel) templateModel).getAsNumber();
                if(Checker.BeNotNull(number)){
                    return number.longValue();
                }
            }
        }
        return null;
    }

    public static Integer toIntegerArray(TemplateModel templateModel) throws TemplateModelException {
        if (Checker.BeNotNull(templateModel)) {
            if (templateModel instanceof TemplateSequenceModel) {
                toIntegerArray(((TemplateSequenceModel) templateModel).get(0));
            }
            if (templateModel instanceof TemplateScalarModel) {
                return Integer.valueOf(((TemplateScalarModel) templateModel).getAsString());
            } else if ((templateModel instanceof TemplateNumberModel)) {
                Number number = ((TemplateNumberModel) templateModel).getAsNumber();
                if(Checker.BeNotNull(number)){
                    return number.intValue();
                }
            }
        }
        return null;
    }

    private Number formatVal(Number number, LongEnum langEnum){
        if(LongEnum.LONG.equals(langEnum)){
            return Long.valueOf( number.longValue());
        }else if(LongEnum.INTEGER.equals(langEnum)){
            return Integer.valueOf( number.intValue());
        }else if(LongEnum.DOUBLE.equals(langEnum)){
            return Double.valueOf( number.doubleValue());
        }else if(LongEnum.FLOAT.equals(langEnum)){
            return Float.valueOf( number.floatValue());
        }else if(LongEnum.SHORT.equals(langEnum)){
            return Short.valueOf( number.shortValue());
        }
        return number;
    }


    public <T> T getParam(String name,T defval, LongEnum langEnum) throws TemplateModelException {
        T value = getParam(name,langEnum);
        return Checker.BeNotNull(value)?value:defval;
    }



    private SimpleScalar getStringModel(String name, LongEnum langEnum){
        TemplateModel templateModel = map.get(name);
        if(Checker.BeNotNull(templateModel)){
            if(LongEnum.STRING.equals(langEnum)){
                return (SimpleScalar) templateModel;
            }
        }
        return null;
    }

    private TemplateBooleanModel getBooleanModel(String name, LongEnum langEnum){
        TemplateModel templateModel = map.get(name);
        if(Checker.BeNotNull(templateModel)){
            if(LongEnum.BOOLEAN.equals(langEnum)){
                return (TemplateBooleanModel) templateModel;
            }
        }
        return null;
    }

    private SimpleNumber getNumberModel(String name, LongEnum langEnum){
        TemplateModel templateModel = map.get(name);
        if(Checker.BeNotNull(templateModel)){
            if(LongEnum.isNumber(langEnum)){
                return (SimpleNumber) templateModel;
            }
        }
        return null;
    }

    private TemplateDateModel getDateModel(String name, LongEnum langEnum){
        TemplateModel templateModel = map.get(name);
        if(Checker.BeNotNull(templateModel)){
            if(LongEnum.isNumber(langEnum)){
                return (TemplateDateModel) templateModel;
            }
        }
        return null;
    }

    private TemplateHashModel getMapModel(String name, LongEnum langEnum){
        TemplateModel templateModel = map.get(name);
        if(Checker.BeNotNull(templateModel)){
            if(LongEnum.MAP.equals(langEnum)){
                if(templateModel instanceof  TemplateHashModel){
                    return (TemplateHashModel) templateModel;
                }
            }
        }
        return null;
    }

    private Object getBeanModel(String name, LongEnum langEnum){
        TemplateModel templateModel = map.get(name);
        if(Checker.BeNotNull(templateModel)){
            if(LongEnum.BEAN.equals(langEnum)){
                if (templateModel instanceof BeanModel) {
                    return ((BeanModel) templateModel).getWrappedObject();
                }
            }
        }
        return null;
    }

    private Object getObjectModel(String name, LongEnum langEnum){
        TemplateModel templateModel = map.get(name);
        if(Checker.BeNotNull(templateModel)){
            if(LongEnum.OBJECT.equals(langEnum)){
                if (templateModel instanceof BeanModel) {
                    return ((BeanModel) templateModel).getWrappedObject();
                }
            }
        }
        return null;
    }

    private Object getListModel(String name, LongEnum langEnum) throws TemplateModelException {
        TemplateModel templateModel = map.get(name);
        if(Checker.BeNotNull(templateModel)){
            if(LongEnum.LIST.equals(langEnum)){
                if (templateModel instanceof SimpleSequence) {
                    return ((SimpleSequence) templateModel).toList();
                }
            }
        }
       return null;
    }

    private TemplateSequenceModel getArrayModel(String name, LongEnum langEnum){
        TemplateModel templateModel = map.get(name);
        if(Checker.BeNotNull(templateModel)){
            if(LongEnum.isArray(langEnum)){
                return (SimpleSequence) templateModel;
            }
        }
        return null;
    }


    public Wrapper construct(Map<String, Object> params) throws IOException, TemplateException {
        if (!isRender) {
            Map<String, TemplateModel> reduceMap = buildModel(params);
            if (Checker.BeNotNull(templateDirectiveBody)) {
                templateDirectiveBody.render(environment.getOut());
            }
            if(Checker.BeNotNull(reduceMap)){
                Environment.Namespace namespace = environment.getCurrentNamespace();
                namespace.putAll(reduceMap);
            }
            isRender = true;
        }
        return this;
    }

    private Map<String, TemplateModel> buildModel(Map<String, Object> params) throws TemplateModelException {
        Map<String, TemplateModel> models = new LinkedHashMap<>();
        ObjectWrapper objectWrapper = environment.getObjectWrapper();
        Environment.Namespace namespace = environment.getCurrentNamespace();
        Iterator<Map.Entry<String, Object>> iterator = params.entrySet().iterator();
        for (int i = 0; iterator.hasNext(); i++) {
            Map.Entry<String, Object> entry = iterator.next();
            if (i < templateModels.length) {
                templateModels[i] = objectWrapper.wrap(entry.getValue());
            } else {
                String key = entry.getKey();
                models.put(key, namespace.get(key));
                namespace.put(key, objectWrapper.wrap(entry.getValue()));
            }
        }
        return models;
    }


    public DirectiveUnit getUnit() {
        return directiveUnit;
    }
}
