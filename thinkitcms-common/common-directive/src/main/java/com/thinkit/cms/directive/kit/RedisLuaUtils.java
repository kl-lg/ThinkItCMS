package com.thinkit.cms.directive.kit;

import cn.hutool.extra.spring.SpringUtil;
import cn.hutool.json.JSONUtil;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.thinkit.cms.annotation.IndexField;
import com.thinkit.cms.annotation.IndexId;
import com.thinkit.cms.constant.SysConst;
import com.thinkit.cms.directive.unit.RsUnit;
import com.thinkit.cms.enums.FieldType;
import com.thinkit.cms.model.ModelJson;
import com.thinkit.cms.utils.Checker;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import redis.clients.jedis.UnifiedJedis;
import redis.clients.jedis.json.Path2;
import redis.clients.jedis.search.*;

import java.lang.reflect.Field;
import java.util.*;
@Slf4j
public class RedisLuaUtils {

    private static StringRedisTemplate stringRedisTemplate = SpringUtil.getBean(StringRedisTemplate .class);

    private static UnifiedJedis unifiedJedis = SpringUtil.getBean(UnifiedJedis .class);

    // lua 脚本配置
    public static void setJson(String indexName,String jsonStr)
    {
        DefaultRedisScript<String> redisScript = new DefaultRedisScript<>();
        String scriptStr = "return redis.call('JSON.SET', KEYS[1], '$', ARGV[1]);";
        redisScript.setScriptText(scriptStr);
        redisScript.setResultType(String.class);
        stringRedisTemplate.execute(redisScript, Collections.singletonList(indexName), jsonStr);
    }


    public static void setJsons(List<ModelJson> modelJsons)
    {
        if(Checker.BeNotEmpty(modelJsons)){
            modelJsons.forEach(modelJson->{
                setJson(true,modelJson);
            });
        }
    }


    public static void setJsonProperty(String prefix,String indexKey, Map json)
    {
        if(Checker.BeNotEmpty(json) && json.containsKey(indexKey)){
            String key = json.get(indexKey).toString();
            key = prefix+key;
            setJson(key, json);
        }
    }

    public static void setJson(String indexName, Map json)
    {
        DefaultRedisScript<String> redisScript = new DefaultRedisScript<>();
        String scriptStr = "return redis.call('JSON.SET', KEYS[1], '$', ARGV[1]);";
        redisScript.setScriptText(scriptStr);
        redisScript.setResultType(String.class);
        stringRedisTemplate.execute(redisScript, Collections.singletonList(indexName), JSONUtil.toJsonPrettyStr(json));
    }

    public static void setJson(ModelJson modelJson,String... keys)
    {
        if(Checker.BeNotNull(modelJson) && Checker.BeNotEmpty(modelJson.getObjectMap())){
            DefaultRedisScript<String> redisScript = new DefaultRedisScript<>();
            String scriptStr = "return redis.call('JSON.SET', KEYS[1], '$', ARGV[1]);";
            redisScript.setScriptText(scriptStr);
            redisScript.setResultType(String.class);
            String key = Checker.BeNotBlank(modelJson.getKey())?modelJson.getKey():modelJson.buildKey(keys);
            stringRedisTemplate.execute(redisScript, Collections.singletonList(key),
            JSONUtil.toJsonPrettyStr(modelJson.getObjectMap()));
        }
    }

    public static void setJson(Boolean useJedis,ModelJson modelJson,String... keys)
    {
         if(useJedis){
             if(Checker.BeNotNull(modelJson) && Checker.BeNotEmpty(modelJson.getObjectMap())){
                 String key = Checker.BeNotBlank(modelJson.getKey())?modelJson.getKey():modelJson.buildKey(keys);
                 unifiedJedis.jsonSet(key, Path2.ROOT_PATH,JSONUtil.toJsonPrettyStr(modelJson.getObjectMap()));
             }
         }else{
             setJson(modelJson,keys);
         }
    }



    public static void setJson(Boolean useJedis,Boolean forceUpDb,ModelJson modelJson,String... keys)
    {
        if(useJedis){
            if(!forceUpDb){
                setJson(useJedis,modelJson,keys);
            }else{
                String key = Checker.BeNotBlank(modelJson.getKey())?modelJson.getKey():modelJson.buildKey(keys);
                Map map = getJson(key);
                if(Checker.BeNotEmpty(map) && Checker.BeNotEmpty(modelJson.getObjectMap())){
                    map.putAll(modelJson.getObjectMap());
                    modelJson.setObjectMap(map);
                    setJson(useJedis,modelJson,keys);
                }
            }
        }
    }




    public static void setJson(Boolean useJedis,String indexName, Map json)
    {
        if(useJedis){
            if(Checker.BeNotBlank(indexName) && Checker.BeNotEmpty(json)){
                unifiedJedis.jsonSet(indexName, Path2.ROOT_PATH,JSONUtil.toJsonPrettyStr(json));
            }
        }else{
            setJson(indexName,json);
        }
    }


    public static SearchResult ftSearch(String sql)
    {
        if(Checker.BeNotBlank(sql)){
            RsUnit rsUnit = JSONUtil.toBean(sql,RsUnit.class);
            String table = rsUnit.getTable();
            String queryString = rsUnit.getSql();
            if(Checker.BeNotBlank(queryString)){
                Query query = new Query(queryString);
                query.setLanguage(SysConst.chinese);
                if(Checker.BeNotEmpty(rsUnit.getLimit())){
                    Integer pageNum = (rsUnit.getLimit()[0] - 1) * rsUnit.getLimit()[1];
                    Integer pageSize = rsUnit.getLimit()[1];
                    query.limit(pageNum,pageSize);
                }
                if(Checker.BeNotEmpty(rsUnit.getFields())){
                    query.returnFields(rsUnit.getFields());
                }
                if(Checker.BeNotEmpty(rsUnit.getSort())){
                    for (String sortStr:rsUnit.getSort()){
                        String[] s = sortStr.split("\\s+");
                        query.setSortBy(s[0],"asc".equals(s[1]));
                    }
                }
                return unifiedJedis.ftSearch(table,query);
            }
        }
        return  SearchResult.SEARCH_RESULT_BUILDER.build(null);
    }

    public static SearchResult ftSearch(RsUnit rsUnit)
    {
        if(Checker.BeNotNull(rsUnit)){
            String table = rsUnit.getTable();
            String queryString = rsUnit.getSql();
            if(Checker.BeNotBlank(queryString)){
                Query query = new Query(queryString);
                query.setLanguage(SysConst.chinese);
                if(Checker.BeNotEmpty(rsUnit.getLimit())){
                    Integer pageNum = (rsUnit.getLimit()[0] - 1) * rsUnit.getLimit()[1];
                    Integer pageSize = rsUnit.getLimit()[1];
                    query.limit(pageNum,pageSize);
                }
                if(Checker.BeNotEmpty(rsUnit.getFields())){
                    query.returnFields(rsUnit.getFields());
                }
                if(Checker.BeNotEmpty(rsUnit.getSort())){
                    for (String sortStr:rsUnit.getSort()){
                        String[] s = sortStr.split("\\s+");
                        query.setSortBy(s[0],"asc".equals(s[1]));
                    }
                }
                return unifiedJedis.ftSearch(table,query);
            }
        }
        return  SearchResult.SEARCH_RESULT_BUILDER.build(null);
    }

    public static Map<String,Object> getJson(String indexName)
    {
        DefaultRedisScript<String> redisScript = new DefaultRedisScript<>();
        String scriptStr = "return redis.call('JSON.GET', KEYS[1]);";
        redisScript.setScriptText(scriptStr);
        redisScript.setResultType(String.class);
        String data = stringRedisTemplate.execute(redisScript, Collections.singletonList(indexName));
        if(Checker.BeNotBlank(data)){
            return JSONUtil.toBean(data,Map.class);
        }
        return Maps.newHashMap();
    }



    public static String createIndexAs(String indexName, List<String> indexs)
    {
       if(Checker.BeNotEmpty(indexs)){
           Map<String,String> mapIndex = new HashMap<>();
           for (String index:indexs){
               Map<String, Object> objectMap = unifiedJedis.ftInfo(index);
               if(Checker.BeNotEmpty(objectMap)){
                    if(objectMap.containsKey("attributes")){
                        List<ArrayList> list = (List) objectMap.get("attributes");
                        if(Checker.BeNotEmpty(list)){
                            for(ArrayList param:list){
                                mapIndex.put(param.get(3).toString(),param.get(5).toString());
                            }
                        }
                    }
               }
           }
           if(Checker.BeNotEmpty(mapIndex)){
               ModelJson modelJson = new ModelJson();
               modelJson.setPrefix(indexName);
               modelJson.setObjectMap(mapIndex);
               modelJson.setIndexName(indexName);
               modelJson.setPk("");
               createIndex(modelJson,Lists.newArrayList());
           }
       }
       return "OK";
    }

    public static String createIndex(ModelJson modelJson, List<Field> fields)
    {
        Map map = modelJson.getObjectMap();
        List<SchemaModel> schemaModels = new ArrayList<>();
        schema(fields, schemaModels);
        IndexDefinition indexDefinition = new IndexDefinition(IndexDefinition.Type.JSON);
        indexDefinition.setLanguage(SysConst.chinese);
        indexDefinition.setPrefixes(modelJson.buildKey(true,":"));
        IndexOptions indexOptions = IndexOptions.defaultOptions().setDefinition(indexDefinition);
        if(Checker.BeNotEmpty(map)){
            for(Object key:map.keySet()){
                 String keyString = key.toString();
                 Schema.FieldType fieldType =  Schema.FieldType.valueOf(map.get(key).toString());
                 schemaModels.add(new SchemaModel(keyString,fieldType.name()));
            }
        }
        Schema sc =  buildSchema(schemaModels);
        try {
            unifiedJedis.ftDropIndex(modelJson.buildKey());
        }catch (Exception e){
            log.error("执行错误：{}",e.getMessage());
        }
        String res =   unifiedJedis.ftCreate(modelJson.buildKey(),indexOptions,sc);
        log.info("创建索引:{},结果:{}",modelJson.buildKey(),res);
        return res;
    }

    public static Set<String> listIndex()
    {
          Set<String>  indexs =  unifiedJedis.ftList();
          return Checker.BeNotEmpty(indexs)?indexs: Sets.newHashSet();
    }

    private static Schema buildSchema(List<SchemaModel> schemaModels){
        Schema sc = new Schema();
        if(Checker.BeNotEmpty(schemaModels)){
            List<String> fieldNames = new ArrayList<>();
            for(SchemaModel schemaModel:schemaModels){
                Schema.FieldType fieldType =  Schema.FieldType.valueOf(schemaModel.getFieldType());
                Boolean isSort = schemaModel.getSortField();
                String fieldName = schemaModel.getFieldName();
                if(!fieldNames.contains(fieldName)){
                    fieldNames.add(fieldName);
                    String rootField = Path2.ROOT_PATH+SysConst.point+fieldName;
                    switch (fieldType){
                        case TEXT -> {
                            if(isSort){
                                sc.addSortableTextField(rootField, schemaModel.getWeight()).as(fieldName);
                            }else{
                                sc.addTextField(rootField, schemaModel.getWeight()).as(fieldName);
                            }
                        }case NUMERIC -> {
                            sc.addSortableNumericField(rootField).as(fieldName);
                        }case GEO -> {
                            sc.addGeoField(rootField).as(fieldName);
                        }case TAG -> {
                            if(isSort){
                                sc.addSortableTagField(rootField,SysConst.douhao,false).as(fieldName);
                            }else{
                                sc.addTagField(rootField,SysConst.douhao,false).as(fieldName);
                            }
                        }
                    }
                }
            }
        }
        return sc;
    }

    private static void schema(List<Field> fields,List<SchemaModel> schemaModels){
        if(Checker.BeNotEmpty(fields)){
             for(Field field:fields){
                 field.setAccessible(true);
                 IndexField indexField = field.getAnnotation(IndexField.class);
                 IndexId indexId =  field.getAnnotation(IndexId.class);
                 boolean indexIdOk = Checker.BeNotNull(indexId);
                 if(Checker.BeNotNull(indexField) && indexField.exist()){
                     FieldType fieldType = indexField.fieldType();
                     boolean isSort = indexField.sortField();
                     String fieldName = Checker.BeNotBlank(indexField.value())?indexField.value():field.getName();
                     schemaModels.add(new SchemaModel(fieldName,fieldType.name(),isSort,indexField.weight()));
                 }
                 if(indexIdOk){
                     String fieldName = Checker.BeNotBlank(indexId.value())?indexId.value():field.getName();
                     schemaModels.add(new SchemaModel(fieldName,FieldType.TAG.name(),true,indexId.weight()));
                 }
             }
        }
    }

    public static List<Map> getJsons(List<String> keys)
    {
        List<Map> maps = new ArrayList<>();
        if(Checker.BeNotEmpty(keys)){
            keys.forEach(key->{
                maps.add(getJson(key));
            });
            return maps;
        }
        return java.util.List.of(Maps.newHashMap());
    }

    public static Map getJson(boolean useJedis ,String key)
    {
        if(useJedis){
            return unifiedJedis.jsonGet(key,Map.class);
        }else{
            return getJson(key);
        }
    }

    public static List<Map> getJsons(Boolean useJedis,List<String> keys)
    {
       List<Map> maps = new ArrayList<>();
       if(useJedis){
           if(Checker.BeNotEmpty(keys)){
               keys.forEach(key->{
                   Map data =  unifiedJedis.jsonGet(key,Map.class);
                   maps.add(data);
               });
               return maps;
           }
       }else{
           return getJsons(keys);
       }
       return maps;
    }

    public static List<ModelJson> getJsons(List<String> keys,String prefix)
    {
        List<ModelJson> modelJsons = new ArrayList<>();
        if(Checker.BeNotEmpty(keys)){
            keys.forEach(key->{
                ModelJson modelJson = new ModelJson();
                modelJson.setObjectMap(getJson(key));
                modelJson.setPk(key);
                modelJson.setPrefix(prefix);
                modelJson.setKey(key);
                modelJsons.add(modelJson);
            });
            return modelJsons;
        }
        return Lists.newArrayList();
    }



    public static void delete(String... keys)
    {
        stringRedisTemplate.delete(Arrays.asList(keys));
    }

    public static void delete(List<String> keys)
    {
        stringRedisTemplate.delete(keys);
    }

    public static void delete(boolean useJedis,List<String> keys)
    {
        if(useJedis){
            unifiedJedis.del(keys.stream().toArray(String[]::new));
        }else{
            delete(keys);
        }
    }

    public static void setJsonBatch(String indexName, List<Map> jsons)
    {
        if(Checker.BeNotEmpty(jsons)){
            for(Map json:jsons){
                setJson(indexName,json);
            }
        }
    }
}
