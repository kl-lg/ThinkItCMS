package com.thinkit.cms.directive.render;

import lombok.Data;

@Data
public class DirectiveMeta {

    private String name;

}
