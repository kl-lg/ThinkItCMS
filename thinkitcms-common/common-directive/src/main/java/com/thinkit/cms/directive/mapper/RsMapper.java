package com.thinkit.cms.directive.mapper;

import com.thinkit.cms.directive.unit.db.DirectsModel;
import com.thinkit.cms.utils.Checker;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Component
public class RsMapper extends RsBaseQuery implements  BaseMapper {


    @Override
    public Map getOne(String sql) {
        List<Map> maps = super.getList(sql);
        if(Checker.BeNotEmpty(maps)){
            return maps.get(0);
        }
        return new HashMap(16);
    }


    @Override
    public List<Map> getList(String sql) {
           return super.getList(sql);
    }

    @Override
    public List<Long> getLongList(String sql) {
        return super.getLongList(sql);
    }

    @Override
    public List<Integer> getIntegerList(String sql) {
        return super.getIntegerList(sql);
    }

    @Override
    public List<String> getStringList(String sql) {
        return super.getList(sql,String.class);
    }

    @Override
    public Long getLong(String sql) {
        return super.getLong(sql);
    }

    @Override
    public Integer getInteger(String sql) {
        return super.getInteger(sql);
    }

    @Override
    public String getString(String sql) {
        return super.getString(sql);
    }

    @Override
    public Double getDouble(String sql) {
        return super.getDouble(sql);
    }

    @Override
    public Float getFloat(String sql) {
        return super.getFloat(sql);
    }

    @Override
    public Boolean getBoolean(String sql) {
        return super.getBoolean(sql);
    }

    @Override
    public Short getShort(String sql) {
        return super.getShort(sql);
    }

    @Override
    public Date getDate(String sql) {
        return super.getDate(sql);
    }

    @Override
    public void updatePath(String tableName, String pk, String fieldName, String val) {

    }

    @Override
    public List<DirectsModel> queryDirects() {
        return null;
    }
}
