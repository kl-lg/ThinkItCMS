package com.thinkit.cms.directive.mapper;

import com.thinkit.cms.directive.unit.db.DirectsModel;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Mapper
public interface BaseMapper {

    /**
     * get one
     * @param sql
     * @return
     */
    Map getOne(@Param("sql") String sql);


    /**
     * @param sql
     * @return
     */
    List<Map> getList(@Param("sql") String sql);

    /**
     * 获取Long list
     * @param sql
     * @return
     */
    List<Long> getLongList(@Param("sql") String sql);

    /**
     * 获取数组
     * @param sql
     * @return
     */
    List<Integer> getIntegerList(@Param("sql") String sql);

    /**
     * 获取数组
     * @param sql
     * @return
     */
    List<String> getStringList(@Param("sql") String sql);

    /**
     * 获取数组
     * @param sql
     * @return
     */
    Long getLong(@Param("sql") String sql);

    /**
     * 获取数组
     * @param sql
     * @return
     */
    Integer getInteger(@Param("sql") String sql);

    /**
     * 获取数组
     * @param sql
     * @return
     */
    String getString(@Param("sql") String sql);

    /**
     * 获取数组
     * @param sql
     * @return
     */
    Double getDouble(@Param("sql") String sql);

    /**
     * 获取数据
     * @param sql
     * @return
     */
    Float getFloat(@Param("sql") String sql);

    /**获取数据
     * @param sql
     * @return
     */
    Boolean getBoolean(@Param("sql") String sql);

    /**
     * 获取数据
     * @param sql
     * @return
     */
    Short getShort(@Param("sql") String sql);

    /**
     * 获取数据
     * @param sql
     * @return
     */
    Date getDate(@Param("sql") String sql);


    /**
     * 更新数据库字段值
     * @param tableName
     * @param pk
     * @param fieldName
     * @param val
     */
    void updatePath(@Param("tableName") String tableName, @Param("pk") String pk, @Param("fieldName") String fieldName, @Param("val") String val);


    /**
     * 查询指令列表
     * @return
     */
    List<DirectsModel> queryDirects();
}
