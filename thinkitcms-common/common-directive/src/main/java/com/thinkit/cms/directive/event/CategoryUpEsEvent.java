package com.thinkit.cms.directive.event;

import com.thinkit.cms.enums.EventEnum;
import lombok.Getter;
import lombok.Setter;
import org.springframework.context.ApplicationEvent;

@Getter
public class CategoryUpEsEvent extends ApplicationEvent {

    @Setter
    public EventEnum eventEnum;

    public CategoryUpEsEvent(Object source, EventEnum eventEnum) {
        super(source);
        this.eventEnum = eventEnum;
    }
}
