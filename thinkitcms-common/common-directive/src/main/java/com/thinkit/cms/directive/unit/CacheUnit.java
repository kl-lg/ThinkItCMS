package com.thinkit.cms.directive.unit;

import lombok.Data;

/**
 * @author LONG
 */
@Data
public class CacheUnit {

    /**
     * 缓存时间
     */
    private Long cacheTime;


    /**
     * 缓存时间单位
     */
    private String timeUnit;

    /**
     * 缓存名称
     */
    private String cacheName;

    /**
     * 缓存位置 本地缓存,redis 缓存
     */
    private String location;


    /**
     * 开启状态
     */
    private Boolean enable;


}
