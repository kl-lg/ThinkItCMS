package com.thinkit.cms.directive.task;

import lombok.Data;

import java.io.Serializable;

@Data
public class TaskDto implements Serializable {
    /**
     * 任务ID
     */
    private String taskId;

    /**
     * 任务动作
     */
    private String action;

    /**
     * 站点ID
     */
    private String siteId;

    /**
     * 任务状态
     */
    private String status;


    private String date;

    private String finshedDate;
}
