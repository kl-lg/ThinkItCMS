package com.thinkit.cms.directive.cache;
import com.thinkit.cms.utils.Checker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;


/**
 * @author LONG
 */
@Component
public class CacheManager {


    @Autowired
    private RedisTemplate redisTemplate;

    public void put(String key, Object obj, Long timeout){
        redisTemplate.opsForValue().set(key,obj,timeout,TimeUnit.SECONDS);
    }

    public Object get(String key){
        return redisTemplate.opsForValue().get(key);
    }

    public Boolean hasKey(String key){
        return redisTemplate.hasKey(key);
    }

    public Object getCacheData(String key){
        boolean hasKey = this.hasKey(key);
        if(hasKey){
            return this.get(key);
        }
        return null;
    }


}
