package com.thinkit.cms.directive.unit;

import com.thinkit.cms.directive.handler.DataHandler;
import lombok.Data;

/**
 * @author LONG
 */
@Data
public class ReturnUnit {

    /**
     * 返回类型 map obj list string integer
     */
    private String type;

    /**
     * handler 格式化配置
     */
    private String handler="default";

    /**
     * 结果处理器
     */
    private DataHandler dataHandler;
}
