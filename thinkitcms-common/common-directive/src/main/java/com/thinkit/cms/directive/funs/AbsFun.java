package com.thinkit.cms.directive.funs;

import com.thinkit.cms.utils.Checker;
import org.springframework.beans.factory.annotation.Autowired;
import redis.clients.jedis.UnifiedJedis;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public abstract class AbsFun {

    @Autowired
    private UnifiedJedis unifiedJedis;

    protected Map getJson(String key){
       Object jsonMap = unifiedJedis.jsonGet(key);
       if(Checker.BeNotNull(jsonMap)){
           return (Map) jsonMap;
       }
       return null;
    }

    protected  List<Map> getJsons(String pattenKey)
    {
        List<Map> maps = new ArrayList<>();
        Set<String> keys = unifiedJedis.keys(pattenKey);
        if(Checker.BeNotEmpty(keys)){
            keys.forEach(key->{
                Map map = getJson(key);
                if(Checker.BeNotEmpty(map)){
                    maps.add(map);
                }
            });
        }
        return maps;
    }
}
