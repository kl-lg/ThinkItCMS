package com.thinkit.cms.directive.kit;

import lombok.Data;

@Data
public class SchemaModel {

    private String fieldName;

    private String fieldType;

    private Boolean sortField = false;

    private Double weight=1.0;

    public SchemaModel(String fieldName, String fieldType, Boolean sortField, Double weight) {
        this.fieldName = fieldName;
        this.fieldType = fieldType;
        this.sortField = sortField;
        this.weight = weight;
    }

    public SchemaModel(String fieldName, String fieldType) {
        this.fieldName = fieldName;
        this.fieldType = fieldType;
    }


    public SchemaModel() {
    }
}
