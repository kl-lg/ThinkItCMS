package com.thinkit.cms.directive.emums;

import lombok.Getter;

public enum MethodEnum {

	FORMAT("时间格式化","format"),

	FORMAT_URL("时间格式化","formatUrl"),

	CLEAR_HTML("html格式化","clearHtml"),

	SUB("字符串截取","sub"),

	IMPORT("页面片段导入","import"),

	HELP("参数打印辅助文档","help"),

	REPLACE("字符串替换","replace"),

	TAG_SPLIT("字符串替换","tagSplit"),

	GLOBAL("全局变量","global");


	@Getter
	private String name;
	@Getter
	private String value;

	MethodEnum(String name, String value) {
		this.name = name;
		this.value = value;
	}
}
