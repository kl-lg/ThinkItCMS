package com.thinkit.cms.directive.task;

import cn.hutool.core.date.DateUtil;
import com.thinkit.cms.constant.SysConst;
import com.thinkit.cms.enums.TaskEnum;
import com.thinkit.cms.enums.TaskStatusEnum;
import com.thinkit.cms.utils.Checker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;


@Service
public class TaskServiceImpl implements TaskService {

    @Autowired
    private RedisTemplate redisTemplate;

    @Override
    public String genTaskKey(TaskEnum action, String siteId,String rowStr) {
        String key = SysConst.task+siteId+SysConst.colon+action.getVal();
        if(Checker.BeNotBlank(rowStr)){
            key+=SysConst.colon+rowStr;
        }
        return key;
    }

    @Override
    public Map<String, Object> query(TaskDto taskDto) {
        return null;
    }

    @Override
    public String startTask(String key,TaskDto taskDto) {
        String taskId = UUID.randomUUID().toString();
        String processId = SysConst.taskId+taskId;
        taskDto.setDate(DateUtil.format(new Date(),"yyyy-MM-dd HH:mm:ss"));
        taskDto.setTaskId(taskId);
        taskDto.setStatus(TaskStatusEnum.RUNING.getVal());
        redisTemplate.opsForValue().set(key,taskDto,30, TimeUnit.DAYS);

        redisTemplate.opsForHash().put(processId,SysConst.count,0);
        redisTemplate.opsForHash().put(processId,SysConst.okTimes,0);
        redisTemplate.opsForHash().put(processId,SysConst.failTimes,0);
        return taskId;
    }

    @Override
    public TaskDto getTask(String key) {
        Boolean hasKey =  redisTemplate.hasKey(key);
        if(hasKey){
            TaskDto taskDto = (TaskDto) redisTemplate.opsForValue().get(key);
            return Checker.BeNotNull(taskDto)?taskDto:null;
        }
        return null;
    }

    @Override
    public Boolean isRuning(String key) {
        Boolean hasKey =  redisTemplate.hasKey(key);
        if(hasKey){
            TaskDto taskDto = (TaskDto) redisTemplate.opsForValue().get(key);
            return Checker.BeNotNull(taskDto) && TaskStatusEnum.RUNING.getVal().equals(taskDto.getStatus());
        }
        return false;
    }

    @Override
    public void upStatus(String key, TaskStatusEnum taskStatusEnum) {
        Boolean hasKey =  redisTemplate.hasKey(key);
        if(hasKey){
            TaskDto taskDto = (TaskDto) redisTemplate.opsForValue().get(key);
            taskDto.setStatus(taskStatusEnum.getVal());
            if(taskStatusEnum.equals(TaskStatusEnum.FINSHED)){
                taskDto.setFinshedDate(DateUtil.format(new Date(),"yyyy-MM-dd HH:mm:ss"));
            }
            redisTemplate.opsForValue().set(key,taskDto);
        }
    }

    @Override
    public void upProgress(String taskId,String mapKey, BigDecimal progress) {
        if(Checker.BeNotBlank(taskId)){
            synchronized (this){
                String key = SysConst.taskId+taskId;
                String progressStr = progress.stripTrailingZeros().toPlainString();
                progress = new BigDecimal(progressStr);
                redisTemplate.opsForHash().put(key,mapKey,progress.floatValue());
            }
        }
    }

    @Override
    public void upProgress(String taskId, String mapKey, Integer delta) {
        if(Checker.BeNotBlank(taskId)){
            String key = SysConst.taskId+taskId;
            redisTemplate.opsForHash().increment(key,mapKey,delta);
        }
    }

    @Override
    public Double taskProgress(String taskId,String mapKey,TaskEnum taskEnum) {
        String key = SysConst.taskId+taskId;
        if(redisTemplate.hasKey(key)){
            if(Checker.BeNotBlank(mapKey)){
                Integer progress = (Integer) redisTemplate.opsForHash().get(key,mapKey);
                return Double.valueOf(String.format("%.2f",progress));
            }else{
                Integer count = (Integer)redisTemplate.opsForHash().get(key,SysConst.count);
                count = Checker.BeNull(count)?0:count;
                Integer ok =  (Integer)redisTemplate.opsForHash().get(key,SysConst.okTimes);
                ok = Checker.BeNull(ok)?0:ok;
                Integer fail = (Integer) redisTemplate.opsForHash().get(key,SysConst.failTimes);
                fail = Checker.BeNull(fail)?0:fail;
                count = count.equals(0)?1:count;
                BigDecimal resultBigDecimal = new BigDecimal(ok+fail).divide(new BigDecimal(count), 2, RoundingMode.HALF_UP);
                return Double.valueOf(resultBigDecimal.toString());
            }
        }
        return Double.valueOf("0");
    }

    @Override
    public void clear(String key) {
        Boolean hasKey = redisTemplate.hasKey(key);
        if(hasKey){
            TaskDto taskDto = (TaskDto) redisTemplate.opsForValue().get(key);
            String taskId = taskDto.getTaskId();
            String taskIdKey = SysConst.taskId+taskId;
            redisTemplate.delete(taskIdKey);
            redisTemplate.delete(key);
            redisTemplate.opsForValue().set(SysConst.taskStop+taskId,DateUtil.format(new Date(),"yyyy-MM-dd HH:mm:ss"),10,TimeUnit.DAYS);
        }
    }

    @Override
    public boolean isStop(String takeId) {
        if(Checker.BeNotBlank(takeId)){
            return redisTemplate.hasKey(SysConst.taskStop+takeId);
        }
        return false;
    }

    @Override
    public boolean isUpdated(String key) {
        return redisTemplate.hasKey(key);
    }

    @Override
    public void setKey(String key,  String val ,long time) {
        redisTemplate.opsForValue().set(key,val,time,TimeUnit.SECONDS);
    }
}
