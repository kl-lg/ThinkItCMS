package com.thinkit.cms.directive.render;

import com.thinkit.cms.directive.emums.LongEnum;
import com.thinkit.cms.directive.unit.DirectiveUnit;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 *模板渲染 基类 用于包装 模板指令参数封装等
 * @author LONG
 */
public class RenderWrapper implements BaserRender{

     private Wrapper wrapper;

     protected Map<String, Object> map = new LinkedHashMap<>();

     public RenderWrapper(Wrapper wrapper){
          this.wrapper = wrapper;
     }


     @Override
     public void render() throws Exception {
          wrapper.construct(map);
     }

     @Override
     public String getString(String name) throws Exception {
          return wrapper.getParam(name, LongEnum.STRING);
     }

     @Override
     public String getString(String name, String defaultVal) throws Exception {
          return wrapper.getParam(name,defaultVal, LongEnum.STRING);
     }

     @Override
     public Integer getInteger(String name) throws Exception {
          return wrapper.getParam(name, LongEnum.INTEGER);
     }

     @Override
     public Integer getInteger(String name, Integer defaultVal) throws Exception {
          return wrapper.getParam(name,defaultVal, LongEnum.INTEGER);
     }

     @Override
     public Long getLong(String name) throws Exception {
          return wrapper.getParam(name, LongEnum.LONG);
     }

     @Override
     public Long getLong(String name, Long defaultVal) throws Exception {
          return wrapper.getParam(name,defaultVal, LongEnum.LONG);
     }

     @Override
     public Short getShort(String name) throws Exception {
          return wrapper.getParam(name, LongEnum.SHORT);
     }

     @Override
     public Short getShort(String name, Short defaultVal) throws Exception {
          return wrapper.getParam(name,defaultVal, LongEnum.SHORT);
     }

     @Override
     public Double getDouble(String name) throws Exception {
          return wrapper.getParam(name, LongEnum.DOUBLE);
     }

     @Override
     public Double getDouble(String name, Double defaultVal) throws Exception {
          return wrapper.getParam(name,defaultVal, LongEnum.DOUBLE);
     }

     @Override
     public String[] getStringArray(String name) throws Exception {
          return wrapper.getParam(name, LongEnum.STRING_ARRAY);
     }

     @Override
     public Long[] getLongArray(String name) throws Exception {
          return wrapper.getParam(name, LongEnum.LONG_ARRAY);
     }

     @Override
     public Integer[] getIntegerArray(String name) throws Exception {
          return wrapper.getParam(name, LongEnum.INTEGER_ARRAY);
     }

     @Override
     public Float getFloat(String name) throws Exception {
          return wrapper.getParam(name, LongEnum.FLOAT);
     }

     @Override
     public Float getFloat(String name, Float defaultVal) throws Exception {
          return wrapper.getParam(name,defaultVal, LongEnum.FLOAT);
     }

     @Override
     public Boolean getBoolean(String name) throws Exception {
          return wrapper.getParam(name, LongEnum.BOOLEAN);
     }

     @Override
     public Boolean getBoolean(String name, Boolean defaultVal) throws Exception {
          return wrapper.getParam(name,defaultVal, LongEnum.BOOLEAN);
     }

     @Override
     public Object getData(String name, Object defaultVal,LongEnum longEnum) {
          try {
               return wrapper.getParam(name,defaultVal, longEnum);
          } catch (TemplateModelException e) {
               throw new RuntimeException(e);
          }
     }

     @Override
     public <T> T getBean(String name, Class<T> clz) throws Exception {
          return wrapper.getParam(name, LongEnum.BEAN);
     }

     @Override
     public TemplateModel getMap(String name) throws Exception {
          return wrapper.getParam(name, LongEnum.MAP);
     }

     @Override
     public BaserRender putAll(Map maps) {
          map.putAll(maps);
          return this;
     }

     @Override
     public BaserRender put(String key, Object value) {
          map.put(key,value);
          return this;
     }

     @Override
     public DirectiveUnit getUnit() {
          return wrapper.getUnit();
     }
}