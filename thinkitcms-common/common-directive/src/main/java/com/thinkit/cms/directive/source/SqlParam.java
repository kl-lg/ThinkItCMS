package com.thinkit.cms.directive.source;

import com.thinkit.cms.directive.emums.LongEnum;
import lombok.Data;
/**
 * @author LONG
 */
@Data
public class SqlParam {

    private LongEnum type;

    private Object value;

    private String expression;

    private String formatExpress;

    private String[] property;
}
