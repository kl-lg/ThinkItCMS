package com.thinkit.cms.directive.handler;
import com.thinkit.cms.directive.annotation.Handler;
import com.thinkit.cms.directive.emums.LongEnum;
import com.thinkit.cms.directive.render.BaserRender;
import com.thinkit.cms.directive.render.ConvertExecutor;
import com.thinkit.cms.directive.unit.ParamUnit;
import com.thinkit.cms.utils.Checker;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author LONG
 */
@Slf4j
@Handler("default")
public class DefaultDataHandler extends AbstractDataHandler{


    @Override
    public Object getData(Object value,BaserRender render) {
        List<ParamUnit> outs = render.getUnit().getConfig().getParams().getOut();
        if(Checker.BeNotEmpty(outs)){
            for(ParamUnit out:outs){
               return handlerValue(value,out);
            }
        }
        return value;
    }

    private Object handlerValue(Object value,ParamUnit out){
        if(isMap(value)){
            return getMapData(value,out);
        }else if(isList(value)){
            List values = toList(value);
            List<Object> list = new ArrayList<>();
            for(Object obj:values){
                if(isMap(obj)){
                     Map map = getMapData(obj,out);
                     list.add(map);
                }else{
                     list.add(obj);
                }
            }
            return list;
        }
        return value;
    }

    private Map getMapData(Object value,ParamUnit out){
        if(isMap(value)){
            Map mapValue = toMap(value);
            return formatMap(mapValue,out);
        }
        log.info("不是Map类型不进行格式化 输出为 null");
        return null;
    }

    private Map formatMap(Map value,ParamUnit out){
        if(value.containsKey(out.getName())){
            Object val = value.get(out.getName());
            if(Checker.BeNotBlank(out.getAlias())){
                value.put(out.getAlias(),val);
                value.remove(out.getName());
                out.setName(out.getAlias());
            }
            if(Checker.BeNotBlank(out.getType())){
                LongEnum longEnum =LongEnum.getEnum(out.getType());
                String name = out.getName();
                if(Checker.BeNotNull(longEnum)){
                    switch (longEnum){
                        case LIST -> ConvertExecutor.convertList(val,name,value);
                        case LONG -> ConvertExecutor.convertLong(val,name,value);
                        case INTEGER -> ConvertExecutor.convertInteger(val,name,value);
                        case MAP -> ConvertExecutor.convertMap(val,name,value);
                        case STRING_ARRAY -> ConvertExecutor.convertStringArray(val,name,value);
                    }
                }
            }
        }
        return value;
    }
}
