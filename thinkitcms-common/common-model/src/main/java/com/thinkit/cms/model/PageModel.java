package com.thinkit.cms.model;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.google.common.collect.Lists;
import com.thinkit.cms.utils.Checker;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * @ClassName: PageDto
 * @Author: LG
 * @Date: 2019/5/15 13:25
 * @Version: 1.0
 **/
@Data
@Accessors(chain = true)
public class PageModel<D extends BaseDto> implements Serializable {

    private static final long serialVersionUID = 1L;

    private D dto;

    private long total;

    private List<D> rows;

    private long current=1;

    private long pageCount;

    private long pageSize=10;

    private boolean hasNext=true;

    public PageModel() {

    }

    public PageModel(long total, List<D> rows) {
        this.total = total;
        this.rows = rows;
    }

    public PageModel(long total, long pageSize, List<D> rows) {
        this.total = total;
        this.rows = rows;
        this.pageCount = (total + pageSize - 1)/pageSize;
    }

    public PageModel(long total, long pageSize, int current , List<D> rows) {
        this.total = total;
        this.rows = rows;
        this.current  = current ;
        this.pageCount = (total + pageSize - 1)/pageSize;
    }

    public PageModel(long total, long pageCount, long  current , List<D> rows) {
        this.total = total;
        this.pageCount = pageCount;
        this.current  = current ;
        this.rows = rows;
    }

    public PageModel(long total, long pageCount, long  current) {
        this.total = total;
        this.pageCount = pageCount;
        this.current  = current ;
        this.rows = Lists.newArrayList();
    }

    public PageModel(IPage<D> result) {
        this.total = result.getTotal();
        this.pageCount = result.getPages();
        this.current  = result.getCurrent() ;
        this.rows = Checker.BeNotEmpty(result.getRecords())?result.getRecords(): Lists.newArrayList();
    }

    public PageModel(long total, List<D> rows, boolean hasNext) {
        this.total = total;
        this.rows = rows;
        this.hasNext=hasNext;
    }
}
