package com.thinkit.cms.model;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * @ClassName: PageDto
 * @Author: LG
 * @Date: 2019/5/15 13:25
 * @Version: 1.0
 **/
@Data
@Accessors(chain = true)
public class PageTemp extends PageModel{

    private String url;

    private String murl;


    private String baseUrl;

    private String baseMUrl;

    public PageTemp() {

    }

    public PageTemp(long total, long pageCount,long current, long pageSize) {
        super(total, pageCount, current);
        super.setPageSize(pageSize);
    }
}
