package com.thinkit.cms.enums;

import lombok.Getter;

/**
 * @author LONG
 */

public enum StatusEnum {

    DEFAULT(0, "草稿"),
    PUBLISH(1, "已发布"),
    DELETED(2, "已删除");
    @Getter
    private String name;

    @Getter
    private Integer val;


    StatusEnum(Integer val, String name) {
        this.val = val;
        this.name = name;
    }
}
