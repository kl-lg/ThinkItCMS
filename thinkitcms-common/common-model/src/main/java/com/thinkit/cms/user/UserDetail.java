package com.thinkit.cms.user;

/**
 * @author LONG
 */
public interface UserDetail {
    /**
     * 判断是否锁定
     * @param status
     * @return
     */
    boolean lock(String status);


    /**
     * 校验是否密码通过认证
     * @param password
     * @return
     */
    boolean passCk(String password);


    /**
     * 获取用户唯一ID
     * @return
     */
    String getId();

    /**
     * 设置 clientId
     * @param clientId
     */
    void setClientId(String clientId);


    /**
     * @return
     * 获取 clientId
     */
    String getClientId();
    /**
     * 设置上下文
     */
    void setUserCtx(UserCtx userCtx);

    /**
     *
     * @return
     */
    boolean isSuperAdmin();

    /**
     *
     * @return
     */
    boolean isSuperRole();


    Integer roles();
}
