package com.thinkit.cms.user;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

/**
 * @author LONG
 */
@Slf4j
public class SysUserDetail extends User {

    @Getter @Setter
    private Integer roles=0;

    @Getter @Setter
    private String[] roleSign;

    @Override
    public boolean passCk(String password) {
        log.info("++++++++++++++++++++++++++++++密码校验开始+++++++++++++++++++++++++");
        if(StringUtils.isBlank(password)){
            return false;
        }
        String rightPw = getPassword();
        if(rightPw==null || rightPw.length()==0){
            return false;
        }
        return rightPw.equals(password);
    }

    @Override
    public Integer roles() {
        return roles;
    }

}
