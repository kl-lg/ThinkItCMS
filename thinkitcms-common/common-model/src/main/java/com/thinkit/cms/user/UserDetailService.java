package com.thinkit.cms.user;


import java.util.List;

/**
 * @author LONG
 */
public interface UserDetailService  {
    /**
     * 根据用户名称加载用户数据
     *  @param userName 账号
     * @return
     */
    UserDetail loadUserName(String userName);


    /**
     * 获取权限集合
     * @param loginId
     * @param loginType
     * @return
     */
    List<String> getPermissionList(String loginId, String loginType);


    /**
     * 获取角色集合
     * @param loginId
     * @param loginType
     * @return
     */
    List<String> getRoleList(Object loginId, String loginType);

    /**
     * 校验验证码是否合法
     * @param verifyUid
     * @param verifyCode
     * @return
     */
    boolean checkVerifyCode(String verifyUid, String verifyCode);


    /**
     * 销毁验证码
     * @param verifyUid
     * @param verifyCode
     */
    void destroyVerifyCode(String verifyUid, String verifyCode);
}
