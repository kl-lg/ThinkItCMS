package com.thinkit.cms.enums;

import lombok.Getter;

import java.util.Arrays;

/**
 * @author LONG
 */

@Getter
public enum ModelFieldType {
    input("input","TEXT",false),
    textarea("textarea","TEXT",false),
    radio("radio","TEXT",false),
    inputNumber("inputNumber","NUMERIC",true),
    uploadImgOne("uploadImgOne","TEXT",false),
    edit("edit","TEXT",false),
    datePicker("datePicker","TEXT",true),
    dateTime("dateTime","TEXT",true),
    dateRange("dateRange","TEXT",false),
    select("select","TEXT",false),
    selectMultiple("selectMultiple","TAG",false),
    checkbox("checkbox","TEXT",false),
    uploadFileOne("uploadFileOne","TEXT",false),
    uploadFileMultiple("uploadFileMultiple","TEXT",false),
    uploadImgMultiple("uploadImgMultiple","TEXT",false),
    slider("slider","NUMERIC",true),
    aiEdit("aiEdit","TEXT",false);
    /**
     * 值
     */
    private String val;


    /**
     * 名称
     */
    private String type;


    private Boolean sortTable;


    ModelFieldType(String val, String type,Boolean sortTable) {
        this.val = val;
        this.type = type;
        this.sortTable = sortTable;
    }

    public static ModelFieldType getType(String val) {
        return Arrays.stream(ModelFieldType.values())
                .filter(v -> v.getVal().equals(val))
                .findFirst()
                .orElse(ModelFieldType.input);
    }
}
