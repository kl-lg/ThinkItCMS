package com.thinkit.cms.model;

import com.thinkit.cms.utils.Checker;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ClassPathResource;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Kv (Key Value)
 * 引用来自 jfinal https://gitee.com/jfinal/enjoy/tree/master
 * Example：
 *    Kv para = Kv.by("id", 123);
 *    User user = user.findFirst(getSqlPara("find", para));
 */
@Slf4j
public class CmsResult extends HashMap {

    public static final int okCode=0;

    public static final int failCode=-1;

    public static final String code="code";

    public static final String message="msg";

    public static final String result="data";

    private static final long serialVersionUID = 1L;

    public static final Properties zhCnProperties = new Properties();

    public static final Properties enUsProperties = new Properties();

    static {
        ClassPathResource classPathResource = new ClassPathResource("language/zh-cn.properties");
        try {
            InputStream inputStream = classPathResource.getInputStream();
            BufferedReader bf = new BufferedReader(new InputStreamReader(inputStream));
            zhCnProperties.load(bf);
        } catch (IOException e) {
            log.error("读取language-zn 配置文件异常!{}",e.getMessage());
            e.printStackTrace();
        }
    }

    static {
        ClassPathResource classPathResource = new ClassPathResource("language/en-us.properties");
        try {
            InputStream inputStream = classPathResource.getInputStream();
            BufferedReader bf = new BufferedReader(new InputStreamReader(inputStream));
            enUsProperties.load(bf);
        } catch (IOException e) {
            log.error("读取language-en 配置文件异常!{}",e.getMessage());
            e.printStackTrace();
        }
    }

    public static String getLanguageMsg(String message){
        if(message.startsWith("#{") && message.endsWith("}")){
            String code = message.replace("#{","").replace("}","");
            message = get(Integer.valueOf(code));
        }
        return message;
    }


    public  String getMsg() {
        return getStr(message);
    }

    public int getCode() {
        return (Integer) this.get(code);
    }

    public boolean success() {
        Integer res=  getInt(code);
        return Checker.BeNotNull(res) && res == okCode;
    }


    public static CmsResult result(Object obj, int busCode) {
        return CmsResult.create().set(result, obj).set(code, busCode).set(message, get(busCode));
    }

    public static CmsResult result(Object obj) {
        return CmsResult.create().set(result, obj).set(code, okCode).set(message, get(okCode));
    }

    public static CmsResult result(int busCode) {
        return CmsResult.create().set(code, busCode).set(message, get(busCode));
    }


    public static CmsResult result(int busCode, String msg) {
        return CmsResult.create().set(code, busCode).set(message, msg);
    }

    public static CmsResult result(int busCode, Map data) {
        return CmsResult.create().set(code, busCode).set(message, get(busCode)).set(data);
    }

    public static CmsResult result() {
        return CmsResult.create().set(code, okCode).set(message, get(okCode));
    }

    /**
     * 通过返回码获取返回信息
     * @param errCode 错误码
     * @return {String}
     */
    public static String get(Integer errCode){
        String result = zhCnProperties.getProperty(String.valueOf(errCode));
        return result != null ? result : zhCnProperties.getProperty("unknown") + errCode;
    }

    public CmsResult() {

    }

    private static CmsResult create() {
        return new CmsResult();
    }

    public CmsResult set(Object key, Object value) {
        if(value instanceof Map){
            Map vs = (Map) value;
            //super.putAll(vs);
            super.put(result, vs);
        }else{
            super.put(key, value);
        }
        return this;
    }


    public CmsResult set(Map map) {
        super.putAll(map);
        return this;
    }

    public CmsResult set(CmsResult cmsResult) {
        super.putAll(cmsResult);
        return this;
    }

    public CmsResult delete(Object key) {
        super.remove(key);
        return this;
    }

    public <T> T getAs(Object key) {
        Object object = get(key);
        return object==null?null:(T) object;
    }

    public String getStr(Object key) {
        Object s = get(key);
        return s != null ? s.toString() : null;
    }

    public Integer getInt(Object key) {
        Number n = (Number)get(key);
        return n != null ? n.intValue() : null;
    }
}
