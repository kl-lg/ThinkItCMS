package com.thinkit.cms.model;
import com.google.common.base.CaseFormat;
import lombok.Data;

/**
 * @author LONG
 */
@Data
public class AuthUnity {

    private String key;
    private Object value;
    private String model;

    public String getKey() {
        if(!key.contains("_")){
            return CaseFormat.LOWER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, key);
        }
        return key;
    }

    public static void main(String[] args) {
        String a = CaseFormat.LOWER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, "testData");
        System.out.println(a);
        System.out.println(CaseFormat.LOWER_UNDERSCORE.to(CaseFormat.LOWER_CAMEL,a));
    }
}
