package com.thinkit.cms.utils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ClassPathResource;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Properties;

/**
 * @author LONG
 */
@Slf4j
public class CmsConf {

    public static final Properties confProperties = new Properties();

    static {
        ClassPathResource classPathResource = new ClassPathResource("config/config.properties");
        try {
            InputStream inputStream = classPathResource.getInputStream();
            BufferedReader bf = new BufferedReader(new InputStreamReader(inputStream));
            confProperties.load(bf);
        } catch (IOException e) {
            log.error("读取 config 配置文件异常!{}",e.getMessage());
            e.printStackTrace();
        }
    }

    public static String get(String key){
        return confProperties.getProperty(key);
    }
    public static Boolean getAsBoolean(String key){
         String val = get(key);
         if(Checker.BeNotBlank(val)){
             return Boolean.valueOf(val);
         }
         return Boolean.FALSE;
    }
}
