package com.thinkit.cms.enums;

import java.util.Arrays;

public enum  FieldType {
    /**
     * none Required inside the framework, do not use 框架内部需要,切勿使用,若不慎使用则会被当做keyword_text类型
     */
    NONE("none"),
    TEXT("text"),
    TAG("tag"),
    GEO("geo"),
    NUMERIC("numeric"),
    VECTOR("vector");

    private String type;

    FieldType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    /**
     * 根据类型字符串获取对应枚举
     *
     * @param type 类型字符串
     * @return 对应枚举
     */
    public static FieldType getByType(String type) {
        return Arrays.stream(FieldType.values())
                .filter(v -> v.getType().equals(type))
                .findFirst()
                .orElse(FieldType.TEXT);
    }
}
