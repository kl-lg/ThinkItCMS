package com.thinkit.cms.enums;

import lombok.Getter;

/**
 * @author LONG
 */

public enum EventEnum {

    SAVE_CONTENT("SAVE_CONTENT", "保存文章"),
    SAVE_BATCH_CONTENT("SAVE_BATCH_CONTENT", "批量保存文章"),
    UPDATE_CONTENT("UPDATE_CONTENT", "更新文章"),

    UPDATE_CONTENT_FIELD("UPDATE_CONTENT_FIELD", "更新内容字段"),
    UPDATE_TAG("UPDATE_TAG", "更新文章标签"),

    UPDATE_BATCH_STATUS("UPDATE_BATCH_STATUS", "批量更新文章状态"),
    DELETE_CONTENT("DELETE_CONTENT", "删除文章"),
    DELETE_BATCH_CONTENT("DELETE_BATCH_CONTENT", "批量删除文章"),

    SAVE_RELATED("SAVE_RELATED","保持推荐关系"),

    SAVE_BATCH_RELATED("SAVE_BATCH_RELATED","批量保持推荐关系"),

    DELETE_RELATED("DELETE_RELATED","删除推荐关系"),
    SAVE_CATEGORY("SAVE_CATEGORY", "保存栏目"),
    SAVE_BATCH_CATEGORY("SAVE_BATCH_CATEGORY","批量保持栏目"),

    UPDATE_CATEGORY_FIELD("UPDATE_CATEGORY_FIELD", "更新栏目字段"),
    UPDATE_CATEGORY("UPDATE_CATEGORY", "更新栏目"),
    SAVE_FORM_DATA("SAVE_FORM_DATA", "保存表单扩展数据"),

    SAVE_BATCH_FORM_DATA("SAVE_BATCH_FORM_DATA", "批量保存表单扩展数据"),
    UPDATE_FORM_DATA("SAVE_FORM_DATA", "更新表单扩展数据"),

    DELETE_FORM_DATA("DELETE_FORM_DATA", "山粗表单扩展数据"),
    SAVE_FORM_MODEL("SAVE_FORM_MODEL", "保存模型数据"),

    SAVE_BATCH_FORM_MODEL("SAVE_BATCH_FORM_MODEL", "批量保存模型数据"),
    UPDATE_FORM_MODEL("UPDATE_FORM_MODEL", "更新模型数据"),
    DELETE_FORM_MODEL("DELETE_FORM_MODEL","删除模型数据" ),

    TOP_CONTENT("TOP_CONTENT","置顶内容" ),
    RECOMD_CONTENT("RECOMD_CONTENT","推荐内容" ),
    HEAD_CONTENT("HEAD_CONTENT","设置头条" ),
    SAVE_SITE("SAVE_SITE", "创建站点"),
    DELETE_SITE("DELETE_SITE", "删除站点"),
    UPDATE_SITE("UPDATE_SITE", "更新站点")

    ;


    @Getter
    private String name;

    @Getter
    private String val;


    EventEnum(String val, String name) {
        this.val = val;
        this.name = name;
    }
}
