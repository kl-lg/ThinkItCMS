package com.thinkit.cms.utils;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class MenuMeta implements Serializable {

    private List<String> roles;
    private Boolean requiresAuth =true;
    private String icon;
    private String locale;
    private Boolean hideInMenu =false;
    private Boolean hideChildrenInMenu =false;
    private String activeMenu;
    private Integer order;
    private Boolean noAffix =false;
    private Boolean ignoreCache = false;

}
