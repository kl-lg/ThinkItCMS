package com.thinkit.cms.user;
import lombok.Data;

import java.io.Serializable;

/**
 * @author LONG
 */
@Data
public class UserCtx implements Serializable {

    /**
     * 部门ID
     */
    private String deptId;

    /**
     * 性别
     */
    private Integer sex;

    /**
     * 等级
     */
    private String level;

    /**
     * 角色标记
     */
    private String roleSign;

    /**
     * 是否是超级管理员
     */
    private Boolean isSuperAdmin;


    /**
     * 是否是超级角色
     */
    private Boolean  isSuperRole;
}
