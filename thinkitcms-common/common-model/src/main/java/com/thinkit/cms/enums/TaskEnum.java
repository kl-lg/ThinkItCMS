package com.thinkit.cms.enums;

import lombok.Getter;

/**
 * @author LONG
 */

public enum TaskEnum {

    INDEX_TASK("INDEX_TASK", "首页生成任务",10),
    ARTICLE_TASK("ARTICLE_TASK", "文章生成任务",10),
    CATEGORY_TASK("CATEGORY_TASK", "栏目生成任务",10),

    CATEGORY_TASK_BATCH("CATEGORY_TASK_BATCH", "批量栏目生成任务",10),
    ES_TASK("ES_TASK", "ES同步任务",20),
    ALL_TASK("ALL_TASK", "全站生成任务",100);

    @Getter
    private String name;

    @Getter
    private String val;

    @Getter
    private Integer level;


    TaskEnum(String val, String name,Integer level) {
        this.val = val;
        this.name = name;
        this.level = level;
    }
}
