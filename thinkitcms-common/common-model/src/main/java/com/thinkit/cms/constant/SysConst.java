package com.thinkit.cms.constant;

/**
 * @author LONG
 */
public class SysConst {

    public static final String none = "none";
    public static String defaultHandler="defaultHandler";
    public static String baseHandler="baseHandler";
    public static String defaultTmpErrMsg="defaultTmpErrMsg";
    public static String errKeep ="errKeep";
    public static String defaultTmpGlobalKey="defaultTmpGlobalKey";

    public static String data="data";
    public static String token="token";
    public static String allowLoginNoRole="allowLoginNoRole";
    public static String cmsFolder="cmsFolder";
    public static String templates="templates";

    public static String templateId="templateId";
    public static String cmsTemplate="cmsTemplate";

    public static String html=".html";

    public static String index="index";

    public static String home="index.html";

    public static String siteDefConf="siteDefConf:";

    public static String prefix="/";

    public static String blank_="_";

    public static String point=".";

    public static String douhao=",";



    public static String pprefix="//";

    public static String brefix="\\";

    public static String blank="";

    public static String blankReplace="\\s*";


    public static String starChar="*";

    public static String likeChar="%";
    public static String cmsSites="cmsSites";

    public static String sites="sites";

    public static String defSites="defSites:";

    public static String defSiteConf="defSites:conf:";

    public static String genPageRoot="genPageRoot";

    public static String clientName="ossClient";


    public static String enableSSL="enableSSL";

    public static String url="url";

    public static String murl="murl";

    public static String m_url="m_url";

    public static String https="https://";

    public static String http="http://";

    public static String defCache="directive:cache:";

    public static String colon=":";

    public static String jobCategory="jobs:category:";
    public static String fragment="/fragment";
    public static String fragmentName="fragment";
    public static String categoryId ="categoryId";
    public static String category ="category";
    public static String content ="content";
    public static String contentRelated ="contentrelated";

    public static String startEndTime="startEndTime";
    public static String page="page";
    public static String enableES="enableES";
    public static String siteId="siteId";
    public static String site="site";
    public static String domain="domain";
    public static String domainRoot="domainRoot";
    public static String domainMRoot="domainMRoot";
    public static String zip=".zip";
    public static String zipFile="zip";
    public static String expiredKey="expiredKey::";
    public static String expiredContent="expiredKey:content:";

    public static String expKey=":expKey";
    public static String task="TASK:";

    public static String taskd="TASK:FD:";

    public static String taskStop="TASK:STOP:";

    public static String taskId=task+"IDS:";

    public static String status=":status:";
    public static String progress="progress";
    public static String es="es:";
    public static String count="count";
    public static String okTimes="ok";
    public static String failTimes="fail";
    public static String clickTotals="total:";
    public static String clickView=clickTotals+"view:";
    public static String clickTop=clickTotals+"top:";

    public static String clickIp=clickTotals+"ips:";
    public static String utf8="UTF-8";
    public static String authCache="cached::class com.thinkit.cms.boot.service.sysuser.SysUserServiceImpl.queryAuthSign.*";
    public static String bucketName="bucketName";
    public static String endpoint="endpoint";
    public static String accessKeySecret="accessKeySecret";
    public static String accessKeyId="accessKeyId";
    public static String server="server";
    public static String self="self";
    public static String localhost="127.0.0.1";
    public static String privateKey="privateKey";

    public static String pk="pk";
    public static String publicKey="publicKey";
    public static String signaturer="signaturer";
    public static String license="license.dat";
    public static String dat=".dat";
    public static String effectDate="effectDate";
    public static String deadline="deadline";
    public static String handPackage="com.thinkit.cms.boot.directive.handler";

    public static String instructPackage="com.thinkit.cms.boot.funs.instruct";

    public static String authorization="Authorization";

    public static String verifyUid="verifyUid";
    public static String verifyCode="verifyCode";
    public static Object verifyScope="verifyScope:";
    public static String cmsUpload="cmsUpload";


    public final static String sitePrefix ="site:";

    public final static String contentPrefix ="content:";

    public final static String formModePrefix ="formmode:";

    public final static String formDataPrefix ="formdata:";

    public final static String formData ="formdata";

    public final static String categoryPrefix ="category:";

    public final static String relatePrefix ="relate:";

    public static String chinese="chinese";

    public final static String modelCode ="modelCode";

    public final static String table ="table";

    public static String contentId="contentId";
    public static String code="code";
    public static String pageNum="pageNum";
    public static String pageSize="pageSize";

    public static String fileFlat="flat::";

    public static String fileFlatAli="flat::ali::";

    public static String directoryFlat="flat::directory::";
}
