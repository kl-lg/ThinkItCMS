package com.thinkit.cms.model;

import lombok.Data;

/**
 * @author LONG
 */
@Data
public class ModelTempParma {


    public ModelTempParma(){


    }

    public ModelTempParma(String siteId,String templateId, String formModelId) {
        this.siteId = siteId;
        this.templateId = templateId;
        this.formModelId = formModelId;
    }

    private String templateId;

    private String formModelId;

    private String rowId;

    private String siteId;

    private Boolean isHomePage = false;
}
