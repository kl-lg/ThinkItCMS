package com.thinkit.cms.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by LG on 2022/4/14.
 */
@Data
@Accessors(chain = true)
public class BaseModel<M> extends Model {

    public BaseModel(){

    }

    @JsonIgnore
    @TableField(exist = false)
    Map<String,List<AuthUnity>> querySet = new HashMap<>(16);

    @TableId(value="id",type = IdType.ASSIGN_ID)
    protected String id;

	/**
     * 创建人
     */
    @TableField(value = "create_id")
    protected String createId;


    /**
     * 创建时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(value = "gmt_create")
    protected Date gmtCreate;

    /**
     * 修改人
     */
    @TableField(value = "modified_id")
    protected String modifiedId;


    /**
     * 修改时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(value = "gmt_modified")
    protected Date gmtModified;

}
