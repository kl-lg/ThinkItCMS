package com.thinkit.cms.model;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author LG
 * @date 2022/9/14
 */
@Data
@Accessors(chain = true)
public class BaseDto implements Serializable {

    public BaseDto(){

    }

    private static final long serialVersionUID = 1L;

    protected String id;
	/**
     * 创建人
     */
    protected String createId;

    /**
     * 创建时间
     */
    //@IndexField(fieldType = FieldType.TEXT, ignoreCase = true,sortField = true)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    protected Date gmtCreate;

    /**
     * 修改人
     */
    protected String modifiedId;

    /**
     * 修改时间
     */
    //@IndexField(fieldType = FieldType.TEXT, ignoreCase = true,sortField = true)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    protected Date gmtModified;


    public <T extends BaseModel> QueryWrapper<T> toWrapper() {
        return new QueryWrapper<>();
    }



    public String[] select() {
        return null;
    }

    private String siteId;

    private String templateId;
}
