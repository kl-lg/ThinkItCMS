package com.thinkit.cms.netio.callback;

import java.util.Map;
import java.util.concurrent.CompletableFuture;

/**
 * @author LONG
 */
public abstract class UploadCallBack {

    /**
     * 上传后的回调
     * @param isOk
     * @param params
     */
    public abstract void callBack(boolean isOk,Map<String,Object> params);


    /**
     * 删除成功的回调
     * @param isOk
     * @param params
     */
    public abstract void callDelBack(boolean isOk,Map<String,Object> params);


    /**
     * 异步删除
     * @param isOk
     * @param params
     */
    protected abstract void asyncDelBack(boolean isOk,Map<String,Object> params);


    /**
     * 删除成功的回调
     * @param isOk
     * @param params
     */
    public  void callAsyncDelBack(boolean isOk,Map<String,Object> params){
        CompletableFuture.runAsync(()->{
            asyncDelBack(isOk,params);
        });
    }
}
