package com.thinkit.cms.netio.clients;
import com.thinkit.cms.annotation.Client;
import com.thinkit.cms.constant.SysConst;
import com.thinkit.cms.utils.Checker;
import com.thinkit.cms.utils.CmsConf;
import lombok.extern.slf4j.Slf4j;
import org.reflections.Reflections;
import org.springframework.stereotype.Component;
import java.util.Set;
/**
 * @author LONG
 */
@Component
@Slf4j
public class ClientFactory {

   private OssClient ossClient;

   public OssClient getOssClient(){
       try {
           if(Checker.BeNull(ossClient)) {
               init();
           }
       }catch (Exception e){
           log.error("初始化CLIENT加载器异常!");
       }
       return this.ossClient;
   }

   private void init() throws Exception {
      String clientName =  CmsConf.get(SysConst.clientName);
      clientName=Checker.BeNotBlank(clientName)?clientName:"aliyunOss";
      Reflections reflections = new Reflections(this.getClass());
      Set<Class<?>> classes = reflections.getTypesAnnotatedWith(Client.class);
      if(Checker.BeNotEmpty(classes)){
          for(Class clz:classes){
              Client client = (Client) clz.getAnnotation(Client.class);
              if(client.value().equals(clientName)){
                  this.ossClient = (OssClient) clz.getConstructor().newInstance();
                  break;
              }
          }
      }
   }
}
