package com.thinkit.cms.netio.clients;
import com.thinkit.cms.model.CmsResult;
import com.thinkit.cms.netio.callback.UploadCallBack;
import com.thinkit.cms.dto.netio.Chunk;

import java.io.IOException;
import java.io.InputStream;

public interface OssClient {
    /**
     * 构建CLIENT
     * @param endpoint
     * @param accessKeyId
     * @param accessKeySecret
     * @return
     */
    OssClient build(String endpoint, String accessKeyId, String accessKeySecret);


    /**
     * 上传文件
     * @param bucketName
     * @param objectName
     * @param bytes
     * @return
     */
    CmsResult uploadFile(String bucketName, String objectName,byte[] bytes,String siteId);


    /**
     * 上传文件
     * @param bucketName
     * @param objectName
     * @param inputStream
     * @return
     */
    CmsResult uploadFile(String bucketName, String objectName, InputStream inputStream,long size,String siteId);



    /**
     * 断点续传
     * @param chunk
     * @return
     */
   CmsResult uploadFile(Chunk chunk) throws IOException;


    /**
     * 上传文件 并回调结果
     * @param bucketName
     * @param objectName
     * @param bytes
     * @param callBack
     * @return
     */
    CmsResult uploadFile(String bucketName, String objectName,byte[] bytes,UploadCallBack callBack,String siteId);


    /**
     * 删除文件
     * @param bucketName
     * @param objectName
     * @return
     */
    CmsResult deleteFile(String bucketName, String objectName,String siteId);

    CmsResult merge(Chunk chunk);


    /**
     * 取消
     * @param chunk
     */
    void cancel(Chunk chunk);
}
