package com.thinkit.cms.netio.clients;

import cn.hutool.extra.spring.SpringUtil;
import com.aliyun.oss.OSSException;
import com.thinkit.cms.annotation.Client;
import com.thinkit.cms.api.attach.AttachService;
import com.thinkit.cms.constant.SysConst;
import com.thinkit.cms.dto.attach.AttachDto;
import com.thinkit.cms.dto.netio.Chunk;
import com.thinkit.cms.model.CmsResult;
import com.thinkit.cms.netio.callback.LocalCallBack;
import com.thinkit.cms.netio.callback.UploadCallBack;
import com.thinkit.cms.utils.Checker;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Map;

@Slf4j
@Data
@Client(value = "local")
public class LocalClient implements OssClient{

    private LocalUpload ossClient;

    private UploadCallBack uploadCallBack;

    private String endpoint;

    private AttachService attachService = SpringUtil.getBean(AttachService.class);

    @Override
    public OssClient build(String endpoint, String accessKeyId, String accessKeySecret) {
        if(Checker.BeNull(this.ossClient)){
            this.ossClient = new LocalUpload(endpoint, accessKeyId, accessKeySecret);
        }
        if(Checker.BeNull(this.uploadCallBack)){
            this.uploadCallBack = new LocalCallBack();
        }
        return this;
    }

    @Override
    public CmsResult uploadFile(String bucketName, String objectName, byte[] bytes,String siteId) {
        Map<String, Object> objectMap = null;
        try {
            objectMap = ossClient.putObject(bucketName, objectName, new ByteArrayInputStream(bytes));
            objectMap.put(SysConst.siteId, siteId);
            uploadCallBack.callBack(true, objectMap);
            return CmsResult.result(getFileUrl(objectMap));
        } catch (Exception e) {
            log.error("文件上传异常:" + e.getMessage());
            uploadCallBack.callBack(false, objectMap);
        }
        return CmsResult.result(-1);
    }

    @Override
    public CmsResult uploadFile(String bucketName, String objectName, InputStream inputStream,long size,String siteId) {
        Map<String, Object> objectMap = null;
        try {
            objectMap = ossClient.putObject(bucketName, objectName, inputStream);
            objectMap.put(SysConst.siteId, siteId);
            uploadCallBack.callBack(true, objectMap);
            return CmsResult.result(getFileUrl(objectMap));
        } catch (Exception e) {
            log.error("文件上传异常:" + e.getMessage());
            uploadCallBack.callBack(false, objectMap);
        }
        return CmsResult.result(-1);
    }

    public CmsResult merge(Chunk chunk) {
        return ossClient.merge(chunk);
    }

    @Override
    public void cancel(Chunk chunk) {

    }

    @Override
    public CmsResult uploadFile(Chunk chunk) {
       try {
           ossClient.putObject(chunk);
       }catch (Exception e){
           log.error("文件上传异常:" + e.getMessage());
       }
       return CmsResult.result();
    }



//    public  Map<String,Object> keepUploadFile(Chunk chunk) {
//        Map<String,Object> params=new HashMap<>();
//        MultipartFile multipartFile=chunk.getFile();
//        String root=thinkCmsConfig.getFileResourcePath();
//        try {
//            if(!checkIsUpload(chunk)){
//                String fileName = getFileName(multipartFile);
//                boolean isLastChunk=chunk.getChunkNumber().intValue()==chunk.getTotalChunks().intValue();
//                if(chunk.getTotalChunks().intValue()==1){
//                    FileUtil.upload(multipartFile.getBytes(),root+getFilePath(multipartFile),root+fileName,false);
//                    params.put("fileMd5",chunk.getIdentifier());
//                    params.put("fileName",multipartFile.getOriginalFilename());
//                    params.put("filePath",fileName);
//                    params.put("path",fileName);
//                    params.put("fileFullPath", thinkCmsConfig.getServerApi()+Constants.localtionUploadPattern+fileName);
//                    params.put("group",getGroup(multipartFile));
//                    params.put("finshed",true);
//                }else{
//                    String pathKey=Constants.fastDfsKeepUpload+chunk.getIdentifier()+"_path";
//                    if(chunk.getChunkNumber()==1){
//                        FileUtil.upload(multipartFile.getBytes(),root+getFilePath(multipartFile),root+fileName,false);
//                        markUpload(chunk,isLastChunk);
//                        baseRedisService.set(pathKey,fileName);
//                    }else{
//                        Object fileNameObj=baseRedisService.get(pathKey);
//                        if(Checker.BeNotNull(fileNameObj)){
//                            FileUtil.uploadAppend(root,fileNameObj.toString(),multipartFile.getBytes());
//                            markUpload(chunk,isLastChunk);
//                            params.put("finshed",isLastChunk);
//                            if(isLastChunk){
//                                params.put("fileMd5",chunk.getIdentifier());
//                                params.put("fileName",multipartFile.getOriginalFilename());
//                                params.put("filePath",fileNameObj.toString());
//                                params.put("path",fileNameObj.toString());
//                                params.put("fileFullPath", thinkCmsConfig.getServerApi()+Constants.localtionUploadPattern+fileNameObj.toString());
//                                params.put("group",getGroup(multipartFile));
//                            }
//                        }
//                    }
//                }
//            }
//            return ApiResult.result(params);
//        }catch (Exception e){
//            return  ApiResult.result(20004);
//        }
//    }
//
//    private boolean checkIsUpload(Chunk chunk){
//        String fileKey=Constants.fastDfsKeepUpload+chunk.getIdentifier();
//        List<Integer> uploadChunk = ( List<Integer>)baseRedisService.get(fileKey);
//        if(Checker.BeNotEmpty(uploadChunk)){
//            Integer curetChunk=chunk.getChunkNumber();
//            return uploadChunk.contains(curetChunk);
//        }else{
//            return false;
//        }
//    }

//    private boolean markUpload(Chunk chunk,boolean isLastChunk){
//        String fileKey=Constants.fastDfsKeepUpload+chunk.getIdentifier();
//        String pathKey=Constants.fastDfsKeepUpload+chunk.getIdentifier()+"_path";
//        if(isLastChunk){
//            baseRedisService.removeByKeys(Arrays.asList(fileKey,pathKey));
//        }else{
//            List<Integer> uploadChunk = ( List<Integer>)baseRedisService.get(fileKey);
//            if(Checker.BeEmpty(uploadChunk)){
//                uploadChunk=new ArrayList<>();
//            }
//            uploadChunk.add(chunk.getChunkNumber());
//            baseRedisService.set(fileKey,uploadChunk);
//        }
//        return true;
//    }

    @Override
    public CmsResult uploadFile(String bucketName, String objectName, byte[] bytes, UploadCallBack callBack,String siteId) {
        Map<String, Object> objectMap = null;
        try {
            objectMap = ossClient.putObject(bucketName, objectName, new ByteArrayInputStream(bytes));
            objectMap.put(SysConst.siteId, siteId);
            callBack.callBack(true, objectMap);
            return CmsResult.result(getFileUrl(objectMap));
        } catch (Exception e) {
            log.error("文件上传异常:{}" + e.getMessage());
            callBack.callBack(false, objectMap);
        }
        return null;
    }

    @Override
    public CmsResult deleteFile(String bucketName, String objectName,String siteId) {
        Map<String, Object> objectMap = null;
        try {
            AttachDto attachDto = attachService.getByField("full_path", objectName);
            if (Checker.BeNotNull(attachDto)) {
                objectMap = ossClient.deleteObject(bucketName, attachDto.getRelativePath());
                attachService.deleteByPk(attachDto.getId());
                uploadCallBack.callAsyncDelBack(true, objectMap);
            }
        } catch (OSSException e) {
            uploadCallBack.callAsyncDelBack(false, objectMap);
            log.error("删除文件失败:{}", e.getMessage());
        }
        return CmsResult.result();
    }


    private String getFileUrl(Map<String,Object> objectMap){
        if(Checker.BeNotEmpty(objectMap) && objectMap.containsKey("fileUrl")){
            Object fileUel = objectMap.get("fileUrl");
            return Checker.BeNotNull(fileUel)?fileUel.toString():null;
        }
        return null;
    }


}
