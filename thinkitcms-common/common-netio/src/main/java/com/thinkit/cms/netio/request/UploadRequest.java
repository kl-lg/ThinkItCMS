package com.thinkit.cms.netio.request;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class UploadRequest implements Serializable {

    private String bucketName;

    private String objectName;

    private String uploadId;

    private List<AliPartETag> partETags;

}
