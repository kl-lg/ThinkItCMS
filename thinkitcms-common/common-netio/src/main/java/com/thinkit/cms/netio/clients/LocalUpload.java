package com.thinkit.cms.netio.clients;

import cn.hutool.core.io.FileUtil;
import cn.hutool.extra.spring.SpringUtil;
import com.google.common.collect.Maps;
import com.thinkit.cms.constant.SysConst;
import com.thinkit.cms.dto.netio.Chunk;
import com.thinkit.cms.model.CmsResult;
import com.thinkit.cms.utils.Checker;
import com.thinkit.cms.utils.CmsConf;
import com.thinkit.cms.utils.CmsFolderUtil;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.atomic.AtomicLong;
import java.util.regex.Pattern;

@Slf4j
@Data
public class LocalUpload {

    private String endpoint;
    private String accessKeyId;
    private String accessKeySecret;
    private String baseFolder;
    private RedisTemplate redisTemplate = SpringUtil.getBean("redisTemplate");

    public LocalUpload(String endpoint, String accessKeyId, String accessKeySecret) {
        this.endpoint = endpoint;
        this.accessKeyId = accessKeyId;
        this.accessKeySecret = accessKeySecret;
        String root =  CmsConf.get(SysConst.cmsFolder);
        this.baseFolder = CmsFolderUtil.appendFolder(root,SysConst.prefix,CmsConf.get(SysConst.cmsUpload));
    }

    public LocalUpload() {

    }

    public Map<String,Object> putObject(String bucketName, String objectName, InputStream inputStream) {
        bucketName = Checker.BeBlank(bucketName)? CmsConf.get(SysConst.bucketName):bucketName;
        String uploadPath = buildPath(bucketName);
        String reName = getReName(objectName);
        String fileName = CmsFolderUtil.appendFolder(uploadPath,reName);
        if(Checker.BeNotBlank(fileName)){
            File file  = FileUtil.writeFromStream(inputStream,new File(fileName));
            return params(bucketName,objectName,reName,file.length());
        }
        return Maps.newHashMap();
    }

    public Map<String,Object> putObject(Chunk chunk) {
        String bucketName = chunk.getBucketName();
        bucketName = Checker.BeBlank(bucketName)? CmsConf.get(SysConst.bucketName):bucketName;
        try {
            InputStream inputStream = chunk.getFile().getInputStream();
            String suffix = SysConst.point+FileUtil.getSuffix(chunk.getFile().getOriginalFilename());
            String uploadPath = buildPath(bucketName);
            LocalDate now = LocalDate.now();
            String data = now.format(DateTimeFormatter.ofPattern("yyyyMMdd"));
            String filePath = data+SysConst.prefix+ chunk.getIdentifier()+SysConst.prefix+chunk.getChunkNumber()+suffix;
            String fileName = CmsFolderUtil.appendFolder(uploadPath,filePath);
            Path path = Path.of(fileName);
            String parent = path.getParent().toString().replace(SysConst.brefix,SysConst.prefix);
            boolean isHasParent = FileUtil.exist(parent);
            if(!isHasParent){
                File file = FileUtil.mkdir(parent);
                log.info("创建新的目录：{}",file.getAbsoluteFile());
            }
            chunk.setDirectory(parent);
            File file  = FileUtil.writeFromStream(inputStream,new File(fileName));
            mkdirChunk(chunk);
            return Maps.newHashMap();
        }catch (Exception e){
            log.error("创建新的目录上传文件失败：{}",e.getMessage());
        }
        return Maps.newHashMap();
    }


    private   Map<String, Object> params(String bucketName,String objectName,String reNameFile,long size){
        Map<String, Object> params = new HashMap<>(16);
        params.put("bucket",bucketName);
        params.put("objectName",objectName);
        params.put("reNameFile",reNameFile);
        params.put("size",size);
        params.put("fileUrl",fileUrl(CmsConf.getAsBoolean(SysConst.enableSSL),bucketName,reNameFile));
        return params;
    }


    private String fileUrl(boolean ssl,String bucketName,String reNameFile){
        String prefix= "http://";
        if(ssl){
            prefix="https://";
        }
        return CmsFolderUtil.appendFolder(prefix,endpoint,SysConst.prefix,bucketName,SysConst.prefix,reNameFile);
    }

    private String buildPath(String bucketName){
        String folderStr = CmsFolderUtil.appendFolder(baseFolder,SysConst.prefix,bucketName,SysConst.prefix);
        if(!FileUtil.exist(folderStr)){
            FileUtil.mkdir(folderStr);
        }
        return folderStr;
    }

    private String getReName(String objectName){
        String suffix = FileUtil.getSuffix(objectName);
        LocalDate now = LocalDate.now();
        String data = now.format(DateTimeFormatter.ofPattern("yyyyMMdd"));
        return data+"/"+ UUID.randomUUID()+"."+suffix;
    }


    public Map<String, Object>  deleteObject(String bucketName, String relativePath) {
        String upload = CmsFolderUtil.getUpload();
        String fileName = CmsFolderUtil.appendFolder(upload,bucketName,SysConst.prefix,relativePath);
        FileUtil.del(fileName);
        return params(bucketName,relativePath,relativePath,0);
    }

    public CmsResult merge(Chunk chunk) {
        try {
            String identifier = chunk.getIdentifier();
            String directoryKey = SysConst.directoryFlat+identifier;
            if(redisTemplate.hasKey(directoryKey)){
                String targetFolder = redisTemplate.opsForValue().get(directoryKey).toString();
                String objectName = UUID.randomUUID()+SysConst.point+ FileUtil.getSuffix(chunk.getFilename());
                String targetFile = CmsFolderUtil.appendFolder(targetFolder,SysConst.brefix,objectName);
                AtomicLong totalSize = new AtomicLong(0L);
                String finalTargetFile = targetFile.replace(SysConst.brefix,SysConst.prefix);
                Files.createFile(Paths.get(finalTargetFile));
                Files.list(Paths.get(targetFolder))
                        .filter(path ->{
                            String fileName = path.getFileName().toString();
                            return Checker.BeNotBlank(fileName) && isNumber(fileName.substring(0,fileName.lastIndexOf(SysConst.point)));
                        })
                        .sorted((o1, o2) -> {
                            String p1Name = o1.getFileName().toString();
                            String p2Name = o2.getFileName().toString();
                            Integer p1 = Integer.valueOf(p1Name.substring(0,p1Name.lastIndexOf(SysConst.point)));
                            Integer p2 = Integer.valueOf(p2Name.substring(0,p2Name.lastIndexOf(SysConst.point)));
                            return Integer.valueOf(p1).compareTo(Integer.valueOf(p2));
                        })
                        .forEach(path -> {
                            try {
                                //以追加的形式写入文件
                                byte[] bytes = Files.readAllBytes(path);
                                Files.write(Paths.get(finalTargetFile), bytes, StandardOpenOption.APPEND);
                                totalSize.addAndGet(bytes.length);
                                //合并后删除该块
                                Files.delete(path);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        });
                removeChunk(chunk);
                String bucketName = chunk.getBucketName();
                bucketName = Checker.BeBlank(bucketName)? CmsConf.get(SysConst.bucketName):bucketName;
                String finalFile = finalTargetFile.replace(SysConst.brefix,SysConst.prefix);
                String root = baseFolder.replace(SysConst.brefix,SysConst.prefix)+SysConst.prefix+bucketName+SysConst.prefix;
                String reNameFile = finalFile.replace(root,"");
                Map map = params(bucketName,objectName,reNameFile,totalSize.get());
                map.put("fileName",chunk.getFilename());
                return CmsResult.result(map);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return CmsResult.result();
    }


    /**
     * 标记已经上传成功的片段
     * @param chunk
     */
    private void mkdirChunk(Chunk chunk){
        String key = SysConst.fileFlat+chunk.getIdentifier();
        redisTemplate.opsForHash().put(key,
                String.valueOf(chunk.getChunkNumber()),String.valueOf(chunk.getCurrentChunkSize()));
        String directoryKey = SysConst.directoryFlat+chunk.getIdentifier();
        if(!redisTemplate.hasKey(directoryKey)){
            redisTemplate.opsForValue().set(directoryKey,chunk.getDirectory());
        }
    }



    private void removeChunk(Chunk chunk){
        String key = SysConst.fileFlat+chunk.getIdentifier();
        String directoryKey = SysConst.directoryFlat+chunk.getIdentifier();
        redisTemplate.delete(Arrays.asList(key,directoryKey));
    }

    private static boolean isNumber(String str) {
        Pattern pattern = Pattern.compile("^[-\\+]?[\\d]*$");
        return pattern.matcher(str).matches();
    }



    private boolean checkIsUpload(Chunk chunk){
        Set<String> ids =  redisTemplate.opsForHash().keys(SysConst.fileFlat+chunk.getIdentifier());
        return Checker.BeNotEmpty(ids) && ids.contains(String.valueOf(chunk.getChunkNumber()));
    }
}
