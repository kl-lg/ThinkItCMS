package com.thinkit.cms.authen.secret;

import com.thinkit.cms.authen.enums.GrantTypes;
import com.thinkit.cms.authen.model.LoginModel;
import com.thinkit.cms.authen.enums.LoginTypes;

/**
 * @author LONG
 */
public class Muserlogin extends LoginModel implements SecretParam {


    @Override
    public LoginModel getLoginParam() {
        return this;
    }

    @Override
    public GrantTypes grantType() {
        return GrantTypes.PASSWORD;
    }

    @Override
    public LoginTypes loginType() {
        return LoginTypes.PLAT_USER;
    }

}
