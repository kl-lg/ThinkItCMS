package com.thinkit.cms.authen.secret;

import com.thinkit.cms.authen.enums.GrantTypes;
import com.thinkit.cms.authen.enums.LoginTypes;
import com.thinkit.cms.authen.model.LoginModel;

/**
 * 秘钥获取器
 * @author LONG
 */
public interface SecretParam {

    /**
     * 获取用户登录参数
     * @return
     */
    LoginModel getLoginParam();


    /**
     * 获取授权模式
     * @return
     */
    GrantTypes grantType();

    /**
     * 获取登录类型
     * @return
     */
    LoginTypes loginType();

}
