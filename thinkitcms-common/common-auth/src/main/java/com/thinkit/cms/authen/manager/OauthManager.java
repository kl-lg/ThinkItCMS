package com.thinkit.cms.authen.manager;

import com.thinkit.cms.authen.auth.PassManager;
import com.thinkit.cms.authen.model.LoginModel;
import com.thinkit.cms.authen.secret.SecretParam;
import com.thinkit.cms.user.UserDetail;
import com.thinkit.cms.user.UserDetailService;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * @author LONG
 */
@Slf4j
public class OauthManager implements PassManager {

    private SecretParam secretParam;

    private UserDetailService userDetailService;

    public OauthManager(SecretParam secretParam, UserDetailService userDetailService){
        this.secretParam = secretParam;
        this.userDetailService = userDetailService;
    }

    @Override
    public Map<String, Object> login() {
        LoginModel loginModel = secretParam.getLoginParam();
        return null;
    }


    @Override
    public UserDetail getUserDetail() {
        return null;
    }
}
