package com.thinkit.cms.authen.exceptions;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/**
 * @author LONG
 */
@Data
public class AuthenException extends RuntimeException{

	public int code=-1;

	private Map<String,Object> result;

	public Map<String,Object> errMsg(){
		return this.result;
	}

	public AuthenException(Integer code,String message) {
		super(message);
		Map<String,Object> map = new HashMap<>();
		map.put("code",code);
		map.put("msg",message);
		this.result = map;
	}
}
