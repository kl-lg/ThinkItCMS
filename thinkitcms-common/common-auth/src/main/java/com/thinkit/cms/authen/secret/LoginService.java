package com.thinkit.cms.authen.secret;

import com.thinkit.cms.model.CmsResult;

/**
 * @author LONG
 */
public interface LoginService {

    /**
     * 登录接口
     * @param secretParam
     * @return
     */
    CmsResult login(SecretParam secretParam);

    /**
     * 退出登陆
     * @return
     */
    CmsResult logout();


    /**
     * 获取验证码
     * @return
     */
    CmsResult verifyCode();
}
