package com.thinkit.cms.authen.auth;

import com.thinkit.cms.authen.manager.OauthManager;
import com.thinkit.cms.authen.secret.SecretParam;
import com.thinkit.cms.user.UserDetailService;

import java.util.List;

/**
 * @author LONG
 */
public class OauthProvide extends AuthenProvide {

    private SecretParam secretParam;


    List<UserDetailService> userDetailServices;

    public List<UserDetailService> getUserDetailServices() {
        return userDetailServices;
    }

    public AuthenProvide setUserDetailServices(List<UserDetailService> userDetailServices) {
        this.userDetailServices = userDetailServices;
        return this;
    }

    public OauthProvide(SecretParam secretParam){
        this.secretParam = secretParam;
    }

    @Override
    public PassManager passManager() {
        UserDetailService userDetailService = loadUserDetailService(secretParam.loginType());
        return new OauthManager(secretParam,userDetailService);
    }


    public static PassManager buildManager(SecretParam secretParam, List<UserDetailService> userDetailServices){
        return new OauthProvide(secretParam).setUserDetailServices(userDetailServices).passManager();
    }
}
