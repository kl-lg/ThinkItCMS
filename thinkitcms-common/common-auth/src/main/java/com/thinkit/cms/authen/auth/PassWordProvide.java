package com.thinkit.cms.authen.auth;

import com.thinkit.cms.authen.manager.UserNamePassWordManager;
import com.thinkit.cms.authen.secret.SecretParam;
import com.thinkit.cms.user.UserDetailService;

import java.util.List;

/**
 * @author LONG
 */
public class PassWordProvide extends AuthenProvide {

    private SecretParam secretParam;

    List<UserDetailService> userDetailServices;

    public List<UserDetailService> getUserDetailServices() {
        return userDetailServices;
    }

    public PassWordProvide setUserDetailServices(List<UserDetailService> userDetailServices) {
        this.userDetailServices = userDetailServices;
        return this;
    }

    public PassWordProvide(SecretParam secretParam,List<UserDetailService> userDetailServices){
        super(userDetailServices);
        this.secretParam = secretParam;
    }

    @Override
    public PassManager passManager() {
        UserDetailService userDetailService = loadUserDetailService(secretParam.loginType());
        return new UserNamePassWordManager(secretParam,userDetailService);
    }


    public static PassManager buildManager(SecretParam secretParam,List<UserDetailService> userDetailServices){
        return new PassWordProvide(secretParam,userDetailServices).passManager();
    }
}
