package com.thinkit.cms.authen.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author LONG
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface NoDecorate {
	String value() default "";
	String 	module() default "";
	int type() default 0;//不同项目的日志
}
