import axios from 'axios';
import type { RouteRecordNormalized } from 'vue-router';
import { UserState } from '@/store/modules/user/types';

export interface LoginData {
  pw: any;
  userName: string;
}

export interface Response {
  code: string;
  msg: string;
  data: any;
  token: any;
}

export function login(data: LoginData) {
  return axios.post<Response>('/api/login', data);
}

export function logout() {
  return axios.post<Response>('/api/logout');
}

export function getUserInfo() {
  return axios.get<UserState>('/api/sysUser/info');
}

export function getMenuList() {
  //  return  axios.post<RouteRecordNormalized[]>('/api/user/menu');
  return axios.get<RouteRecordNormalized[]>('/api/sysMenu/menuSlideTree');
}

export function verifyCode() {
  return axios.get<Response>('/api/verifyCode');
}
