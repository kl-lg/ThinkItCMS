import axios, { AxiosResponse } from 'axios';

export interface Record {
  id: string;
  roleName: string;
  roleSign: string;
  remark: string;
  isSystem: number;
}

export interface PageParams extends Partial<Record> {
  current: number;
  pageSize: number;
  dto: object;
}

export interface ListRes {
  rows: Record[];
  total: number;
  current: number;
}

export interface Response {
  code: string;
  msg: string;
  data: any;
}

export interface ResponseMenu {
  code: string;
  msg: string;
  menus: [];
  checkedKeys: [];
  expandedKeys: [];
}

export function queryList(params: PageParams) {
  return axios.post<ListRes>('/api/sysRole/page', params);
}

export function genMyCode(rowId: string) {
  return axios.put(`/api/sysRole/genCode?id=${rowId}`);
}

export function detail(rowId: string) {
  return axios.get<Record>(`/api/sysRole/detail?id=${rowId}`);
}

export function update(params: any) {
  return axios.post<Response>('/api/sysRole/update', params);
}

export function deleteRow(rowId: string) {
  return axios.delete(`/api/sysRole/delete?id=${rowId}`);
}

export function addForm(params: any) {
  return axios.post<Response>('/api/sysRole/save', params);
}

export function editForm(params: any) {
  return axios.put<Response>('/api/sysRole/update', params);
}

export function tipMenus(roleId: string) {
  return axios.get<ResponseMenu>(
    `/api/sysMenu/menuTreeAssign?roleId=${roleId}`
  );
}

export function saveTreeAssign(params: any) {
  return axios.put<Response>(`/api/sysRoleMenu/saveTreeAssign`, params);
}
