import axios, { AxiosResponse } from 'axios';

export interface Record {
  id: string;
  pid: string | '0';
  orgCode: string;
  orgName: string | '';
  parentName: '';
  order: 0;
}

export interface Response {
  code: string;
  msg: string;
  data: any;
}

export function organizeTree() {
  return axios.get<Record[]>('/api/sysOrganize/orgTableTree');
}

export function addForm(params: any) {
  return axios.post<Response>('/api/sysOrganize/save', params);
}

export function editForm(params: any) {
  return axios.put<Response>('/api/sysOrganize/update', params);
}

export function detail(rowId: string) {
  return axios.get<Record>(`/api/sysOrganize/detail?id=${rowId}`);
}

export function deleteIt(rowId: string) {
  return axios.delete<Response>(`/api/sysOrganize/delete?id=${rowId}`);
}
