import axios, { AxiosResponse } from 'axios';
import { BasicInfoModel } from "@/api/user-center";

export interface Record {
  id: string;
  userName: string;
  nickName: string;
  password: string;
  email: string;
  mobile: string;
  status: string;
  orgId: string;
  sex: string;
  defSiteId: string;
  isSuperAdmin: boolean;
}

export interface PageParams extends Partial<Record> {
  current: number;
  pageSize: number;
  dto: object;
}

export interface ListRes {
  rows: Record[];
  total: number;
  current: number;
}

export interface Response {
  code: string;
  msg: string;
  data: any;
}

export interface Role {
  id: string;
  roleName: string;
  roleSign: string;
  remark: string;
}

export interface ResponseRole {
  roleIds: [];
  roles: Role[];
}

export function queryList(params: PageParams) {
  return axios.post<ListRes>('/api/sysUser/page', params);
}

export function tipRoles(userId: string) {
  return axios.get<ResponseRole>(`/api/sysUser/tipRoles?userId=${userId}`);
}

export function detail(rowId: string) {
  return axios.get<Record>(`/api/sysUser/detail?id=${rowId}`);
}

export function userInfo() {
  return axios.get<BasicInfoModel>(`/api/sysUser/info`);
}


export function deleteRow(rowId: string) {
  return axios.delete(`/api/sysUser/delete?id=${rowId}`);
}

export function addForm(params: any) {
  return axios.post<Response>('/api/sysUser/save', params);
}

export function editForm(params: any) {
  return axios.put<Response>('/api/sysUser/update', params);
}

export function updateBaseInfo(params: any) {
  return axios.put<Response>('/api/sysUser/updateBaseInfo', params);
}

export function updateAvatar(params: any) {
  return axios.put<Response>('/api/sysUser/updateAvatar', params);
}

export function updatePw(params: any) {
  return axios.put<Response>('/api/sysUser/updatePw', params);
}


export function saveRoleAssign(params: any) {
  return axios.put<Response>('/api/userRole/save', params);
}

export function lockData(params: any) {
  return axios.put<Response>('/api/sysUser/lock', params);
}
