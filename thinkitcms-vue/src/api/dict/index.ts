import axios from "axios";
import { Response } from "@/api/aicode";

export interface Record {
  id: string | '';
  parentId: string;
  type: string | '0';
  name: string | '';
  value: string | '0';
  description: string | '';
  pid:'';
  num: number;
}
export function dictTree() {
  return axios.get<Record[]>('/api/sysDict/dictTableTree');
}

export function addForm(params: any) {
  return axios.post<Response>('/api/sysDict/save', params);
}

export function editForm(params: any) {
  return axios.put<Response>('/api/sysDict/update', params);
}

export function detail(rowId: string) {
  return axios.get<Record>(`/api/sysDict/detail?id=${rowId}`);
}

export function deleteIt(rowId: string) {
  return axios.delete<Response>(`/api/sysDict/delete?id=${rowId}`);
}


