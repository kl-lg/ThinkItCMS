import axios, { AxiosResponse } from 'axios';
import { Tree } from '@/api/templates';

export interface Record {
  id: string;
  name: string | '0';
  direct: string;
  enable: string;
  sql: string;
  result: string;
  resultConvert: string;
  directType:string;
  sqlType:string;
  inParams:  {
    name: string,
    type:string,
    required: boolean
  };
  outParams: {
    name: string,
    type: string,
    alias:string
  };
  config: {
    cacheTime: number,
    timeUnit:string,
    cacheName: string,
    location: string,
    enable: true
  };
  isShare: string;
}

export interface Response {
  code: string;
  msg: string;
  data: any;
}

export interface ListRes {
  rows: Record[];
  total: number;
  current: number;
}

export interface PageParams extends Partial<Record> {
  current: number;
  pageSize: number;
  dto: object;
}

export function queryList(params: PageParams) {
  return axios.post<ListRes>('/api/directs/page', params);
}

export function detail(rowId: string) {
  return axios.get<Record>(`/api/directs/detail?id=${rowId}`);
}

export function deleteIt(rowId: string) {
  return axios.delete(`/api/directs/delete?id=${rowId}`);
}

export function deleteCache(directs: string) {
  return axios.delete(`/api/directs/delCache?directs=${directs}`);
}

export function delAllCache() {
  return axios.delete(`/api/directs/delAllCache`);
}

export function addForm(params: any) {
  return axios.post<Response>('/api/directs/save', params);
}

export function editForm(params: any) {
  return axios.put<Response>('/api/directs/update', params);
}


export function enable(params: any) {
  return axios.put<Response>('/api/directs/enable', params);
}

export function apply() {
  return axios.put<Response>('/api/directs/apply');
}

export function loadHands() {
  return axios.get<Response>('/api/directs/loadHands');
}

export function loadInstructs() {
  return axios.get<Response>('/api/directs/loadInstructs');
}
