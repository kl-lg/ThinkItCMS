import axios from 'axios';
import type { TableData } from '@arco-design/web-vue/es/table/interface';

export interface ContentDataRecord {
  date: string;
  count: number;
}

export interface TxtDataRecord {
  name: string;
  value: number;
}


export function queryContentData() {
  return axios.get<ContentDataRecord[]>('/api/statistics/dateTotal');
}

export function categoryTotal() {
  return axios.get<TxtDataRecord[]>('/api/statistics/categoryTotal');
}


export interface PopularRecord {
  key: number;
  clickNumber: string;
  title: string;
  increases: number;
}

export function queryPopularList(params: object) {
 // return axios.get<TableData[]>('/api/popular/list', { params });
  return axios.get<TableData[]>('/api/statistics/topTotal', params);
}


export function contentTotal(params: object) {
  return axios.get<any>('/api/statistics/contentTotal', { params });
}
