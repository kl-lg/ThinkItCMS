import axios, { AxiosResponse } from 'axios';

export interface Record {
  id: string;
  name: string;
  cover: string;
  code: string;
  description: string;
  dir: string;
  share:string
}

export interface Tree {
  id: string;
  absolutePath: string;
  children: [];
  ids: number[];
  pid: string;
  name: string;
  relativePath: string;
  lastModified: number;
}

export interface PageParams extends Partial<Record> {
  current: number;
  pageSize: number;
  dto: object;
}

export interface ListRes {
  rows: Record[];
  total: number;
  current: number;
}

export interface Response extends AxiosResponse {
  code: string;
  msg: string;
  data: any;
}

export interface TreeResponse {
  code: string;
  msg: string;
  data: Tree;
}

export interface List {
  defTempId: string | '';
  rows: [];
  code: string;
  msg: string;
  data: [];
}

export interface File {
  name: string;
  tempId: string;
  tempPath: string;
  isDirectory: boolean;
}

export function queryList(params: PageParams) {
  return axios.post<ListRes>('/api/template/page', params);
}

export function detail(rowId: string) {
  return axios.get<Record>(`/api/template/detail?id=${rowId}`);
}

export function deleteRow(rowId: string) {
  return axios.delete(`/api/template/delete?id=${rowId}`);
}

export function addForm(params: any) {
  return axios.post<Response>('/api/template/save', params);
}

export function uploadTmpFile(params: any) {
  return axios.post<Response>('/api/template/uploadCloud', params);
}

export function pullCloud(params: any) {
  return axios.post<Response>('/api/template/pullCloud', params);
}


export function importFolder(params: any) {
  return axios.post<Response>('/api/template/importFolder', params);
}


export function editForm(params: any) {
  return axios.put<Response>('/api/template/update', params);
}

export function loopFolder(tmpDir: string) {
  return axios.get<Tree>(`/api/template/loopFloder?tmpDir=${tmpDir}`);
}

export function createFile(params: File) {
  return axios.post<Response>('/api/template/mkdirFile', params);
}

export function delFolder(params: any) {
  return axios.post<Response>('/api/template/delFolder', params);
}

export function renameFile(params: any) {
  return axios.post<Response>('/api/template/renameFile', params);
}

export function fileContent(params: any) {
  return axios.post<Response>('/api/template/fileContent', params);
}

export function saveContent(params: any) {
  return axios.post<Response>('/api/template/saveContent', params);
}

export function tipTemp(siteId: string) {
  return axios.get<List>(`/api/template/loadTemp?siteId=${siteId}`);
}

export function loadCmsCodes(type: string) {
  return axios.get<any>(`/api/directs/loadCodes?type=${type}`);
}



export function setDefTemp(params: any) {
  return axios.put<Response>('/api/site/setDefTemplate', params);
}

export function openDir(params: any) {
  return axios.put<Response>('/api/template/openDir', params);
}


export function lockData(params: any) {
  return axios.put<Response>('/api/sysUser/lock', params);
}
