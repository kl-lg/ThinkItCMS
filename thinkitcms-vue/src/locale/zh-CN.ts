import localeMessageBox from '@/components/message-box/locale/zh-CN';
import localeLogin from '@/views/login/locale/zh-CN';

import localeWorkplace from '@/views/dashboard/workplace/locale/zh-CN';
/** simple */
import localeMonitor from '@/views/dashboard/monitor/locale/zh-CN';


import localeAiCode from '@/views/aicode/locale/zh-CN';
import localeRole from '@/views/role/locale/zh-CN';
import localeMenu from '@/views/menu/locale/zh-CN';
import sysuserMenu from '@/views/sysuser/locale/zh-CN';
import sysOrganize from '@/views/organize/locale/zh-CN';
import sysModel from '@/views/sysmodel/locale/zh-CN';
import templates from '@/views/templates/locale/zh-CN';
import category from '@/views/category/locale/zh-CN';
import content from '@/views/content/search-table/locale/zh-CN';
import localSite from '@/views/site/locale/zh-CN';
import localDirects from '@/views/directs/locale/zh-CN';
import localeFragment from '@/views/fragment/card/locale/zh-CN';
import localeStepForm from '@/views/form/step/locale/zh-CN';
import localeGroupForm from '@/views/form/group/locale/zh-CN';
import webFiles from '@/views/webfiles/search-table/locale/zh-CN';
import dicts from '@/views/dict/locale/zh-CN';
import localeBasicProfile from '@/views/profile/basic/locale/zh-CN';
import logs from '@/views/logs/locale/zh-CN';
import localeDataAnalysis from '@/views/visualization/data-analysis/locale/zh-CN';
import localeMultiDAnalysis from '@/views/visualization/multi-dimension-data-analysis/locale/zh-CN';
import license from '@/views/license/locale/zh-CN';
import localeSuccess from '@/views/result/success/locale/zh-CN';
import localeError from '@/views/result/error/locale/zh-CN';

import locale403 from '@/views/exception/403/locale/zh-CN';
import locale404 from '@/views/exception/404/locale/zh-CN';
import locale500 from '@/views/exception/500/locale/zh-CN';

import localeUserInfo from '@/views/user/info/locale/zh-CN';
import localeUserSetting from '@/views/user/setting/locale/zh-CN';
/** simple end */
import localeSettings from './zh-CN/settings';

export default {
  'menu.dashboard': '仪表盘',
  'menu.server.dashboard': '仪表盘-服务端',
  'menu.server.workplace': '工作台-服务端',
  'menu.server.monitor': '实时监控-服务端',
  'menu.list': '列表页',
  'menu.result': '结果页',
  'menu.exception': '异常页',
  'menu.form': '表单页',
  'menu.profile': '详情页',
  'menu.visualization': '数据可视化',
  'menu.user': '个人中心',
  'menu.arcoWebsite': 'Arco Design',
  'menu.faq': '常见问题',
  'navbar.docs': '文档中心',
  'navbar.action.locale': '切换为中文',
   'menu.list.searchTable':'列表查询',
  'form.edit': '编辑',
  'form.create': '创建',
  'form.delete': '删除',
  'form.search': '查询',
  'form.reset': '重置',
  'form.refresh':'刷新',
  'form.gmtCreate': '创建时间',
  'form.id':'主键',
  'form.gmtModified': '修改时间',
  'form.createId': '创建人ID',
  'form.modifiedId': '修改人ID',
  'form.view':'查看',
  'columns.operations': '操作',
  'confirm.delete': '确定要删除吗?',
  'confirm.do': '确定要执行该操作吗?',
  'request.ok': '操作成功!',
  'copy.ok': '复制成功!',
  'request.fail': '操作失败!',
  'option.lable': '项',
  'option.value': '值',
  'searchTable.columns.operations.view':'查看',
  'searchTable.form.reset':'重置',
  'searchTable.form.search':'查询',
  ...localeSettings,
  ...localeMessageBox,
  ...localeLogin,
  ...localeWorkplace,
  /** simple */
  ...localeMonitor,
  ...localeStepForm,
  ...localeGroupForm,
  ...localeBasicProfile,
  ...localeDataAnalysis,
  ...localeMultiDAnalysis,
  ...localeSuccess,
  ...localeError,
  ...locale403,
  ...locale404,
  ...localeMenu,
  ...locale500,
  ...localeUserInfo,
  ...localeUserSetting,
  ...localeAiCode,
  ...localeRole,
  ...sysuserMenu,
  ...sysOrganize,
  ...localSite,
  ...sysModel,
  ...templates,
  ...category,
  ...content,
  ...localDirects,
  ...localeFragment,
    ...webFiles,
  ...logs,
  ...dicts,
  ...license
  /** simple end */
};
