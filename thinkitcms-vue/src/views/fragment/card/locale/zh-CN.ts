export default {
  'form.fragment': '模型部件',
  'form.formCode':'部件编码',
  'form.sysmodel.formCode.required.errMsg':'请输入部件编码',
  'form.designData':'数据',
  'form.devcode':'开发',
  'columns.formData':'数据',
  'form.gencode':'生成',
  'fragment.searchInput.placeholder':'页面片段搜索'
};
