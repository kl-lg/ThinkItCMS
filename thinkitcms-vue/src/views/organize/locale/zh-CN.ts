export default {
  'form.organize': '组织管理',
  'form.copyRoute': '复制路由配置',
  'form.organize.columns.orgName': '组织名称',
  'form.organize.columns.orgCode': '组织代码',
  'form.organize.columns.pid': '父ID',
  'form.organize.columns.parentName': '父级资源',

  'form.organize.columns.order': '组织排序',

  'form.organize.orgName.required.errMsg': '请输入组织名称',
  'form.organize.orgName.maxLength.errMsg': '组织名称最大10个字符',
  'form.organize.orgName.minLength.errMsg': '组织名称最小2个字符',

  'form.organize.orgCode.required.errMsg': '请输入组织编码',
  'form.organize.orgCode.maxLength.errMsg': '组织编码最大20位字符',
  'form.organize.orgCode.minLength.errMsg': '组织编码最小2位字符',
};
