export default {
  'menu.visualization.multiDimensionDataAnalysis': '硬件指标',
  'multiDAnalysis.card.title.dataOverview': '数据总览',
  'multiDAnalysis.dataOverview.cpu.count': 'cpu个数',
  'multiDAnalysis.dataOverview.cpu.use': 'cpu使用率',
  'multiDAnalysis.dataOverview.java.version': 'JDK版本',
  'multiDAnalysis.dataOverview.os.name': '系统版本',
  'multiDAnalysis.dataOverview.activeProfiles': '开发环境',
  'multiDAnalysis.dataOverview.java.home':'JDK路径',

  'multiDAnalysis.card.title.system.info': '系统信息',
  'multiDAnalysis.card.title.soft.info': '应用信息',
  'multiDAnalysis.card.title.contentTypeDistribution': '内容题材分布',
  'multiDAnalysis.card.title.retentionTrends': '用户留存趋势',
  'multiDAnalysis.card.title.userRetention': '用户留存量',
  'multiDAnalysis.card.title.contentConsumptionTrends': '内容消费趋势',
  'multiDAnalysis.card.title.contentConsumption': '内容消费量',
  'multiDAnalysis.card.title.contentPublishingSource': '内容发布来源',
};
