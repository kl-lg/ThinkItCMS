export default {
  'form.tips': '元数据说明',
  'form.copyRoute': '复制路由配置',
  'form.license.columns.domain': '授权域名',
  'form.license.columns.issuer': '颁发者',
  'form.license.columns.organization': '被授权组织',
  'form.license.columns.effectDate': '生效时间',
  'form.license.columns.deadline': '过期时间',
  'form.license.columns.version': '版本号',
  'form.license.columns.certificateType': '证书类型',
  'form.license.columns.org':'事业单位商用永久授权',
  'form.license.columns.company':'企业商用永久授权',
  'form.license.columns.person':'个人商用永久授权',
  'form.license.columns.free':'个人非商用永久授权',
  'form.license.licenseName.required.errMsg': '请输入角色名称',
  'form.license.licenseName.maxLength.errMsg': '角色名称最大10个字符',
  'form.license.licenseName.minLength.errMsg': '角色名称最小2个字符',
  'form.license.columns.times':'授权起止日期',
  'form.license.times.required.errMsg':'请选择授权的起止日期',

  'form.license.domain.required.errMsg': '请输入授权域名',
  'form.license.domain.minLength.errMsg': '授权域名最小一个',
  'form.license.domain.maxLength.errMsg': '授权域名一次最多5个',

  'form.license.issuer.required.errMsg': '请输入颁发者',
  'form.license.issuer.minLength.errMsg': '颁发者最小2位字符',
  'form.license.issuer.maxLength.errMsg': '颁发者最大20位字符',

  'form.license.organization.required.errMsg': '请输入被授权单位名称',
  'form.license.organization.minLength.errMsg': '被授权单位名称最小2位字符',
  'form.license.organization.maxLength.errMsg': '被授权单位名称最大20位字符',


  'form.license.version.required.errMsg': '请输入版本号',
  'form.license.version.minLength.errMsg': '版本号最小2位字符',
  'form.license.version.maxLength.errMsg': '版本号最大20位字符',
  'form.license.certificateType.required.errMsg':'请选择证书类型',
  'form.license.down':'下载证书',
  'form.license.deploy':'部署证书'

};
