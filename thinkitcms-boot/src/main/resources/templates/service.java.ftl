package com.thinkit.cms.api.${tkModule?lower_case};
import com.thinkit.cms.base.BaseService;
import com.thinkit.cms.dto.${tkModule?lower_case}.${tkModule}Dto;
/**
* @author ${author!'System'}
*/


public interface ${tkModule}Service extends BaseService<${tkModule}Dto> {




}