<template>
  <div class="container">
    <a-modal
      v-model:visible="visible"
      v-model:draggable="draggable"
      :title="title"
      @cancel="handleCancel"
      @before-ok="onBeforeOk"
    >
      <a-form
        ref="formRef"
        :label-col-props="{ span: 7 }"
        :wrapper-col-props="{ span: 17 }"
        :model="formData"
        class="form"
      >
        <a-spin dot :loading="loading">
          <a-form-item
            :label="$t('form.sysuser.columns.userName')"
            field="userName"
            :validate-trigger="['change', 'input']"
            :rules="[
              {
                required: true,
                message: $t('form.sysuser.userName.required.errMsg'),
              },
              {
                maxLength: 16,
                message: $t('form.sysuser.userName.maxLength.errMsg'),
              },
              {
                minLength: 5,
                message: $t('form.sysuser.userName.minLength.errMsg'),
              },
            ]"
          >
            <a-input v-model="formData.userName" :disabled="!isNew" />
          </a-form-item>
          <a-form-item
            :label="$t('form.sysuser.columns.nickName')"
            field="nickName"
            :validate-trigger="['change', 'input']"
            :rules="[
              {
                required: true,
                message: $t('form.sysuser.nickName.required.errMsg'),
              },
              {
                maxLength: 20,
                message: $t('form.sysuser.nickName.maxLength.errMsg'),
              },
              {
                minLength: 2,
                message: $t('form.sysuser.nickName.minLength.errMsg'),
              },
            ]"
          >
            <a-input v-model="formData.nickName" />
          </a-form-item>

          <a-form-item
            v-if="isNew"
            :label="$t('form.sysuser.columns.password')"
            field="password"
            :validate-trigger="['change', 'input']"
            :rules="[
              {
                required: true,
                message: $t('form.sysuser.password.required.errMsg'),
              },
              {
                maxLength: 20,
                message: $t('form.sysuser.password.maxLength.errMsg'),
              },
              {
                minLength: 2,
                message: $t('form.sysuser.password.minLength.errMsg'),
              },
            ]"
          >
            <a-input v-model="formData.password" />
          </a-form-item>

          <a-form-item
            :label="$t('form.sysuser.columns.mobile')"
            field="mobile"
            :validate-trigger="['change', 'input']"
            :rules="[
              {
                type: 'number',
                message: $t(' form.sysuser.mobile.type.errMsg'),
              },
              {
                maxLength: 11,
                message: $t('form.sysuser.mobile.maxLength.errMsg'),
              },
              {
                minLength: 6,
                message: $t('form.sysuser.mobile.minLength.errMsg'),
              },
            ]"
          >
            <a-input v-model="formData.mobile" />
          </a-form-item>

          <a-form-item
            :label="$t('form.sysuser.columns.email')"
            field="email"
            :validate-trigger="['change', 'input']"
            :rules="[
              {
                type: 'email',
                message: $t('form.sysuser.email.type.errMsg'),
              },
              {
                maxLength: 20,
                message: $t('form.sysuser.email.maxLength.errMsg'),
              },
              {
                minLength: 2,
                message: $t('form.sysuser.email.minLength.errMsg'),
              },
            ]"
          >
            <a-input v-model="formData.email" />
          </a-form-item>

          <a-form-item :label="$t('form.sysuser.columns.sex')" field="sex">
            <a-select
              v-model="formData.sex"
              :placeholder="$t('form.menu.columns.sex')"
            >
              <a-option value="0"
                ><icon-man />{{ $t('form.sysuser.columns.sex.nan') }}</a-option
              >
              <a-option value="1"
                ><icon-woman />{{ $t('form.sysuser.columns.sex.nv') }}</a-option
              >
            </a-select>
          </a-form-item>

          <a-form-item
            :label="$t('form.sysuser.columns.status')"
            field="status"
          >
            <a-select
              v-model="formData.status"
              :placeholder="$t('form.menu.columns.status')"
            >
              <a-option value="0">
                <icon-unlock />{{
                  $t('form.sysuser.columns.status.normal')
                }}</a-option
              >
              <a-option value="1">
                <icon-lock />{{
                  $t('form.sysuser.columns.status.lock')
                }}</a-option
              >
            </a-select>
          </a-form-item>
        </a-spin>
      </a-form>
    </a-modal>
  </div>
</template>

<script lang="ts">
  import { defineComponent, ref } from 'vue';
  import { FormInstance } from '@arco-design/web-vue/es/form';
  import useLoading from '@/hooks/loading';
  import { useI18n } from 'vue-i18n';
  import { detail, addForm, editForm } from '@/api/sysuser';
  import { Message } from '@arco-design/web-vue';

  const formModel = () => {
    return {
      id: '',
      userName: '',
      nickName: '',
      password: '',
      email: '',
      mobile: '',
      status: '0',
      orgId: '',
      sex: '0',
      defSiteId: '',
    };
  };
  export default defineComponent({
    setup(props, context) {
      const { t } = useI18n();
      const formData = ref(formModel());
      const formRef = ref<FormInstance>();
      const { setLoading } = useLoading();
      const operate = ref<number>();
      const isNew = ref<boolean>(false);
      const onBeforeOk = async (done: (arg: boolean) => void) => {
        const res = await formRef.value?.validate();
        if (!res) {
          try {
            setLoading(true);
            if (isNew.value) {
              await addForm(formData.value);
            } else {
              await editForm(formData.value);
            }
            Message.success(t('request.ok'));
            context.emit('ok');
          } catch (e) {
            return done(false);
          } finally {
            setLoading(false);
          }
        }
        return done(!res);
      };
      const reset = async () => {
        formData.value = formModel();
        await formRef.value?.resetFields();
      };
      return {
        formRef,
        reset,
        operate,
        formData,
        isNew,
        t,
        onBeforeOk,
      };
    },
    data() {
      return {
        visible: false,
        draggable: true,
        title: '',
        loading: false,
      };
    },
    methods: {
      editForm(id: string) {
        this.reset();
        this.visible = true;
        this.title = this.t('form.edit');
        this.isNew = false;
        this.setLoading(true);
        detail(id).then((response) => {
          this.setLoading(false);
          const { sex } = response.data;
          this.formData = response.data;
          this.formData.sex = sex.toString();
        });
      },
      addForm() {
        this.reset();
        this.visible = true;
        this.isNew = true;
        this.title = this.t('form.create');
      },
      setLoading(loading: boolean) {
        this.loading = loading;
      },
      handleCancel() {
        this.reset();
        this.visible = false;
      },
    },
  });
</script>

<style scoped lang="less">
  .container {
    padding: 20px;

    .keep-margin {
      margin-bottom: 20px;
    }
  }

  .form {
    width: 420px;
  }
</style>
