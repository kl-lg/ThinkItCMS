package com.thinkit.cms.boot.entity.${tkModule?lower_case};
import lombok.Getter;
import lombok.Setter;
import java.math.BigDecimal;
import lombok.experimental.Accessors;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.thinkit.cms.model.BaseModel;
import com.baomidou.mybatisplus.annotation.TableName;
/**
* @author ${author!'System'}
*/
@Getter
@Setter
@Accessors(chain = true)
@TableName("${table.name}")
public class ${tkModule}Entity extends BaseModel  {
<#macro fieldType jdbcType="" propertyName="" columnName="">
    <#if jdbcType?? && jdbcType == "VARCHAR">
        @TableField("${columnName}")
        private String ${propertyName};
    <#elseif jdbcType?? && jdbcType == "LONGVARCHAR">
        @TableField("${columnName}")
        private String ${propertyName};
    <#elseif jdbcType?? && jdbcType == "INTEGER">
        @TableField("${columnName}")
        private Integer ${propertyName};
    <#elseif jdbcType?? && jdbcType == "BIGINT">
        @TableField("${columnName}")
        private Long ${propertyName};
    <#elseif jdbcType?? && jdbcType == "BIT">
        @TableField("${columnName}")
        private Boolean ${propertyName};
    <#elseif jdbcType?? && jdbcType == "TINYINT">
        @TableField("${columnName}")
        private Integer ${propertyName};
    <#elseif jdbcType?? && jdbcType == "TIMESTAMP">
        @TableField("${columnName}")
        private Date ${propertyName};
    <#elseif jdbcType?? && jdbcType == "DECIMAL">
        @TableField("${columnName}")
        private BigDecimal ${propertyName};
    </#if>
    <#return>
</#macro>
 <#if  table.fields ?? && (table.fields?size > 0) >
     <#list table.fields as field>
       /**
         ${field.comment}
       */
         <@fieldType jdbcType="${field.metaInfo.jdbcType}" propertyName="${field.propertyName}" columnName="${field.columnName}"/>

     </#list>
 </#if>

}