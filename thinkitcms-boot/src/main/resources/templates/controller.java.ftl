package com.thinkit.cms.boot.controller.${tkModule?lower_case};
import lombok.extern.slf4j.Slf4j;
import com.thinkit.cms.core.base.BaseController;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import com.thinkit.cms.dto.${tkModule?lower_case}.${tkModule}Dto;
import com.thinkit.cms.model.PageModel;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.List;
import java.util.Map;
import com.thinkit.cms.api.${tkModule?lower_case}.${tkModule}Service;
/**
* @author ${author!'System'}
*/

@Validated
@RestController
@RequestMapping("/api/${tkModule?uncap_first}")
@Slf4j
public class ${tkModule}Controller extends BaseController<${tkModule}Service> {


        @GetMapping(value="detail")
        public ${tkModule}Dto detail(@NotBlank(message = "ID不能为空!") @RequestParam String id){
             return service.getByPk(id);
        }

        @PostMapping(value = "/page")
        public PageModel<${tkModule}Dto> list(@RequestBody PageModel<${tkModule}Dto> pageDto) {
             return service.listPage(pageDto);
        }

        @PostMapping(value="save")
        public void save(@Validated @RequestBody ${tkModule}Dto ${tkModule?uncap_first}Dto) {
              service.insert(${tkModule?uncap_first}Dto);
        }

        @PutMapping(value="update")
        public void update(@Validated @RequestBody ${tkModule}Dto ${tkModule?uncap_first}Dto) {
              service.updateByPk(${tkModule?uncap_first}Dto);
        }


        @DeleteMapping(value="delete")
        public boolean deleteByPk(@NotBlank @RequestParam String id){
                return service.deleteByPk(id);
        }

}