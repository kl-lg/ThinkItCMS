package com.thinkit.cms.boot.mapper.${tkModule?lower_case};
import org.apache.ibatis.annotations.Mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.thinkit.cms.boot.entity.${tkModule?lower_case}.${tkModule}Entity;

/**
* @author ${author!'System'}
*/

@Mapper
public interface ${tkModule}Mapper extends BaseMapper<${tkModule}Entity> {

}