package com.thinkit.cms.boot.service.${tkModule?lower_case};
import com.thinkit.cms.api.${tkModule?lower_case}.${tkModule}Service;
import com.thinkit.cms.boot.entity.${tkModule?lower_case}.${tkModule}Entity;
import com.thinkit.cms.core.base.BaseServiceImpl;
import com.thinkit.cms.dto.${tkModule?lower_case}.${tkModule}Dto;
import com.thinkit.cms.boot.mapper.${tkModule?lower_case}.${tkModule}Mapper;
import org.springframework.stereotype.Service;
/**
* @author ${author!'System'}
*/

@Service
public class ${tkModule}ServiceImpl extends BaseServiceImpl<${tkModule}Dto,${tkModule}Entity,${tkModule}Mapper> implements ${tkModule}Service{




}