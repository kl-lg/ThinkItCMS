package com.thinkit.cms.boot.service.content;

import cn.hutool.core.date.DateField;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.extra.spring.SpringUtil;
import cn.hutool.json.JSONUtil;
import com.alicp.jetcache.anno.CacheInvalidate;
import com.alicp.jetcache.anno.CacheType;
import com.alicp.jetcache.anno.Cached;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.google.common.collect.Lists;
import com.thinkit.cms.annotation.Site;
import com.thinkit.cms.api.category.CategoryService;
import com.thinkit.cms.api.content.ContentService;
import com.thinkit.cms.api.formdata.FormDataService;
import com.thinkit.cms.api.formmodel.FormModelService;
import com.thinkit.cms.api.formmodeltmp.FormModelTmpFunService;
import com.thinkit.cms.api.related.RelatedService;
import com.thinkit.cms.api.site.SiteService;
import com.thinkit.cms.boot.entity.content.ContentEntity;
import com.thinkit.cms.boot.events.*;
import com.thinkit.cms.boot.mapper.content.ContentMapper;
import com.thinkit.cms.constant.SysConst;
import com.thinkit.cms.core.base.BaseServiceImpl;
import com.thinkit.cms.core.handler.CustomException;
import com.thinkit.cms.directive.emums.ActuatorEnum;
import com.thinkit.cms.directive.kit.Kv;
import com.thinkit.cms.directive.kit.RedisLuaUtils;
import com.thinkit.cms.directive.task.TaskDto;
import com.thinkit.cms.directive.task.TaskService;
import com.thinkit.cms.directive.wrapper.CategoryMData;
import com.thinkit.cms.directive.wrapper.ModelAndView;
import com.thinkit.cms.dto.category.CategoryDto;
import com.thinkit.cms.dto.category.PublishCategory;
import com.thinkit.cms.dto.content.*;
import com.thinkit.cms.dto.formdata.FormDataDto;
import com.thinkit.cms.dto.formmodel.FormModelDto;
import com.thinkit.cms.dto.related.RelatedDto;
import com.thinkit.cms.enums.EventEnum;
import com.thinkit.cms.enums.ModelEnum;
import com.thinkit.cms.enums.StatusEnum;
import com.thinkit.cms.enums.TaskEnum;
import com.thinkit.cms.model.ModelJson;
import com.thinkit.cms.model.PageModel;
import com.thinkit.cms.user.SiteConf;
import com.thinkit.cms.user.User;
import com.thinkit.cms.utils.Checker;
import com.thinkit.cms.utils.CmsFolderUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
* @author LG
*/
@Slf4j
@Service
public class ContentServiceImpl extends BaseServiceImpl<ContentDto,ContentEntity,ContentMapper> implements ContentService{


    @Autowired
    private FormDataService formDataService;

    @Autowired
    private FormModelService formModelService;

    @Autowired
    private FormModelTmpFunService tmpFunService;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private TaskService taskService;

    @Autowired
    private RelatedService relatedService;

    @Autowired
    private SiteService siteService;

    @Site(verifyTmp = false)
    @Override
    public PageModel<ContentDto> listPage(PageModel<ContentDto> pageModel){
        IPage page = new Page(pageModel.getCurrent(),pageModel.getPageSize());
        SiteConf siteConf = getUserCtx().getSite(true);
        String domain = CmsFolderUtil.getSiteDomain(siteConf.getDomain());
        IPage<ContentDto> result  = baseMapper.page(page,pageModel.getDto(),domain);
        return new PageModel(result);
    }

    @Transactional(rollbackFor = Exception.class)
    @Site
    @Override
    public void saveContent(ContentDto contentDto) {
        String contentId = id(),
        siteId = contentDto.getSiteId(),
        formModelId = contentDto.getFormModelId(),
        categoryId = contentDto.getCategoryId();
        contentDto.setId(contentId).setPk(contentId);
        formDataService.formData(ModelEnum.ARTICLE,contentId,siteId,formModelId, contentDto.getExtra());
        String formModelCode = formModelService.queryCode(formModelId);
        contentDto.setFormModelCode(formModelCode);
        CategoryDto categoryDto = categoryService.detail(categoryId,siteId);
        contentDto.setCategoryCode(categoryDto.getCode());
        contentDto.setClicks(0);
        contentDto.setLikes(0);
        super.insert(contentDto);
        publishEvent(new ContentEvent(contentDto, EventEnum.SAVE_CONTENT));
        ModelAndView.create(getUserCtx()).
        category(new CategoryMData(categoryId,categoryDto.getCode(),categoryDto.getPathRule(),contentId)).
        addParam(SysConst.content,buildMap(contentDto)).addActuator(ActuatorEnum.ARTICLE,formModelId,tmpFunService,null).
        addParam(SysConst.category,buildMap(categoryDto)).
        finished().execute(false);
    }


    @Cached(name = "cached::", key = "targetObject.class+'.'+#contentId" ,expire = 7200, cacheType = CacheType.REMOTE)
    @Override
    public ContentDto getDetail(String contentId) {
        ContentDto contentDto = super.getByPk(contentId);
        String cid = contentDto.getId();
        String formModelId = contentDto.getFormModelId();
        String siteId = contentDto.getSiteId();
        if(Checker.BeNotBlank(formModelId)){
            Map<String,Object> extra = formDataService.queryFormData(ModelEnum.ARTICLE,cid,siteId,formModelId);
            contentDto.setExtra(extra);
        }
        return contentDto;
    }

    @Override
    public List<ContentDto> getDetails(Collection<String> contentIds,String siteId) {
        List<ContentDto> contentDtos = baseMapper.getDetails(contentIds,siteId);
        if(Checker.BeNotEmpty(contentDtos)){
            for(ContentDto contentDto:contentDtos){
                String extraStr = contentDto.getExtraStr();
                if(Checker.BeNotBlank(extraStr)){
                    contentDto.setExtra(JSONUtil.toBean(extraStr, Map.class));
                }
                contentDto.setPk(contentDto.getId());
            }
        }
        return Checker.BeNotEmpty(contentDtos)?contentDtos:Lists.newArrayList();
    }

    @CacheInvalidate(name = "cached::", key="targetObject.class+'.'+#contentDto.id")
    @Transactional(rollbackFor = Exception.class)
    @Site
    @Override
    public void update(ContentDto contentDto) {
        String siteId = contentDto.getSiteId(),
        formModelId = contentDto.getFormModelId(),
        categoryId = contentDto.getCategoryId(),
        contentId = contentDto.getId(),
        url =  contentDto.getUrl(),
        murl = contentDto.getMUrl();
        contentDto.setPk(contentDto.getId());
        checkPk(contentId);
        CategoryDto categoryDto = categoryService.detail(categoryId,siteId);
        String formModelCode = formModelService.queryCode(formModelId);
        contentDto.setFormModelCode(formModelCode);
        contentDto.setCategoryCode(categoryDto.getCode());
        publishEvent(new ContentEvent(contentDto, EventEnum.UPDATE_CONTENT));
        super.updateByPk(contentDto);
        formDataService.formData(ModelEnum.ARTICLE,contentId,siteId,formModelId, contentDto.getExtra());
        contentDto = this.getDetail(contentId);
        Kv kv = Kv.create().setIfNotBlank(SysConst.url,url).setIfNotBlank(SysConst.murl,murl);
        ModelAndView modelAndView = ModelAndView.create(getUserCtx()).
        category(new CategoryMData(categoryId,categoryDto.getCode(),categoryDto.getPathRule(),contentId)).
        addParam(SysConst.content,buildMap(contentDto)).addParam(SysConst.category,buildMap(categoryDto)).
        addActuator(ActuatorEnum.ARTICLE,formModelId,tmpFunService,kv).finished();
        modelAndView.execute(false);
    }

    @Override
    public Set<String> tags() {
        Set<String> tagSet = new HashSet<>(16);
        List<String> tagStr = baseMapper.queryTag(getSiteId(true));
        if(Checker.BeNotEmpty(tagStr)){
             for(String tag:tagStr){
                 if(Checker.BeNotBlank(tag)){
                      if(tag.contains(",")){
                          String[] tags = tag.split(",");
                          tagSet.addAll(Arrays.asList(tags));
                      }else{
                          tagSet.add(tag);
                      }
                 }
             }
        }
        return tagSet;
    }

    @Override
    public void updateTag(List<String> tags, String id) {
        if(Checker.BeNotEmpty(tags) && Checker.BeNotBlank(id)){
            ContentService contentService = SpringUtil.getBean(ContentService.class);
            String topTag = StringUtils.join(tags,",");
            String siteId = getSiteId(true);
            baseMapper.updateTag(topTag,id,siteId);
            ContentDto contentDto = new ContentDto();
            contentDto.setId(id);
            contentDto.setTopTag(topTag);
            contentDto.setFormModelCode(contentService.queryFormModelCode(id));
            publishEvent(new ContentEvent(contentDto, EventEnum.UPDATE_TAG));
        }
    }


    @Cached(name = "cached::", key = "targetObject.class+'.queryFormModelCode.'+#contentId" ,expire = 7200, cacheType = CacheType.REMOTE)
    @Override
    public String queryFormModelCode(String contentId) {
        String formModelCode = baseMapper.queryFormCode(contentId);
        return  formModelCode;
    }

    @Override
    public Long queryViews(String contentId) {
        return baseMapper.queryViews(contentId);
    }

    @Override
    public Long queryClicks(String contentId) {
        return baseMapper.queryClicks(contentId);
    }

    @Override
    public void topIt(SelectRows ckRow) {
        List<String> ids = ckRow.getCkRowList();
        if(Checker.BeNotEmpty(ids) && Checker.BeNotNull(ckRow.getStatus())){
            clearKey(ids);
            baseMapper.topIt(ids,ckRow.getStatus());
            List<String> keys = baseMapper.queryKeys(SysConst.contentPrefix,SysConst.none,SysConst.colon,ids);
            publishEvent(new ContentEvent(new Rows(keys,ckRow.getStatus()),EventEnum.TOP_CONTENT));
        }
    }

    @Override
    public void recomdIt(SelectRows ckRow) {
        List<String> ids = ckRow.getCkRowList();
        if(Checker.BeNotEmpty(ids) && Checker.BeNotNull(ckRow.getStatus())){
            clearKey(ids);
            baseMapper.recomdIt(ckRow.getCkRowList(),ckRow.getStatus());
            List<String> keys = baseMapper.queryKeys(SysConst.contentPrefix,SysConst.none,SysConst.colon,ids);
            publishEvent(new ContentEvent(new Rows(keys,ckRow.getStatus()),EventEnum.RECOMD_CONTENT));
        }
    }


    @Override
    public void headIt(SelectRows ckRow) {
        List<String> ids = ckRow.getCkRowList();
        if(Checker.BeNotEmpty(ids) && Checker.BeNotNull(ckRow.getStatus())){
            clearKey(ids);
            baseMapper.headIt(ids,ckRow.getStatus());
            List<String> keys = baseMapper.queryKeys(SysConst.contentPrefix,SysConst.none,SysConst.colon,ids);
            publishEvent(new ContentEvent(new Rows(keys,ckRow.getStatus()),EventEnum.HEAD_CONTENT));
        }
    }

    @Override
    public void upViews(String contentId, Long views) {
        if(Checker.BeNotNull(views) && views.intValue()>=1){
            baseMapper.upViews(contentId,views);
            syncToRedisSearch(contentId,"clicks",views.intValue());
        }
    }

    @Override
    public void upLikes(String contentId, Long likes) {
        if(Checker.BeNotNull(likes) && likes.intValue()>=1){
            baseMapper.upLikes(contentId,likes);
            syncToRedisSearch(contentId,"likes",likes.intValue());
        }
    }

    private void syncToRedisSearch(String contentId,String field,int value){
        ContentService contentService = SpringUtil.getBean(ContentService.class);
        String formModelCode = contentService.queryFormModelCode(contentId);
        ModelJson modelJson = new ModelJson();
        modelJson.setPrefix(SysConst.contentPrefix);
        modelJson.setPk(contentId);
        String key = modelJson.buildKey(formModelCode,SysConst.colon);
        modelJson.setKey(key);
        Map map = RedisLuaUtils.getJson(true,key);
        if(Checker.BeNotEmpty(map)){
            map.put(field,value);
        }
        modelJson.setObjectMap(map);
        RedisLuaUtils.setJson(true,modelJson);
    }

    private void clearKey(List<String> ids ){
        ids.forEach(id->{
            String key = "cached::"+this.getClass()+SysConst.point+id;
            redisTemplate.delete(key);
        });
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void publish(RowsDto rowsDto) {
           List<CkRow> ckRows = rowsDto.getCkRowList();
           if(Checker.BeNotEmpty(ckRows)){
               Set<String> categoryIds = new HashSet<>(16);
               Integer status = rowsDto.getStatus();
               List<String> ids = new ArrayList<>();
               List<String> dels = new ArrayList<>();
               ckRows.forEach(ckRow -> {
                   ids.add(ckRow.getId());
                   categoryIds.add(ckRow.getCategoryId());
                   if(StatusEnum.DELETED.getVal().equals(status)){
                       delFile(ckRow,status);
                   }
               });
               Boolean  async = rowsDto.getAsync();
               String userId =async?rowsDto.getUser().getId():getUserId();
               baseMapper.publish(ids,status,userId);
               List<ContentDto> contentDtos = super.getByPks(ids);
               if(Checker.BeNotEmpty(contentDtos)){
                   contentDtos.forEach(contentDto->contentDto.setStatus(status));
               }
               publishEvent(new ContentEvent(contentDtos, EventEnum.UPDATE_BATCH_STATUS));
               if(Checker.BeNotEmpty(dels)){
                   dels.forEach(delFile->{
                       FileUtil.del(delFile);
                   });
               }
               try {
                   publishCategory(categoryIds,async,rowsDto.getUser());
               }catch (Exception e){
                   log.error("文章状态变动触发定时任务失败：{}",e.getMessage());
               }
           }
    }

    private void delFile(CkRow ckRow,Integer status){
        if(StatusEnum.DELETED.getVal().equals(status)){
            ContentService contentService = SpringUtil.getBean(ContentService.class);
            ContentDto contentDto = contentService.getDetail(ckRow.getId());
            String domain = siteService.getDoamin("",contentDto.getSiteId());
            if(Checker.BeNotBlank(contentDto.getMUrl())){
                String mrul = CmsFolderUtil.getSitePathUrl(null,domain,contentDto.getMUrl());
                Boolean isOk = FileUtil.del(mrul);
                log.info("删除M html 文件：{}",isOk);
            }
            if(Checker.BeNotBlank(contentDto.getUrl())){
                String url = CmsFolderUtil.getSitePathUrl(null,domain,contentDto.getUrl());
                Boolean isOk = FileUtil.del(url);
                log.info("删除 html 文件：{}",isOk);
            }
        }
    }

    @Override
    public void asyncPublish(RowsDto rowsDto) {
        rowsDto.setAsync(true);
        publish(rowsDto);
    }

    @Override
    public void jobPublish(RowsDto rowsDto) {
          String uuid = UUID.randomUUID().toString();
          Date nowTime = new Date();
          Date publishTime = DateUtil.parse(rowsDto.getPublishTime(),"yyyy-MM-dd HH:mm:ss");
          if(publishTime.before(nowTime)){
              throw new CustomException(5059);
          }
          rowsDto.setStatus(StatusEnum.PUBLISH.getVal());
          rowsDto.setUser(getUserCtx());
          Long second = DateUtil.between(nowTime,publishTime, DateUnit.SECOND);
          String key= SysConst.expiredContent+uuid;
          redisTemplate.opsForValue().set(key,null,second, TimeUnit.SECONDS);
          redisTemplate.opsForValue().set(key+SysConst.expKey,rowsDto);
    }

    /**
     * 栏目发布
     * @param categoryIds
     */
    private void publishCategory(Set<String> categoryIds, Boolean async, User user){
        if(Checker.BeNotEmpty(categoryIds)){
            List<Date> startEndTime =new ArrayList<>();
            DateTime endTime = new DateTime();
            Date startTime = DateUtil.offset(endTime, DateField.MONTH,-3);
            startEndTime.add(startTime);
            startEndTime.add(endTime);
            for(String categoryId:categoryIds){
                PublishCategory publishCategory = new PublishCategory();
                publishCategory.setCategoryId(categoryId);
                publishCategory.setContainChild(false);
                publishCategory.setContainTimes(true);
                publishCategory.setStartEndTime(startEndTime);
                publishCategory.setAsync(async);
                publishCategory.setUser(user);
                categoryService.createTmpHome(publishCategory);
            }
        }
    }

    @Site
    @Override
    public void generate(RowsDto rowsDto) {
        List<CkRow> ckRows = rowsDto.getCkRowList();
        if(Checker.BeNotEmpty(ckRows)){
            String key = taskService.genTaskKey(TaskEnum.ARTICLE_TASK,rowsDto.getSiteId(),"BATCH");
            TaskDto taskDto = new TaskDto();
            taskDto.setAction(TaskEnum.ARTICLE_TASK.getVal());
            taskDto.setSiteId(rowsDto.getSiteId());
            String taskId = taskService.startTask(key,taskDto);
            taskService.upProgress(taskId, SysConst.count,ckRows.size());
            for(CkRow ckRow:ckRows){
                String formModelId = ckRow.getFormModelId(),
                categoryId = ckRow.getCategoryId(),
                contentId = ckRow.getId();
                CategoryDto categoryDto = categoryService.detail(categoryId,rowsDto.getSiteId());
                ContentService contentService =  SpringUtil.getBean(ContentService.class);
                ContentDto contentDto = contentService.getDetail(contentId);
                Kv kv = Kv.create().setIfNotBlank(SysConst.url,contentDto.getUrl()).
                setIfNotBlank(SysConst.murl,contentDto.getMUrl());
                CategoryMData categoryMData =new CategoryMData(categoryId,categoryDto.getCode(),categoryDto.getPathRule(),contentId);
                categoryMData.setForceGen(rowsDto.getForceGen());
                ModelAndView modelAndView = ModelAndView.create(getUserCtx()).
                category(categoryMData).
                addParam(SysConst.content,buildMap(contentDto)).addParam(SysConst.category,buildMap(categoryDto)).
                addActuator(ActuatorEnum.ARTICLE,formModelId,tmpFunService,kv).taskId(taskId);
                modelAndView.finished().execute(rowsDto.getAsync());
            }
        }
    }


    @Override
    public void genContent(RowsDto rowsDto,String takeId) {
        List<CkRow> ckRows = rowsDto.getCkRowList();
        if(Checker.BeNotEmpty(ckRows)){
            String siteId= rowsDto.getSiteId();
            Set<String> contentIds = new HashSet<>(16);
            Set<String> categoryIds = new HashSet<>(16);
            Map<String,CategoryDto> categoryDtoMap = new HashMap<>();
            ckRows.forEach(ckRow->{
                String categoryId = ckRow.getCategoryId();
                contentIds.add(ckRow.getId());
                if(!categoryIds.contains(categoryId)){
                    CategoryDto categoryDto = categoryService.detail(categoryId,siteId);
                    categoryDtoMap.put(categoryId,categoryDto);
                    categoryIds.add(ckRow.getCategoryId());
                }
            });
            int pageSize = 1000;
            int size = contentIds.size();
            int pageCount = (size + pageSize - 1) / pageSize;
            for(int pageNo=1;pageNo<=pageCount;pageNo++){
                if(taskService.isStop(takeId)){
                    break;
                }
                List<String> contentIdList = contentIds.stream().skip((pageNo -1)*pageSize).limit(pageSize).
                collect(Collectors.toList());
                List<ContentDto> contents = this.getDetails(contentIdList,siteId);
                genDo(contents,categoryDtoMap,rowsDto,pageNo,takeId);
            }
        }
    }

    @Override
    public void syncEsData(String taskId, String siteId,String categryId) {
        List<CkRow> ckRowList =  this.getContents(siteId,categryId);
        Set<String> categoryIds = new HashSet<>(16);
        Set<String> cids = categoryService.listIds(siteId);
        if(Checker.BeNotEmpty(cids)){
            categoryIds.addAll(cids);
        }
        if(Checker.BeNotEmpty(ckRowList)) {
            Set<String> contentIds = new HashSet<>(16);
            List<CategoryDto> categoryDtos = new ArrayList<>();
            ckRowList.forEach(ckRow->{
                contentIds.add(ckRow.getId());
            });
            categoryIds.forEach(categoryId->{
                CategoryDto categoryDto = categoryService.detail(categoryId,siteId);
                if(Checker.BeBlank(categoryDto.getFormModelCode())){
                    categoryDto.setFormModelCode(formModelService.queryCode(categoryDto.getFormModelId()));
                }
                categoryDtos.add(categoryDto);
            });
            List<RelatedDto> relatedDtos = relatedService.getDatas(siteId);
            publishEvent(new RelatedEvent(relatedDtos,EventEnum.SAVE_BATCH_RELATED),
            new CategoryEvent(categoryDtos,EventEnum.SAVE_BATCH_CATEGORY));
            Map<String,Object> objectMap = new HashMap<>();
            objectMap.put("site_id",siteId);
            List<FormModelDto> formModelDtos =   formModelService.listMap(objectMap);
            List<FormDataDto> formDataDtos =   formDataService.listMap(objectMap);
            formatModelCode(formDataDtos);
            publishEvent(new FormModelEvent(formModelDtos,EventEnum.SAVE_BATCH_FORM_MODEL),
            new FormDataEvent(formDataDtos,EventEnum.SAVE_BATCH_FORM_DATA));
            int pageSize = 1000;
            int size = contentIds.size();
            int pageCount = (size + pageSize - 1) / pageSize;
            BigDecimal count=new BigDecimal(pageCount);
            taskService.upProgress(taskId,SysConst.count,count.intValue());
            for(int pageNo=1;pageNo<=pageCount;pageNo++){
                if(taskService.isStop(taskId)){
                    break;
                }
                List<String> contentIdList = contentIds.stream().skip((pageNo -1)*pageSize).limit(pageSize).
                collect(Collectors.toList());
                List<ContentDto> contents = this.getDetails(contentIdList,siteId);
                publishEvent(new ContentEvent(contents,EventEnum.SAVE_BATCH_CONTENT));
                taskService.upProgress(taskId, SysConst.okTimes,1);
                log.info("同步第{}页数",pageNo);
            }
        }
    }

    private void formatModelCode(List<FormDataDto> formDataDtos){
        if(Checker.BeNotEmpty(formDataDtos)){
            for(FormDataDto formDataDto:formDataDtos){
                if(Checker.BeNotBlank(formDataDto.getFormModelId())){
                    String formModelCode = formModelService.queryCode(formDataDto.getFormModelId());
                    formDataDto.setFormModelCode(formModelCode);
                }
            }
        }
    }


    @Site(verifyTmp = false)
    @Override
    public PageModel<ContentDto> listRecycle(PageModel<ContentDto> pageModel) {
        IPage page = new Page(pageModel.getCurrent(),pageModel.getPageSize());
        SiteConf siteConf = getUserCtx().getSite(true);
        String domain = CmsFolderUtil.getSiteDomain(siteConf.getDomain());
        IPage<ContentDto> result  = baseMapper.listRecycle(page,pageModel.getDto(),domain);
        return new PageModel(result);
    }

    @Override
    public void reduction(RowsDto ckRow) {
         if(Checker.BeNotEmpty(ckRow.getIds())){
             baseMapper.reduction(ckRow.getIds(),StatusEnum.DEFAULT.getVal(), getSiteId(true));
             List<ContentDto> contentDtos = super.getByPks(ckRow.getIds());
             if(Checker.BeNotEmpty(contentDtos)){
                 contentDtos.forEach(contentDto->contentDto.setStatus(StatusEnum.DEFAULT.getVal()));
             }
             publishEvent(new ContentEvent(contentDtos, EventEnum.UPDATE_BATCH_STATUS));
         }
    }

    @Override
    public void readDel(RowsDto ckRow) {
        if(Checker.BeNotEmpty(ckRow.getIds())){
            List<ContentDto> contentDtos = super.getByPks(ckRow.getIds());
            baseMapper.deletes(ckRow.getIds(),getSiteId(true));
            publishEvent(new ContentEvent(contentDtos, EventEnum.DELETE_BATCH_CONTENT));
        }
    }

    @Override
    public void updateField(String id, String field, Object val) {
         baseMapper.updateField(id,field,val);
    }


    private void genDo(List<ContentDto> contentDtos,Map<String,CategoryDto> categoryDtoMap,RowsDto rowsDto,Integer pageNo,String taskId){
        String siteId= rowsDto.getSiteId();
        User user = rowsDto.getUser();
        String templateId = user.getSite(true,user.getId()).getTemplateId();
        Integer index = 0;
        for(ContentDto contentDto:contentDtos){
            if(taskService.isStop(taskId)){
                 break;
            }
            String categoryId = contentDto.getCategoryId();
            String contentId = contentDto.getId();
            String formModelId = contentDto.getFormModelId();
            CategoryDto categoryDto =categoryDtoMap.get(categoryId);
            Kv kv = Kv.create().setIfNotBlank(SysConst.url,contentDto.getUrl()).
            setIfNotBlank(SysConst.murl,contentDto.getMUrl());
            CategoryMData categoryMData =new CategoryMData(categoryId,categoryDto.getCode(),categoryDto.getPathRule(),contentId);
            categoryMData.setForceGen(rowsDto.getForceGen());
            ModelAndView modelAndView = ModelAndView.create(user,siteId,templateId).
            category(categoryMData).
            addParam(SysConst.content,buildMap(contentDto)).addParam(SysConst.category,buildMap(categoryDto)).
            addActuator(ActuatorEnum.ARTICLE,formModelId,tmpFunService,kv).taskId(taskId);
            modelAndView.finished().execute(rowsDto.getAsync());
            index+=1;
            log.info("生成第{}页数",pageNo);
            Integer count = ((pageNo-1)*1000)+index;
            log.info("生成第{}条数",count);
        }
    }

    @Override
    public List<CkRow> getContents(String siteId,String categoryId) {
        List<CkRow> rows = baseMapper.getContents(siteId,StatusEnum.DELETED.getVal(), categoryId);
        return Checker.BeNotEmpty(rows)?rows: Lists.newArrayList();
    }


}