package com.thinkit.cms.boot.directive.handler;

import cn.hutool.core.bean.BeanUtil;
import com.thinkit.cms.boot.directive.model.MenuTree;
import com.thinkit.cms.core.utils.DozerUtils;
import com.thinkit.cms.directive.annotation.Handler;
import com.thinkit.cms.directive.handler.AbstractDataHandler;
import com.thinkit.cms.directive.render.BaserRender;
import com.thinkit.cms.dto.sysorganize.SysOrganizeTree;
import com.thinkit.cms.utils.BuildTree;
import com.thinkit.cms.utils.Tree;

import java.util.ArrayList;
import java.util.List;

@Handler(value = "menu")
public class MenuHand extends AbstractDataHandler {

    @Override
    public Object getData(Object value, BaserRender render) {
        List<MenuTree> menuTrees = toList(value, MenuTree.class);

        List<Tree> trees = new ArrayList<>(16);
        Tree tree = BuildTree.build(mentToTree(menuTrees, MenuTree.class));
        if("-1".equals(tree.getId())){
            trees.addAll(tree.getChildren());
        }else{
            trees.add(tree);
        }
        return trees;
    }


    private <T>List<Tree<T>> mentToTree(List<MenuTree> menus, Class clz){
        List<Tree<T>> trees = new ArrayList<>();
        for(MenuTree menu:menus){
            try {
                MenuTree tree = (MenuTree) clz.getConstructor().newInstance();
                BeanUtil.copyProperties(menu,tree);
                trees.add(tree);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return trees;
    }
    protected void copy(Object dest,Object target){
        BeanUtil.copyProperties(dest,target);
    }
}
