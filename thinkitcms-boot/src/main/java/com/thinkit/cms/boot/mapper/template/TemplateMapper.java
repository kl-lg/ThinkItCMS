package com.thinkit.cms.boot.mapper.template;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.thinkit.cms.dto.template.TemplateDto;
import org.apache.ibatis.annotations.Mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.thinkit.cms.boot.entity.template.TemplateEntity;
import org.apache.ibatis.annotations.Param;

/**
* @author LG
*/

@Mapper
public interface TemplateMapper extends BaseMapper<TemplateEntity> {


    /**
     * 是否存在
     * @param id
     * @param code
     * @return
     */
    int isExist(@Param("id") String id, @Param("code") String code);


    /**
     * 查找你默认
     * @param siteId
     * @return
     */
    String getDefTemp(@Param("siteId") String siteId);

    /**
     * 查找模板路径
     * @param templateId
     * @return
     */
    String loadTempFolder(@Param("templateId") String templateId);

    /**
     * 根据站点id获取 模板路径
     * @param siteId
     * @return
     */
    String loadTempPathBySiteId(@Param("siteId") String siteId);

    void updateFileUrl(@Param("templateId") String templateId, @Param("url") String url);

    IPage<TemplateDto> listPage(IPage page, @Param("dto") TemplateDto dto);
}