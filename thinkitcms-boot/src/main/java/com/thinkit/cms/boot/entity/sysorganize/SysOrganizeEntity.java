package com.thinkit.cms.boot.entity.sysorganize;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.thinkit.cms.model.BaseModel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
/**
* @author LG
*/
@Getter
@Setter
@Accessors(chain = true)
@TableName("tk_sys_organize")
public class SysOrganizeEntity extends BaseModel {
       /**
         父组织编码
       */
        @TableField("pid")
        private String pid;

       /**
         组织编码
       */
        @TableField("org_code")
        private String orgCode;

       /**
         组织名称
       */
        @TableField("org_name")
        private String orgName;

       /**
         是否删除 0：正常 1：删除
       */
        @TableField("deleted")
        private Boolean deleted;


        @TableField("`order`")
        private Integer order;

}