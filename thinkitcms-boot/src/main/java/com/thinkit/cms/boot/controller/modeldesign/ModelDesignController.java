package com.thinkit.cms.boot.controller.modeldesign;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.thinkit.cms.annotation.Logs;
import com.thinkit.cms.annotation.Site;
import com.thinkit.cms.api.modeldesign.ModelDesignService;
import com.thinkit.cms.authen.annotation.Sentinel;
import com.thinkit.cms.core.base.BaseController;
import com.thinkit.cms.dto.modeldesign.ModelDesignDto;
import com.thinkit.cms.dto.modeldesign.SaveModelDesign;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import java.util.List;

import static com.thinkit.cms.enums.LogModule.MODEL;

/**
* @author LG
*/

@Validated
@RestController
@RequestMapping("/api/modelDesign")
@Slf4j
public class ModelDesignController extends BaseController<ModelDesignService> {



        @Sentinel
        @SaCheckPermission("model:design")
        @Site
        @PostMapping(value = "/saveMdesign")
        public void saveMdesign(@Validated @RequestBody SaveModelDesign saveModelDesign) {
            service.saveMdesign(saveModelDesign);
        }

        @Logs(module = MODEL,operation = "模型设计详情查看")
        @GetMapping(value="detail")
        public List<ModelDesignDto> detail(@NotBlank(message = "模型ID不能为空!") @RequestParam String modelId){
             return service.detail(modelId);
        }


        @Sentinel
        @SaCheckPermission("model:index")
        @Site
        @PostMapping(value = "/createIndex")
        public void createIndex(@Validated @RequestBody SaveModelDesign saveModelDesign) {
                service.createIndex(saveModelDesign);
        }

}