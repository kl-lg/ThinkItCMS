package com.thinkit.cms.boot.controller.sysrolemenu;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.thinkit.cms.annotation.Logs;
import com.thinkit.cms.annotation.ValidGroup1;
import com.thinkit.cms.api.sysrolemenu.SysRoleMenuService;
import com.thinkit.cms.authen.annotation.Sentinel;
import com.thinkit.cms.core.base.BaseController;
import com.thinkit.cms.dto.sysrolemenu.SysRoleAssignDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.thinkit.cms.enums.LogModule.ROLE;

/**
* @author LG
*/

@Validated
@RestController
@RequestMapping("/api/sysRoleMenu")
@Slf4j
public class SysRoleMenuController extends BaseController<SysRoleMenuService> {


    @Logs(module = ROLE,operation = "角色分派菜单")
    @Sentinel
    @SaCheckPermission("role:setmenu")
    @PutMapping(value = "/saveTreeAssign")
    public void saveTreeAssign(@Validated(value = {ValidGroup1.class}) @RequestBody SysRoleAssignDto roleAssignDto) {
        service.saveTreeAssign(roleAssignDto);
    }

}