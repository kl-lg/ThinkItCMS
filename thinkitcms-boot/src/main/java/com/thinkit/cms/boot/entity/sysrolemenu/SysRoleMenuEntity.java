package com.thinkit.cms.boot.entity.sysrolemenu;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.thinkit.cms.model.BaseModel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
/**
* @author LG
*/
@Getter
@Setter
@Accessors(chain = true)
@TableName("tk_sys_role_menu")
public class SysRoleMenuEntity extends BaseModel {
       /**
         角色ID
       */
        @TableField("role_id")
        private String roleId;

       /**
         菜单ID
       */
        @TableField("menu_id")
        private String menuId;


}