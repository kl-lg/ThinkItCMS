package com.thinkit.cms.boot.jobs;

import com.thinkit.cms.api.content.ContentService;
import com.thinkit.cms.api.site.SiteService;
import com.thinkit.cms.constant.SysConst;
import com.thinkit.cms.core.utils.TaskUtils;
import com.thinkit.cms.utils.Checker;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Set;

/**
 * 栏目定时
 * @author LONG
 */
@Slf4j
@Component
public class ClickViewTotalTask {


    @Autowired
    private SiteService siteService;

    @Autowired
    private ContentService contentService;

    @Autowired
    private TaskUtils taskUtils;

    @Autowired
    private RedisTemplate<String,Object> redisTemplate;

    /**
     *  每1分钟 执行一次扫描
     */
    @Scheduled(cron = "0 0/1 * * * ?")
    public void executeTask() {
        synchronized (this){
            Set<String> siteIds = siteService.listSiteIds();
            if(Checker.BeNotEmpty(siteIds)){
                log.info("==========开始扫描查询统计==========");
                for(String siteId:siteIds){
                    String key = SysConst.clickView+siteId+SysConst.colon+SysConst.starChar;
                    Set<String> viewKeys = taskUtils.scan(key);
                    // 同步浏览数
                    syncViews(viewKeys);

                    String key1 = SysConst.clickTop+siteId+SysConst.colon+SysConst.starChar;
                    Set<String> likeKeys = taskUtils.scan(key1);
                    // 同步点赞数
                    syncLikes(likeKeys);
                }
            }
        }
    }

    public void syncViews(Set<String> viewKeys) {
        if(Checker.BeNotEmpty(viewKeys)){
            // total:view:1735587909601202176:1735675145449508864
            log.info("==========开始扫描查询统计 click==========");
            for(String viewKey:viewKeys){
                String contentId = viewKey.split(":")[3];
                Object data = redisTemplate.opsForValue().get(viewKey);
                if(Checker.BeNotNull(data)){
                    Long views = Long.valueOf(data.toString());
                    contentService.upViews(contentId,views);
                    redisTemplate.delete(viewKey);
                }
            }
        }
    }

    public void syncLikes(Set<String> viewKeys) {
        if(Checker.BeNotEmpty(viewKeys)){
            // total:view:1735587909601202176:1735675145449508864
            log.info("==========开始扫描查询统计 likes==========");
            for(String likeKey:viewKeys){
                String contentId = likeKey.split(":")[3];
                Object data = redisTemplate.opsForValue().get(likeKey);
                if(Checker.BeNotNull(data)){
                    Long views = Long.valueOf(data.toString());
                    contentService.upLikes(contentId,views);
                    redisTemplate.delete(likeKey);
                }
            }
        }
    }
}
