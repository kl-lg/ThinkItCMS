package com.thinkit.cms.boot.jobs;
import com.thinkit.cms.api.category.CategoryService;
import com.thinkit.cms.api.site.SiteService;
import com.thinkit.cms.utils.Checker;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Set;

/**
 * 首页定时
 * @author LONG
 */
@Slf4j
@Component
public class IndexTask {

    private final SiteService siteService;


    public IndexTask(SiteService siteService) {
        this.siteService = siteService;
    }

    /**
     *  每1分钟 执行一次扫描
     */
    @Scheduled(cron = "0 0/3 * * * ?")
    public void executeTask() {
         Set<String> siteIds = siteService.listSiteIds();
         if(Checker.BeNotEmpty(siteIds)){
           log.info("==========首页定时生产定时==========");
           for(String siteId:siteIds){
               synchronized (siteService){
                   siteService.jonIndex(siteId);
               }
           }
         }
    }
}
