package com.thinkit.cms.boot.config;

import lombok.Data;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import redis.clients.jedis.JedisPooled;
import redis.clients.jedis.UnifiedJedis;

@Data
@Configuration
@ConfigurationProperties(prefix = "jedis")
public class JedisConfig {

    @Value("${spring.redis.port}")
    private Integer  port;

    @Value("${spring.redis.host}")
    private String  host;

    @Value("${spring.redis.password}")
    private String  password;

    @Value("${spring.redis.database}")
    private Integer  database;

    private Integer  timeout=10000;


    @Bean
    public UnifiedJedis unifiedJedis() {
        UnifiedJedis jedis = new JedisPooled(poolConfig(),host, port,timeout,password,database);
        return jedis;
    }

    @Bean
    public GenericObjectPoolConfig poolConfig(){
        GenericObjectPoolConfig config = new GenericObjectPoolConfig();
        config.setMaxTotal(100); // 设置连接池中最多允许放100个Jedis对象
        config.setMaxIdle(100);// 设置连接池中最大允许空闲连接
        config.setMinIdle(10);// 设置连接池中最小允许的连接数
        config.setTestOnBorrow(false); // 借出连接的时候是否测试有效性,推荐false
        config.setTestOnReturn(false); // 归还时是否测试,推荐false
        config.setTestOnCreate(false); // 创建时是否测试有效  开发的时候设置为false,实践运行的时候设置为true
        config.setBlockWhenExhausted(true); // 当连接池内jedis无可用资源时,是否等待资源,true
        config.setMaxWaitMillis(1000);// 没有获取资源时最长等待1秒,1秒后
        return config;
    }
}
