package com.thinkit.cms.boot.funs.instruct;
import com.thinkit.cms.api.category.CategoryService;
import com.thinkit.cms.constant.SysConst;
import com.thinkit.cms.directive.annotation.Instruct;
import com.thinkit.cms.directive.funs.AbsFun;
import com.thinkit.cms.directive.funs.BaseFunInterface;
import com.thinkit.cms.directive.render.BaserRender;
import com.thinkit.cms.utils.Checker;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Instruct(value = "CgyContentInstruct",description = "获取指令栏目下的文章")
@Component
public class CategoryContentInstruct extends AbsFun implements BaseFunInterface {

    @Autowired
    private CategoryService categoryService;

    @Override
    public void execute(BaserRender render) throws Exception {
            String categoryId = render.getString(SysConst.categoryId);
            String modelCode = render.getString(SysConst.modelCode);
            Integer pageNum = render.getInteger(SysConst.pageNum);
            Integer pageSize = render.getInteger(SysConst.pageSize);
            if(Checker.BeBlank(modelCode)){
                modelCode = categoryService.queryCode(categoryId);
            }

    }
}
