package com.thinkit.cms.boot.service.formdata;

import cn.hutool.json.JSONUtil;
import com.alicp.jetcache.anno.CacheInvalidate;
import com.alicp.jetcache.anno.CacheType;
import com.alicp.jetcache.anno.Cached;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.thinkit.cms.annotation.Site;
import com.thinkit.cms.api.formdata.FormDataService;
import com.thinkit.cms.api.formmodel.FormModelService;
import com.thinkit.cms.boot.entity.formdata.FormDataEntity;
import com.thinkit.cms.boot.events.FormDataEvent;
import com.thinkit.cms.boot.mapper.formdata.FormDataMapper;
import com.thinkit.cms.constant.SysConst;
import com.thinkit.cms.core.base.BaseServiceImpl;
import com.thinkit.cms.dto.formdata.FormDataDto;
import com.thinkit.cms.enums.EventEnum;
import com.thinkit.cms.enums.ModelEnum;
import com.thinkit.cms.model.ModelJson;
import com.thinkit.cms.model.PageModel;
import com.thinkit.cms.utils.Checker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
* @author LG
*/

@Service
public class FormDataServiceImpl extends BaseServiceImpl<FormDataDto,FormDataEntity,FormDataMapper> implements FormDataService{


    @Autowired
    private FormModelService formModelService;

    @CacheInvalidate(name = "cached::",  key = "targetObject.class+'.'+#modelEnum.val+'.'+#rowId+'.'+#siteId+'.'+#formModelId")
    @Override
    public void formData(ModelEnum modelEnum, String rowId,String siteId, String formModelId, Map<String, Object> extra) {
         boolean isOk = Checker.BeNotBlank(siteId) && Checker.BeNotBlank(formModelId) && Checker.BeNotEmpty(extra)
                 && Checker.BeNotBlank(rowId);
         if(isOk){
             baseMapper.deleteData(rowId,modelEnum.getVal(),siteId,formModelId);
             FormDataDto formDataDto = new FormDataDto(formModelId, JSONUtil.toJsonStr(extra),siteId);
             formDataDto.setRowId(rowId);
             formDataDto.setModual(modelEnum.getVal());
             super.insert(formDataDto);
         }
    }

    @Cached(name = "cached::", key = "targetObject.class+'.'+#modelEnum.val+'.'+#rowId+'.'+#siteId+'.'+#formModelId",
    expire = 7200, cacheType = CacheType.REMOTE)
    @Override
    public Map<String, Object> queryFormData(ModelEnum modelEnum, String rowId, String siteId, String formModelId) {
        Map<String, Object> objectMap = new HashMap<>(16);
        String extra = baseMapper.queryFormData(modelEnum.getVal(),rowId,siteId,formModelId);
        if(Checker.BeNotEmpty(extra)){
            objectMap = JSONUtil.toBean(extra,Map.class);
        }
        return objectMap;
    }

    @Site
    @Override
    public void saveExtra(FormDataDto formData) {
         String id = id();
         String formModelId = formData.getFormModelId();
         FormDataEntity formDataEntity = new FormDataEntity();
         formDataEntity.setFormModelId(formModelId);
         formDataEntity.setModual(formData.getModual());
         formDataEntity.setSiteId(formData.getSiteId());
         formDataEntity.setCreateId(getUserId());
         formDataEntity.setGmtCreate(new Date());
         formDataEntity.setId(id);
         formDataEntity.setRowId(id);
         formData.setId(id);
         if(Checker.BeNotNull(formData.getExtra())){
             formDataEntity.setFormData(JSONUtil.toJsonStr(formData.getExtra()));
         }
         if(Checker.BeNotBlank(formModelId)){
             String formModelCode = formModelService.queryCode(formModelId);
             formDataEntity.setFormModelCode(formModelCode);
         }
         publishEvent(new FormDataEvent(T2D(formDataEntity),EventEnum.SAVE_FORM_DATA));
         super.save(formDataEntity);
    }

    @Override
    public FormDataDto detail(String id,Boolean isExtra) {
        FormDataDto formDataDto = super.getByPk(id);
        if(Checker.BeNotBlank(formDataDto.getFormData())){
             if(isExtra){
                 formDataDto.setExtra(JSONUtil.toBean(formDataDto.getFormData(),Map.class));
             }
        }
        return formDataDto;
    }

    @Override
    public void updateExtra(FormDataDto formData) {
        FormDataEntity formDataEntity = baseMapper.selectById(formData.getId());
        formDataEntity.setModual(formData.getModual());
        formDataEntity.setSiteId(formData.getSiteId());
        formDataEntity.setModifiedId(getUserId());
        formDataEntity.setGmtModified(new Date());
        formDataEntity.setRowId(formData.getRowId());
        if(Checker.BeNotNull(formData.getExtra())){
            formDataEntity.setFormData(JSONUtil.toJsonStr(formData.getExtra()));
        }
        if(Checker.BeBlank(formDataEntity.getFormModelCode())){
            String formModelCode = formModelService.queryCode(formDataEntity.getFormModelId());
            formDataEntity.setFormModelCode(formModelCode);
        }
        super.updateById(formDataEntity);
        publishEvent(new FormDataEvent(T2D(formDataEntity),EventEnum.UPDATE_FORM_DATA));
    }

    @Override
    public boolean delete(String id) {
        FormDataDto formDataDto = super.getByPk(id);
        ModelJson modelJson  = new ModelJson();
        modelJson.setPrefix(SysConst.formDataPrefix);
        modelJson.setPk(id);
        String key = modelJson.buildKey(formDataDto.getFormModelCode(),SysConst.colon);
        modelJson.setKey(key);
        publishEvent(new FormDataEvent(modelJson,EventEnum.DELETE_FORM_DATA));
        return super.deleteByPk(id);
    }


    @Site
    @Override
    public PageModel<FormDataDto> listPage(PageModel<FormDataDto> pageModel){
        IPage page = new Page(pageModel.getCurrent(),pageModel.getPageSize());
        IPage<FormDataDto> result  = baseMapper.listPage(page,pageModel.getDto());
        if(Checker.BeNotEmpty(result.getRecords())){
            for(FormDataDto formData:result.getRecords()){
                formData.setExtra(JSONUtil.toBean(formData.getFormData(),Map.class));
            }
        }
        return new PageModel(result);
    }
}