package com.thinkit.cms.boot.config;
import cn.hutool.core.io.FileUtil;
import com.thinkit.cms.constant.SysConst;
import com.thinkit.cms.directive.exception.FreemarkerExceptionHandler;
import com.thinkit.cms.utils.Checker;
import com.thinkit.cms.utils.CmsConf;
import com.thinkit.cms.utils.CmsFolderUtil;
import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * @author LONG
 */
@Slf4j
@org.springframework.context.annotation.Configuration
public class FreeMarkConfig {


    @Primary
    @Bean
    public Configuration configuration() throws IOException, TemplateException {
        Configuration cfg = new Configuration(Configuration.VERSION_2_3_31);
        try {
            cfg.setDirectoryForTemplateLoading(initFile());
        }catch (FileNotFoundException e){
            log.error("模板目录不存在,请检查 config.properties 配置是否正确!");
            throw e;
        }catch (IOException e){
            log.error("freemarker 初始化异常,请检查 config.properties 配置是否正确!");
            throw e;
        }
        cfg.setObjectWrapper(new DefaultObjectWrapper(Configuration.VERSION_2_3_31));
        cfg.setTemplateExceptionHandler(templateExceptionHandler());
        // 将模板处理期间抛出的未经检查的异常包装到 TemplateException-s 中：
        cfg.setWrapUncheckedExceptions(true);
        // 空值不报错
        cfg.setClassicCompatible(true);
        cfg.setDefaultEncoding("UTF-8");
        cfg.setSetting("number_format","0.##");
        return cfg;
    }


    TemplateExceptionHandler templateExceptionHandler(){
        return new FreemarkerExceptionHandler();
    }

    private File initFile() throws IOException {
        String root =  CmsConf.get(SysConst.cmsFolder);
        if(Checker.BeNotBlank(root) && !FileUtil.exist(root)){
            FileUtil.mkdir(root);
            log.info("=====root 初始化根目录成功====");
        }
        String upload = CmsFolderUtil.appendFolder(root,SysConst.prefix,CmsConf.get(SysConst.cmsUpload));
        if(Checker.BeNotBlank(upload) && !FileUtil.exist(upload)){
            FileUtil.mkdir(upload);
            log.info("=====upload 初始化根目录成功====");
        }
        return new File(CmsFolderUtil.baseFolder());
    }
}
