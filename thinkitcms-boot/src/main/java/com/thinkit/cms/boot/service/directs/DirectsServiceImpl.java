package com.thinkit.cms.boot.service.directs;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.json.JSONException;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.alicp.jetcache.anno.CacheType;
import com.alicp.jetcache.anno.Cached;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.google.common.collect.Lists;
import com.thinkit.cms.api.directs.DirectsService;
import com.thinkit.cms.api.formmodel.FormModelService;
import com.thinkit.cms.boot.components.DirectiveComponent;
import com.thinkit.cms.boot.entity.directs.DirectsEntity;
import com.thinkit.cms.boot.mapper.directs.DirectsMapper;
import com.thinkit.cms.constant.SysConst;
import com.thinkit.cms.core.base.BaseServiceImpl;
import com.thinkit.cms.core.handler.CustomException;
import com.thinkit.cms.directive.annotation.Handler;
import com.thinkit.cms.directive.annotation.Instruct;
import com.thinkit.cms.directive.funs.BaseFunInterface;
import com.thinkit.cms.dto.directs.ConfigParam;
import com.thinkit.cms.dto.directs.DirectsDto;
import com.thinkit.cms.dto.directs.InParams;
import com.thinkit.cms.dto.directs.OutParams;
import com.thinkit.cms.model.KeyValueModel;
import com.thinkit.cms.model.PageModel;
import com.thinkit.cms.utils.Checker;
import lombok.extern.slf4j.Slf4j;
import org.reflections.Reflections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import java.util.*;

/**
* @author lg
*/
@Slf4j
@Service
public class DirectsServiceImpl extends BaseServiceImpl<DirectsDto,DirectsEntity,DirectsMapper> implements DirectsService{


    @Autowired
    private DirectiveComponent directiveComponent;

    @Autowired
    private FormModelService formModelService;

    @Autowired
    private ApplicationContext applicationContext;

    @Override
    public void save(DirectsDto directsDto) {
        boolean exist = isExist(null,"direct",directsDto.getDirect());
        if(exist){
            throw new CustomException(5046);
        }
        DirectsEntity directs = new DirectsEntity();
        BeanUtil.copyProperties(directsDto,directs);
        if(Checker.BeNotEmpty(directsDto.getInParams())){
            directs.setInParams(JSONUtil.toJsonStr(directsDto.getInParams()));
        }
        if(Checker.BeNotEmpty(directsDto.getOutParams())){
            directs.setOutParams(JSONUtil.toJsonStr(directsDto.getOutParams()));
        }
        directs.setSiteId(getSiteId(false)).setCreateId(getUserId()).
        setGmtCreate(new Date()).setId(id());
        directs.setConfig(JSONUtil.toJsonStr(directsDto.getConfig()));
//        if(SqlTypeEnum.REDIS.equals(SqlTypeEnum.valueOf(directsDto.getSqlType().toUpperCase()))){
//            checkJson(directsDto.getSql());
//        }
        super.save(directs);
    }

    private void checkJson(String redisSql){
        if(Checker.BeNotBlank(redisSql)){
            try {
                new JSONObject(redisSql);
            } catch (JSONException e) {
                throw new CustomException(5112);
            }
        }
    }

    @Override
    public DirectsDto detail(String id) {
        DirectsEntity directs = baseMapper.selectById(id);
        DirectsDto directsDto = new DirectsDto();
        // BeanUtil.copyProperties(directs,directsDto);
        if(Checker.BeNotNull(directs)){
            String inParams=directs.getInParams();
            if(Checker.BeNotBlank(inParams)){
                directsDto.setInParams(JSONUtil.toList(inParams, InParams.class));
            }
            String outParams=directs.getOutParams();
            if(Checker.BeNotBlank(outParams)){
                directsDto.setOutParams(JSONUtil.toList(outParams, OutParams.class));
            }
            String config=directs.getConfig();
            if(Checker.BeNotBlank(config)){
                directsDto.setConfig(JSONUtil.toBean(config, ConfigParam.class));
            }
            if(Checker.BeBlank(directs.getResultConvert())){
                directs.setResultConvert("default");
            }
            directsDto.setSql(directs.getSql()).setDirect(directs.getDirect()).setDirectType(directs.getDirectType()).
            setName(directs.getName()).setResult(directs.getResult()).setSqlType(directs.getSqlType()).
            setResultConvert(directs.getResultConvert()).setId(directs.getId());
        }
        return directsDto;
    }

    @Override
    public void update(DirectsDto directsDto) {
        boolean exist = isExist(directsDto.getId(),"direct",directsDto.getDirect());
        if(exist){
            throw new CustomException(5046);
        }
//        if(SqlTypeEnum.REDIS.equals(SqlTypeEnum.valueOf(directsDto.getSqlType().toUpperCase()))){
//            checkJson(directsDto.getSql());
//        }
        DirectsEntity directs = new DirectsEntity();
        BeanUtil.copyProperties(directsDto,directs);
        if(Checker.BeNotEmpty(directsDto.getInParams())){
            directs.setInParams(JSONUtil.toJsonStr(directsDto.getInParams()));
        }else{
            directs.setInParams(JSONUtil.toJsonStr(Lists.newArrayList()));
        }
        if(Checker.BeNotEmpty(directsDto.getOutParams())){
            directs.setOutParams(JSONUtil.toJsonStr(directsDto.getOutParams()));
        }else{
            directs.setOutParams(JSONUtil.toJsonStr(Lists.newArrayList()));
        }
        directs.setModifiedId(getUserId()).
        setGmtModified(new Date());
        directs.setConfig(JSONUtil.toJsonStr(directsDto.getConfig()));
        super.updateById(directs);
    }

    @Override
    public void enable(String id, Integer enable) {
        baseMapper.enable(id,enable);
    }

    @Override
    public void apply() {
        try {
            // 获取所有类型为ChildClass的Bean
            List<BaseFunInterface> baseFunInterfaces = new ArrayList<>();
            Map<String, BaseFunInterface> beansOfType = applicationContext.getBeansOfType(BaseFunInterface.class);
            // 遍历所有符合条件的Bean实例
            for (BaseFunInterface funInterface : beansOfType.values()) {
                baseFunInterfaces.add(funInterface);
            }
            directiveComponent.apply(null,baseFunInterfaces);
        }catch (Exception e){
            log.error("指令应用失败：{}",e.getMessage());
        }
    }

    @Override
    public boolean delCache(String directs) {
        String baseKey = null;
        if(Checker.BeNotBlank(directs)){
            baseKey = SysConst.defCache+directs+SysConst.colon+SysConst.starChar;
        }else{
            baseKey = SysConst.defCache+SysConst.starChar;
        }
        Set<String> keys =  taskUtils.scan(baseKey);
        if(Checker.BeNotEmpty(keys)){
            redisTemplate.delete(keys);
        }
        return true;
    }

    @Override
    public List<KeyValueModel> loadDirects(String siteId) {
        List<KeyValueModel> keyValueModels = new ArrayList<>();
        List<DirectsDto> directsDtos = baseMapper.loadDirects(siteId);
        if(Checker.BeNotEmpty(directsDtos)){
           for(DirectsDto directsDto:directsDtos){
               String name = directsDto.getName();
               String code = buildCode(directsDto);
               keyValueModels.add(new KeyValueModel(code,name));
           }
           return keyValueModels;
        }
        return Lists.newArrayList();
    }

    private String buildCode(DirectsDto directsDto){
        String direct = directsDto.getDirect();
        String inParamsStr = directsDto.getInParamsStr();
        StringBuffer stringBuffer = new StringBuffer("<@");
        stringBuffer.append(direct);
        if(Checker.BeNotBlank(inParamsStr)){
            List<InParams> inParams = JSONUtil.toList(inParamsStr,InParams.class);
            if(Checker.BeNotEmpty(inParams)){
                stringBuffer.append("  ");
                for(InParams inParam:inParams){
                    stringBuffer.append(inParam.getName()).append("=").append("xx").append(" ");
                }
            }
        }
        stringBuffer.append(">").append("\n\n");
        stringBuffer.append("</@").append(direct).append(">");
        return stringBuffer.toString();
    }

    @Override
    public PageModel<DirectsDto> listPage(PageModel<DirectsDto> pageModel){
        IPage page = new Page(pageModel.getCurrent(),pageModel.getPageSize());
        IPage<DirectsDto> result  = baseMapper.page(page,pageModel.getDto(),getSiteId(false));
        return new PageModel(result);
    }

    private boolean isExist(String id,String field,String val){
        int count = baseMapper.count(id,field,val);
        return count>0;
    }

    /**
     * 加载 1:指令、2:部件、3:模板、 4：变量等
     * @param type
     * @return
     */
    @Override
    public List<KeyValueModel> loadCodes(String type) {
        if("1".equals(type)){
            return this.loadDirects(null);
        }else if("2".equals(type)){
            return formModelService.loadParmModel(getSiteId(true));
        }else if("4".equals(type)){
            List<KeyValueModel> keyValueModels = new ArrayList<>();
            keyValueModels.add(new KeyValueModel("${domainMRoot}","Mobile域名根"));
            keyValueModels.add(new KeyValueModel("${domainRoot}","PC域名根"));
            keyValueModels.add(new KeyValueModel("${domain}","域名"));
            return keyValueModels;
        }
        return Lists.newArrayList();
    }


    @Cached(name = "cached::", key = "targetObject.class+'.loadHands'" ,expire = 7200, cacheType = CacheType.REMOTE)
    @Override
    public List<String> loadHands() {
        List<String> hands = new ArrayList<>();
        hands.add("default");
        Reflections reflections = new Reflections(SysConst.handPackage);
        Set<Class<?>> classes = reflections.getTypesAnnotatedWith(Handler.class);
        for(Class clz:classes){
            Handler handler = (Handler) clz.getAnnotation(Handler.class);
            String value = handler.value();
            hands.add(value);
        }
        return hands;
    }

    @Override
    public List<String> loadInstructs() {
        List<String> instructs = new ArrayList<>();
        Reflections reflections = new Reflections(SysConst.instructPackage);
        Set<Class<?>> classes = reflections.getTypesAnnotatedWith(Instruct.class);
        for(Class clz:classes){
            Instruct instruct = (Instruct) clz.getAnnotation(Instruct.class);
            String value = instruct.value();
            instructs.add(value);
        }
        return instructs;
    }
}