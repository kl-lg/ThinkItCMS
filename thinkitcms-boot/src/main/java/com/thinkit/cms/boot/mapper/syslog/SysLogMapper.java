package com.thinkit.cms.boot.mapper.syslog;
import org.apache.ibatis.annotations.Mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.thinkit.cms.boot.entity.syslog.SysLogEntity;

/**
* @author lg
*/

@Mapper
public interface SysLogMapper extends BaseMapper<SysLogEntity> {

}