package com.thinkit.cms.boot.service.statistics;

import cn.hutool.core.date.DateField;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import com.google.common.collect.Lists;
import com.thinkit.cms.annotation.Site;
import com.thinkit.cms.api.site.SiteService;
import com.thinkit.cms.api.statistics.StatisticsService;
import com.thinkit.cms.boot.mapper.statistics.StatisticsMapper;
import com.thinkit.cms.constant.SysConst;
import com.thinkit.cms.dto.statistics.*;
import com.thinkit.cms.utils.Checker;
import com.thinkit.cms.utils.CmsConf;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class StatisticsServiceImpl implements StatisticsService {

    @Resource
    private StatisticsMapper statisticsMapper;

    @Autowired
    private SiteService siteService;


    @Site(allow = true)
    @Override
    public Map<String,Object> contentTotal(StatisticsDto statisticsDto) {
        Map<String,Object> res = new HashMap<>();
        StatisticsResDto contentRes =  statisticsMapper.contentTotal(statisticsDto);
        res.put("content",contentRes);
        return res;
    }


    @Site(allow = true)
    @Override
    public List<DateCountResDto> dateTotal(StatisticsDto statisticsDto) {
        Date endDate = new Date();
        Integer days = -30;
        DateTime startDate = DateUtil.offset(endDate, DateField.DAY_OF_MONTH,days);
        String start = DateUtil.format(startDate,"yyyy-MM-dd")+" 00:00:00";
        String end = DateUtil.format(endDate,"yyyy-MM-dd")+" 23:59:59";
        statisticsDto.setStartTime(start);
        statisticsDto.setEndTime(end);
        statisticsDto.setDays(Math.abs(days));
        List<DateCountResDto> dateCountResDtoList =  statisticsMapper.dateTotal(statisticsDto);
        return Checker.BeNotEmpty(dateCountResDtoList)?dateCountResDtoList: Lists.newArrayList();
    }

    @Site(allow = true)
    @Override
    public List<TxtCountResDto> categoryTotal(StatisticsCategoryDto statisticsCategoryDto) {
        List<TxtCountResDto> categoryTotalList =  statisticsMapper.categoryTotal(statisticsCategoryDto);
        return Checker.BeNotEmpty(categoryTotalList)?categoryTotalList: Lists.newArrayList();
    }


    @Site(allow = true)
    @Override
    public List<TopContent> topTotal(TopContent topContent) {
        String prefix = CmsConf.getAsBoolean(SysConst.enableSSL)?SysConst.https:SysConst.http;
        if(Checker.BeBlank(topContent.getSiteId())){
            return Lists.newArrayList();
        }
        String domain = siteService.getDoamin(prefix,topContent.getSiteId());
        List<TopContent> categoryTotalList =  statisticsMapper.topTotal(topContent,5,domain);
        return Checker.BeNotEmpty(categoryTotalList)?categoryTotalList: Lists.newArrayList();
    }
}
