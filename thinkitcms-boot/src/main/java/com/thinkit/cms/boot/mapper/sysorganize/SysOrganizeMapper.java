package com.thinkit.cms.boot.mapper.sysorganize;
import com.thinkit.cms.dto.sysorganize.SysOrganizeDto;
import org.apache.ibatis.annotations.Mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.thinkit.cms.boot.entity.sysorganize.SysOrganizeEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
* @author LG
*/

@Mapper
public interface SysOrganizeMapper extends BaseMapper<SysOrganizeEntity> {

    /**
     * 查询组织
     * @param userId
     * @return
     */
    List<SysOrganizeDto> orgTableTree(@Param("userId") String userId);


    /**
     * 查询组织是否存在
     * @param orgId
     * @param orgCode
     * @return
     */
    Integer hasOrgCode(@Param("orgId") String orgId, @Param("orgCode") String orgCode);

    String getPorgName(@Param("pid") String pid);

    void deleteChild(@Param("id") String id);

    Integer hasChild(@Param("id") String id);
}