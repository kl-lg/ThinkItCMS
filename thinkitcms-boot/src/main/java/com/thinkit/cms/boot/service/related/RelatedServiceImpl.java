package com.thinkit.cms.boot.service.related;

import com.alicp.jetcache.anno.CacheInvalidate;
import com.alicp.jetcache.anno.CacheType;
import com.alicp.jetcache.anno.Cached;
import com.google.common.collect.Lists;
import com.thinkit.cms.api.related.RelatedService;
import com.thinkit.cms.boot.entity.related.RelatedEntity;
import com.thinkit.cms.boot.events.RelatedEvent;
import com.thinkit.cms.boot.mapper.related.RelatedMapper;
import com.thinkit.cms.core.base.BaseServiceImpl;
import com.thinkit.cms.directive.kit.Kv;
import com.thinkit.cms.dto.content.ContentDto;
import com.thinkit.cms.dto.related.RelatedDto;
import com.thinkit.cms.dto.related.Relateds;
import com.thinkit.cms.enums.EventEnum;
import com.thinkit.cms.user.SiteConf;
import com.thinkit.cms.utils.Checker;
import com.thinkit.cms.utils.CmsFolderUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
* @author LG
*/

@Service
public class RelatedServiceImpl extends BaseServiceImpl<RelatedDto,RelatedEntity,RelatedMapper> implements RelatedService{


    @CacheInvalidate(name = "cached::", key = "targetObject.class+'.listRelateds.'+#relateds.contentId" )
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void upRelateds(Relateds relateds) {
        String siteId = getSiteId(true),
        contentId = relateds.getContentId();
        Kv kv = Kv.create().setIfNotBlank("content_id",contentId).
        setIfNotBlank("site_id",siteId);
        super.removeByMap(kv);
        RelatedDto related = new RelatedDto();
        related.setContentId(contentId).setSiteId(siteId);
        publishEvent(new RelatedEvent(related, EventEnum.DELETE_RELATED));
        List<RelatedDto> relatedList = new ArrayList<>();
        for(String relatedId:relateds.getRelatedIds()){
            RelatedDto relatedDto = new RelatedDto();
            relatedDto.setContentId(contentId);
            relatedDto.setRelatedId(relatedId);
            relatedDto.setSiteId(siteId);
            relatedDto.setCreateName(getUserCtx().getAccount());
            relatedDto.setId(id());
            relatedList.add(relatedDto);
        }
        super.insertBatch(relatedList);
        publishEvent(new RelatedEvent(relatedList, EventEnum.SAVE_BATCH_RELATED));
    }


    @Cached(name = "cached::", key = "targetObject.class+'.listRelateds.'+#contentId" ,expire = 7200, cacheType = CacheType.REMOTE)
    @Override
    public List<ContentDto> listRelateds(String contentId) {
        SiteConf siteConf = getUserCtx().getSite(true);
        String domain = CmsFolderUtil.getSiteDomain(siteConf.getDomain());
        List<ContentDto>  contentDtos = baseMapper.listRelateds(contentId,getSiteId(true),domain);
        return Checker.BeNotEmpty(contentDtos)?contentDtos: Lists.newArrayList();
    }

    @Override
    public List<RelatedDto> getDatas(String siteId) {
        List<RelatedDto>  relatedDtos = baseMapper.getDatas(siteId);
        return Checker.BeNotEmpty(relatedDtos)?relatedDtos: Lists.newArrayList();
    }
}