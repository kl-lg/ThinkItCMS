package com.thinkit.cms.boot.directive.handler;

import com.thinkit.cms.directive.annotation.Handler;
import com.thinkit.cms.directive.handler.AbstractDataHandler;
import com.thinkit.cms.directive.render.BaserRender;
import com.thinkit.cms.directive.unit.ParamUnit;

import java.util.List;
import java.util.Map;

@Handler(value = "test")
public class TestHand extends AbstractDataHandler {

//    @Override
//    public Object handData(Map value, List<ParamUnit> outs, BaserRender render) {
//        System.out.println(value);
//        return value;
//    }
//
//    @Override
//    public Object handData(List<Map> value, List<ParamUnit> outs,BaserRender render) {
//        System.out.println(value);
//        return value;
//    }

    @Override
    public Object getData(Object value, BaserRender render) {
        List menus = toList(value);
        return menus;
    }

}
