package com.thinkit.cms.boot.service.login;

import cn.dev33.satoken.stp.StpUtil;
import com.thinkit.cms.api.syslicense.SysLicenseService;
import com.thinkit.cms.authen.auth.AuthedComponent;
import com.thinkit.cms.authen.secret.LoginService;
import com.thinkit.cms.authen.secret.SecretParam;
import com.thinkit.cms.core.handler.CustomException;
import com.thinkit.cms.model.CmsResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * @author LONG
 */
@Slf4j
@Service
public class LoginServiceImpl implements LoginService {

    private final AuthedComponent authedComponent;

    @Autowired
    private SysLicenseService sysLicenseService;

    public LoginServiceImpl(AuthedComponent authedComponent) {
        this.authedComponent = authedComponent;
    }

    @Override
    public CmsResult login(SecretParam secretParam) {
            //TODO 请尊重作者的辛苦付出,商用请自觉购买授权,否则将一经发现将自行承担法律责任
            int code = sysLicenseService.checkLicense();
            switch (code) {
                case 0 -> throw new CustomException(5100);
                case 1 -> throw new CustomException(5101);
                case 2 -> throw new CustomException(5102);
                case 3 -> throw new CustomException(5103);
                case 4 -> throw new CustomException(5104);
                case 5 -> throw new CustomException(5105);
                default -> log.info("证书校验通过");
            }
            Map<String,Object> res= authedComponent.login(secretParam);
            return CmsResult.result(res);
    }

    @Override
    public CmsResult logout() {
         StpUtil.logout();
         return CmsResult.result();
    }

    @Override
    public CmsResult verifyCode() {
        Map<String,Object> res= authedComponent.verifyCode();
        return CmsResult.result(res);
    }
}
