package com.thinkit.cms.boot.service.syslog;
import cn.hutool.json.JSONUtil;
import com.thinkit.cms.annotation.Logs;
import com.thinkit.cms.api.syslog.SysLogService;
import com.thinkit.cms.boot.entity.syslog.SysLogEntity;
import com.thinkit.cms.core.base.BaseServiceImpl;
import com.thinkit.cms.dto.syslog.SysLogDto;
import com.thinkit.cms.boot.mapper.syslog.SysLogMapper;
import com.thinkit.cms.utils.Checker;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Service;

import java.lang.reflect.Method;

/**
* @author lg
*/

@Service
public class SysLogServiceImpl extends BaseServiceImpl<SysLogDto,SysLogEntity,SysLogMapper> implements SysLogService{


    @Override
    public void saveLog(ProceedingJoinPoint joinPoint, Long time) {
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = signature.getMethod();
        SysLogDto sysLog = new SysLogDto();
        Logs logs = method.getAnnotation(Logs.class);
        if (logs != null) {
            String opera=logs.operaEnum().getCode()+logs.operation();
            sysLog.setOperation(opera);
            sysLog.setModule(logs.module().getName());
            sysLog.setType(logs.module().getCode());
        }
        // 请求的方法名
        String className = joinPoint.getTarget().getClass().getName();
        String methodName = signature.getName();
        sysLog.setMethod(className + "." + methodName + "()");
        // 请求的参数
        Object[] args = joinPoint.getArgs();
        try {
            if(Checker.BeNotEmpty(args)){
                String params="";
                for(Object o:args){
                    if(Checker.BeNotNull(o) && Checker.BeNotBlank(o.toString())){
                        params += JSONUtil.toJsonStr(o)+"|";
                    }
                }
                if(Checker.BeNotBlank(params)&&params.endsWith("|"))
                params=params.substring(0,params.length()-1);
                sysLog.setParams(params);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        // 获取request
        // 设置IP地址
        sysLog.setIp(getIpAddr(getRequest()));
        // 用户名
        String userId = getUserId();
        String userAccount = getUserCtx().getAccount();
        String nickName = getUserCtx().getNickName();
        String siteId = getSiteId(true);
        sysLog.setUserId(userId);
        sysLog.setUsername(userAccount);
        sysLog.setName(nickName);
        sysLog.setTime(time).setId(id());
        sysLog.setSiteId(siteId);
        String params = sysLog.getParams();
        if(Checker.BeNotBlank(params)){
            sysLog.setParams(params);
        }
        insert(sysLog);
    }
}