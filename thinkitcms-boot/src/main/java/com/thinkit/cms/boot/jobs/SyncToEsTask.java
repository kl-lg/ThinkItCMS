package com.thinkit.cms.boot.jobs;

import com.thinkit.cms.boot.jobs.executers.EsExecuter;
import com.thinkit.cms.core.handler.CustomException;
import com.thinkit.cms.directive.task.TaskDto;
import com.thinkit.cms.directive.task.TaskService;
import com.thinkit.cms.enums.TaskEnum;
import com.thinkit.cms.user.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.concurrent.CompletableFuture;

/**
 * 全站生成 task
 */
@Slf4j
@Component
public class SyncToEsTask {


    @Autowired
    private TaskService taskService;


    public String esTask(String siteId, User user){
        String key = taskService.genTaskKey(TaskEnum.ES_TASK,siteId,null);
        if(!taskService.isRuning(key)){
            TaskDto taskDto = new TaskDto();
            taskDto.setAction(TaskEnum.ES_TASK.getVal());
            taskDto.setSiteId(siteId);
            String taskId = taskService.startTask(key,taskDto);
            CompletableFuture.runAsync(()->{
                doTask(siteId,user,taskId);
            });
            return taskId;
        }else{
            throw new CustomException(5060);
        }
    }


    public void doTask(String siteId,User user,String taskId){
        user.setTaskId(taskId);
        EsExecuter executer = new EsExecuter(user,siteId,taskId,TaskEnum.ES_TASK);
        executer.run();
    }


}
