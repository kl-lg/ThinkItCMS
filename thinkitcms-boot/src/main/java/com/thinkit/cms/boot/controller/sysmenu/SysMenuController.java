package com.thinkit.cms.boot.controller.sysmenu;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.thinkit.cms.annotation.Logs;
import com.thinkit.cms.annotation.ValidGroup1;
import com.thinkit.cms.api.sysmenu.SysMenuService;
import com.thinkit.cms.authen.annotation.Sentinel;
import com.thinkit.cms.core.base.BaseController;
import com.thinkit.cms.dto.sysmenu.SysMenuDto;
import com.thinkit.cms.model.CmsResult;
import com.thinkit.cms.utils.Tree;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import java.util.List;

import static com.thinkit.cms.enums.LogModule.LICENSE;
import static com.thinkit.cms.enums.LogModule.MENU;

/**
* @author LG
*/

@Validated
@RestController
@RequestMapping("/api/sysMenu")
@Slf4j
public class SysMenuController extends BaseController<SysMenuService> {

        @SaCheckPermission("menu")
        @GetMapping("menuSlideTree")
        public List<Tree> menuSlideTree(){
              return service.menuSlideTree();
        }

        @SaCheckPermission("menu")
        @GetMapping("menuTableTree")
        public List<Tree> menuTableTree(){
            return service.menuTableTree();
        }


        @GetMapping("menuTreeAssign")
        public CmsResult menuTreeAssign(@RequestParam String roleId){
            return service.menuTreeAssign(roleId);
        }


        @Logs(module = MENU,operation = "创建菜单")
        @Sentinel
        @SaCheckPermission("menu:add")
        @PostMapping(value="save")
        public void save(@Validated(value = {ValidGroup1.class}) @RequestBody SysMenuDto sysMenuDto) {
              service.save(sysMenuDto);
        }


        @Logs(module = MENU,operation = "查看菜单详情")
        @SaCheckPermission("menu:edit")
        @GetMapping(value="detail")
        public SysMenuDto detail(@Validated @NotBlank(message = "#{5029}") String id) {
            return service.detail(id);
        }


        @Logs(module = MENU,operation = "查看编辑更新")
        @Sentinel
        @SaCheckPermission("menu:edit")
        @PutMapping(value="update")
        public void update(@Validated @RequestBody SysMenuDto sysMenuDto) {
              service.update(sysMenuDto);
        }



        @Logs(module = MENU,operation = "查看删除")
        @Sentinel
        @SaCheckPermission("menu:delete")
        @DeleteMapping(value="delete")
        public boolean deleteByPk(@NotBlank @RequestParam String id){
                return service.delete(id);
        }


        @Logs(module = MENU,operation = "下载菜单文件")
        @SaCheckPermission("menu:down")
        @PostMapping(value="downMenuTs")
        public void downMenuTs(){
             service.downMenuTs();
        }
}