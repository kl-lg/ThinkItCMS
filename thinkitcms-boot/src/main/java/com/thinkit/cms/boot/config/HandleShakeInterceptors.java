package com.thinkit.cms.boot.config;
import cn.dev33.satoken.stp.StpUtil;
import com.thinkit.cms.utils.Checker;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.server.HandshakeInterceptor;
import java.security.Principal;
import java.util.Map;

/**
 * 检查握手请求和响应, 对WebSocketHandler传递属性
 */
@Slf4j
public class HandleShakeInterceptors implements HandshakeInterceptor {

    /**
     * 在握手之前执行该方法, 继续握手返回true, 中断握手返回false.
     * 通过attributes参数设置WebSocketSession的属性
     *
     * @param request
     * @param response
     * @param wsHandler
     * @param attributes
     * @return
     * @throws Exception
     */

    public boolean beforeHandshake(ServerHttpRequest request, ServerHttpResponse response,
                                   WebSocketHandler wsHandler, Map<String, Object> attributes) throws Exception {
        //保存客户端标识
        ServletServerHttpRequest req = (ServletServerHttpRequest) request;
        String token = req.getServletRequest().getParameter("Authorization");
        if(Checker.BeBlank(token)){
            log.error("Authorization can't be null websocket权限拒绝");
            return false;
        }
        Principal principal= ckToken(token);
        if(Checker.BeNull(principal)){
            log.error("token 无效 websocket权限拒绝");
            return false;
        }
        attributes.put("socketUser", principal);
        return true;
    }

    /**
     * 在握手之后执行该方法. 无论是否握手成功都指明了响应状态码和相应头.
     *
     * @param request
     * @param response
     * @param wsHandler
     * @param exception
     */
    public void afterHandshake(ServerHttpRequest request, ServerHttpResponse response,
                               WebSocketHandler wsHandler, Exception exception) {

        System.out.println(exception);
    }

    private Principal ckToken(String tokenValue) {
        if (Checker.BeNotBlank(tokenValue)) {
            String userId = StpUtil.getLoginIdByToken(tokenValue).toString();
            CustomSocketPrincipal customPrincipal=new CustomSocketPrincipal(userId,userId);
            return customPrincipal;
        }
        return null;
    }
}