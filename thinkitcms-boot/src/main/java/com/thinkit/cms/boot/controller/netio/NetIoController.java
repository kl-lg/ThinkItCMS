package com.thinkit.cms.boot.controller.netio;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.thinkit.cms.api.netio.NetIoService;
import com.thinkit.cms.authen.annotation.Sentinel;
import com.thinkit.cms.dto.netio.Chunk;
import com.thinkit.cms.model.CmsResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.IOException;

/**
 * @author LONG
 */
@Validated
@RestController
@RequestMapping("/api/netio")
@Slf4j
public class NetIoController {

    @Autowired
    private NetIoService netIoService;

    @RequestMapping(value="upload",method= RequestMethod.POST)
    public CmsResult update(@Validated @NotNull(message = "请上传文件") @RequestParam("file") MultipartFile multipartFile) throws IOException {
        return  netIoService.upload(multipartFile.getOriginalFilename(),multipartFile.getName(),multipartFile.getInputStream(),multipartFile.getSize());
    }

    @PostMapping("uploadFlat")
    public CmsResult update(Chunk chunk){
        return netIoService.upload(chunk);
    }


    @GetMapping("uploadFlat")
    public CmsResult checkFileIsExist(Chunk chunk){
        return netIoService.checkFileIsExist(chunk);
    }

    @PostMapping("merge")
    public CmsResult merge(@RequestBody  Chunk chunk){
        return netIoService.merge(chunk);
    }


    @RequestMapping(value="delete",method= RequestMethod.DELETE)
    public CmsResult delete(@NotBlank(message = "请输入文件ID") String key) throws IOException {
        return  netIoService.delete(key);
    }



    @Sentinel
    @SaCheckPermission("upload:local")
    @RequestMapping(value="uploadLocal",method= RequestMethod.POST)
    public CmsResult uploadLocal(@Validated @NotNull(message = "请上传文件")
                                 @RequestParam("file") MultipartFile multipartFile,String path) throws IOException {
        return  netIoService.uploadLocal(path,multipartFile.getOriginalFilename(),multipartFile.getName(),
        multipartFile.getInputStream(),multipartFile.getSize());
    }
}
