package com.thinkit.cms.boot.service.modeldesign;

import cn.hutool.core.util.ReflectUtil;
import cn.hutool.json.JSONUtil;
import com.alicp.jetcache.anno.CacheInvalidate;
import com.alicp.jetcache.anno.CacheType;
import com.alicp.jetcache.anno.Cached;
import com.google.common.collect.Lists;
import com.thinkit.cms.api.modeldesign.ModelDesignService;
import com.thinkit.cms.boot.entity.modeldesign.ModelDesignEntity;
import com.thinkit.cms.boot.mapper.formmodel.FormModelMapper;
import com.thinkit.cms.boot.mapper.modeldesign.ModelDesignMapper;
import com.thinkit.cms.constant.SysConst;
import com.thinkit.cms.core.base.BaseServiceImpl;
import com.thinkit.cms.directive.kit.RedisLuaUtils;
import com.thinkit.cms.dto.category.CategoryDto;
import com.thinkit.cms.dto.content.ContentDto;
import com.thinkit.cms.dto.formdata.FormDataDto;
import com.thinkit.cms.dto.modeldesign.ModelDesignDto;
import com.thinkit.cms.dto.modeldesign.OptionsDto;
import com.thinkit.cms.dto.modeldesign.SaveModelDesign;
import com.thinkit.cms.enums.ModelEnum;
import com.thinkit.cms.model.ModelJson;
import com.thinkit.cms.utils.Checker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import redis.clients.jedis.UnifiedJedis;
import redis.clients.jedis.search.Schema;

import java.lang.reflect.Field;
import java.util.*;

/**
* @author LG
*/

@Service
public class ModelDesignServiceImpl extends BaseServiceImpl<ModelDesignDto,ModelDesignEntity,ModelDesignMapper> implements ModelDesignService{


    @Autowired
    FormModelMapper formModelMapper;

    @Autowired
    UnifiedJedis unifiedJedis;


    @CacheInvalidate(name = "cached::", key = "targetObject.class+'.detail.'+#saveModelDesign.modelId")
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveMdesign(SaveModelDesign saveModelDesign) {
          String siteId = saveModelDesign.getSiteId();
          String modelId = saveModelDesign.getModelId();
          baseMapper.deleteDesign(siteId,modelId);
          List<ModelDesignEntity> modelDesigns = new ArrayList<>();
          for(ModelDesignDto modelDesign:saveModelDesign.getRows()){
              ModelDesignEntity entity =  D2T(modelDesign);
              entity.setFormModelId(modelId);
              if(Checker.BeNotNull(modelDesign.getOptions())){
                  entity.setOptions(JSONUtil.toJsonStr(modelDesign.getOptions()));
              }
              entity.setSiteId(siteId);
              entity.setFieldDefaultVal(modelDesign.getFieldDefaultVal());
              entity.setId(id());
              entity.setCreateId(getUserId());
              entity.setGmtCreate(new Date());
              modelDesigns.add(entity);
          }
          super.saveBatch(modelDesigns);
    }

    @Cached(name = "cached::", key = "targetObject.class+'.detail.'+#modelId" ,expire = 7200, cacheType = CacheType.REMOTE)
    @Override
    public List<ModelDesignDto> detail(String modelId) {
        List<ModelDesignDto> modelDesignDtos = baseMapper.detail(modelId,getSiteId(true));
        modelDesignDtos = Checker.BeNotEmpty(modelDesignDtos)?modelDesignDtos: Lists.newArrayList();
        for(ModelDesignDto modelDesign:modelDesignDtos){
            if(Checker.BeNotNull(modelDesign.getOptionStr())){
               modelDesign.setOptions(JSONUtil.toList(modelDesign.getOptionStr(), OptionsDto.class));
            }else{
                modelDesign.setOptions(Lists.newArrayList());
            }
        }
        return modelDesignDtos;
    }

    @Override
    public void createIndex(SaveModelDesign saveModelDesign) {
        String siteId = saveModelDesign.getSiteId();
        String modelId = saveModelDesign.getModelId();
        String formModelCode = formModelMapper.queryFormCode(modelId);
        String formType = baseMapper.queryFormType(siteId,modelId);
        if(Checker.BeNotBlank(formType)){
            ModelEnum modelEnum =  ModelEnum.getEnums(formType);
            String code = formModelMapper.queryFormCode(modelId);
            if(Checker.BeNotBlank(code)){
                if(Checker.BeNotEmpty(saveModelDesign.getRows())){
                    Map<String,Object> objectMap = new HashMap<>();
                    for(ModelDesignDto modelDesign:saveModelDesign.getRows()){
                        boolean index = "1".equals(modelDesign.getIsIndex());
                        if(index){
                            objectMap.put(modelDesign.getFieldCode(), Schema.FieldType.valueOf(modelDesign.getIndexType()));
                        }
                    }
                    if(!objectMap.isEmpty()){
                        switch (modelEnum){
                            case ARTICLE -> {
                                ModelJson modelJson = new ModelJson();
                                modelJson.setPrefix(SysConst.contentPrefix);
                                modelJson.setObjectMap(objectMap);
                                modelJson.setIndexName(SysConst.content);
                                modelJson.setPk(formModelCode);
                                RedisLuaUtils.createIndex(modelJson,fields(ContentDto.class));
                                Set<String> indexs  = RedisLuaUtils.listIndex();
                                List<String> keys = indexs.stream().filter(t->t.startsWith(SysConst.contentPrefix)).toList();
                                RedisLuaUtils.createIndexAs(SysConst.content,keys);
                            }
                            case FRAGMENT -> {
                                ModelJson modelJson = new ModelJson();
                                modelJson.setPrefix(SysConst.formDataPrefix);
                                modelJson.setObjectMap(objectMap);
                                modelJson.setIndexName(SysConst.formData);
                                modelJson.setPk(formModelCode);
                                RedisLuaUtils.createIndex(modelJson,fields(FormDataDto.class));
                                Set<String> indexs  = RedisLuaUtils.listIndex();
                                List<String> keys = indexs.stream().filter(t->t.startsWith(SysConst.formDataPrefix)).toList();
                                RedisLuaUtils.createIndexAs(SysConst.formData,keys);
                            }
                            case CATEGORY -> {
                                ModelJson modelJson = new ModelJson();
                                modelJson.setPrefix(SysConst.category);
                                modelJson.setObjectMap(objectMap);
                                modelJson.setIndexName(SysConst.category);
                                modelJson.setPk(formModelCode);
                                RedisLuaUtils.createIndex(modelJson, fields(CategoryDto.class));
                                Set<String> indexs  = RedisLuaUtils.listIndex();
                                List<String> keys = indexs.stream().filter(t->t.startsWith(SysConst.categoryPrefix)).toList();
                                RedisLuaUtils.createIndexAs(SysConst.category,keys);
                            }
                        }
                    }
                }
            }
        }
    }

    private List<Field> fields(Class clz){
        List<Field> fieldArrayList = new ArrayList<>();
        Field[] fields = ReflectUtil.getFields(clz);
        Collections.addAll(fieldArrayList,fields);
        return fieldArrayList;
    }
}