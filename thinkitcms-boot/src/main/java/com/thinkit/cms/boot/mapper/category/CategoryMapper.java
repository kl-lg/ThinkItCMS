package com.thinkit.cms.boot.mapper.category;
import com.thinkit.cms.dto.category.CategoryDto;
import org.apache.ibatis.annotations.Mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.thinkit.cms.boot.entity.category.CategoryEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Set;

/**
* @author LG
*/

@Mapper
public interface CategoryMapper extends BaseMapper<CategoryEntity> {

    /**
     * 查询栏目列表
     * @param siteId
     * @param pid
     * @param  domain
     * @return
     */
    List<CategoryDto> selectCategorys(@Param("siteId") String siteId,@Param("pid") String pid,@Param("domain")String domain);


    /**
     * 是否存在栏目编码
     * @param code
     * @param id
     * @param siteId
     * @return
     */
    Integer hasExist(@Param("siteId") String siteId,@Param("code") String code, @Param("id") String id);


    /**
     * 查询栏目数据详情
     * @param id
     * @return
     */
    CategoryDto categoryData(@Param("id") String id);


    /**
     * 获取子栏目id
     * @param id
     * @return
     */
    List<String> getId(@Param("id") String id);


    /**
     * 查找需要发布的栏目
     * @param siteId
     * @return
     */
    List<CategoryDto> queryCategorys(@Param("siteId") String siteId);

    Set<String> listIds(@Param("siteId") String siteId);

    /**
     * 查询 code
     * @param categoryId
     * @return
     */
    String queryCode(@Param("categoryId") String categoryId);
}