package com.thinkit.cms.boot.controller.content;

import com.thinkit.cms.annotation.*;
import com.thinkit.cms.api.content.ContentService;
import com.thinkit.cms.authen.annotation.Sentinel;
import com.thinkit.cms.core.base.BaseController;
import com.thinkit.cms.dto.content.ContentDto;
import com.thinkit.cms.dto.content.RowsDto;
import com.thinkit.cms.dto.content.SelectRows;
import com.thinkit.cms.dto.content.TagDto;
import com.thinkit.cms.model.PageModel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import java.util.Set;

import static com.thinkit.cms.enums.LogModule.CONTENT;

/**
* @author LG
*/

@Validated
@RestController
@RequestMapping("/api/content")
@Slf4j
public class ContentController extends BaseController<ContentService> {


        @RequestMapping(value = "/page", method = RequestMethod.POST)
        public PageModel<ContentDto> list(@RequestBody PageModel<ContentDto> pageDto) {
             return service.listPage(pageDto);
        }

        @Logs(module = CONTENT,operation = "文章内容保存")
        @RequestMapping(value="save",method= RequestMethod.POST)
        public void save(@Validated(value = {ValidGroup1.class}) @RequestBody ContentDto contentDto) {
              service.saveContent(contentDto);
        }


        @Logs(module = CONTENT,operation = "文章内容更新")
        @Sentinel
        @RequestMapping(value="update",method= RequestMethod.PUT)
        public void update(@Validated(value = {ValidGroup2.class}) @RequestBody ContentDto contentDto) {
              service.update(contentDto);
        }


        @Logs(module = CONTENT,operation = "文章内容删除")
        @Sentinel
        @DeleteMapping(value="delete")
        public boolean deleteByPk(@NotBlank @RequestParam String id){
                return service.deleteByPk(id);
        }



        @Logs(module = CONTENT,operation = "文章内容查看")
        @RequestMapping(value = "/detail", method = RequestMethod.GET)
        public ContentDto detail(@NotBlank(message = "ID不能为空!")  String contentId) {
                return service.getDetail(contentId);
        }


        @Logs(module = CONTENT,operation = "文章标签查看")
        @RequestMapping(value = "/tags", method = RequestMethod.GET)
        public Set<String> tags() {
                return service.tags();
        }


        @Logs(module = CONTENT,operation = "文章打标签")
        @RequestMapping(value = "/updateTag", method = RequestMethod.PUT)
        public void updateTag(@Validated(value = {ValidGroup2.class}) @RequestBody TagDto tagDto) {
              service.updateTag(tagDto.getTags(),tagDto.getId());
        }



        @Logs(module = CONTENT,operation = "文章发布/下架/删除")
        @RequestMapping(value = "/publish", method = RequestMethod.PUT)
        public void publish(@Validated(value = {ValidGroup1.class}) @RequestBody RowsDto rowsDto) {
                service.publish(rowsDto);
        }

        @RequestMapping(value = "/jobPublish", method = RequestMethod.PUT)
        public void jobPublish(@Validated(value = {ValidGroup3.class}) @RequestBody RowsDto rowsDto) {
                service.jobPublish(rowsDto);
        }


        @Logs(module = CONTENT,operation = "文章生成静态页面")
        @RequestMapping(value = "/generate", method = RequestMethod.PUT)
        public void generate(@Validated(value = {ValidGroup1.class}) @RequestBody RowsDto rowsDto) {
                rowsDto.setAsync(true);
                service.generate(rowsDto);
        }


        @Logs(module = CONTENT,operation = "文章回收站查看")
        @RequestMapping(value = "/listRecycle", method = RequestMethod.POST)
        public PageModel<ContentDto> listRecycle (@RequestBody PageModel<ContentDto> pageDto) {
                return service.listRecycle(pageDto);
        }


        @Logs(module = CONTENT,operation = "文章回收站还原")
        @RequestMapping(value = "/reduction", method = RequestMethod.POST)
        public void reduction (@Validated(value = {ValidGroup4.class})  @RequestBody RowsDto ckRow) {
                service.reduction(ckRow);
        }


        @Logs(module = CONTENT,operation = "文章彻底删除")
        @Sentinel
        @RequestMapping(value = "/readDel", method = RequestMethod.POST)
        public void readDel (@Validated(value = {ValidGroup4.class}) @RequestBody RowsDto ckRow) {
                service.readDel(ckRow);
        }

        @Logs(module = CONTENT,operation = "置顶/取消置顶")
        @RequestMapping(value = "/topIt", method = RequestMethod.PUT)
        public void topIt (@Validated(value = {ValidGroup4.class})  @RequestBody SelectRows ckRow) {
                service.topIt(ckRow);
        }

        @Logs(module = CONTENT,operation = "推荐/取消推荐")
        @RequestMapping(value = "/recomdIt", method = RequestMethod.PUT)
        public void recomdIt (@Validated(value = {ValidGroup4.class})  @RequestBody SelectRows ckRow) {
                service.recomdIt(ckRow);
        }

        @Logs(module = CONTENT,operation = "设置头条/取消设置头条")
        @RequestMapping(value = "/headIt", method = RequestMethod.PUT)
        public void headIt (@Validated(value = {ValidGroup4.class})  @RequestBody SelectRows ckRow) {
                service.headIt(ckRow);
        }

}