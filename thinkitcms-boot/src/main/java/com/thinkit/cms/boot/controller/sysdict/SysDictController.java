package com.thinkit.cms.boot.controller.sysdict;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.thinkit.cms.annotation.Logs;
import com.thinkit.cms.annotation.ValidGroup1;
import com.thinkit.cms.api.sysdict.SysDictService;
import com.thinkit.cms.authen.annotation.Sentinel;
import com.thinkit.cms.core.base.BaseController;
import com.thinkit.cms.dto.sysdict.SysDictDto;
import com.thinkit.cms.utils.Tree;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import java.util.List;

import static com.thinkit.cms.enums.LogModule.DICT;

/**
* @author LG
*/

@Validated
@RestController
@RequestMapping("/api/sysDict")
@Slf4j
public class SysDictController extends BaseController<SysDictService> {



        @Logs(module = DICT,operation = "基础字典详情查看")
        @SaCheckPermission("dict:edit")
        @GetMapping(value="detail")
        public SysDictDto detail(@NotBlank(message = "ID不能为空!") @RequestParam String id){
             return service.getByPk(id);
        }

        @SaCheckPermission("dict:page")
        @GetMapping("dictTableTree")
        public List<Tree> menuTableTree(){
            return service.dictTableTree();
        }


        @Logs(module = DICT,operation = "基础字典保存")
        @Sentinel
        @SaCheckPermission("dict:add")
        @PostMapping(value="save")
        public void save(@Validated(value = {ValidGroup1.class}) @RequestBody SysDictDto sysDictDto) {
              service.save(sysDictDto);
        }


        @Logs(module = DICT,operation = "基础字典编辑")
        @Sentinel
        @SaCheckPermission("dict:edit")
        @PutMapping(value="update")
        public void update(@Validated(value = {ValidGroup1.class}) @RequestBody SysDictDto sysDictDto) {
              service.update(sysDictDto);
        }



        @Logs(module = DICT,operation = "基础字典删除")
        @Sentinel
        @SaCheckPermission("dict:delete")
        @DeleteMapping(value="delete")
        public boolean deleteByPk(@NotBlank @RequestParam String id){
                return service.delete(id);
        }

}