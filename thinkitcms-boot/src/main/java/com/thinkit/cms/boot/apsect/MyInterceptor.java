package com.thinkit.cms.boot.apsect;
import cn.dev33.satoken.stp.StpUtil;
import cn.hutool.json.JSONUtil;
import com.thinkit.cms.authen.annotation.Sentinel;
import com.thinkit.cms.authen.enums.UserTypeDef;
import com.thinkit.cms.authen.exceptions.AuthenException;
import com.thinkit.cms.core.handler.CustomException;
import com.thinkit.cms.model.CmsResult;
import com.thinkit.cms.user.User;
import com.thinkit.cms.utils.Checker;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;
import java.util.Arrays;

@Slf4j
public class MyInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        if(handler instanceof HandlerMethod){
            HandlerMethod handlerMethod = (HandlerMethod) handler;
            Method method = handlerMethod.getMethod();
            Sentinel sentinel = method.getAnnotation(Sentinel.class);
            if(Checker.BeNotNull(sentinel)){
                boolean isUser = Arrays.asList(sentinel.users()).contains(getUserCtx().getAccount());
                if(isUser){
                    throw new CustomException(5108);
                }
            }
        }
        return true;
    }

    /**
     * 获取上下文
     * @return
     */
    protected User getUserCtx(){
        Object object = StpUtil.getExtra(UserTypeDef.USER_CLIENT_DETAIL.getType());
        if(Checker.BeNull(object)){
            throw new AuthenException(403,"获取用户信息失败!");
        }
        User user = JSONUtil.toBean(JSONUtil.toJsonStr(object),User.class);
        return user;
    }

}
