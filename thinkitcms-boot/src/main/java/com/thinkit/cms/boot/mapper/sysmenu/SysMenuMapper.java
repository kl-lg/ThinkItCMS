package com.thinkit.cms.boot.mapper.sysmenu;
import com.thinkit.cms.dto.sysmenu.SysMenuDto;
import org.apache.ibatis.annotations.Mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.thinkit.cms.boot.entity.sysmenu.SysMenuEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
* @author LG
*/

@Mapper
public interface SysMenuMapper extends BaseMapper<SysMenuEntity> {


    /**
     * 菜单列表页查询
     * @param userId
     * @return
     */
    List<SysMenuDto> menuTableTree(@Param("userId") String userId);

    /**
     * 菜单侧边栏查询
     * @param userId
     * @return
     */
    List<SysMenuDto> listMenuSlideTree(@Param("userId") String userId,@Param("isSuperAdmin")  Boolean isSuperAdmin);

    /**
     * 获取菜单父类名称
     * @param pid
     * @return
     */
    String getPMenuName(@Param("pid") String pid);


    /**
     * 删除
     * @param pid
     */
    void deleteByPid(@Param("pid") String pid);


    /**
     * 菜单分配
     * @param userId
     * @return
     */
   // List<SysMenuDto> listMenuTreeAssign(@Param("userId") String userId);

    List<SysMenuDto> listMenuTreeAssign(@Param("userId") String userId, @Param("isSuperAdmin") Boolean isSuperAdmin);
}