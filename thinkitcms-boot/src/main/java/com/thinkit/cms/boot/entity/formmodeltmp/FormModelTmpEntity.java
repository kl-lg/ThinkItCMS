package com.thinkit.cms.boot.entity.formmodeltmp;
import lombok.Getter;
import lombok.Setter;
import java.math.BigDecimal;
import lombok.experimental.Accessors;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.thinkit.cms.model.BaseModel;
import com.baomidou.mybatisplus.annotation.TableName;
/**
* @author LG
*/
@Getter
@Setter
@Accessors(chain = true)
@TableName("tk_form_model_tmp")
public class FormModelTmpEntity extends BaseModel  {
       /**
         
       */
        @TableField("template_id")
        private String templateId;

       /**
         
       */
        @TableField("form_model_id")
        private String formModelId;

       /**
         模板路径
       */
        @TableField("tmp_path")
        private String tmpPath;

       /**
         手机端模板路径
       */
        @TableField("mobile_tmp_path")
        private String mobileTmpPath;

        /**
         手机端模板路径
         */
        @TableField("site_id")
        private String siteId;

       /**
         是否删除 0：正常 1：删除
       */
        @TableField("deleted")
        private Boolean deleted;


}