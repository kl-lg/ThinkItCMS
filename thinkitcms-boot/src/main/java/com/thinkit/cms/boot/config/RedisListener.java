package com.thinkit.cms.boot.config;

public abstract class RedisListener {

    public void filter(String expiredKey){
        if(isFit(expiredKey)){
            doIt(expiredKey);
        }
    }

    public abstract boolean isFit(String expiredKey);

    public abstract void doIt(String expiredKey);
}
