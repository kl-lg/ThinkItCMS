package com.thinkit.cms.boot.mapper.sysdict;
import com.thinkit.cms.dto.sysdict.SysDictDto;
import com.thinkit.cms.dto.sysmenu.SysMenuDto;
import org.apache.ibatis.annotations.Mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.thinkit.cms.boot.entity.sysdict.SysDictEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
* @author LG
*/

@Mapper
public interface SysDictMapper extends BaseMapper<SysDictEntity> {

    List<SysDictDto> dictTableTree();

    int checkHasRow(@Param("id") String id, @Param("field") String field, @Param("value") String value);
}