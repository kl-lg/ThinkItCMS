package com.thinkit.cms.boot.entity.site;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.thinkit.cms.model.BaseModel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
/**
* @author LG
*/
@Getter
@Setter
@Accessors(chain = true)
@TableName("tk_site")
public class SiteEntity extends BaseModel {
       /**
         站点名称
       */
        @TableField("name")
        private String name;

       /**
         站点关键字
       */
        @TableField("keyword")
        private String keyword;

       /**
         站点描述
       */
        @TableField("description")
        private String description;

       /**
         站点LOGO 地址
       */
        @TableField("logo_img")
        private String logoImg;

       /**
         域名地址
       */
        @TableField("domain")
        private String domain;

       /**
         站点跟目录地址
       */
        @TableField("dir")
        private String dir;

       /**
         手机端模板根目录
       */
        @TableField("mobile_dir")
        private String mobileDir;

       /**
         站点备案信息
       */
        @TableField("icp")
        private String icp;

       /**
         站点版权
       */
        @TableField("copyright")
        private String copyright;

       /**
         是否删除 0：正常 1：删除
       */
        @TableField("deleted")
        private Boolean deleted;

       /**
         默认模板ID
       */
        @TableField("def_template_id")
        private String defTemplateId;


        @TableField("`order`")
        private Integer order;

        @TableField("`code`")
        private String code;

}