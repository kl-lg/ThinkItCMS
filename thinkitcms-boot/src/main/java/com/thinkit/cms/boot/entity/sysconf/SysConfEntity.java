package com.thinkit.cms.boot.entity.sysconf;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.thinkit.cms.model.BaseModel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
/**
* @author System
*/
@Getter
@Setter
@Accessors(chain = true)
@TableName("tk_sys_conf")
public class SysConfEntity extends BaseModel {
       /**
         配置项名称
       */
        @TableField("name")
        private String name;

       /**
         配置项编码
       */
        @TableField("code")
        private String code;

       /**
         配置项值
       */
        @TableField("value")
        private String value;

       /**
         描述
       */
        @TableField("description")
        private String description;

       /**
         是否删除 0：正常 1：删除
       */
        @TableField("deleted")
        private Boolean deleted;


}