package com.thinkit.cms.boot.events.listener;

import com.thinkit.cms.api.content.ContentService;
import com.thinkit.cms.constant.SysConst;
import com.thinkit.cms.directive.event.ContentUpEsEvent;
import com.thinkit.cms.directive.kit.RedisLuaUtils;
import com.thinkit.cms.model.ModelJson;
import com.thinkit.cms.utils.Checker;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import java.util.Map;

@Slf4j
@Component
public class ContentUpListener extends BaseListener  implements ApplicationListener<ContentUpEsEvent> {


    @Autowired
    private ContentService contentService;

    @Override
    public void onApplicationEvent(ContentUpEsEvent event) {
        try {
            if(enable()){
                switch (event.getEventEnum()){
                    case UPDATE_CONTENT_FIELD -> {
                        ModelJson modelJson = ( ModelJson)event.getSource();
                        String pk = modelJson.getPk();
                        if(Checker.BeNotBlank(pk)){
                            String formModelCode = contentService.queryFormModelCode(pk);
                            String key = modelJson.buildKey(formModelCode, SysConst.colon);
                            Map<String,Object> data = RedisLuaUtils.getJson(key);
                            if(Checker.BeNotEmpty(data)){
                                data.putAll(modelJson.getObjectMap());
                                modelJson.setKey(key);
                                modelJson.setObjectMap(data);
                                RedisLuaUtils.setJson(modelJson);
                            }
                        }
                    }
                }
            }
        }catch (Exception e){
            log.error("ContentUpEsListener 执行失败：{}",e.getMessage());
        }
    }
}
