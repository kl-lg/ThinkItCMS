package com.thinkit.cms.boot.events.listener;

import com.thinkit.cms.boot.events.FormDataEvent;
import com.thinkit.cms.constant.SysConst;
import com.thinkit.cms.directive.kit.RedisLuaUtils;
import com.thinkit.cms.dto.content.ContentDto;
import com.thinkit.cms.dto.formdata.FormDataDto;
import com.thinkit.cms.model.ModelJson;
import com.thinkit.cms.utils.Checker;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Component
public class FormDataListener extends BaseListener implements ApplicationListener<FormDataEvent> {

    @Override
    public void onApplicationEvent(FormDataEvent event) {
        try {
            if(enable()){
                switch (event.getEventEnum()) {
                    case SAVE_FORM_DATA,UPDATE_FORM_DATA -> {
                        FormDataDto formDataDto =getObj(event);
                        ModelJson modelJson  = getToMap(formDataDto);
                        RedisLuaUtils.setJson(modelJson,formDataDto.getFormModelCode(),SysConst.colon);
                    }
                    case DELETE_FORM_DATA -> {
                        ModelJson modelJson = getObj(event);
                        RedisLuaUtils.delete(modelJson.getKey());
                    }
                    case SAVE_BATCH_FORM_DATA -> {
                        List<FormDataDto> formDataDtos = getList(event);
                        formDataDtos.forEach(formDataDto->{
                            ModelJson modelJson  = getToMap(formDataDto);
                            RedisLuaUtils.setJson(modelJson,formDataDto.getFormModelCode(),SysConst.colon);
                        });
                    }
                }
            }
        }catch (Exception e){
            log.error("FormDataEsListener 执行失败：{}",e.getMessage());
        }
    }

    private List<String> keys(List<FormDataDto>  formDataDtos){
        List<String> keys = new ArrayList<>();
        if(Checker.BeNotEmpty(formDataDtos)){
            formDataDtos.forEach(formDataDto->{
                ModelJson modelJson = new ModelJson();
                modelJson.setPrefix(SysConst.contentPrefix);
                modelJson.setPk(formDataDto.getId());
                keys.add(modelJson.buildKey(formDataDto.getFormModelCode(),SysConst.colon));
            });
        }
        return keys;
    }
}
