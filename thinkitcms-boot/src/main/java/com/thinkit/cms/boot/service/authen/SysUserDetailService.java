package com.thinkit.cms.boot.service.authen;

import cn.dev33.satoken.stp.StpUtil;
import cn.hutool.json.JSONUtil;
import com.thinkit.cms.api.sysuser.SysUserService;
import com.thinkit.cms.authen.annotation.UserSvr;
import com.thinkit.cms.authen.enums.GrantTypes;
import com.thinkit.cms.authen.enums.LoginTypes;
import com.thinkit.cms.authen.enums.UserTypeDef;
import com.thinkit.cms.authen.exceptions.AuthenException;
import com.thinkit.cms.user.User;
import com.thinkit.cms.user.UserDetail;
import com.thinkit.cms.user.UserDetailService;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

/**
 * @author LONG
 */
@Service
@UserSvr(plat = LoginTypes.PLAT_USER,grant = GrantTypes.PASSWORD)
public class SysUserDetailService implements UserDetailService {

    private final SysUserService sysUserService;

    public SysUserDetailService(SysUserService sysUserService) {
        this.sysUserService = sysUserService;
    }

    @Override
    public UserDetail loadUserName(String userName) {
        return sysUserService.loadUserName(userName);
    }


    @Override
    public List<String> getPermissionList(String loginId, String loginType) {
        User user = getUserCtx();
        return sysUserService.queryAuthSign(loginId,user.getUserCtx().getIsSuperRole());
    }

    @Override
    public List<String> getRoleList(Object loginId, String loginType) {
        System.out.println("=============开始查询 角色列表===============");
        return Arrays.asList("manager","sysuser");
    }

    @Override
    public boolean checkVerifyCode(String verifyUid, String verifyCode) {
        return sysUserService.checkVerifyCode(verifyUid,verifyCode);
    }

    @Override
    public void destroyVerifyCode(String verifyUid, String verifyCode) {

    }

    protected User getUserCtx(){
        Object object =  StpUtil.getExtra(UserTypeDef.USER_CLIENT_DETAIL.getType());
        if(object==null){
            throw new AuthenException(403,"用户信息获取失败!");
        }
        return JSONUtil.toBean(JSONUtil.toJsonStr(object),User.class);
    }
}
