package com.thinkit.cms.boot.apsect;

import cn.dev33.satoken.stp.StpUtil;
import cn.hutool.json.JSONUtil;
import com.thinkit.cms.annotation.Site;
import com.thinkit.cms.authen.enums.UserTypeDef;
import com.thinkit.cms.authen.exceptions.AuthenException;
import com.thinkit.cms.core.handler.CustomException;
import com.thinkit.cms.model.BaseDto;
import com.thinkit.cms.model.PageModel;
import com.thinkit.cms.user.SiteConf;
import com.thinkit.cms.user.User;
import com.thinkit.cms.utils.Checker;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
/**
 * @author LONG
 */
@Aspect
@Component
public class SiteAspect {

		@Autowired
		protected RedisTemplate redisTemplate;

		protected User getUserCtx(){
			Object object = StpUtil.getExtra(UserTypeDef.USER_CLIENT_DETAIL.getType());
			if(Checker.BeNull(object)){
				throw new AuthenException(403,"获取用户信息失败!");
			}
			User user = JSONUtil.toBean(JSONUtil.toJsonStr(object),User.class);
			return user;
		}

		protected String getSiteId(boolean allow){
		    SiteConf siteConf = getUserCtx().getSite(!allow);
			if(Checker.BeNull(siteConf)){
				return "";
			}
			return siteConf.getSiteId();
		}

		protected String getSiteTmpId(boolean allow){
			return getUserCtx().getSite(!allow).getTemplateId();
		}

	  @Around("execution(* com.thinkit.cms.*..*.*(..)) && @annotation(com.thinkit.cms.annotation.Site)")
      public Object doAround(ProceedingJoinPoint pjp) throws Throwable {
		    Object[] args = pjp.getArgs();
		    MethodSignature ms=(MethodSignature)pjp.getSignature();
		    boolean allow=ms.getMethod().getAnnotation(Site.class).allow();
		    boolean verifyTmp=ms.getMethod().getAnnotation(Site.class).verifyTmp();
		    String templateId = null;
			if(verifyTmp){
				templateId = getSiteTmpId(allow);
				if(Checker.BeBlank(templateId)){
					throw new CustomException(5050);
				}
		    }
			filterParams(args,allow,templateId);
			Object result = pjp.proceed();
			return result;
      }

	  private void filterParams(Object[] args,boolean allow,String templateId){
		  if(Checker.BeNotEmpty(args)){
			  for(Object arg:args){
				  if(isDto(arg)){
					  setSiteId((BaseDto) arg,allow,templateId);
					  break;
				  }else if(isPageDto(arg)){
					  setSiteId(((PageModel) arg).getDto(),allow,templateId);
					  break;
				  }
			  }
		  }
	  }

	  private boolean isDto(Object arg){
		  return Checker.BeNotNull(arg) && arg instanceof BaseDto;
	  }

	  private boolean isPageDto(Object arg){
		return Checker.BeNotNull(arg) && arg instanceof PageModel;
	  }

	  private void setSiteId(BaseDto baseDto,boolean allow,String templateId){
		  baseDto.setSiteId(getSiteId(allow));
		  baseDto.setTemplateId(templateId);
	  }
	  
}
