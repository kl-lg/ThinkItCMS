package com.thinkit.cms.boot.events;

import com.thinkit.cms.enums.EventEnum;
import lombok.Getter;
import lombok.Setter;
import org.springframework.context.ApplicationEvent;
public class ContentEvent extends ApplicationEvent {

    @Getter @Setter
    public EventEnum eventEnum;

    public ContentEvent(Object source, EventEnum eventEnum) {
        super(source);
        this.eventEnum = eventEnum;
    }
}
