package com.thinkit.cms.boot.controller;

import com.thinkit.cms.boot.entity.sysuser.SysUserEntity;
import com.thinkit.cms.directive.unit.DirectiveUnit;
import com.thinkit.cms.directive.unit.ParamUnit;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("test")
public class TestController {


    @Autowired
    Configuration configuration;


    @GetMapping(value="/gen")
    public void gen(@RequestParam(required = false) String title,@RequestParam(required = false) String  categoryId) throws IOException, TemplateException {
        Map map = new HashMap();
        SysUserEntity sysUserEntity = new SysUserEntity();
        sysUserEntity.setEmail("826319429@qq.com");
        List<ParamUnit> listobj=new ArrayList<>();

        ParamUnit p1  = new ParamUnit();
        p1.setName("test");
        listobj.add(p1);



        Map maptest = new HashMap();
        maptest.put("k1",111);
        maptest.put("k2","222");
        DirectiveUnit k3 = new DirectiveUnit();
        k3.setDirective("k3");
        k3.setName("的活动积极");
        maptest.put("k3",k3);

        Map k4 = new HashMap();
        SysUserEntity sysUser = new SysUserEntity();
        sysUser.setEmail("8263194ddddddddddddddddddddd29@qq.com");
        k4.put("k4","7777777777");
        k4.put("k5",sysUser);
        maptest.put("k4",k4);

        map.put("title",title);
        map.put("user",sysUserEntity);
        map.put("listobj",listobj);
        map.put("mapObj",maptest);
        map.put("contentId","595748320299712512");
        map.put("categoryId",categoryId);
        FileOutputStream outputStream = new FileOutputStream("D:\\freemark\\test.html");
        Template t = configuration.getTemplate("test.ftl");
        Writer out = new OutputStreamWriter(outputStream, Charset.forName("UTF-8"));
        t.process(map, out);
        out.flush();
        out.close();
    }

}
