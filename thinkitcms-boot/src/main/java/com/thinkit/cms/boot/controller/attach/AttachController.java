package com.thinkit.cms.boot.controller.attach;

import com.thinkit.cms.annotation.Logs;
import com.thinkit.cms.api.attach.AttachService;
import com.thinkit.cms.authen.annotation.Sentinel;
import com.thinkit.cms.core.base.BaseController;
import com.thinkit.cms.dto.attach.AttachDto;
import com.thinkit.cms.model.PageModel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;

import static com.thinkit.cms.enums.LogModule.ATTACH;

/**
* @author LG
*/

@Validated
@RestController
@RequestMapping("/api/attach")
@Slf4j
public class AttachController extends BaseController<AttachService> {


        @GetMapping(value="detail")
        public AttachDto detail(@NotBlank(message = "ID不能为空!") @RequestParam String id){
             return service.getByPk(id);
        }

        @PostMapping(value = "/page")
        public PageModel<AttachDto> list(@RequestBody PageModel<AttachDto> pageDto) {
                pageDto.getDto().setSiteId(getSiteId(true));
             return service.listPage(pageDto);
        }

        @Logs(module = ATTACH,operation = "附件保存")
        @PostMapping(value="save")
        public void save(@Validated @RequestBody AttachDto attachDto) {
              service.insert(attachDto);
        }

        @PutMapping(value="update")
        public void update(@Validated @RequestBody AttachDto attachDto) {
              service.updateByPk(attachDto);
        }

        @Logs(module = ATTACH,operation = "附件删除")
        @Sentinel
        @DeleteMapping(value="delete")
        public boolean deleteByPk(@NotBlank @RequestParam String id){
                return service.deleteByPk(id);
        }

}