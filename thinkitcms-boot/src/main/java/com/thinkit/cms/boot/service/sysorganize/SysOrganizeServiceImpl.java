package com.thinkit.cms.boot.service.sysorganize;

import cn.hutool.core.date.DateUtil;
import com.google.common.collect.Lists;
import com.thinkit.cms.api.sysorganize.SysOrganizeService;
import com.thinkit.cms.boot.entity.sysorganize.SysOrganizeEntity;
import com.thinkit.cms.boot.mapper.sysorganize.SysOrganizeMapper;
import com.thinkit.cms.utils.BuildTree;
import com.thinkit.cms.core.base.BaseServiceImpl;
import com.thinkit.cms.core.handler.CustomException;
import com.thinkit.cms.utils.Tree;
import com.thinkit.cms.dto.sysorganize.SysOrganizeDto;
import com.thinkit.cms.dto.sysorganize.SysOrganizeTree;
import com.thinkit.cms.model.CmsResult;
import com.thinkit.cms.utils.Checker;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
* @author LG
*/

@Service
public class SysOrganizeServiceImpl extends BaseServiceImpl<SysOrganizeDto,SysOrganizeEntity,SysOrganizeMapper> implements SysOrganizeService{

    private final String rootPid ="0";

    @Override
    public List<Tree> orgTableTree() {
        List<SysOrganizeDto> orgs = baseMapper.orgTableTree(getUserId());
        if(Checker.BeNotEmpty(orgs)){
            List<Tree> trees = new ArrayList<>(16);
            Tree tree = BuildTree.build(orgToTree(orgs, SysOrganizeTree.class));
            if("-1".equals(tree.getId())){
                trees.addAll(tree.getChildren());
            }else{
                trees.add(tree);
            }
            return trees;
        }
        return Lists.newArrayList();
    }


    @Transactional(rollbackFor = Exception.class)
    @Override
    public void save(SysOrganizeDto sysOrganizeDto) {
        if(hasOrgCode(null,sysOrganizeDto.getOrgCode())){
           throw new CustomException(CmsResult.result(5031));
        }
        super.insert(sysOrganizeDto);
    }


    @Transactional(rollbackFor = Exception.class)
    @Override
    public void update(SysOrganizeDto sysOrganizeDto) {
        if(hasOrgCode(sysOrganizeDto.getId(),sysOrganizeDto.getOrgCode())){
            throw new CustomException(CmsResult.result(5031));
        }
        super.updateByPk(sysOrganizeDto);
    }

    @Override
    public SysOrganizeDto detail(String id) {
        SysOrganizeDto organizeDto = super.getByPk(id);
        String pid = organizeDto.getPid();
        if(Checker.BeNotBlank(pid) ){
            String parentName="顶级";
            if(!rootPid.equals(pid)){
                parentName = baseMapper.getPorgName(pid);
            }
            organizeDto.setParentName(parentName);
        }
        return organizeDto;
    }


    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean delete(String id) {
        SysOrganizeDto organizeDto = super.getByPk(id);
        if(Checker.BeNotNull(organizeDto)){
            String orgId= organizeDto.getId();
            Integer childCount = baseMapper.hasChild(orgId);
            if(childCount.intValue()>0){
                throw new CustomException(CmsResult.result(5034));
            }
            return super.deleteByPk(orgId);
        }
        return false;
    }



    private Boolean hasOrgCode(String orgId,String orgCode){
        Integer count = baseMapper.hasOrgCode(orgId,orgCode);
        return count>0;
    }

    private <T>List<Tree<T>> orgToTree(List<SysOrganizeDto> orgs,Class clz){
        List<Tree<T>> trees = new ArrayList<>();
        for(SysOrganizeDto org:orgs){
            try {
                Tree tree = (Tree) clz.getConstructor().newInstance();
                copy(org,tree);
                tree.setName(org.getOrgName());
                Map<String,Object> meta = new HashMap<>(16);
                meta.put("gmtCreate", DateUtil.format(org.getGmtCreate(),"yyyy-MM-dd"));
                tree.setMeta(meta);
                trees.add(tree);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return trees;
    }
}