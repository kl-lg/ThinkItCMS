package com.thinkit.cms.boot.entity.syslicense;
import lombok.Getter;
import lombok.Setter;
import java.math.BigDecimal;
import lombok.experimental.Accessors;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.thinkit.cms.model.BaseModel;
import com.baomidou.mybatisplus.annotation.TableName;
/**
* @author LG
*/
@Getter
@Setter
@Accessors(chain = true)
@TableName("tk_sys_license")
public class SysLicenseEntity extends BaseModel  {
       /**
         站点域名
       */
        @TableField("domain")
        private String domain;

       /**
         颁发者
       */
        @TableField("issuer")
        private String issuer;

       /**
         企业/组织名称
       */
        @TableField("organization")
        private String organization;

       /**
         生效开始时间
       */
        @TableField("effect_date")
        private Date effectDate;

       /**
         截止时间
       */
        @TableField("deadline")
        private Date deadline;

       /**
         签名
       */
        @TableField("signaturer")
        private String signaturer;

       /**
         版本号
       */
        @TableField("version")
        private String version;

       /**
         证书类型
       */
        @TableField("certificate_type")
        private String certificateType;

       /**
         0:正常 1：删除
       */
        @TableField("deleted")
        private Boolean deleted;

        @TableField("license_url")
        private String licenseUrl;
}