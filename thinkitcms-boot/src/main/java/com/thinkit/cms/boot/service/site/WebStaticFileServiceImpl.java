package com.thinkit.cms.boot.service.site;

import cn.hutool.core.codec.Base64;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.IoUtil;
import cn.hutool.core.util.ZipUtil;
import com.thinkit.cms.api.site.SiteService;
import com.thinkit.cms.api.site.WebStaticFileService;
import com.thinkit.cms.constant.SysConst;
import com.thinkit.cms.core.base.BaseSiteService;
import com.thinkit.cms.dto.site.WebFileDto;
import com.thinkit.cms.model.CmsResult;
import com.thinkit.cms.model.FileViewModel;
import com.thinkit.cms.utils.Checker;
import com.thinkit.cms.utils.CmsFolderUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributeView;
import java.util.List;
import java.util.*;

@Slf4j
@Service
public class WebStaticFileServiceImpl extends BaseSiteService implements WebStaticFileService {

    @Autowired
    SiteService siteService;
    private static List<String> suffixs = new ArrayList<>();

    private static List<String> imgSuffixs = new ArrayList<>();

    static {
        suffixs.addAll(Arrays.asList("html","js","txt","css","vue","md"));
        imgSuffixs.addAll(Arrays.asList("jpg","png","gif"));
    }

    @Override
    public List<FileViewModel> listFile(String path) {
        boolean isRoot = false;
        String domain = siteService.getDoamin(null,getSiteId(false));
        if(Checker.BeBlank(path)){
            path = CmsFolderUtil.getSitePath(null,domain);
            isRoot = true;
        }else{
            path = Base64.decodeStr(path, StandardCharsets.UTF_8);
            String rootPath = CmsFolderUtil.getSitePath(null,domain);
            isRoot = rootPath.equals(path);
        }
        List<FileViewModel> fileInfoModels = new ArrayList<>();
        if(Checker.BeNotBlank(path)){
            File[] files = FileUtil.ls(path);
            if(Checker.BeNotEmpty(files)){
                int i=0;
                for(File file:files){
                    if(i%9==0){
                        if(Checker.BeNotBlank(path)){
                            if(!isRoot){
                                fileInfoModels.add(buildPre(path));
                            }
                        }
                    }
                    Path pathInfo= Paths.get(file.getAbsolutePath());
                    BasicFileAttributeView basicview= Files.getFileAttributeView(pathInfo, BasicFileAttributeView.class);
                    FileViewModel fileInfoModel;
                    try {
                        fileInfoModel = new FileViewModel(file.getName(),file.isDirectory(),basicview.readAttributes());
                        String url = path+SysConst.prefix+file.getName();
                        fileInfoModel.setRelativePath(Base64.encode(url,StandardCharsets.UTF_8));
                        fileInfoModels.add(fileInfoModel);
                    } catch (IOException e) {
                        log.error(e.getMessage());
                    }
                    i++;
                }
                return fileInfoModels;
            }
        }
        fileInfoModels.add(buildPre(path));
        return fileInfoModels;
    }

    @Override
    public CmsResult fileContent(String path) {
        String content="";
        path = Base64.decodeStr(path, StandardCharsets.UTF_8);
        String suffix = FileUtil.getSuffix(path.toLowerCase());
        if(Checker.BeNotBlank(suffix) && suffixs.contains(suffix)){
            content = FileUtil.readString(path,StandardCharsets.UTF_8);
            return CmsResult.result(content);
        }else if(imgSuffixs.contains(suffix)){
            byte[] blog = FileUtil.readBytes(path);
            return CmsResult.result(blog);
        }
        return CmsResult.result(content);
    }

    @Override
    public CmsResult setContent(WebFileDto webFileDto) {
        String path=webFileDto.getPath(),content = webFileDto.getContent();
        path = Base64.decodeStr(path, StandardCharsets.UTF_8);
        FileUtil.writeString(content,new File(path),StandardCharsets.UTF_8);
        return CmsResult.result();
    }

    @Override
    public void deleteFile(String filePath) {
        filePath = Base64.decodeStr(filePath, StandardCharsets.UTF_8);
        FileUtil.del(filePath);
    }


    @Override
    public void openDir(String absolutePath) {
        try {
            absolutePath = Base64.decodeStr(absolutePath, StandardCharsets.UTF_8);
            if(!FileUtil.isDirectory(absolutePath)){
                absolutePath = FileUtil.file(absolutePath).getParent();
            }
            Desktop.getDesktop().open(new File(absolutePath));
        }catch (Exception e){
            log.error("打开文件夹失败："+e.getMessage());
        }
    }

    @Override
    public void zipDown(String absolutePath) {
        absolutePath = Base64.decodeStr(absolutePath, StandardCharsets.UTF_8);
        if(!FileUtil.isDirectory(absolutePath)){
            absolutePath = FileUtil.file(absolutePath).getParent();
        }
        String zipFilePath = CmsFolderUtil.getAbsolutePath(UUID.randomUUID()+".zip");
        try {
            ZipUtil.zip( absolutePath,zipFilePath, true);
            downZip(zipFilePath);
        }catch (Exception e){
            log.error(e.getMessage());
        }finally {
            FileUtil.del(zipFilePath);
        }
    }

    private void downZip(String filePath)  {
        HttpServletResponse response = getResponse();
        String fileName = new Date().getTime()+"."+FileUtil.getSuffix(filePath);
        response.setCharacterEncoding("utf-8");
        response.setHeader("FileName", fileName);
        response.setHeader("Access-Control-Expose-Headers", "FileName");
        response.setHeader(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" +fileName);
        response.setContentType("application/octet-stream");
        Path path = Paths.get(filePath);
        byte[] data;
        try {
            data = Files.readAllBytes(path);
            response.setContentLengthLong(data.length);
            IoUtil.write(response.getOutputStream(),true,data);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private FileViewModel buildPre(String path){
        FileViewModel viewModel = new FileViewModel("返回上一级",false);
        String url = path.substring(0,path.lastIndexOf("/"));
        viewModel.setRelativePath(Base64.encode(url,StandardCharsets.UTF_8));
        viewModel.setRoot(true);
        return viewModel;
    }
}
