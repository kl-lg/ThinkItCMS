package com.thinkit.cms.boot.controller.category;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.thinkit.cms.annotation.Logs;
import com.thinkit.cms.annotation.Site;
import com.thinkit.cms.annotation.ValidGroup1;
import com.thinkit.cms.annotation.ValidGroup3;
import com.thinkit.cms.api.category.CategoryService;
import com.thinkit.cms.authen.annotation.Sentinel;
import com.thinkit.cms.core.base.BaseController;
import com.thinkit.cms.directive.wrapper.rule.PathRule;
import com.thinkit.cms.dto.category.*;
import com.thinkit.cms.dto.sysmenu.CategoryTree;
import com.thinkit.cms.enums.TaskStatusEnum;
import com.thinkit.cms.model.KeyValueModel;
import com.thinkit.cms.utils.Tree;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import java.util.List;

import static com.thinkit.cms.enums.LogModule.CATEGORY;

/**
* @author LG
*/

@Validated
@RestController
@RequestMapping("/api/category")
@Slf4j
public class CategoryController extends BaseController<CategoryService> {

        @RequestMapping(value = "/tree", method = RequestMethod.GET)
        public Tree<CategoryTree> categoryTree(@RequestParam("pid") String pid) {
                return service.categoryTree(pid);
        }

        @RequestMapping(value = "/treeTable", method = RequestMethod.GET)
        public List<Tree<CategoryTree>> treeTable(@RequestParam("pid") String pid) {
                return service.treeTable(pid);
        }


        @RequestMapping(value = "/treeTableForContent", method = RequestMethod.GET)
        public List<Tree<CategoryTree>> treeTableForContent(@RequestParam("pid") String pid) {
                return service.treeTableForContent(pid);
        }


        @Logs(module = CATEGORY,operation = "栏目保存")
        @Sentinel
        @Site
        @RequestMapping(value="save",method= RequestMethod.POST)
        public void save(@Validated(value = {ValidGroup1.class}) @RequestBody AddEditCategory addEditCategory) {
              service.save(addEditCategory);
        }


        @RequestMapping(value = "/getKeyValueModels", method = RequestMethod.GET)
        public List<KeyValueModel> getKeyValueModels() {
                return  PathRule.keyValues();
        }


        @Logs(module = CATEGORY,operation = "栏目更新")
        @Sentinel
        @RequestMapping(value="update",method= RequestMethod.PUT)
        public void update(@Validated @RequestBody AddEditCategory addEditCategory) {
              service.update(addEditCategory);
        }



        @Logs(module = CATEGORY,operation = "栏目生成")
        @RequestMapping(value="createTmpHome",method= RequestMethod.PUT)
        public  String createTmpHome(@Validated @RequestBody  PublishCategory publishCategory) {
                return service.createTmpHome(publishCategory);
        }


        /**
         * 批量生成栏目首页
         * @param publishCategory
         */
        @Logs(module = CATEGORY,operation = "栏目批量生成")
        @RequestMapping(value="createTmpHomes",method= RequestMethod.PUT)
        public String createTmpHomes(@Validated(value = {ValidGroup3.class}) @RequestBody  PublishCategory publishCategory) {
                return service.createTmpHomes(publishCategory);
        }



        @Logs(module = CATEGORY,operation = "首页生成")
        @SaCheckPermission("site:inithome")
        @RequestMapping(value="createIndexHome",method= RequestMethod.PUT)
        public void createIndexHome(@Validated @RequestBody IndexCategory indexCategory) {
                service.initIndexTemp(indexCategory.getSiteId());
        }


        @Logs(module = CATEGORY,operation = "栏目详情查看")
        @RequestMapping(value = "/detail", method = RequestMethod.GET)
        public CategoryDto detail(@NotBlank(message = "ID不能为空!")  String id) {
             return service.detail(id, getSiteId(true));
        }


        @Logs(module = CATEGORY,operation = "栏目删除")
        @Sentinel
        @PostMapping(value="deletes")
        public boolean deletes(@Validated(value = {ValidGroup3.class}) @RequestBody DeleteCategory deleteCategory){
                return service.deletes(deleteCategory.getCategoryIds());
        }

        @Logs(module = CATEGORY,operation = "栏目任务清理")
        @DeleteMapping(value="clearTaskId")
        public void clearTaskId(String categoryId){
                service.clearTaskId(categoryId);
        }



        @PutMapping(value="upStatus")
        public void upStatus(String categoryId){
                service.upStatus(categoryId, TaskStatusEnum.FINSHED);
        }




}