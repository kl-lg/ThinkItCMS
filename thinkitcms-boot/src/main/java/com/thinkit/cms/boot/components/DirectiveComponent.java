package com.thinkit.cms.boot.components;

import cn.hutool.core.util.StrUtil;
import com.thinkit.cms.constant.SysConst;
import com.thinkit.cms.directive.annotation.Handler;
import com.thinkit.cms.directive.annotation.Instruct;
import com.thinkit.cms.directive.executor.BaseMethod;
import com.thinkit.cms.directive.executor.Executor;
import com.thinkit.cms.directive.factory.DirectiveFactory;
import com.thinkit.cms.directive.funs.BaseFunInterface;
import com.thinkit.cms.directive.handler.DataHandler;
import com.thinkit.cms.directive.handler.DefaultDataHandler;
import com.thinkit.cms.directive.render.DirectiveExecutor;
import com.thinkit.cms.directive.unit.DirectiveUnit;
import com.thinkit.cms.utils.Checker;
import com.thinkit.cms.utils.CmsConf;
import com.thinkit.cms.utils.CmsFolderUtil;
import freemarker.template.Configuration;
import freemarker.template.SimpleHash;
import freemarker.template.TemplateExceptionHandler;
import freemarker.template.TemplateModelException;
import lombok.extern.slf4j.Slf4j;
import org.reflections.Reflections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.util.*;

/**
 * 指令组件(注册所有的指令组件)
 * @author LONG
 */
@Slf4j
@Component
public class DirectiveComponent {
    
    private final Configuration configuration;

    private final TemplateExceptionHandler templateExceptionHandler;

    private static Map<String,DataHandler> handMap=new HashMap<>(16);

    private static List<BaseMethod> baseMethodList = new ArrayList<>();


    public DirectiveComponent(Configuration configuration, TemplateExceptionHandler templateExceptionHandler) {
        this.configuration = configuration;
        this.templateExceptionHandler = templateExceptionHandler;
    }

    static {
        handMap.put(CmsConf.get(SysConst.defaultHandler),new DefaultDataHandler());
        String[] packages = CmsConf.get(SysConst.baseHandler).split(",");
        Reflections reflections = new Reflections(packages);
        Set<Class<?>> classes = reflections.getTypesAnnotatedWith(Handler.class);
        for(Class clz:classes){
            Constructor<?> cons;
            try {
                Handler handler = (Handler) clz.getAnnotation(Handler.class);
                if(Checker.BeNotNull(handler)){
                    cons = clz.getConstructor();
                    DataHandler filter = (DataHandler)cons.newInstance();
                    if(!handMap.containsKey(handler.value())){
                        handMap.put(handler.value(),filter);
                    }
                }
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }

    @Autowired
    public void configDirective(Configuration configuration, List<BaseMethod> baseMethods,List<BaseFunInterface> baseFunInterfaces) throws TemplateModelException, IOException {
        if(Checker.BeEmpty(baseMethodList)){
            baseMethodList = baseMethods;
        }
        apply(configuration,baseFunInterfaces);
        configuration.setTemplateExceptionHandler(templateExceptionHandler);
        try {
            configuration.setDirectoryForTemplateLoading(CmsFolderUtil.initBaseFolder());
        }catch (IOException e){
            log.error(e.getMessage());
            throw new RuntimeException("初始化模板加载路径异常,请检查 config.properties 中是否存在 [cmsFolder] 配置项，并且是否存在该路径 ");
        }
    }

    public void apply(Configuration configuration,List<BaseFunInterface> baseFunInterfaces) throws TemplateModelException {
        if(Checker.BeNull(configuration)){
            configuration = this.configuration;
        }
        Map<String, BaseFunInterface> baseFunMap= new HashMap<>(16);
        Map<String, Object> freemarkerVariables = new HashMap<>(16);
        if(Checker.BeNotEmpty(baseFunInterfaces)){
            for(BaseFunInterface baseFunInterface:baseFunInterfaces){
                Instruct instruct = baseFunInterface.getClass().getAnnotation(Instruct.class);
                if(Checker.BeNotNull(instruct)){
                    baseFunMap.put(instruct.value(),baseFunInterface);
                }else{
                    String instructName = baseFunInterface.getClass().getSimpleName();
                    instructName = StrUtil.lowerFirst(instructName);
                    baseFunMap.put(instructName,baseFunInterface);
                }
            }
        }
        List<DirectiveUnit> directiveUnits =  DirectiveFactory.directives();
        for(DirectiveUnit directiveUnit:directiveUnits){
            String handler = directiveUnit.getConfig().getResult().getHandler();
            DataHandler dataHandler = getHandler(handler);
            directiveUnit.getConfig().getResult().setDataHandler(dataHandler);
            DirectiveExecutor directiveExecutor = new Executor();
            directiveExecutor.setDirectiveMeta(directiveUnit);
            directiveExecutor.setDataHandler(dataHandler);
            if(Checker.BeNotEmpty(baseFunMap)){
                directiveExecutor.setFuncInterface(baseFunMap);
            }
            freemarkerVariables.put(directiveUnit.getDirective(),directiveExecutor);
        }
        if(Checker.BeNotEmpty(baseMethodList)){
            for (BaseMethod baseMethod:baseMethodList){
                freemarkerVariables.put(baseMethod.getName().getValue(),baseMethod);
            }
        }
        if(!freemarkerVariables.isEmpty()){
            configuration.setAllSharedVariables(new SimpleHash(freemarkerVariables,configuration.getObjectWrapper()));
        }
    }



    private DataHandler getHandler(String handler){
        DataHandler dataHandler = handMap.get(handler);
        if(Checker.BeNull(dataHandler)){
            dataHandler = new DefaultDataHandler();
        }
        return dataHandler;
    }


}
