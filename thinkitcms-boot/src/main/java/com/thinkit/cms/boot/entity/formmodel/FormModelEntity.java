package com.thinkit.cms.boot.entity.formmodel;
import lombok.Getter;
import lombok.Setter;
import java.math.BigDecimal;
import lombok.experimental.Accessors;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.thinkit.cms.model.BaseModel;
import com.baomidou.mybatisplus.annotation.TableName;
/**
* @author LG
*/
@Getter
@Setter
@Accessors(chain = true)
@TableName("tk_form_model")
public class FormModelEntity extends BaseModel  {
       /**
         站点ID
       */
        @TableField("site_id")
        private String siteId;

       /**
         表单名称
       */
        @TableField("form_name")
        private String formName;

       /**
         模板路径
       */
        @TableField(exist = false)
        private String tmpPath;

       /**
         手机端模板路径
       */
        @TableField(exist = false)
        private String mobileTmpPath;

       /**
         表单类型  1、内容模型 2、分类模型 3、页面片段
       */
        @TableField("form_type")
        private String formType;

       /**
         是否删除 0：正常 1：删除
       */
        @TableField("deleted")
        private Boolean deleted;

        /**
         * 模型描述
         */
        @TableField("form_desc")
        private String formDesc;

        /**
         * 模型编码
         */
        @TableField("form_code")
        private String formCode;


}