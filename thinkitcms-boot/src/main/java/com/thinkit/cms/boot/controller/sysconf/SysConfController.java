package com.thinkit.cms.boot.controller.sysconf;

import com.thinkit.cms.api.sysconf.SysConfService;
import com.thinkit.cms.authen.annotation.Sentinel;
import com.thinkit.cms.core.base.BaseController;
import com.thinkit.cms.dto.sysconf.SysConfDto;
import com.thinkit.cms.model.PageModel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
/**
* @author System
*/

@Validated
@RestController
@RequestMapping("/sysConf")
@Slf4j
public class SysConfController extends BaseController<SysConfService> {


        @RequestMapping(value = "/page", method = RequestMethod.POST)
        public PageModel<SysConfDto> list(@RequestBody PageModel<SysConfDto> pageDto) {
             return service.listPage(pageDto);
        }

        @Sentinel
        @RequestMapping(value="save",method= RequestMethod.POST)
        public void save(@Validated @RequestBody SysConfDto sysConfDto) {
              service.insert(sysConfDto);
        }

        @Sentinel
        @RequestMapping(value="update",method= RequestMethod.PUT)
        public void update(@Validated @RequestBody SysConfDto sysConfDto) {
              service.updateByPk(sysConfDto);
        }


        @Sentinel
        @DeleteMapping(value="delete")
        public boolean deleteByPk(@NotBlank @RequestParam String id){
                return service.deleteByPk(id);
        }

}