package com.thinkit.cms.boot.mapper.sysrolemenu;
import org.apache.ibatis.annotations.Mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.thinkit.cms.boot.entity.sysrolemenu.SysRoleMenuEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
* @author LG
*/

@Mapper
public interface SysRoleMenuMapper extends BaseMapper<SysRoleMenuEntity> {

    List<String> queryMenuIdsByRoleId(@Param("roleId") String roleId);

    Integer isSysRole(@Param("roleId")  String roleId);
}