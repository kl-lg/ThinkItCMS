package com.thinkit.cms.boot.service.aicode;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.builder.CustomFile;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;
import com.thinkit.cms.api.aicode.CmsCodeGenrater;
import com.thinkit.cms.constant.SysConst;
import com.thinkit.cms.core.base.BaseController;
import com.thinkit.cms.dto.sysautocode.SysAutoCodeDto;
import com.thinkit.cms.model.BaseModel;
import com.thinkit.cms.utils.Checker;
import com.thinkit.cms.utils.CmsConf;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * @author LG
 */
@Component
public class CmsCodeGenraterImpl implements CmsCodeGenrater {

    @Autowired
    private DataSourceProperties dataSourceProperties ;

    private final String bootPath="\\thinkitcms-boot\\src\\main\\java\\com\\thinkit\\cms\\boot\\";
    private final String resource="\\thinkitcms-boot\\src\\main\\resources\\mapper";
    private final String apiPath="\\thinkitcms-api\\src\\main\\java\\com\\thinkit\\cms\\";

    private DataSourceConfig.Builder buildConfig(){
        return new DataSourceConfig.Builder(dataSourceProperties.getUrl(), dataSourceProperties.getUsername(), dataSourceProperties.getPassword());
    }

    @Override
    public void genrateCode(SysAutoCodeDto sysAutoCode) {
        String tkModule = StrUtil.upperFirst(sysAutoCode.getModuleName());
         FastAutoGenerator.create(buildConfig())
        .globalConfig((scanner, builder) -> builder.author(sysAutoCode.getAuthor()))
        .strategyConfig((scanner, builder) -> builder.addInclude(sysAutoCode.getTableName()).addTablePrefix("tk_")
        .controllerBuilder().superClass(BaseController.class).enableFileOverride().enableRestStyle().convertFileName(obj-> tkModule)
        .entityBuilder().enableLombok().superClass(BaseModel.class).enableTableFieldAnnotation().enableFileOverride().disableSerialVersionUID().convertFileName(obj-> tkModule)
        .serviceBuilder().enableFileOverride().convertServiceFileName(obj-> tkModule).convertServiceImplFileName(obj->tkModule)
        )
        .templateEngine(new FreemarkerTemplateEngine()).injectionConfig((scanner, builder) ->builder.beforeOutputFile((tableinfo,objMap)->{
             objMap.put("tkModule", tkModule);
             System.out.printf("mapInfo:"+JSONUtil.toJsonStr(objMap));
         }).customFile(customFiles( StrUtil.upperFirst(sysAutoCode.getModuleName())))
         )
        .execute();
    }

    @Override
    public void genMyPageCode(SysAutoCodeDto sysAutoCode) {
        String tkModule = StrUtil.upperFirst(sysAutoCode.getModuleName());
        FastAutoGenerator.create(buildConfig())
        .globalConfig((scanner, builder) -> builder.author(sysAutoCode.getAuthor()))
                .strategyConfig((scanner, builder) -> builder.addInclude(sysAutoCode.getTableName()).addTablePrefix("tk_"))
                .templateEngine(new FreemarkerTemplateEngine()).injectionConfig((scanner, builder) ->builder.beforeOutputFile((tableinfo,objMap)->{
                    objMap.put("tkModule", tkModule);
                    System.out.printf("mapInfo:"+JSONUtil.toJsonStr(objMap));
                }).customFile(customPageFiles( StrUtil.upperFirst(sysAutoCode.getModuleName())))
        )
        .execute();
    }


    public List<CustomFile> customPageFiles(String tkModule) {
        // 设置自定义输出文件
        String root = initPageRoot(tkModule);
        List<CustomFile> customFiles = new ArrayList<>();
        customFiles.add(new CustomFile.Builder().fileName("/index.vue").filePath(root).templatePath("/templates/index.vue.ftl").build());
        customFiles.add(new CustomFile.Builder().fileName("/form.vue").filePath(root).templatePath("/templates/form.vue.ftl").build());
        customFiles.add(new CustomFile.Builder().fileName("/zh-CN.ts").filePath(root+"/local").templatePath("/templates/zh-CN.ts.ftl").build());
        return customFiles;
    }

    private String initPageRoot(String tkModule){
        String filePath = CmsConf.get(SysConst.genPageRoot);
        filePath=filePath+"/"+tkModule;
        if(Checker.BeNotBlank(filePath)&& FileUtil.isDirectory(filePath)){
            if(!FileUtil.exist(filePath)){
                FileUtil.mkdir(filePath);
            }
        }
        return filePath;
    }

    public List<CustomFile> customFiles(String tkModule) {
        // 设置自定义输出文件
        List<CustomFile> customFiles = new ArrayList<>();
        customFiles.add(new CustomFile.Builder().fileName("Entity.java").filePath(bootPath("entity")).templatePath("/templates/entity.java.ftl").packageName(tkModule.toLowerCase(Locale.ROOT)).build());
        customFiles.add(new CustomFile.Builder().fileName("Dto.java").filePath(apiPath("dto")).templatePath("/templates/dto.java.ftl").packageName(tkModule.toLowerCase(Locale.ROOT)).build());
        customFiles.add(new CustomFile.Builder().fileName("Controller.java").filePath(bootPath("controller")).templatePath("/templates/controller.java.ftl").packageName(tkModule.toLowerCase(Locale.ROOT)).build());
        customFiles.add(new CustomFile.Builder().fileName("Service.java").filePath(apiPath("api")).templatePath("/templates/service.java.ftl").packageName(tkModule.toLowerCase(Locale.ROOT)).build());
        customFiles.add(new CustomFile.Builder().fileName("ServiceImpl.java").filePath(bootPath("service")).templatePath("/templates/serviceImpl.java.ftl").packageName(tkModule.toLowerCase(Locale.ROOT)).build());
        customFiles.add(new CustomFile.Builder().fileName("Mapper.java").filePath(bootPath("mapper")).templatePath("/templates/mapper.java.ftl").packageName(tkModule.toLowerCase(Locale.ROOT)).build());
        customFiles.add(new CustomFile.Builder().fileName("Mapper.xml").filePath(resource(null)).templatePath("/templates/mapper.xml.ftl").packageName("").enableFileOverride().build());
        return customFiles;
    }

    private String bootPath(String pack){
        String baseDir =System.getProperty("user.dir");
        return baseDir+bootPath+pack;
    }


    private String resource(String pack){
        String baseDir =System.getProperty("user.dir");
        String resourcePath= resource;
        if(Checker.BeNotBlank(pack)){
            resourcePath+=pack;
        }
        return baseDir+resourcePath;
    }

    private String apiPath(String pack){
        String baseDir =System.getProperty("user.dir");
        return baseDir+apiPath+pack;
    }

    public static void main(String[] args) {
       // System.out.println(Thread.currentThread().getContextClassLoader().getResource(".").getPath());
        String baseDir =System.getProperty("user.dir");
        System.out.println(baseDir);
    }
}
