package com.thinkit.cms.boot.controller.tasks;

import com.thinkit.cms.directive.task.TaskDto;
import com.thinkit.cms.directive.task.TaskService;
import com.thinkit.cms.enums.TaskEnum;
import com.thinkit.cms.enums.TaskStatusEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;
import java.util.Map;

/**
* @author LG
*/

@Validated
@RestController
@RequestMapping("/api/task")
@Slf4j
public class TaskController {

        @Autowired
        private TaskService taskService;

        @RequestMapping(value = "/task", method = RequestMethod.GET)
        public Map<String,Object> query(@RequestBody TaskDto taskDto) {
              Map<String,Object> taskMap =  taskService.query(taskDto);
              return taskMap;
        }

        @RequestMapping(value = "/taskProgress", method = RequestMethod.GET)
        public Double taskProgress(@NotBlank(message = "请输入任务ID") String taskId, TaskEnum taskEnum) {
              Double progress = taskService.taskProgress(taskId,null,taskEnum);
             return progress;
        }

        @RequestMapping(value = "/upStatus", method = RequestMethod.PUT)
        public void upStatus(@NotBlank(message = "请输入任务ID") String key, String status) {
            taskService.upStatus(key,TaskStatusEnum.valueOf(status));
        }


        @RequestMapping(value = "/clear", method = RequestMethod.DELETE)
        public void clear(@NotBlank(message = "请输入任务ID") String key) {
            taskService.clear(key);
        }
}