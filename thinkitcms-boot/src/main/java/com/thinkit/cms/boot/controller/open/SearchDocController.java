package com.thinkit.cms.boot.controller.open;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.thinkit.cms.annotation.ValidGroup1;
import com.thinkit.cms.api.open.OpenSearchService;
import com.thinkit.cms.dto.open.OpenSearchDto;
import com.thinkit.cms.model.PageModel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import java.util.Map;

@Validated
@RestController
@RequestMapping("/openapi/search")
@Slf4j
public class SearchDocController {


    @Autowired
    private OpenSearchService openSearchService;

    @RequestMapping(value = "/doc", method = RequestMethod.POST)
    public Page<Map> searchDoc(@Validated(value = {ValidGroup1.class}) @RequestBody PageModel<OpenSearchDto> openSearchDto) {
       return openSearchService.searchDoc(openSearchDto);
    }
}
