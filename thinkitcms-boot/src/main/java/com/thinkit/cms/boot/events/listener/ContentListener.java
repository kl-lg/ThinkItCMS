package com.thinkit.cms.boot.events.listener;

import com.google.common.collect.Lists;
import com.thinkit.cms.boot.events.ContentEvent;
import com.thinkit.cms.constant.SysConst;
import com.thinkit.cms.directive.kit.RedisLuaUtils;
import com.thinkit.cms.dto.content.ContentDto;
import com.thinkit.cms.dto.content.Rows;
import com.thinkit.cms.model.ModelJson;
import com.thinkit.cms.utils.Checker;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Slf4j
@Component
public class ContentListener extends BaseListener  implements ApplicationListener<ContentEvent> {


    @Override
    public void onApplicationEvent(ContentEvent event) {
        try {
            if(enable()){
                switch (event.getEventEnum()){
                    case SAVE_BATCH_CONTENT -> {
                        List<ContentDto> contents = getList(event);
                        contents.forEach(content->{
                            ModelJson modelJson  = getToMap(content);
                            RedisLuaUtils.setJson(true,modelJson,content.getFormModelCode(),SysConst.colon);
                        });
                    }
                    case SAVE_CONTENT -> {
                        ContentDto content =  getObj(event);
                        ModelJson modelJson  = getToMap(content);
                        RedisLuaUtils.setJson(true,modelJson,content.getFormModelCode(),SysConst.colon);
                    }
                    case UPDATE_CONTENT -> {
                        ContentDto content =  getObj(event);
                        ModelJson modelJson  = getToMap(content);
                        RedisLuaUtils.setJson(true,true,modelJson,content.getFormModelCode(),SysConst.colon);
                    }
                    case TOP_CONTENT -> {
                        List<ModelJson>  modelJsons =  maps(event,"isTop");
                        RedisLuaUtils.setJsons(modelJsons);
                    }
                    case RECOMD_CONTENT -> {
                        List<ModelJson>  modelJsons = maps(event,"isRecomd");
                        RedisLuaUtils.setJsons(modelJsons);
                    }
                    case HEAD_CONTENT -> {
                        List<ModelJson> modelJsons = maps(event,"isHeadline");
                        RedisLuaUtils.setJsons(modelJsons);
                    }
                    case UPDATE_TAG -> {
                        ContentDto content =  getObj(event);
                        ModelJson modelJson = new ModelJson();
                        modelJson.setPrefix(SysConst.contentPrefix);
                        modelJson.setPk(content.getId());
                        String key = modelJson.buildKey(content.getFormModelCode(),SysConst.colon);
                        Map<String,Object> map = RedisLuaUtils.getJson(key);
                        if(Checker.BeNotEmpty(map)){
                            map.put("topTag",content.getTopTag());
                        }
                        modelJson.setObjectMap(map);
                        RedisLuaUtils.setJson(true,modelJson,content.getFormModelCode(),SysConst.colon);
                    }
                    case DELETE_CONTENT -> {
                        ContentDto content =  getObj(event);
                        RedisLuaUtils.delete(SysConst.contentPrefix+content.getId());
                    }
                    case DELETE_BATCH_CONTENT->{
                        List<ContentDto>  contents =  getList(event);
                        List<String> keys = keys(contents);
                        RedisLuaUtils.delete(keys);
                    }
                    case UPDATE_BATCH_STATUS->{
                        List<ContentDto>  contents =  getList(event);
                        if(Checker.BeNotEmpty(contents)){
                            Date date = new Date();
                            List<String> keys = keys(contents);
                            List<ModelJson> maps = RedisLuaUtils.getJsons(keys,SysConst.contentPrefix);
                            Integer status = contents.get(0).getStatus();
                            maps.forEach(map->{
                                map.getObjectMap().put("status",status);
                                map.getObjectMap().put("publishDate",date);
                                RedisLuaUtils.setJson(map);
                            });
                        }
                    }
                }
            }
        }catch (Exception e){
            log.error("ContentRedisListener 执行失败：{}",e.getMessage());
        }
    }

    private List<String> keys(List<ContentDto>  contents){
        List<String> keys = new ArrayList<>();
        if(Checker.BeNotEmpty(contents)){
            contents.forEach(content->{
                ModelJson modelJson = new ModelJson();
                modelJson.setPrefix(SysConst.contentPrefix);
                modelJson.setPk(content.getId());
                keys.add(modelJson.buildKey(content.getFormModelCode(),SysConst.colon));
            });
        }
        return keys;
    }

    private List<ModelJson> maps(ContentEvent event,String field){
        Rows rows = getObj(event);
        List<String> keys = rows.getKeys();
        Integer status = rows.getStatus();
        if(Checker.BeNotEmpty(keys)){
            List<ModelJson> modelJsons = new ArrayList<>();
            keys.forEach(key->{
                Map map=   RedisLuaUtils.getJson(true,key);
                if(Checker.BeNotEmpty(map)){
                    map.put(field,status);
                    ModelJson modelJson =  new ModelJson();
                    modelJson.setKey(key);
                    modelJson.setObjectMap(map);
                    modelJsons.add(modelJson);
                }
            });
            return modelJsons;
        }
        return Lists.newArrayList();
    }
}
