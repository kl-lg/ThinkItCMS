package com.thinkit.cms.boot.events.listener;

import cn.hutool.core.util.ReflectUtil;
import cn.hutool.json.JSONUtil;
import com.google.common.collect.Lists;
import com.thinkit.cms.annotation.IndexField;
import com.thinkit.cms.annotation.IndexId;
import com.thinkit.cms.constant.SysConst;
import com.thinkit.cms.model.ModelData;
import com.thinkit.cms.model.ModelJson;
import com.thinkit.cms.utils.Checker;
import com.thinkit.cms.utils.CmsConf;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationEvent;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
@Slf4j
public class BaseListener {

    protected boolean enable(){
        return CmsConf.getAsBoolean(SysConst.enableES);
    }

    protected <T> List<T> getList(ApplicationEvent event){
         if(Checker.BeNotNull(event.getSource())){
             return (List<T>) event.getSource();
         }
         return Lists.newArrayList();
    }

    protected <T> T getObj(ApplicationEvent event){
        if(Checker.BeNotNull(event.getSource())){
            return (T) event.getSource();
        }
        return null;
    }

    protected Map<String,Object> toMap(Object object,Map<String,Object> params){
        Map<String,Object> map = new HashMap<>();
        if(Checker.BeNotNull(object)){
            String str = JSONUtil.toJsonStr(object);
            map = JSONUtil.toBean(str,Map.class);
            map.putAll(params);
        }
        return  map;
    }

    protected Map<String,Object> toMap(ModelData modelData){
        Map<String,Object> map = new HashMap<>();
        if(Checker.BeNotNull(modelData)){
            if(Checker.BeNotEmpty(modelData.getExtra())){
                map.putAll(modelData.getExtra());
                modelData.setExtra(null);
            }
            Map obj = JSONUtil.toBean(JSONUtil.toJsonStr(modelData),Map.class);
            map.putAll(obj);
        }
        return  map;
    }

    protected ModelJson getToMap(Object object)  {
        try {
            Map<String,Object> objectMap = new HashMap<>();
            Field[] fields = ReflectUtil.getFields(object.getClass());
            if(Checker.BeNotEmpty(fields)){
                ModelJson modelJson = new ModelJson();
                for(Field field:fields){
                    IndexField indexField =  field.getAnnotation(IndexField.class);
                    IndexId indexId =  field.getAnnotation(IndexId.class);
                    boolean indexFieldOk = Checker.BeNotNull(indexField) ;
                    boolean indexIdOk = Checker.BeNotNull(indexId);
                    field.setAccessible(true);
                    Object value = field.get(object);
                    if(Checker.BeNotNull(value)){
                        if(indexIdOk){
                            String pk = value.toString();
                            String indexName = Checker.BeNotBlank(indexId.value())?indexId.value():field.getName();
                            modelJson.setPk(pk);
                            modelJson.setPrefix(indexId.prefix());
                            modelJson.setIndexName(indexName);
                            objectMap.put(indexName,pk);
                        }
                        if(indexFieldOk){
                            String filedName = Checker.BeNotBlank(indexField.value())?indexField.value():field.getName();
                            if(indexField.convertMap()){
                                Class objClass = field.getType();
                                boolean isString = objClass == String.class || String.class.isAssignableFrom(objClass);
                                if (isString) {
                                    objectMap.putAll(JSONUtil.toBean((String) value,Map.class));
                                }
                            }else{
                                boolean isMap = field.getType().isAssignableFrom(Map.class);
                                if(isMap){
                                    objectMap.putAll((Map)value);
                                }else{
                                    objectMap.put(filedName,value);
                                }
                            }
                        }
                    }
                }
                modelJson.setObjectMap(objectMap);
                return modelJson;
            }
        }catch (Exception e){
            log.error("转换异常：{}",e.getMessage());
        }
        return null;
    }

    protected void format(ModelData modelData){
        if(Checker.BeNotNull(modelData)){
             modelData.extraJson();
        }
    }

    protected void format(List<? extends ModelData> modelDatas){
        if(Checker.BeNotEmpty(modelDatas)){
            for(ModelData modelData:modelDatas){
                modelData.extraJson();
            }
        }
    }
}
