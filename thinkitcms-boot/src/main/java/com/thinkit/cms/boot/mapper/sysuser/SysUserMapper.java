package com.thinkit.cms.boot.mapper.sysuser;
import com.thinkit.cms.dto.sysuser.SysUserDto;
import org.apache.ibatis.annotations.Mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.thinkit.cms.boot.entity.sysuser.SysUserEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
* @author LG
*/

@Mapper
public interface SysUserMapper extends BaseMapper<SysUserEntity> {

    /**
     * 根据用户名加载账户记录
     * @param userName
     * @return
     */
    SysUserDto loadUserName(@Param("userName") String userName);


    /**
     * 是否存在用户名称
     * @param rowId
     * @param userName
     * @return
     */
    Integer hasUserName(@Param("rowId") String rowId, @Param("userName") String userName);


    /**
     * 锁定或者解锁用户
     * @param status
     * @param userIds
     */
    void lock(@Param("status") String status, @Param("userIds") List<String> userIds);

    /**
     *  查询权限标识
     * @param userId
     * @param isSuper
     * @return
     */
    List<String> queryAuthSign(@Param("userId") String userId,@Param("isSuper") boolean isSuper);

    /**
     * 查看是否是超级用户
     * @param userId
     * @return
     */
    Integer isSuperUser(@Param("userId") String userId);


    /**
     * 查询当前用户是否拥有角色
     * @param userId
     * @return
     */
    Integer hasRole(String userId);


    /**
     * 查询当前用户是否拥有角色 标识
     * @param userId
     * @return
     */
    String hasRoleSign(String userId);

    /**
     * 设置默认站点
     * @param siteId
     * @param userId
     */
    void setDefaultSite(@Param("siteId") String siteId,@Param("userId") String userId);


    /**
     * 获取默认站点
     * @param userId
     * @return
     */
    String getDefaultSite(@Param("userId") String userId);

    void updatePw(@Param("userId") String userId, @Param("npw") String npw);
}