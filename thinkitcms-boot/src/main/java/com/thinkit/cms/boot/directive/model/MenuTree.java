package com.thinkit.cms.boot.directive.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.thinkit.cms.utils.Tree;
import lombok.Data;

@Data
public class MenuTree<T> extends Tree<T> {

    /**
     编码
     */
    private String code;

    /**
     封面图片
     */
    private String cover;


    /**
     外链跳转地址
     */
    private String goUrl;

    /**
     地址
     */
    private String url;


    /**
     * 手机端地址
     */
    private String mUrl;


    /**
     是否是单页
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Integer singlePage;

    /**
     标题(分类标题用于 SEO 关键字优化)
     */
    private String title;

    /**
     关键词用于 SEO 关键字优化
     */
    private String keywords;

    /**
     描述用于 SEO 关键字优化
     */
    private String description;


}
