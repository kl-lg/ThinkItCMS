package com.thinkit.cms.boot.funs.mehod;

import com.thinkit.cms.api.formmodel.FormModelService;
import com.thinkit.cms.directive.emums.LongEnum;
import com.thinkit.cms.directive.emums.MethodEnum;
import com.thinkit.cms.directive.executor.BaseMethod;
import com.thinkit.cms.utils.Checker;
import freemarker.template.TemplateModelException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 时间格式化
 * @author LONG
 */
@Component
public class Import extends BaseMethod {

    @Autowired
    FormModelService formModelService;
    @Override
    public MethodEnum getName() {
        return MethodEnum.IMPORT;
    }

    @Override
    public String exec(List list) throws TemplateModelException {
        String code = getString(0,list, LongEnum.STRING);
        if(Checker.BeNotBlank(code)){
            return  formModelService.findPath(code);
        }
        return null;
    }
}
