package com.thinkit.cms.boot.controller.sysrole;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.thinkit.cms.annotation.Logs;
import com.thinkit.cms.annotation.ValidGroup1;
import com.thinkit.cms.api.sysrole.SysRoleService;
import com.thinkit.cms.authen.annotation.Sentinel;
import com.thinkit.cms.core.base.BaseController;
import com.thinkit.cms.dto.sysrole.SysRoleDto;
import com.thinkit.cms.model.CmsResult;
import com.thinkit.cms.model.PageModel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;

import static com.thinkit.cms.enums.LogModule.ORG;

/**
* @author LG
*/

@Validated
@RestController
@RequestMapping("/api/sysRole")
@Slf4j
public class SysRoleController extends BaseController<SysRoleService> {



        @Logs(module = ORG,operation = "查看角色详情")
        @SaCheckPermission("role:edit")
        @GetMapping(value="detail")
        public SysRoleDto detail(@NotBlank(message = "ID不能为空!") @RequestParam String id){
                return service.getByPk(id);
        }


        @SaCheckPermission("role")
        @PostMapping(value = "/page")
        public PageModel<SysRoleDto> list(@RequestBody PageModel<SysRoleDto> pageDto) {
             return service.listPage(pageDto);
        }


        @Logs(module = ORG,operation = "创建角色")
        @Sentinel
        @SaCheckPermission("role:add")
        @PostMapping(value="save")
        public CmsResult save(@Validated(value = {ValidGroup1.class}) @RequestBody SysRoleDto sysRoleDto) {
              return service.save(sysRoleDto);
        }


        @Logs(module = ORG,operation = "编辑角色")
        @Sentinel
        @SaCheckPermission("role:edit")
        @PutMapping(value="update")
        public CmsResult update( @RequestBody SysRoleDto sysRoleDto) {
              return service.update(sysRoleDto);
        }


        @Logs(module = ORG,operation = "删除角色")
        @Sentinel
        @SaCheckPermission("role:delete")
        @DeleteMapping(value="delete")
        public boolean deleteByPk(@NotBlank @RequestParam String id){
                return service.delete(id);
        }

}