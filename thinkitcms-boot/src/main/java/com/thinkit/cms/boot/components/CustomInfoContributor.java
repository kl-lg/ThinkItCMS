package com.thinkit.cms.boot.components;

import com.sun.management.OperatingSystemMXBean;
import com.thinkit.cms.model.HardInfo;
import com.thinkit.cms.utils.Checker;
import org.springframework.boot.actuate.info.Info;
import org.springframework.boot.actuate.info.InfoContributor;
import org.springframework.stereotype.Component;
import oshi.SystemInfo;
import oshi.hardware.HWDiskStore;
import oshi.hardware.HWPartition;
import oshi.hardware.HardwareAbstractionLayer;
import oshi.util.FormatUtil;

import java.lang.management.ManagementFactory;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class CustomInfoContributor implements InfoContributor {

    @Override
    public void contribute(Info.Builder builder) {
        memory(builder);
    }
    private void memory(Info.Builder builder){
        Map<String,Object> params = new HashMap<>(16);
        OperatingSystemMXBean osmxb = (OperatingSystemMXBean) ManagementFactory.getOperatingSystemMXBean();
        // 总的物理内存
        long totalMemorySize = osmxb.getTotalPhysicalMemorySize();
        // 剩余的物理内存
        long freePhysicalMemorySize =osmxb.getFreePhysicalMemorySize();
        // 已使用的物理内存
        long usedMemory = osmxb.getTotalPhysicalMemorySize() - osmxb.getFreePhysicalMemorySize();

        List<HardInfo> parts = new ArrayList<>();
        SystemInfo si = new SystemInfo();
        HardwareAbstractionLayer hal = si.getHardware();
        for (HWDiskStore disk : hal.getDiskStores()) {
            boolean readwrite = disk.getReads() > 0 || disk.getWrites() > 0;
            System.out.format(" %s: (model: %s - S/N: %s) size: %s, reads: %s (%s), writes: %s (%s), xfer: %s ms%n",
            disk.getName(), disk.getModel(), disk.getSerial(),
            disk.getSize() > 0 ? FormatUtil.formatBytesDecimal(disk.getSize()) : "?",
            readwrite ? disk.getReads() : "?", readwrite ? FormatUtil.formatBytes(disk.getReadBytes()) : "?",
            readwrite ? disk.getWrites() : "?", readwrite ? FormatUtil.formatBytes(disk.getWriteBytes()) : "?",
            readwrite ? disk.getTransferTime() : "?");
            List<HWPartition> partitions = disk.getPartitions();
            if (Checker.BeNotEmpty(partitions)) {
                for (HWPartition part : partitions) {
                    HardInfo hardInfo = new HardInfo();
                    hardInfo.setIdentification(part.getIdentification());
                    hardInfo.setName(part.getName());
                    hardInfo.setType(part.getType());
                    hardInfo.setMajor(part.getMajor());
                    hardInfo.setSize(part.getSize());
                    parts.add(hardInfo);
                }
            }
        }
        params.put("totalMemorySize",totalMemorySize);
        params.put("freePhysicalMemorySize",freePhysicalMemorySize);
        params.put("usedMemorySize",usedMemory);
        params.put("parts",parts);
        builder.withDetail("customInfo", params);
    }

//    private static void printProcesses() {
//        SystemInfo si = new SystemInfo();
//        HardwareAbstractionLayer hal = si.getHardware();
//        OperatingSystem os = si.getOperatingSystem();
//        System.out.println("Processes: " + os.getProcessCount() + ", Threads: " + os.getThreadCount());
//        // Sort by highest CPU
////        List<OSProcess> procs = os.getProcesses();
////        System.out.println("   PID  %CPU %MEM       VSZ       RSS Name");
////        for (int i = 0; i < procs.size() && i < 50; i++) {
////            OSProcess p = procs.get(i);
////            System.out.format(" %5d %5.1f %4.1f %9s %9s %s%n", p.getProcessID(),
////                    100d * (p.getKernelTime() + p.getUserTime()) / p.getUpTime(),
////                    100d * p.getResidentSetSize() /  hal.getMemory().getTotal(), FormatUtil.formatBytes(p.getVirtualSize()),
////                    FormatUtil.formatBytes(p.getResidentSetSize()), p.getName());
////        }
//
//        System.out.println("Disks:");
//        for (HWDiskStore disk : hal.getDiskStores()) {
//            boolean readwrite = disk.getReads() > 0 || disk.getWrites() > 0;
//            System.out.format(" %s: (model: %s - S/N: %s) size: %s, reads: %s (%s), writes: %s (%s), xfer: %s ms%n",
//                    disk.getName(), disk.getModel(), disk.getSerial(),
//                    disk.getSize() > 0 ? FormatUtil.formatBytesDecimal(disk.getSize()) : "?",
//                    readwrite ? disk.getReads() : "?", readwrite ? FormatUtil.formatBytes(disk.getReadBytes()) : "?",
//                    readwrite ? disk.getWrites() : "?", readwrite ? FormatUtil.formatBytes(disk.getWriteBytes()) : "?",
//                    readwrite ? disk.getTransferTime() : "?");
//            List<HWPartition> partitions = disk.getPartitions();
//            if (partitions == null) {
//                // TODO Remove when all OS's implemented
//                continue;
//            }
//            for (HWPartition part : partitions) {
//                System.out.format(" |-- %s: %s (%s) Maj:Min=%d:%d, size: %s%s%n", part.getIdentification(),
//                        part.getName(), part.getType(), part.getMajor(), part.getMinor(),
//                        FormatUtil.formatBytesDecimal(part.getSize()),
//                        part.getMountPoint().isEmpty() ? "" : " @ " + part.getMountPoint());
//            }
//        }
//
//    }

//    public static void main(String[] args) {
//        printProcesses();
//
//    }


}
