package com.thinkit.cms.boot.service.open;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.thinkit.cms.api.open.OpenSearchService;
import com.thinkit.cms.constant.SysConst;
import com.thinkit.cms.core.handler.CustomException;
import com.thinkit.cms.directive.kit.RedisLuaUtils;
import com.thinkit.cms.directive.mapper.RsBaseQuery;
import com.thinkit.cms.directive.unit.RsUnit;
import com.thinkit.cms.dto.open.OpenSearchDto;
import com.thinkit.cms.model.PageModel;
import com.thinkit.cms.utils.Checker;
import org.springframework.stereotype.Service;
import redis.clients.jedis.search.SearchResult;

import java.util.List;
import java.util.Map;

@Service
public class OpenSearchServiceImpl implements OpenSearchService {
    @Override
    public Page<Map> searchDoc(PageModel<OpenSearchDto> openSearchDto) {
        String siteId=openSearchDto.getDto().getSiteId();
        if(Checker.BeBlank(siteId)){
            throw new CustomException(5113);
        }
        String keywords=openSearchDto.getDto().getKeywords();
        RsUnit rsUnit = new RsUnit();
        rsUnit.setTable(SysConst.content);
        long current = openSearchDto.getCurrent();
        long pageSize =openSearchDto.getPageSize();
        Boolean hasText= Checker.BeNotBlank(keywords) && !SysConst.starChar.equals(keywords);
        String queryStr =  "@siteId:{${siteId}} @status:[1,1]";
        if(hasText){
            keywords = keywords.replaceAll("\\s*", "");
            queryStr+=" AND ((@title:${keywords}| @subTitle:${keywords} | @keywords:${keywords} | @description:${keywords} | @aicontent:${keywords} | @content:${keywords})";
            queryStr+= " |@togTag:{"+keywords+"})";
        }
        queryStr= queryStr.replace("${siteId}",siteId);
        if(hasText){
           queryStr=queryStr.replace("${keywords}",SysConst.likeChar+keywords+SysConst.likeChar);
        }
        rsUnit.setLimit(new Integer[]{Long.valueOf(current).intValue(),Long.valueOf(pageSize).intValue()});
        rsUnit.setSql(queryStr);
        rsUnit.setSort(new String[]{"publishDate desc"});
        SearchResult searchResult = RedisLuaUtils.ftSearch(rsUnit);
        List<Map> maps =  RsBaseQuery.toMap(searchResult.getDocuments());
        if(Checker.BeNotEmpty(maps)){
            Page page = new Page(current,pageSize);
            page.setRecords(maps);
            page.setTotal(searchResult.getTotalResults());
            return page;
        }
        return new Page<>(current,pageSize);
    }
}
