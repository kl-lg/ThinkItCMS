package com.thinkit.cms.boot.funs.instruct;

import cn.hutool.json.JSONUtil;
import com.thinkit.cms.constant.SysConst;
import com.thinkit.cms.directive.annotation.Instruct;
import com.thinkit.cms.directive.funs.AbsFun;
import com.thinkit.cms.directive.funs.BaseFunInterface;
import com.thinkit.cms.directive.render.BaserRender;
import com.thinkit.cms.model.ModelJson;
import com.thinkit.cms.utils.Checker;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Slf4j
@Instruct(value = "ModelDataInstruct",description = "获取模型数据单个")
@Component
public class ModelDataInstruct extends AbsFun implements BaseFunInterface {

    @Override
    public void execute(BaserRender render) throws Exception {
          String modelCode = render.getString(SysConst.code);
          ModelJson modelJson = new ModelJson();
          modelJson.setPrefix(SysConst.formDataPrefix);
          modelJson.setPk(SysConst.blank);
          String key = modelJson.buildKey(modelCode,SysConst.colon,SysConst.starChar);
          List<Map> maps=  getJsons(key);
          if(Checker.BeNotEmpty(maps)){
                log.info("指令 [ModelDataInstruct] 获取数据为：{}", JSONUtil.toJsonStr(maps));
                render.putAll(maps.get(0)).render();
          }
    }
}
