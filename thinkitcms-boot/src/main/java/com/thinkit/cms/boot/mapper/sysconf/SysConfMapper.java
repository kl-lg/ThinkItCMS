package com.thinkit.cms.boot.mapper.sysconf;
import org.apache.ibatis.annotations.Mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.thinkit.cms.boot.entity.sysconf.SysConfEntity;

/**
* @author System
*/

@Mapper
public interface SysConfMapper extends BaseMapper<SysConfEntity> {

}