package com.thinkit.cms.boot.events;

import com.thinkit.cms.enums.EventEnum;
import lombok.Getter;
import lombok.Setter;
import org.springframework.context.ApplicationEvent;

public class RelatedEvent extends ApplicationEvent {

    @Getter @Setter
    public EventEnum eventEnum;

    public RelatedEvent(Object source, EventEnum eventEnum) {
        super(source);
        this.eventEnum = eventEnum;
    }
}
