package com.thinkit.cms.boot.funs.instruct;

import cn.hutool.json.JSONUtil;
import com.thinkit.cms.constant.SysConst;
import com.thinkit.cms.directive.annotation.Instruct;
import com.thinkit.cms.directive.funs.AbsFun;
import com.thinkit.cms.directive.funs.BaseFunInterface;
import com.thinkit.cms.directive.render.BaserRender;
import com.thinkit.cms.model.ModelJson;
import com.thinkit.cms.utils.Checker;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Map;

@Slf4j
@Instruct(value = "SiteInstruct",description = "获取站点详情")
@Component
public class SiteInstruct extends AbsFun implements BaseFunInterface {

    @Override
    public void execute(BaserRender render) throws Exception {
          String siteId = render.getString(SysConst.siteId);
          ModelJson modelJson = new ModelJson();
          modelJson.setPrefix(SysConst.sitePrefix);
          modelJson.setPk(siteId);
          String key = modelJson.buildKey();
          Map map = getJson(key);
          if(Checker.BeNotEmpty(map)){
                log.info("指令 [SiteInstruct] 获取数据为：{}", JSONUtil.toJsonStr(map));
          }
          render.putAll(map).render();
    }
}
