package com.thinkit.cms.boot.controller.open;
import com.thinkit.cms.annotation.ValidGroup1;
import com.thinkit.cms.api.content.ContentService;
import com.thinkit.cms.api.open.OpenViewService;
import com.thinkit.cms.core.base.BaseController;
import com.thinkit.cms.dto.open.OpenViewDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
* @author LG
*/

@Validated
@RestController
@RequestMapping("/openapi")
@Slf4j
public class OpenViewController extends BaseController<ContentService> {

        @Autowired
        private OpenViewService openViewService;

        @RequestMapping(value = "/click", method = RequestMethod.PUT)
        public Long click(@Validated(value = {ValidGroup1.class}) @RequestBody OpenViewDto openViewDto) {
                return openViewService.click(openViewDto);
        }

        @RequestMapping(value = "/queryClick", method = RequestMethod.PUT)
        public Long queryClick(@Validated(value = {ValidGroup1.class}) @RequestBody OpenViewDto openViewDto) {
                return openViewService.queryClick(openViewDto);
        }

        @RequestMapping(value = "/view", method = RequestMethod.PUT)
        public Long view(@Validated(value = {ValidGroup1.class}) @RequestBody OpenViewDto openViewDto) {
                return  openViewService.view(openViewDto);
        }


}