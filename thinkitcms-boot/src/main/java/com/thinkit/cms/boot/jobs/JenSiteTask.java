package com.thinkit.cms.boot.jobs;

import com.thinkit.cms.boot.jobs.executers.CategoryExecuter;
import com.thinkit.cms.boot.jobs.executers.ContentExecuter;
import com.thinkit.cms.boot.jobs.executers.IndexExecuter;
import com.thinkit.cms.core.handler.CustomException;
import com.thinkit.cms.directive.task.TaskDto;
import com.thinkit.cms.directive.task.TaskService;
import com.thinkit.cms.enums.TaskEnum;
import com.thinkit.cms.user.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.concurrent.*;

/**
 * 全站生成 task
 */
@Slf4j
@Component
public class JenSiteTask {


    @Autowired
    private TaskService taskService;

    private static ExecutorService executorService = new ThreadPoolExecutor(2, 16, 500, TimeUnit.SECONDS,
    new ArrayBlockingQueue<>(6000), new ThreadPoolExecutor.CallerRunsPolicy());


    public String genDoTask(String siteId, User user){
        String key = taskService.genTaskKey(TaskEnum.ALL_TASK,siteId,null);
        if(!taskService.isRuning(key)){
            TaskDto taskDto = new TaskDto();
            taskDto.setAction(TaskEnum.ALL_TASK.getVal());
            taskDto.setSiteId(siteId);
            String taskId = taskService.startTask(key,taskDto);
            CompletableFuture.runAsync(()->{
                doTask(siteId,user,taskId);
            });
            return taskId;
        }else{
            throw new CustomException(5060);
        }
    }


    public void doTask(String siteId,User user,String taskId){
        user.setTaskId(taskId);
        CountDownLatch countDownLatch = new CountDownLatch(2);
        executorService.submit(new IndexExecuter(user,siteId,taskId,TaskEnum.INDEX_TASK,countDownLatch));
        executorService.submit(new ContentExecuter(user,siteId,taskId,TaskEnum.ARTICLE_TASK,countDownLatch));
        try {
            // 减轻数据库压力(如果不使用es 查询语法 查询建议开启)
            // countDownLatch.await(3,TimeUnit.MINUTES);
            executorService.submit(new CategoryExecuter(user,siteId,taskId,TaskEnum.CATEGORY_TASK,countDownLatch));
        }catch (Exception e){
             log.error("全站生成异常：{}",e.getMessage());
        }
    }


}
