package com.thinkit.cms.boot.service.open;
import com.thinkit.cms.api.content.ContentService;
import com.thinkit.cms.api.open.OpenViewService;
import com.thinkit.cms.constant.SysConst;
import com.thinkit.cms.core.base.BasePlus;
import com.thinkit.cms.dto.open.OpenViewDto;
import com.thinkit.cms.utils.Checker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.util.Date;
import java.util.concurrent.TimeUnit;

@Service
public class OpenViewServiceImpl implements OpenViewService {

    @Autowired
    private RedisTemplate<String,Object> redisTemplate;

    @Autowired
    private ContentService contentService;


    /**
     * 点赞
     * @param openViewDto
     * @return
     */
    @Override
    public Long click(OpenViewDto openViewDto) {
        String ipAddr =  BasePlus.getIpAddr(getRequestAttributes().getRequest());
        String siteId = openViewDto.getSiteId();
        String contentId = openViewDto.getContentId();
        String key = SysConst.clickTop+siteId+SysConst.colon+contentId;
        String ipKey = SysConst.clickIp+siteId+SysConst.colon+contentId+SysConst.colon+ipAddr;
        if(redisTemplate.hasKey(ipKey)){
            return -2L;
        }
        Long delta = 1L;
        if(!redisTemplate.hasKey(key)){
            delta = contentService.queryClicks(contentId)+1;
        }
        if(Checker.BeNotNull(delta)){
            redisTemplate.opsForValue().set(ipKey,new Date(),86400L, TimeUnit.SECONDS);
            return redisTemplate.opsForValue().increment(key,delta);
        }
        return  0L;
    }


    /**
     * 查找浏览数
     * @param openViewDto
     * @return
     */
    @Override
    public Long view(OpenViewDto openViewDto) {
        String siteId = openViewDto.getSiteId();
        String contentId = openViewDto.getContentId();
        String key = SysConst.clickView+siteId+SysConst.colon+contentId;
        Long delta = 1L;
        if(!redisTemplate.hasKey(key)){
            delta = contentService.queryViews(contentId)+1;
        }
        if(Checker.BeNotNull(delta)){
            return redisTemplate.opsForValue().increment(key, delta);
        }
        return  0L;
    }

    @Override
    public Long queryClick(OpenViewDto openViewDto) {
        String siteId = openViewDto.getSiteId();
        String contentId = openViewDto.getContentId();
        String key = SysConst.clickTop+siteId+SysConst.colon+contentId;
        Long clickTimes;
        if(!redisTemplate.hasKey(key)){
            clickTimes = contentService.queryClicks(contentId);
        }else{
            Object likes =  redisTemplate.opsForValue().get(key);
            if(Checker.BeNotNull(likes)){
                return Long.valueOf(likes.toString());
            }
            return 0L;
        }
        return  clickTimes;
    }

    private static ServletRequestAttributes getRequestAttributes() {
        RequestAttributes attributes = RequestContextHolder.getRequestAttributes();
        return (ServletRequestAttributes) attributes;
    }
}
