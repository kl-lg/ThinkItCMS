package com.thinkit.cms.boot.entity.directs;
import lombok.Getter;
import lombok.Setter;
import java.math.BigDecimal;
import lombok.experimental.Accessors;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.thinkit.cms.model.BaseModel;
import com.baomidou.mybatisplus.annotation.TableName;
/**
* @author lg
*/
@Getter
@Setter
@Accessors(chain = true)
@TableName("tk_directs")
public class DirectsEntity extends BaseModel  {
       /**
         指令名称
       */
        @TableField("`name`")
        private String name;

       /**
         指令定义
       */
        @TableField("direct")
        private String direct;

       /**
         是否启用 1：未启用 2:启用
       */
        @TableField("`enable`")
        private Integer enable;

       /**
         模板sql
       */
        @TableField("`sql`")
        private String sql;

       /**
         返回值类型 Object Map List 等
       */
        @TableField("result")
        private String result;

       /**
         返回结果转化器（需要继承 AbstractDataHandler）
       */
        @TableField("result_convert")
        private String resultConvert;

       /**
         入参参数
       */
        @TableField("in_params")
        private String inParams;

       /**
         出参参数格式化
       */
        @TableField("out_params")
        private String outParams;

       /**
         指令配置
       */
        @TableField("config")
        private String config;

       /**
         站点ID
       */
        @TableField("site_id")
        private String siteId;

       /**
         是否共享全局
       */
        @TableField("is_share")
        private Integer isShare;

        /**
         站点ID
         */
        @TableField("sql_type")
        private String sqlType;

        /**
         指令类型
         */
        @TableField("direct_type")
        private Integer directType;



}