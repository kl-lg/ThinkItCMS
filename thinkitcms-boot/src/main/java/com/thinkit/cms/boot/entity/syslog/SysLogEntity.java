package com.thinkit.cms.boot.entity.syslog;
import lombok.Getter;
import lombok.Setter;
import java.math.BigDecimal;
import lombok.experimental.Accessors;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.thinkit.cms.model.BaseModel;
import com.baomidou.mybatisplus.annotation.TableName;
/**
* @author lg
*/
@Getter
@Setter
@Accessors(chain = true)
@TableName("tk_sys_logs")
public class SysLogEntity extends BaseModel  {
       /**
         模块类型
       */
        @TableField("type")
        private String type;

       /**
         模块名称
       */
        @TableField("module")
        private String module;

       /**
         用户id
       */
        @TableField("user_id")
        private String userId;

       /**
         用户账号
       */
        @TableField("username")
        private String username;

       /**
         用户姓名
       */
        @TableField("name")
        private String name;

       /**
         用户操作
       */
        @TableField("operation")
        private String operation;

       /**
         响应时间
       */
        @TableField("time")
        private Long time;

       /**
         请求方法
       */
        @TableField("method")
        private String method;

       /**
         请求参数
       */
        @TableField("params")
        private String params;

       /**
         IP地址
       */
        @TableField("ip")
        private String ip;

       /**
         站点ID
       */
        @TableField("site_id")
        private String siteId;

       /**
         是否删除 0：正常 1：删除
       */
        @TableField("deleted")
        private Boolean deleted;


}