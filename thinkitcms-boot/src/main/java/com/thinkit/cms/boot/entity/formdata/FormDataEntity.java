package com.thinkit.cms.boot.entity.formdata;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.thinkit.cms.model.BaseModel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
/**
* @author LG
*/
@Getter
@Setter
@Accessors(chain = true)
@TableName("tk_form_data")
public class FormDataEntity extends BaseModel  {
       /**
         表单模型ID
       */
        @TableField("form_model_id")
        private String formModelId;


        /**
         表单模型ID
         */
        @TableField("form_model_code")
        private String formModelCode;



       /**
         表单内容
       */
        @TableField("form_data")
        private String formData;

       /**
         是否删除 0：正常 1：删除
       */
        @TableField("deleted")
        private Boolean deleted;

       /**
         站点ID
       */
        @TableField("site_id")
        private String siteId;


        @TableField("row_id")
        private String rowId;

        @TableField("modual")
        private String modual;
}