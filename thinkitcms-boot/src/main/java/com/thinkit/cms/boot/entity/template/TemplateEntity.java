package com.thinkit.cms.boot.entity.template;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.thinkit.cms.model.BaseModel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
/**
* @author LG
*/
@Getter
@Setter
@Accessors(chain = true)
@TableName("tk_template")
public class TemplateEntity extends BaseModel  {
       /**
         模板名称
       */
        @TableField("`name`")
        private String name;

       /**
         封面
       */
        @TableField("cover")
        private String cover;

       /**
         模板 code 
       */
        @TableField("code")
        private String code;

       /**
         模板描述
       */
        @TableField("description")
        private String description;

       /**
         资源目录
       */
        @TableField("dir")
        private String dir;


        @TableField("deleted")
        private Integer deleted;


        /**
         * 模板路径
         */
        @TableField("tmp_dir")
        private String tmpDir;

        /**
         * 模板远程地址
         */
        @TableField("tmp_file_url")
        private String tmpFileUrl;


        /**
         *  是否共享
         */
        @TableField("share")
        private Integer share;
}