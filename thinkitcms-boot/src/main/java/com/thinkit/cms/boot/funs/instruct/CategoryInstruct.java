package com.thinkit.cms.boot.funs.instruct;
import cn.hutool.json.JSONUtil;
import com.thinkit.cms.api.category.CategoryService;
import com.thinkit.cms.constant.SysConst;
import com.thinkit.cms.directive.annotation.Instruct;
import com.thinkit.cms.directive.funs.AbsFun;
import com.thinkit.cms.directive.funs.BaseFunInterface;
import com.thinkit.cms.directive.render.BaserRender;
import com.thinkit.cms.model.ModelJson;
import com.thinkit.cms.utils.Checker;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

@Slf4j
@Instruct(value = "CategoryInstruct",description = "获取栏目详情")
@Component
public class CategoryInstruct extends AbsFun implements BaseFunInterface {

    @Autowired
    private CategoryService categoryService;
    @Override
    public void execute(BaserRender render) throws Exception {
            String categoryId = render.getString(SysConst.categoryId);
            String modelCode = render.getString(SysConst.modelCode);
            if(Checker.BeBlank(modelCode)){
                modelCode = categoryService.queryCode(categoryId);
            }
            ModelJson modelJson = new ModelJson();
            modelJson.setPrefix(SysConst.categoryPrefix);
            modelJson.setPk(categoryId);
            String key = modelJson.buildKey(modelCode,SysConst.colon);
            Map map = getJson(key);
            if(Checker.BeNotEmpty(map)){
                log.info("指令 [CategoryInstruct] 获取数据为：{}", JSONUtil.toJsonStr(map));
            }
            render.putAll(map).render();
    }
}
