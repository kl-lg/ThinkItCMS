package com.thinkit.cms.boot.service.sysmenu;

import cn.hutool.core.io.FileUtil;
import cn.hutool.extra.spring.SpringUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.google.common.collect.Lists;
import com.thinkit.cms.api.sysmenu.SysMenuService;
import com.thinkit.cms.api.sysrolemenu.SysRoleMenuService;
import com.thinkit.cms.api.sysuser.SysUserService;
import com.thinkit.cms.boot.entity.sysmenu.SysMenuEntity;
import com.thinkit.cms.boot.mapper.sysmenu.SysMenuMapper;
import com.thinkit.cms.constant.SysConst;
import com.thinkit.cms.core.base.BaseServiceImpl;
import com.thinkit.cms.dto.sysmenu.*;
import com.thinkit.cms.model.CmsResult;
import com.thinkit.cms.utils.BuildTree;
import com.thinkit.cms.utils.Checker;
import com.thinkit.cms.utils.CmsFolderUtil;
import com.thinkit.cms.utils.Tree;
import freemarker.template.Configuration;
import freemarker.template.Template;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.nio.charset.Charset;
import java.util.*;

/**
* @author LG
*/
@Slf4j
@Service
public class SysMenuServiceImpl extends BaseServiceImpl<SysMenuDto,SysMenuEntity,SysMenuMapper> implements SysMenuService{


    private final SysRoleMenuService sysRoleMenuService;

    private final SysUserService sysUserService;

    private final String rootPid ="0";

    protected Configuration configuration = SpringUtil.getBean(Configuration.class);

    public SysMenuServiceImpl(SysRoleMenuService sysRoleMenuService, SysUserService sysUserService) {
        this.sysRoleMenuService = sysRoleMenuService;
        this.sysUserService = sysUserService;
    }

    @Override
    public List<Tree> menuSlideTree() {
        String userId = getUserId();
        boolean isSuperAdmin = getUserCtx().isSuperAdmin();
        List<SysMenuDto> menus = baseMapper.listMenuSlideTree(userId,isSuperAdmin);
        if(Checker.BeNotEmpty(menus)){
            List<Tree> trees = new ArrayList<>(16);
            Tree tree = BuildTree.build(mentToTree(menus, MenuSlide.class));
            trees.addAll(tree.getChildren());
            return trees;
        }
        return Lists.newArrayList();
    }

    @Override
    public List<Tree> menuTableTree() {
        List<SysMenuDto> menus = baseMapper.menuTableTree(getUserId());
        if(Checker.BeNotEmpty(menus)){
            List<Tree> trees = new ArrayList<>(16);
            Tree tree = BuildTree.build(mentToTree(menus,MenuTable.class));
            trees.addAll(tree.getChildren());
            return trees;
        }
        return Lists.newArrayList();
    }

    @Override
    public CmsResult menuTreeAssign(String roleId) {
        String userId = getUserId();
        List<SysMenuDto> menus;
        boolean isSuperAdmin = sysUserService.isSuperUser(userId);
        menus = baseMapper.listMenuTreeAssign(userId,isSuperAdmin);
        List<String> menuIds = sysRoleMenuService.queryMenuIdsByRoleId(roleId);
        if(Checker.BeNotEmpty(menus)){
            List<String> expandedKeys = new ArrayList<>(16);
            menus.forEach(menu -> {
                menu.setIsLeaf(menu.getType().equals(2));
                if(!menu.getType().equals(2)){
                    expandedKeys.add(menu.getId());
                }
            });
            List<Tree> trees = new ArrayList<>(16);
            Tree tree = BuildTree.build(mentToTree(menus,MenuAssign.class));
            if("-1".equals(tree.getId())){
                trees.addAll(tree.getChildren());
            }else{
                trees.add(tree);
            }
            return CmsResult.result(new SysMenuCheckKey(trees,menuIds,expandedKeys));
        }
        return CmsResult.result(new SysMenuCheckKey(Lists.newArrayList(),menuIds,Lists.newArrayList()));
    }

    @Override
    public void save(SysMenuDto menu) {
        if(Checker.BeNotNull(menu.getMeta())){
            menu.setMeta(JSONUtil.toJsonStr(menu.getMeta()));
        }
        super.insert(menu);
    }

    @Override
    public SysMenuDto detail(String rowId) {
        SysMenuDto sysMenuDto = super.getByPk(rowId);
        if(Checker.BeNotNull(sysMenuDto.getMeta())){
            sysMenuDto.setMeta(JSONUtil.parseObj(sysMenuDto.getMeta()));
        }
        String pid = sysMenuDto.getPid();
        if(Checker.BeNotBlank(pid) ){
            String parentName="顶级";
            if(!rootPid.equals(pid)){
                parentName = baseMapper.getPMenuName(pid);
            }
            sysMenuDto.setParentName(parentName);
        }
        return sysMenuDto;
    }

    @Override
    public void update(SysMenuDto menu) {
        if(Checker.BeNotNull(menu.getMeta())){
            menu.setMeta(JSONUtil.toJsonStr(menu.getMeta()));
        }
        super.updateByPk(menu);
        clearCache(SysConst.authCache);
    }

    @Override
    public boolean delete(String id) {
        super.deleteLogicByPk(id);
        baseMapper.deleteByPid(id);
        clearCache(SysConst.authCache);
        return true;
    }

    @Override
    public void downMenuTs() {
        List<Tree> trees = new ArrayList<>(16);
        List<SysMenuDto> menus = baseMapper.listMenuSlideTree(null,true);
        if(Checker.BeNotEmpty(menus)){
            Tree tree = BuildTree.build(mentToTree(menus, MenuSlide.class));
            trees.addAll(tree.getChildren());
        }
        String targetPath = null;
        File file = null;
        try {
            Map<String, Object> params = new HashMap<>();
            params.put("trees",trees);
            targetPath = CmsFolderUtil.getAbsolutePath(UUID.randomUUID() +".ts");
            file = new File(targetPath);
            FileOutputStream outputStream = new FileOutputStream(file);
            Template t = configuration.getTemplate("index.ts.ftl");
            Writer out = new OutputStreamWriter(outputStream, Charset.forName("UTF-8"));
            t.process(params, out);
            out.flush();
            out.close();
            outputStream.close();
            down(targetPath,getResponse());
        }catch (Exception e){
            log.error("模板生成异常：{}",e.getMessage());
            e.printStackTrace();
        }finally {
            FileUtil.del(targetPath);
        }

    }

    private File down(String filePath, HttpServletResponse response) throws IOException {
        File file = new File(filePath);
        if(file.exists()){
            InputStream in = null;
            OutputStream out = null;
            response.setCharacterEncoding(SysConst.utf8);
            response.setHeader("content-disposition","attachment;fileName="+file.getName());
            response.setHeader("FileName", "index."+FileUtil.getSuffix(file.getName()));
            response.setHeader("Access-Control-Expose-Headers", "FileName");
            try {
                out=  response.getOutputStream();
                in = new FileInputStream(file);
                byte[] buffer  = new byte[1024];
                int i=in.read(buffer );
                while (i!=-1){
                    out.write(buffer , 0, i);//将缓冲区的数据输出到浏览器
                    i= in.read(buffer );
                }
            } catch (IOException e) {
                throw e;
            }finally {
                in.close();
                out.close();
            }
        }
        return file;
    }



    private <T>List<Tree<T>> mentToTree(List<SysMenuDto> menus,Class clz){
        List<Tree<T>> trees = new ArrayList<>();
        for(SysMenuDto menu:menus){
            try {
                Tree tree = (Tree) clz.getConstructor().newInstance();
                copy(menu,tree);
                if(Checker.BeNotNull(menu.getMeta())){
                    JSONObject meta = JSONUtil.parseObj(menu.getMeta());
                    boolean isSetLocale = !meta.containsKey("locale") || Checker.BeNull(meta.get("locale")) || Checker.BeBlank(meta.getStr("locale"));
                    if(isSetLocale){
                        meta.put("locale",menu.getMenuName());
                    }
                    tree.setMeta(meta);
                }
                trees.add(tree);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return trees;
    }
}