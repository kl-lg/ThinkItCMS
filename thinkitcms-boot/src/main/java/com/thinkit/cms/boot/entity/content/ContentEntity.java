package com.thinkit.cms.boot.entity.content;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.thinkit.cms.model.BaseModel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.Date;
/**
* @author LG
*/
@Getter
@Setter
@Accessors(chain = true)
@TableName("tk_content")
public class ContentEntity extends BaseModel {
       /**
         主键
       */
        @TableField("id")
        private String id;

       /**
         站点ID
       */
        @TableField("site_id")
        private String siteId;

       /**
         标题
       */
        @TableField("title")
        private String title;

       /**
         关键字
       */
        @TableField("keywords")
        private String keywords;

       /**
         简介
       */
        @TableField("description")
        private String description;

       /**
         副标题
       */
        @TableField("sub_title")
        private String subTitle;

       /**
         栏目名称
       */
        @TableField(exist = false)
        private String categoryName;

       /**
         分类
       */
        @TableField("category_id")
        private String categoryId;

       /**
         模型表ID
       */
        @TableField("form_model_id")
        private String formModelId;

        /**
         模型表 code
         */
        @TableField("form_model_code")
        private String formModelCode;


       /**
         是否转载
       */
        @TableField("copied")
        private Integer copied;

       /**
         作者
       */
        @TableField("author")
        private String author;

       /**
         编辑
       */
        @TableField("editor")
        private String editor;

       /**
         是否置顶
       */
        @TableField("is_top")
        private Integer isTop;

       /**
         是否推荐
       */
        @TableField("is_recomd")
        private Integer isRecomd;

        /**
         * 内容
         */
        @TableField("content")
        private String content;

       /**
         是否头条
       */
        @TableField("is_headline")
        private Integer isHeadline;

       /**
         拥有附件列表
       */
        @TableField("has_files")
        private Integer hasFiles;

       /**
         是否有推荐内容
       */
        @TableField("has_related")
        private Integer hasRelated;

       /**
         是否包标签
       */
        @TableField("has_tags")
        private Integer hasTags;

       /**
         地址
       */
        @TableField("url")
        private String url;



        /**
         * 手机端地址
         */
        @TableField("m_url")
        private String mUrl;

       /**
         置顶标签
       */
        @TableField("top_tag")
        private String topTag;

       /**
         封面
       */
        @TableField("cover")
        private String cover;

       /**
         多图轮播
       */
        @TableField("images")
        private String images;

       /**
         评论数
       */
        @TableField("comments")
        private Integer comments;

       /**
         点击数
       */
        @TableField("clicks")
        private Integer clicks;

       /**
         日期生成规则
       */
        @TableField("path_rule")
        private String pathRule;

       /**
         点赞数
       */
        @TableField("likes")
        private Integer likes;

       /**
         发布日期
       */
        @TableField("publish_date")
        private Date publishDate;

       /**
         发表用户ID
       */
        @TableField("publish_user_id")
        private String publishUserId;

       /**
         顺序
       */
        @TableField("sort")
        private Integer sort;

       /**
         状态：0、草稿 1、已发布 2、删除
       */
        @TableField("status")
        private Integer status;

       /**
         创建用户id
       */
        @TableField("create_id")
        private String createId;

       /**
         修改人id
       */
        @TableField("modified_id")
        private String modifiedId;

       /**
         创建时间
       */
        @TableField("gmt_create")
        private Date gmtCreate;

       /**
         修改时间
       */
        @TableField("gmt_modified")
        private Date gmtModified;

       /**
         创建人名称
       */
        @TableField("create_name")
        private String createName;

       /**
         修改人名称
       */
        @TableField("modified_name")
        private String modifiedName;


        @TableField("category_code")
        private String categoryCode;


        @TableField("source")
        private String source;

        @TableField("is_sync_es")
        private Integer isSyncEs;
}