package com.thinkit.cms.boot.controller.login;

import com.thinkit.cms.authen.secret.LoginService;
import com.thinkit.cms.api.sysuser.SysUserService;
import com.thinkit.cms.authen.secret.Muserlogin;
import com.thinkit.cms.core.base.BaseController;
import com.thinkit.cms.model.CmsResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Size;

/**
 * @author LONG
 */
@RestController
@RequestMapping("/api")
public class LoginController extends BaseController<SysUserService> {

    private final LoginService loginService;

    public LoginController(LoginService loginService) {
        this.loginService = loginService;
    }

    /**
     * 登录接口
     * @return
     */
    @PostMapping(value="/login")
    public CmsResult login(@RequestBody Muserlogin muserlogin){
        return loginService.login(muserlogin);
    }


    /**
     * 登录接口
     * @return
     */
    @PostMapping(value="/logout")
    public CmsResult logout(){
        return loginService.logout();
    }

    /**
     * @return
     */
    @GetMapping(value="/verifyCode")
    public CmsResult verifyCode(){
        return loginService.verifyCode();
    }

}
