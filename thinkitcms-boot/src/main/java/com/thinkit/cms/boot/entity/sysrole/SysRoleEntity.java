package com.thinkit.cms.boot.entity.sysrole;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.thinkit.cms.model.BaseModel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
/**
* @author LG
*/
@Getter
@Setter
@Accessors(chain = true)
@TableName("tk_sys_role")
public class SysRoleEntity extends BaseModel {
       /**
         角色名称
       */
        @TableField("role_name")
        private String roleName;

       /**
         角色标识
       */
        @TableField("role_sign")
        private String roleSign;

       /**
         备注
       */
        @TableField("remark")
        private String remark;

       /**
         是否是系统内置 0：非系统内置 1：系统内置
       */
        @TableField("is_system")
        private Integer isSystem;


}