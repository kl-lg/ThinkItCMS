package com.thinkit.cms.boot.controller.template;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.thinkit.cms.annotation.Logs;
import com.thinkit.cms.api.template.TemplateService;
import com.thinkit.cms.authen.annotation.Sentinel;
import com.thinkit.cms.core.base.BaseController;
import com.thinkit.cms.dto.template.*;
import com.thinkit.cms.model.CmsResult;
import com.thinkit.cms.model.PageModel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.io.IOException;

import static com.thinkit.cms.enums.LogModule.TEMPLATES;

/**
* @author LG
*/

@Validated
@RestController
@RequestMapping("/api/template")
@Slf4j
public class TemplateController extends BaseController<TemplateService> {



        @Logs(module = TEMPLATES,operation = "查看模板详情")
        @GetMapping(value="detail")
        public TemplateDto detail(@NotBlank(message = "ID不能为空!") @RequestParam String id){
             return service.getByPk(id);
        }


        @SaCheckPermission("templates")
        @PostMapping(value = "/page")
        public PageModel<TemplateDto> list(@RequestBody PageModel<TemplateDto> pageDto) {
             return service.listPage(pageDto);
        }


        @Logs(module = TEMPLATES,operation = "创建模板")
        @Sentinel
        @SaCheckPermission("templates:add")
        @PostMapping(value="save")
        public void save(@Validated @RequestBody TemplateDto templateDto) {
              service.save(templateDto);
        }


        @Logs(module = TEMPLATES,operation = "更新模板")
        @Sentinel
        @SaCheckPermission("templates:edit")
        @PutMapping(value="update")
        public void update(@Validated @RequestBody TemplateDto templateDto) {
              service.update(templateDto);
        }


        @Logs(module = TEMPLATES,operation = "删除模板")
        @Sentinel
        @SaCheckPermission("templates:delete")
        @DeleteMapping(value="delete")
        public boolean deleteByPk(@NotBlank @RequestParam String id){
                return service.delete(id);
        }

        @GetMapping(value="loopFloder")
        public CmsResult loopFolder(@Valid @NotBlank(message = "tmpDir 不能为空!") @RequestParam String tmpDir){
            return service.loopFolder(tmpDir);
        }

        @PutMapping(value="openDir")
        public void openDir(@Validated @RequestBody OpenTemplate openTemplate){
             service.openDir(openTemplate.getAbsolutePath());
        }

        @Sentinel
        @PostMapping(value="mkdirFile")
        public CmsResult mkdirFile(@RequestBody AddTemplate addTemplate){
            return service.mkdirFile(addTemplate);
        }



        @Logs(module = TEMPLATES,operation = "删除模板")
        @Sentinel
        @PostMapping(value="delFolder")
        public CmsResult delFolder(@RequestBody DelTemplateFile delTemplateFile){
            return service.delFolder(delTemplateFile);
        }


        @Logs(module = TEMPLATES,operation = "上传模板")
        @Sentinel
        @SaCheckPermission("templates:upload")
        @PostMapping(value="uploadCloud")
        public Boolean uploadCloud(@Validated @RequestBody UploadTemplate uploadTemplate){
             return service.uploadCloud(uploadTemplate);
        }


        @Logs(module = TEMPLATES,operation = "拉取模板")
        @Sentinel
        @SaCheckPermission("templates:pull")
        @PostMapping(value="pullCloud")
        public Boolean pullCloud(@Validated @RequestBody PullTemplate pullTemplate){
            return service.pullCloud(pullTemplate);
        }



        /**
         * 获取文件内容
         * @param getTemplate
         * @return
         */

        @Logs(module = TEMPLATES,operation = "获取模板内容")
        @PostMapping(value="fileContent")
        public CmsResult fileContent(@RequestBody GetTemplate getTemplate){
            return service.fileContent(getTemplate);
        }


    /**
     * 保存文件内容
     * @param content
     * @return
     */
        @Logs(module = TEMPLATES,operation = "保存模板内容")
        @Sentinel
        @PostMapping(value="saveContent")
        public CmsResult saveContent(@RequestBody SaveContent  content){
            return service.saveContent(content);
        }


        @Logs(module = TEMPLATES,operation = "模板文件重命名")
        @Sentinel
        @PostMapping(value="renameFile")
        public CmsResult renameFile(@RequestBody RenameFile renameFile){
            return service.renameFile(renameFile);
        }


        @GetMapping(value="loadTemp")
        public CmsResult loadTemp(@Valid @NotBlank(message = "请选择站点!") String siteId){
            return service.loadTemp(siteId);
        }


        @Logs(module = TEMPLATES,operation = "导入zip文件")
        @Sentinel
        @SaCheckPermission("templates:import")
        @PostMapping(value="importFolder")
        public CmsResult importFolder(MultipartFile file, @Valid @NotBlank(message = "tmpDir 不能为空!") @RequestParam String tmpDir) throws IOException {
            return service.importFolder(tmpDir,file.getOriginalFilename(),file.getInputStream());
        }
}