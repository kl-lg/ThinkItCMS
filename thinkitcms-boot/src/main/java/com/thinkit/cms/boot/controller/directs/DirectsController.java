package com.thinkit.cms.boot.controller.directs;

import com.thinkit.cms.annotation.Logs;
import com.thinkit.cms.api.directs.DirectsService;
import com.thinkit.cms.authen.annotation.Sentinel;
import com.thinkit.cms.core.base.BaseController;
import com.thinkit.cms.dto.directs.DirectsDto;
import com.thinkit.cms.model.KeyValueModel;
import com.thinkit.cms.model.PageModel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.List;

import static com.thinkit.cms.enums.LogModule.DIRECT;

/**
* @author lg
*/

@Validated
@RestController
@RequestMapping("/api/directs")
@Slf4j
public class DirectsController extends BaseController<DirectsService> {


        @GetMapping(value="detail")
        public DirectsDto detail(@NotBlank(message = "ID不能为空!") @RequestParam String id){
             return service.detail(id);
        }

        @PostMapping(value = "/page")
        public PageModel<DirectsDto> list(@RequestBody PageModel<DirectsDto> pageDto) {
             return service.listPage(pageDto);
        }


        @Logs(module = DIRECT,operation = "指令创建")
        @Sentinel
        @PostMapping(value="save")
        public void save(@Validated @RequestBody DirectsDto directsDto) {
              service.save(directsDto);
        }


        @Logs(module = DIRECT,operation = "指令更新")
        @Sentinel
        @PutMapping(value="update")
        public void update(@Validated @RequestBody DirectsDto directsDto) {
              service.update(directsDto);
        }



        @Logs(module = DIRECT,operation = "指令删除")
        @Sentinel
        @DeleteMapping(value="delete")
        public boolean deleteByPk(@NotBlank @RequestParam String id){
                return service.deleteByPk(id);
        }



        @Logs(module = DIRECT,operation = "指令缓存删除")
        @Sentinel
        @DeleteMapping(value="delCache")
        public boolean delCache(@NotBlank @RequestParam String directs){
            return service.delCache(directs);
        }



        @Logs(module = DIRECT,operation = "全部指令缓存删除")
        @Sentinel
        @DeleteMapping(value="delAllCache")
        public boolean delAllCache(){
            return service.delCache(null);
        }



        @Logs(module = DIRECT,operation = "指令启用")
        @Sentinel
        @PutMapping(value="enable")
        public void enable(@RequestBody DirectsDto directsDto) {
            service.enable(directsDto.getId(),directsDto.getEnable());
        }



        @Logs(module = DIRECT,operation = "指令重新应用")
        @Sentinel
        @PutMapping(value="apply")
        public void apply() {
            service.apply();
        }


        /**
         * 加载 1:指令、2:部件、3:模板、 4：变量等
         * @param type
         * @return
         */
        @Logs(module = DIRECT,operation = "指令加载")
        @GetMapping(value="loadCodes")
        public List<KeyValueModel> loadCodes(@Valid @NotBlank(message = "请选择站点!") String type){
            return service.loadCodes(type);
        }

        @GetMapping(value="loadHands")
        public List<String> loadHands(){
            return service.loadHands();
        }

        @GetMapping(value="loadInstructs")
        public List<String> loadInstructs(){
             return service.loadInstructs();
        }



}