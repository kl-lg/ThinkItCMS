package com.thinkit.cms.boot.entity.sysuser;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.thinkit.cms.model.BaseModel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.Date;

/**
* @author LG
*/
@Getter
@Setter
@Accessors(chain = true)
@TableName("tk_sys_user")
public class SysUserEntity extends BaseModel {
       /**
         用户账户名
       */
        @TableField("user_name")
        private String userName;

       /**
         用户名称(昵称)
       */
        @TableField("nick_name")
        private String nickName;

       /**
         密码
       */
        @TableField("password")
        private String password;

       /**
         邮箱
       */
        @TableField("email")
        private String email;

       /**
         手机号
       */
        @TableField("mobile")
        private String mobile;

       /**
         状态 0:正常，1:禁用
       */
        @TableField("status")
        private String status;

       /**
         0:男 1：女
       */
        @TableField("sex")
        private Integer sex;

       /**
         组织id
       */
        @TableField("org_id")
        private String orgId;

       /**
         组织代码
       */
        @TableField("org_code")
        private String orgCode;

       /**
         组织名
       */
        @TableField("org_name")
        private String orgName;

       /**
         默认站点ID
       */
        @TableField("def_site_id")
        private String defSiteId;

       /**
         0:正常 1：删除
       */
        @TableField("deleted")
        private Boolean deleted;


        @TableField("is_super_admin")
        private Boolean isSuperAdmin;

        @TableField("avatar")
        private String avatar;

        @TableField("last_login_time")
        private Date last_login_time;

        @TableField("remark")
        private String remark;
}