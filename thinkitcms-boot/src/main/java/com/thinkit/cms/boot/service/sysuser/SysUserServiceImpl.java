package com.thinkit.cms.boot.service.sysuser;

import cn.hutool.crypto.SecureUtil;
import cn.hutool.json.JSONUtil;
import com.alicp.jetcache.anno.CacheType;
import com.alicp.jetcache.anno.Cached;
import com.google.common.collect.Lists;
import com.thinkit.cms.api.sysrole.SysRoleService;
import com.thinkit.cms.api.sysuser.SysUserService;
import com.thinkit.cms.api.userrole.UserRoleService;
import com.thinkit.cms.authen.constant.ConfConst;
import com.thinkit.cms.boot.entity.sysuser.SysUserEntity;
import com.thinkit.cms.boot.mapper.sysuser.SysUserMapper;
import com.thinkit.cms.constant.SysConst;
import com.thinkit.cms.core.base.BaseServiceImpl;
import com.thinkit.cms.core.handler.CustomException;
import com.thinkit.cms.dto.sysrole.MyRoleDto;
import com.thinkit.cms.dto.sysrole.SysRoleDto;
import com.thinkit.cms.dto.sysuser.*;
import com.thinkit.cms.model.CmsResult;
import com.thinkit.cms.user.SysUserDetail;
import com.thinkit.cms.user.User;
import com.thinkit.cms.user.UserCtx;
import com.thinkit.cms.utils.Checker;
import com.thinkit.cms.utils.CmsConf;
import com.thinkit.cms.utils.MenuMeta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
* @author LG
*/
@Service
public class SysUserServiceImpl extends BaseServiceImpl<SysUserDto,SysUserEntity,SysUserMapper> implements SysUserService{


    @Autowired
    private UserRoleService userRoleService;

    @Autowired
    private SysRoleService sysRoleService;

    @Autowired
    private RedisTemplate redisTemplate;

    @Override
    public User loadUserName(String userName) {
        SysUserDto sysUser = baseMapper.loadUserName(userName);
        if(Checker.BeNotNull(sysUser)){
            String roleSign= baseMapper.hasRoleSign(sysUser.getId());
            SysUserDetail user = new SysUserDetail();
            user.setAccount(sysUser.getUserName());
            user.setPassword(sysUser.getPassword());
            user.setStatus(sysUser.getStatus());
            user.setId(sysUser.getId());
            user.setUserCtx(userCtx(sysUser));
            user.setRoles(Checker.BeBlank(roleSign)?0:1);
            user.setNickName(sysUser.getNickName());
            if(Checker.BeNotBlank(roleSign)){
                user.setRoleSign(roleSign.split(","));
            }
            return user;
        }
        return null;
    }

    /**
     * 构造上下文
     * @param sysUser
     * @return
     */
    private UserCtx userCtx(SysUserDto sysUser){
        boolean isSuperRole = sysRoleService.isSuperRole(sysUser.getId());
        UserCtx userCtx = new UserCtx();
        userCtx.setIsSuperAdmin(sysUser.getIsSuperAdmin());
        userCtx.setIsSuperRole(isSuperRole);
        return userCtx;
    }

    @Override
    public CmsResult save(SysUserDto sysUserDto) {
       String userName = sysUserDto.getUserName();
       if(hasUserName(null,userName)){
           return CmsResult.result(5028);
       }
       String privateKey= CmsConf.get(ConfConst.privateKey);
       String pw = sysUserDto.getPassword();
       String password = SecureUtil.md5(userName+pw+privateKey);
       sysUserDto.setPassword(password);
       insert(sysUserDto);
       return CmsResult.result();
    }

    @Override
    public CmsResult update(SysUserDto sysUserDto) {
        String userName = sysUserDto.getUserName();
        String rowId = sysUserDto.getId();
        if(hasUserName(rowId,userName)){
            return CmsResult.result(5028);
        }
        updateByPk(sysUserDto);
        return CmsResult.result();
    }

    @Override
    public CmsResult detail(String id) {
        return CmsResult.result(getByPk(id));
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean delete(String id) {
        if(isSuperUser(id)){
            throw new CustomException(CmsResult.result(5031));
        }
        if(getUserId().equals(id)){
            throw new CustomException(CmsResult.result(5109));
        }
        userRoleService.deleteByFiled("user_id",id);
        clearCache(SysConst.authCache);
        return super.deleteByPk(id);
    }

    @Override
    public CmsResult tipRoles(String userId) {
        List<SysRoleDto> roles = sysRoleService.listRoles();
        List<String> roleIds = userRoleService.roleIdsByUserId(userId);
        return CmsResult.result(new MyRoleDto(roles,roleIds));
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public CmsResult lock(LockUserDto lockUserDto) {
        String status = lockUserDto.getStatus();
        List<String> userIds = lockUserDto.getUserIds();
        if(Checker.BeNotEmpty(userIds)){
           baseMapper.lock(status,userIds);
        }
        return CmsResult.result();
    }

    @Override
    public CmsResult info() {
        String userId = getUserId();
        boolean isSuper = getUserCtx().isSuperAdmin() ||  getUserCtx().isSuperRole();
        SysUserDto sysUserDto = super.getByPk(userId);
        List<String> authSigns = baseMapper.queryAuthSign(userId,isSuper);
        SysUserInfoDto infoDto = new SysUserInfoDto();
        copy(sysUserDto,infoDto);
        infoDto.setAuthSign(filterAuthSign(authSigns));
        return CmsResult.result(infoDto);
    }

    private Set<String> filterAuthSign(List<String> authSigns){
        Set<String> signs = new HashSet<>(16);
        if(Checker.BeNotEmpty(authSigns)){
            List<MenuMeta> menuMetas = JSONUtil.toList(authSigns.toString(), MenuMeta.class);
            for(MenuMeta meta:menuMetas){
                if(Checker.BeNotEmpty(meta.getRoles())){
                    signs.addAll(meta.getRoles());
                }
            }
        }
        return signs;
    }

    private boolean hasUserName(String rowId,String userName){
       Integer count =  baseMapper.hasUserName(rowId,userName);
       return count>0;
    }

    @Override
    public boolean isSuperUser(String userId){
        if(Checker.BeNotBlank(userId)){
            Integer isSuperUser =  baseMapper.isSuperUser(userId);
            return Checker.BeNotNull(isSuperUser) && isSuperUser==1;
        }
        return  false;
    }

    @Override
    public CmsResult setDefaultSite(String siteId) {
        redisTemplate.opsForHash().put(SysConst.defSites,getUserId(),siteId);
        baseMapper.setDefaultSite(siteId,getUserId());
        return CmsResult.result();
    }

    @Override
    public String getDefaultSit(String userId) {
        Object siteObj = redisTemplate.opsForHash().get(SysConst.defSites,userId);
        if(Checker.BeNotNull(siteObj)){
            return String.valueOf(siteObj);
        }
        String siteId= baseMapper.getDefaultSite(userId);
        if(Checker.BeNotBlank(siteId)){
            redisTemplate.opsForHash().put(SysConst.defSites,getUserId(),siteId);
        }
        return siteId;
    }

    @Override
    public SysUserDto userInfo() {
        return super.getByPk(getUserId());
    }

    @Override
    public void updateBaseInfo(UpdateBaseUser updateBaseUser) {
         updateBaseUser.setId(getUserId());
         SysUserEntity userEntity = new SysUserEntity();
         copy(updateBaseUser,userEntity);
         super.updateById(userEntity);
    }

    @Override
    public void updatePw(UpdatePw updatePw) {
        if(!updatePw.getNpw().equals(updatePw.getAnpw())){
            throw new CustomException(5052);
        }
        if(updatePw.getPw().equals(updatePw.getAnpw())){
            throw new CustomException(5053);
        }
        SysUserDto sysUserDto =  this.getByPk(getUserId());
        String privateKey= CmsConf.get(ConfConst.privateKey);
        String pw = updatePw.getPw();
        String npw = updatePw.getNpw();
        String userName = sysUserDto.getUserName();
        // 旧密码
        String password = SecureUtil.md5(userName+pw+privateKey);
        if(!password.equals(sysUserDto.getPassword())){
            throw new CustomException(5054);
        }
        String newPw = SecureUtil.md5(userName+npw+privateKey);
        baseMapper.updatePw(getUserId(),newPw);
        logout();
    }


    @Cached(name = "cached::", key = "targetObject.class+'.queryAuthSign.'+#uid" ,expire = 7200, cacheType = CacheType.REMOTE)
    @Override
    public List<String> queryAuthSign(String uid, Boolean isSuper) {
        List<String> authSigns = baseMapper.queryAuthSign(uid,isSuper);
        if(Checker.BeNotEmpty(authSigns)){
            Set<String> signs = filterAuthSign(authSigns);
            return new ArrayList<>(signs);
        }
        return Lists.newArrayList();
    }

    @Override
    public boolean checkVerifyCode(String verifyUid, String verifyCode) {
        Object val = redisTemplate.opsForValue().get((SysConst.verifyScope + verifyUid));
        return Checker.BeNotNull(val) && val.toString().equals(verifyCode);
    }

}