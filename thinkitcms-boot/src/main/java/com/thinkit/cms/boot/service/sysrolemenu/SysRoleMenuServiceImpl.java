package com.thinkit.cms.boot.service.sysrolemenu;

import com.google.common.collect.Lists;
import com.thinkit.cms.api.sysrolemenu.SysRoleMenuService;
import com.thinkit.cms.boot.entity.sysrolemenu.SysRoleMenuEntity;
import com.thinkit.cms.boot.mapper.sysrolemenu.SysRoleMenuMapper;
import com.thinkit.cms.constant.SysConst;
import com.thinkit.cms.core.base.BaseServiceImpl;
import com.thinkit.cms.core.handler.CustomException;
import com.thinkit.cms.dto.sysrolemenu.SysRoleAssignDto;
import com.thinkit.cms.dto.sysrolemenu.SysRoleMenuDto;
import com.thinkit.cms.model.CmsResult;
import com.thinkit.cms.utils.Checker;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
* @author LG
*/

@Service
public class SysRoleMenuServiceImpl extends BaseServiceImpl<SysRoleMenuDto,SysRoleMenuEntity,SysRoleMenuMapper> implements SysRoleMenuService{


    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveTreeAssign(SysRoleAssignDto roleAssignDto) {
        String roleId = roleAssignDto.getRoleId();
        if(isSysRole(roleId)){
            throw  new CustomException(CmsResult.result(5030));
        }
        List<String> menuIds = roleAssignDto.getMenuIds();
        super.deleteByFiled("role_id",roleId);
        if(Checker.BeNotEmpty(menuIds)){
            List<SysRoleMenuDto> roleMenus=new ArrayList<>();
            for(String menuId:menuIds){
                SysRoleMenuDto sysRoleMenuDto = new SysRoleMenuDto(roleId,menuId);
                roleMenus.add(sysRoleMenuDto);
            }
            super.insertBatch(roleMenus);
        }
        clearCache(SysConst.authCache);
    }

    public boolean isSysRole(String roleId){
        Integer isSys = baseMapper.isSysRole(roleId);
        return Checker.BeNotNull(isSys) && isSys==1;
    }

    @Override
    public List<String> queryMenuIdsByRoleId(String roleId) {
        List<String> menuIds = baseMapper.queryMenuIdsByRoleId(roleId);
        return Checker.BeNotEmpty(menuIds)?menuIds: Lists.newArrayList();
    }
}