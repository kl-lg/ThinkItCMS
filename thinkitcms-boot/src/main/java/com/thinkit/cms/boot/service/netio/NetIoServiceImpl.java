package com.thinkit.cms.boot.service.netio;

import cn.hutool.core.codec.Base64;
import cn.hutool.core.io.FileUtil;
import com.thinkit.cms.api.netio.NetIoService;
import com.thinkit.cms.constant.SysConst;
import com.thinkit.cms.core.base.BaseSiteService;
import com.thinkit.cms.core.handler.CustomException;
import com.thinkit.cms.dto.netio.Chunk;
import com.thinkit.cms.model.CmsResult;
import com.thinkit.cms.netio.clients.ClientFactory;
import com.thinkit.cms.netio.clients.OssClient;
import com.thinkit.cms.utils.Checker;
import com.thinkit.cms.utils.CmsConf;
import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.stereotype.Service;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

/**
 * @author LONG
 */
@Slf4j
@Service
public class NetIoServiceImpl extends BaseSiteService implements NetIoService {

    private final ClientFactory clientFactory;

    private final String bucketName = CmsConf.get(SysConst.bucketName);

    public NetIoServiceImpl(ClientFactory clientFactory) {
        this.clientFactory = clientFactory;
    }

    @Override
    public CmsResult upload(Chunk chunk) {
       try {
           return getClient().uploadFile(chunk);
       }catch (Exception e){
            getClient().cancel(chunk);
            getResponse().setStatus(501);
       }
       return CmsResult.result(5114);
    }

    @Override
    public CmsResult upload(String originalFilename, String name, byte[] bytes) {
        return getClient().uploadFile(bucketName,originalFilename,bytes,getSiteId(false));
    }

    @Override
    public CmsResult upload(String originalFilename, String name, InputStream inputStream,long size) {
        return getClient().uploadFile(bucketName,originalFilename,inputStream,size,getSiteId(false));
    }

    @Override
    public CmsResult upload(String originalFilename, String name, File file) {
        try {
            return upload(originalFilename,name,new FileInputStream(file),file.length());
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public CmsResult delete(String key) {
        return getClient().deleteFile(bucketName,key,getSiteId(true));
    }

    @Override
    public Boolean down(String fileUrl, File file)  {
        InputStream inputStream = null;
        FileOutputStream outputStream = null;
        try {
            if(Checker.BeBlank(fileUrl)){
                throw new CustomException(5056);
            }
            URL url = new URL(fileUrl);
            URLConnection connection = url.openConnection();
            inputStream = connection.getInputStream();
            outputStream = new FileOutputStream(file);
            IOUtils.copy(inputStream, outputStream);
        }catch (Exception e){
            log.error("下载文件失败：{}",e.getMessage());
            // throw new CustomException(5056);
            return Boolean.FALSE;
        }finally {
            try {
                if(inputStream!=null){
                    inputStream.close();
                }
                if(outputStream!=null){
                    outputStream.close();
                }
            }catch (IOException e){
                return Boolean.FALSE;
            }
        }
        return Boolean.TRUE;
    }

    @Override
    public CmsResult uploadLocal(String path,String originalFilename, String name, InputStream inputStream, long size) {
        path = Base64.decodeStr(path, StandardCharsets.UTF_8);
        if(FileUtil.exist(path)){
             FileUtil.writeFromStream(inputStream,new File(path+SysConst.prefix+originalFilename));
        }
        return CmsResult.result();
    }

    @Override
    public CmsResult checkFileIsExist(Chunk chunk) {
        Map<String,Object> params=new HashMap<>(16);
        params.put("skip",false);
        return CmsResult.result(params);
    }

    @Override
    public CmsResult merge(Chunk chunk) {
        return getClient().merge(chunk);
    }

    private OssClient getClient(){
        return clientFactory.getOssClient().build(CmsConf.get(SysConst.endpoint),CmsConf.get(SysConst.accessKeyId),CmsConf.get(SysConst.accessKeySecret));
    }
}
