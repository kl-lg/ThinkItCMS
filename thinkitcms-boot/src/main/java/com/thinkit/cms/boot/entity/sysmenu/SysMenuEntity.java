package com.thinkit.cms.boot.entity.sysmenu;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.thinkit.cms.model.BaseModel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
/**
* @author LG
*/
@Getter
@Setter
@Accessors(chain = true)
@TableName("tk_sys_menu")
public class SysMenuEntity extends BaseModel {
       /**
         父ID
       */
        @TableField("pid")
        private String pid;

        @TableField("api")
        private String api;

        /**
         * 菜单名称
         */
        @TableField("menu_name")
        private String menuName;

       /**
         路由名称
       */
        @TableField("name")
        private String name;

       /**
         路由路径
       */
        @TableField("path")
        private String path;

       /**
         菜单组件地址
       */
        @TableField("component")
        private String component;

       /**
         排序
       */
        @TableField("`order`")
        private Integer order;

       /**
         路由 Meta 元信息
       */
        @TableField("meta")
        private String meta;

       /**
         0:目录 1：菜单 2：按钮
       */
        @TableField("type")
        private Integer type;

       /**
         是否删除 0：正常 1：删除
       */
        @TableField("deleted")
        private Boolean deleted;

}