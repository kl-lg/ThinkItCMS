package com.thinkit.cms.core.utils;

import lombok.Getter;

public class LicenseGen {



    public static void main(String[] args) throws Exception {
//        Map<String, Object> keyMap = RSAUtils.genKeyPair();
//        String publicKey = RSAUtils.getPublicKey(keyMap);
//        String privateKey = RSAUtils.getPrivateKey(keyMap);
//        System.err.println("公钥: \n\r" + publicKey);
//        System.err.println("私钥： \n\r" + privateKey);
          testAuth();
    }

    static  void testAuth() throws Exception {
        LicenseGen licenseProperties=new LicenseGen();
        licenseProperties.setAuthorizeDesc("正式商用版授权");
        licenseProperties.setCopyrightOwner("ThinkItCMS");
        licenseProperties.setStartStopTime("2021-12-03~5022-12-10");
        licenseProperties.setDomain("*");
        licenseProperties.setOrganization("湖南华美信息技术有限公司");
        licenseProperties.setVersion("2.0");
        String source=licenseProperties.toString();
        byte[] data = source.getBytes();
        byte[] encodedData = RSAUtils.encryptByPrivateKey(data, privateKey);
        System.out.println("加密后：\r\n" + new String(encodedData));

        byte[] decodedData = RSAUtils.decryptByPublicKey(encodedData, publicKey);
        String target = new String(decodedData);
        System.out.println("解密后: \r\n" + target);

        String sign = RSAUtils.sign(licenseProperties.toString().getBytes(), privateKey).
        replace("\r\n","").
        replace("\n","").
        replace("\r","");
        System.out.println("签名:" + sign);

        String str="\r\n";
        StringBuffer bytes=new StringBuffer("");
//        bytes.append("copyrightOwner"+ SecurityConstants.LICENSE_NAME_SPLIT+licenseProperties.getCopyrightOwner()+str);
//        bytes.append("version"+ SecurityConstants.LICENSE_NAME_SPLIT+licenseProperties.getVersion()+str);
//        bytes.append("authorizeDesc"+ SecurityConstants.LICENSE_NAME_SPLIT+licenseProperties.getAuthorizeDesc()+str);
//        bytes.append("organization"+ SecurityConstants.LICENSE_NAME_SPLIT+licenseProperties.getOrganization()+str);
//        bytes.append("startStopTime"+ SecurityConstants.LICENSE_NAME_SPLIT+licenseProperties.getStartStopTime()+str);
//        bytes.append("domain"+ SecurityConstants.LICENSE_NAME_SPLIT+licenseProperties.getDomain()+str);
        boolean status = RSAUtils.verify(licenseProperties.toString().getBytes(), publicKey, sign);
        System.out.println("签名验证结果:"+status);
       // bytes.append("signaturer"+ SecurityConstants.LICENSE_NAME_SPLIT+sign+str);
        Base64Utils.byteArrayToFile(bytes.toString().getBytes(), "E:\\tk1\\license.dat");
    }



    @Getter
   // @LicenseMark
    private String version;

    @Getter
   // @LicenseMark
    private String  organization;

    @Getter
  //  @LicenseMark
    private String domain;

    @Getter
   // @LicenseMark
    private String startStopTime;

    @Getter
   // @LicenseMark
    private String authorizeDesc ;

    @Getter
   // @LicenseMark
    private String copyrightOwner;

    @Getter
   // @LicenseMark
    private String signaturer;

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getStartStopTime() {
        return startStopTime;
    }

    public void setStartStopTime(String startStopTime) {
        this.startStopTime = startStopTime;
    }

    public String getAuthorizeDesc() {
        return authorizeDesc;
    }

    public void setAuthorizeDesc(String authorizeDesc) {
        this.authorizeDesc = authorizeDesc;
    }

    public String getCopyrightOwner() {
        return copyrightOwner;
    }

    public void setCopyrightOwner(String copyrightOwner) {
        this.copyrightOwner = copyrightOwner;
    }

    public String getSignaturer() {
        return signaturer;
    }

    public void setSignaturer(String signaturer) {
        this.signaturer = signaturer;
    }

    public static String getPublicKey() {
        return publicKey;
    }

    public static void setPublicKey(String publicKey) {
        LicenseGen.publicKey = publicKey;
    }

    public static String getPrivateKey() {
        return privateKey;
    }

    public static void setPrivateKey(String privateKey) {
        LicenseGen.privateKey = privateKey;
    }

    private static String publicKey="MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCkD7vzjM6IFj9dBftSbf0piS9A9mLW8YnGAV4Rdy1Z3P0wJ+lGnBDWNWlKX7cRtHsGsodar4vJelgOjQWNMDqlT1ngJEbz3EbJlwem7d0e59Bww1HF+DuPBM1/+jpspjk5zy1bgLmSrH8tJ5LX/O0t4A+BtnvZPUZKCBytAbxJlwIDAQAB";

    private static String privateKey = "MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAKQPu/OMzogWP10F+1Jt/SmJL0D2YtbxicYBXhF3LVnc/TAn6UacENY1aUpftxG0ewayh1qvi8l6WA6NBY0wOqVPWeAkRvPcRsmXB6bt3R7n0HDDUcX4O48EzX/6OmymOTnPLVuAuZKsfy0nktf87S3gD4G2e9k9RkoIHK0BvEmXAgMBAAECgYAqKe0I89gke2s/cgkcLy5QHXSqgmf2i873t2RAotth6/yaArfeML/W/ZUYyy6We0be+sT0locbebjlzHJvzcRUTHpZ5w7Io8yBHkD907uLfuSAyYVSJOV5hPMoSayccOwzNDRU5ErBeUvHMWA7mcI1iA5pUCB1QfbEFPymiLzsWQJBANVm9gUyEOA0V7I07VXrRkybgfJbKNqFlyiJzrbsZA9fOkA+IwlmeHj7Rprnp1HCmFpVuKymXicF3nzgB+z+nNUCQQDEz2sr6hsD5b7OMJvxsLbP04533TDJwZq+Y0Hubx7VMFuwj+esRM2wDbgspiEPJkc+E92EzIHnqm8LIIDTNdK7AkEAgST/K+PBnL1wePNY0g9BCultOar7TUtdT/Yd+EuUvkzF5szzzSOLMsP2VZZthqg6JSkNSsaf4gPGQlId2cm4RQJATzPBx2xw7sKYZNX/C+a3yy8yGF61cZwuMAw1zkR9vgG2BHoFG7icpgr/YUiB6VzYMs0/yl+ar6ZRBcNQ1K7GXQJAGbhkgsSBGBtarR51MrmcuSqIVcd5wpSwDI5ZgAGxRnW/+NkthkudSN3T/7bUtcdqcAUPHxhCl96BgVVJ8D52Gw==";

    @Override
    public String toString() {
        return "version="+version+",organization="+organization+",domain="+domain+",startStopTime="+startStopTime+",authorizeDesc="+authorizeDesc+",copyrightOwner="+copyrightOwner;
    }
}
