package com.thinkit.cms.core.handler;
import com.thinkit.cms.model.CmsResult;
import lombok.Data;

/**
 * @author LONG
 */
@Data
public class CustomException extends RuntimeException{

	public CmsResult r;

	public int code= CmsResult.failCode;

	public CustomException(CmsResult r) {
		super(r.getMsg());
		this.r = r;
		this.code = r.getCode();
	}

	public CustomException(String msg) {
		super(msg);
		this.r = CmsResult.result(CmsResult.failCode,msg);
		this.code = CmsResult.failCode;
	}

	public CustomException(String msg,int code) {
		super(msg);
		this.code = code;
		this.r = CmsResult.result(code,msg);
	}

	public CustomException(int code) {
		super(CmsResult.get(code));
		this.code = code;
		this.r = CmsResult.result(code);
	}

}
