package com.thinkit.cms.core.base;

import cn.dev33.satoken.stp.StpUtil;
import com.thinkit.cms.constant.SysConst;
import com.thinkit.cms.core.handler.CustomException;
import com.thinkit.cms.model.CmsResult;
import com.thinkit.cms.utils.Checker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class BaseSiteService {

    @Autowired
    RedisTemplate redisTemplate;

    protected String getSiteId(Boolean allow){
        Object siteObj = redisTemplate.opsForHash().get(SysConst.defSites,getUserId());
        if(Checker.BeNotNull(siteObj)){
            return String.valueOf(siteObj);
        }
        if(!allow){
            throw new CustomException(CmsResult.result(5040));
        }
        return null;
    }

    protected String getUserId(){
        return StpUtil.getLoginIdAsString();
    }

    /**
     * 获取request
     */
    protected HttpServletRequest getRequest(){
        return getRequestAttributes().getRequest();
    }

    /**
     * 获取response
     */
    protected HttpServletResponse getResponse(){
        return getRequestAttributes().getResponse();
    }

    private static ServletRequestAttributes getRequestAttributes() {
        RequestAttributes attributes = RequestContextHolder.getRequestAttributes();
        return (ServletRequestAttributes) attributes;
    }
}
