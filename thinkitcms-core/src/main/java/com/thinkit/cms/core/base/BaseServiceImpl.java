package com.thinkit.cms.core.base;

import cn.hutool.json.JSONUtil;
import com.alicp.jetcache.anno.CacheType;
import com.alicp.jetcache.anno.Cached;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.google.common.collect.Lists;
import com.thinkit.cms.base.BaseService;
import com.thinkit.cms.core.handler.CustomException;
import com.thinkit.cms.core.utils.DozerUtils;
import com.thinkit.cms.model.*;
import com.thinkit.cms.utils.Checker;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.Cursor;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.ScanOptions;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.*;

/**
 * @author LONG
 */
@Slf4j
public class BaseServiceImpl<D extends BaseDto,T extends BaseModel,M extends BaseMapper<T>> extends BasePlus<D,T,M> implements BaseService<D> {

	private Class<T> tclz;

	private Class<D> dclz;

	public BaseServiceImpl(){
		Type genType = getClass().getGenericSuperclass();
		Type[] params = ((ParameterizedType) genType).getActualTypeArguments();
		dclz = (Class<D>) params[0];
		tclz = (Class<T>) params[1];
	}

	protected Map<String,Object> buildMap(ModelData modelData){
		// 扩展 map
		Map<String,Object> objectMap = new HashMap<>(16);
		Map<String,Object> extraMap = modelData.getExtra();
		if(Checker.BeNotEmpty(extraMap)){
			objectMap.putAll(extraMap);
		}
		modelData.setExtra(null);
		String modelDataStr = JSONUtil.toJsonStr(modelData);
		Map<String,Object> contentMap = JSONUtil.toBean(modelDataStr,Map.class);
		if(Checker.BeNotEmpty(contentMap)){
			objectMap.putAll(contentMap);
		}
		log.info("注：当前方法注入的参数在指定模板页面均可以 ${} 获取,注入参数：{}",JSONUtil.toJsonStr(objectMap));
		return objectMap;
	}

	protected void checkPk(String pk){
		if(Checker.BeBlank(pk)){
			throw new CustomException(102);
		}
	}

	@Override
	public List<D> listDto(D d) {
        T t=D2T(d);
		QueryWrapper<T> queryWrapper = Checker.BeNotNull(d.toWrapper())?d.toWrapper():new QueryWrapper<>();
		String[] columns = d.select();
		if(Checker.BeNotEmpty(columns)){
			queryWrapper.select(columns);
		}
        List<T> list=super.list(queryWrapper);
        if (Checker.BeEmpty(list)) {
			return Lists.newArrayList();
		}
        return DozerUtils.T2DList(list,dclz);
	}

	@Override
	public List<D> listMap(Map<String, Object> map) {
		List<T> list=  super.listByMap(map);
		if (Checker.BeEmpty(list)) {
			return Lists.newArrayList();
		}
		return DozerUtils.T2DList(list,dclz);
	}

	@Override
	public PageModel<D> listPage(PageModel<D> pageDto) {
		D d = pageDto.getDto();
        T t=D2T(d);
		QueryWrapper<T> queryWrapper = Checker.BeNotNull(d.toWrapper())?d.toWrapper():new QueryWrapper<>();
		String[] columns = d.select();
		if(Checker.BeNotEmpty(columns)){
			queryWrapper.select(columns);
		}
		if(Checker.BeNotBlank(d.getSiteId())){
			queryWrapper.eq("site_id",d.getSiteId());
		}
        Page<T> page = new Page<T>(pageDto.getCurrent(),pageDto.getPageSize());
        IPage<T> pageRes = super.page(page, queryWrapper);
        if(Checker.BeEmpty(pageRes.getRecords())){
            return new PageModel<>(0,Lists.newArrayList());
        }
        List<D> rows =DozerUtils.T2DList(pageRes.getRecords(),dclz);
        long count = pageRes.getTotal();
        return  new PageModel<>(count,pageRes.getPages(),pageRes.getCurrent(),rows);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public boolean insert(D d) {
		fillD(d);
        T t=D2T(d);
		String pk=Checker.BeNotBlank(t.getId())?t.getId(): id();
        t.setId(pk);
        return super.save(t);
	}

	@Override
	public boolean insertBatch(List<D> dtos) {
		if(Checker.BeNotEmpty(dtos)){
			dtos.forEach(dto->{
				fillD(dto);
			});
			List<T> rows =DozerUtils.D2TList(dtos,tclz);
			return super.saveBatch(rows);
		}
		return false;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public boolean updateByPk(D d) {
        fillD(d);
        T t=D2T(d);
		checkPkIsNull(t);
		return super.updateById(t);
	}

	@Override
	public boolean updateByPks(List<D> dtos) {
    	if(Checker.BeNotEmpty(dtos)){
			dtos.forEach(d -> {fillD(d);});
			List<T> rows =DozerUtils.D2TList(dtos,tclz);
			return super.updateBatchById(rows);
		}
		return false;
	}

	@Override
	public boolean updateByField(String field,String val, D dval) {
		T t =D2T(dval);
		QueryWrapper<T> queryWrapper=new QueryWrapper<>();
		queryWrapper.eq(field,val);
		return update(t,queryWrapper);
	}

	@Override
	public D getByPk(Serializable pk) {
		T t = super.getById(pk);
	    return Checker.BeNotNull(t)?T2D(t):null;
    }

	@Override
	public List<D> getByPks(Collection<String> pks) {
		if(Checker.BeNotEmpty(pks)){
			List<T> ts = listByIds(pks);
			return Checker.BeNotEmpty(ts)? T2DList(ts): Lists.newArrayList();
		}
		return Lists.newArrayList();
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public boolean deleteByPk(String pk) {
		QueryWrapper<T> queryWrapper=new QueryWrapper<>();
		queryWrapper.eq("id",pk);
		return super.remove(queryWrapper);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public boolean deleteLogicByPk(String pk) {
		return super.removeById(pk);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public boolean deleteLogicByPks(Collection<String> pks) {
		return super.removeBatchByIds(pks);
	}

	@Override
	public boolean deleteByPks(Collection<String> ids) {
		return super.removeByIds(ids);
	}

	@Override
	public boolean removeByMap(Map<String, Object> columnMap) {
		return super.removeByMap(columnMap);
	}

	@Override
	public boolean deleteByFiled(String field, Object val) {
		if(Checker.BeNotBlank(field) && Checker.BeNotNull(val)){
			QueryWrapper<T> queryWrapper=  new QueryWrapper<>();
			if(val instanceof String) {
				queryWrapper.eq(field,val);
			}
			if(val instanceof Collection) {
				queryWrapper.in(field,((Collection)val).toArray());
			}
			return super.remove(queryWrapper);
		}
		return false;
	}

	@Override
	public D getOneDto(D d) {
        QueryWrapper<T> queryWrapper= wrapperIt(D2T(d),d);
        T t=super.getOne(queryWrapper);
        return Checker.BeNotNull(t)?T2D(t):null;
	}

	@Override
	public D getOneByMap(Map<String, Object> param) {
		List<D> dtos=listMap(param);
		return Checker.BeNotEmpty(dtos)?dtos.get(0):null;
	}

	@Override
	public D getByField(String field, String val) {
		QueryWrapper<T> queryWrapper= new QueryWrapper<>();
		queryWrapper.eq(field,val).last("limit 1");
		T t=super.getOne(queryWrapper);
		return Checker.BeNotNull(t)?T2D(t):null;
	}

	@Override
	public List<D> listByField(String field, Object val,String[] orderByDesc,String[] orderByAsc) {
		QueryWrapper<T> queryWrapper= new QueryWrapper<>();
		if(Checker.BeNotEmpty(orderByDesc)){
			queryWrapper.orderByDesc(Arrays.asList(orderByDesc));
		}
		if(Checker.BeNotEmpty(orderByAsc)){
			queryWrapper.orderByAsc(Arrays.asList(orderByAsc));
		}
		if(val instanceof String)queryWrapper.eq(field,val);
		if(val instanceof Collection) {
			if(Checker.BeNotEmpty((Collection)val)){
				queryWrapper.in(field,((Collection)val).toArray());
			}else{
				return Lists.newArrayList();
			}
		}
		List<T> ts=super.list(queryWrapper);
		return Checker.BeNotEmpty(ts)?T2DList(ts):Lists.newArrayList();
	}


	@Override
	public List<D> listByField(String field, Object val) {
		QueryWrapper<T> queryWrapper= new QueryWrapper<>();
		if(val instanceof String) {
			queryWrapper.eq(field,val);
		}
		if(val instanceof Collection) {
			if(Checker.BeNotEmpty((Collection)val)){
				queryWrapper.in(field,((Collection)val).toArray());
			}else{
				return Lists.newArrayList();
			}
		}
		List<T> ts=super.list(queryWrapper);
		return Checker.BeNotEmpty(ts)?T2DList(ts):Lists.newArrayList();
	}

	@Override
	public List<D> listAll(String... fields) {
		QueryWrapper<T> queryWrapper= new QueryWrapper<>();
		if(Checker.BeNotEmpty(fields)){
			queryWrapper.select(fields);
		}
		List<T> ts = super.list(queryWrapper);
		if(Checker.BeNotEmpty(ts)){
			return T2DList(ts);
		}
		return Lists.newArrayList();
	}


	private void checkPkIsNull(T t){
		 if(Checker.BeBlank(t.getId())){
	        throw new CustomException(CmsResult.result(4001));
		 }
	}

	private QueryWrapper<T> wrapperIt(T t,D d) {
		if (Checker.BeNull(t)) {
			return new QueryWrapper<>(t);
		}
		QueryWrapper<T> wrapper = null;
		wrapper = new QueryWrapper<T>();
		return wrapper;
	}


	private void fillD(D d){
		Date date = new Date();
		if(Checker.BeBlank(d.getCreateId())){
			d.setCreateId(getUserId());
		}
		if(Checker.BeBlank(d.getModifiedId())){
			d.setModifiedId(getUserId());
		}
		if(Checker.BeNull(d.getGmtCreate())){
			d.setGmtCreate(date);
		}
		if(Checker.BeNull(d.getGmtModified())){
			d.setGmtModified(date);
		}
	}

	protected void copy(Object dest,Object target){
		DozerUtils.copy(dest,target);
	}

	protected List<D> T2DList(List<T> list){
		return DozerUtils.T2DList(list,dclz);
	}

	protected List<T> D2TList(List<D> list){
		return DozerUtils.D2TList(list,tclz);
	}

	protected List<D> ResultT2D(List<T> list){
		return Checker.BeNotEmpty(list) ? T2DList(list):Lists.newArrayList();
	}

	protected T D2T(D d){
		return DozerUtils.D2T(d,tclz);
	}

	protected D T2D(T t){
		return DozerUtils.T2D(t,dclz);
	}
}
