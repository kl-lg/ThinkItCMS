package com.thinkit.cms.core.handler;
import cn.dev33.satoken.exception.NotLoginException;
import cn.dev33.satoken.exception.NotPermissionException;
import cn.dev33.satoken.exception.NotRoleException;
import cn.dev33.satoken.jwt.exception.SaJwtException;
import com.google.common.base.Throwables;
import com.thinkit.cms.authen.exceptions.AuthenException;
import com.thinkit.cms.model.CmsResult;
import com.thinkit.cms.utils.Checker;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.RedisConnectionFailureException;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @ClassName: GlobalExceptionHander
 * @Author: LG
 * @Date: 2019/5/22 13:58
 * @Version: 1.0
 **/
@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    @ExceptionHandler(value = { MethodArgumentNotValidException.class })
    public CmsResult handleResourceNotFoundException(MethodArgumentNotValidException e) {
        List<ObjectError> errors=e.getBindingResult().getAllErrors();
        if(Checker.BeNotEmpty(errors)){
            String msg = errors.get(0).getDefaultMessage();
            msg=CmsResult.getLanguageMsg(msg);
            return  CmsResult.result(99999,msg);
        }
        return CmsResult.result(-1);
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public CmsResult validationErrorHandler(ConstraintViolationException ex) {
         List<String> errors = ex.getConstraintViolations()
        .stream()
        .map(ConstraintViolation::getMessage)
        .collect(Collectors.toList());
        if(Checker.BeNotEmpty(errors)){
            String msg = errors.get(0);
            msg=CmsResult.getLanguageMsg(msg);
           return CmsResult.result(99999,msg);
        }
        return CmsResult.result(-1);
    }

    @ExceptionHandler(BindException.class)
    public Object validationBindingErrorHandler(BindingResult result) {
        if(result.hasErrors()){
            List<ObjectError> ls = result.getAllErrors();
            if(ls.size()>0){
                String msg = ls.get(0).getDefaultMessage();
                msg=CmsResult.getLanguageMsg(msg);
                return CmsResult.result(99999,msg);
            }
        }
        return CmsResult.result(-1);
    }



    @ExceptionHandler(MaxUploadSizeExceededException.class)
    public CmsResult maxUploadSizeException(MaxUploadSizeExceededException ex) {
        log.error(ex.getMessage());
        return CmsResult.result(20027);
    }


    @ExceptionHandler(NotLoginException.class)
    public CmsResult notLoginException(NotLoginException ex) {
        log.error(ex.getMessage());
        return CmsResult.result(401);
    }

    @ExceptionHandler(NotPermissionException.class)
    public CmsResult notPermissionException(NotPermissionException ex) {
        log.error(ex.getMessage());
        return CmsResult.result(403);
    }

    @ExceptionHandler(NotRoleException.class)
    public CmsResult notRoleException(NotRoleException ex) {
        log.error(ex.getMessage());
        return CmsResult.result(403);
    }

    @ExceptionHandler(SaJwtException.class)
    public CmsResult jwtException(SaJwtException ex) {
        log.error(ex.getMessage());
        return CmsResult.result(403);
    }

    @ExceptionHandler(AuthenException.class)
    public CmsResult jwtException(AuthenException ex) {
        log.error(ex.getMessage());
        return CmsResult.result(403,ex.getMessage());
    }

    @ExceptionHandler(RedisConnectionFailureException.class)
    public CmsResult redisException(RedisConnectionFailureException ex) {
        log.error(ex.getMessage());
        return CmsResult.result(6379,"Redis 连接异常请查看!");
    }




    @ExceptionHandler
    public CmsResult handle(Exception ex) {
        log.error(ex.getMessage());
        CmsResult result = adornerExcepiton(ex);
        return result;
    }


    private CmsResult adornerExcepiton(Exception ex){
        if (CustomException.class.isInstance(ex)) {
            return ((CustomException) ex).getR();
        }else  if (AuthenException.class.isInstance(ex)) {
            return CmsResult.result(((AuthenException) ex).getResult());
        }
        else{
            String message= Checker.BeNotBlank(ex.getMessage())?ex.getMessage(): Throwables.getStackTraceAsString(ex);
            log.error("===============================系统异常捕捉:异常原因: {}=========================================",message);
            return CmsResult.result(-1);
        }
    }
}
