package com.thinkit.cms.core.iterator;

import cn.hutool.core.date.DateUtil;
import com.thinkit.cms.annotation.License;
import com.thinkit.cms.utils.Checker;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.Field;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

@Slf4j
public class SignPropertyIterator implements Iterator {

    public BeanPropertyBox beanPropertyBox;

    private int i=0;

    Map<String,Object> param=new LinkedHashMap<>(16);

    public SignPropertyIterator(BeanPropertyBox beanPropertyBox){
        this.beanPropertyBox=beanPropertyBox;
    }

    @Override
    public boolean hasNext() {
        return i<beanPropertyBox.size();
    }

    @Override
    public Object next() {
        param.clear();
        Object obj=beanPropertyBox.getBean();
        Field field= beanPropertyBox.getField(i);
        field.setAccessible(true);
        if(Checker.BeNotNull(field)){
            try {
                if(Checker.BeNotNull(field.getAnnotation(License.class))){
                    Object val = field.get(obj);
                    if(Checker.BeNotNull(val)){
                        if(val instanceof Date){
                            param.put(field.getName(), DateUtil.format((Date)val,"yyyy-MM-dd"));
                        }else{
                            param.put(field.getName(), val);
                        }
                    }
                }
            }catch (IllegalAccessException e) {
                log.error(e.getMessage());
            }
        }
        i++;
        return param;
    }
}
